# IDEAL STEEL REPOSITORY - (PRIVATE)

Tracking and analyzing day to day enquiries and helping Steel businesses grow. 

## Getting Started

Please Follow below steps to get started,

1. Clone code from git repository.
2. Create virtual environment and install dependencies using pip.
3. Run migrations: "python manage.py migrate"
4. Run app: python manage.py runserver 
5. Flask app will run at 127.0.0.1:8000 by default.

### Prerequisites
Install all packages used in the project as specified in requirements.txt
```
pip install -r requirements.txt
```

## Built With

* [Django](https://www.djangoproject.com/) - The Micro Web framework used
* [Pypi](https://pypi.python.org/) - Dependency Management
* [MongoDB](https://docs.mongodb.com/) - NoSQL DB to store data

## Author

**Bharat Gupta**

## License
This project is licensed under the Commercial license.