"""
    # Author - Bharat Gupta
    # Date - 07th October, 2019
    # Description: MongoDB functions.
    # Copyright (c) CloudLab Solution Pvt Ltd. All Rights Reserved.
"""

from pymongo import MongoClient
from bson.objectid import ObjectId
import traceback
from datetime import datetime

class MongoHandler():
    def __init__(self):
        try:
            self.client = MongoClient('localhost:27017')
            # MongoClient('mongodb://root:pass@localhost:27017/')
            # self.client = MongoClient("mongodb+srv://dbuser:dbuser@cluster0-ull9s.mongodb.net/test?retryWrites=true&w=majority")
            self.operators = {}
            self.operators["and"] = "$and"
            self.operators["or"] = "$or"
            self.operators["gt"] = "$gt"
            self.operators["lt"] = "$lt"
        except Exception as e:
            print("MongoDbHandler : Exception while connecting to MongoDB.. :%s" % (e))
            print(traceback.format_exc().splitlines())

    """-----------------------------------------------------------
    Get the Mongodb client.
    -----------------------------------------------------------"""

    def GetClient(self):
        return self.client

    """-----------------------------------------------------------
        Releases any valuable MongoDb Resources
    -----------------------------------------------------------"""

    def CloseClient(self):
        self.client.close()

    """-----------------------------------------------------------
        Gets the Mongodb database specified by
        the @dbname parameter
    -----------------------------------------------------------"""

    def GetDb(self, dbname):
        if self.client:
            return self.client[dbname]
        else:
            raise Exception("MongoDbHandler : Could not connect to db..")

    """-----------------------------------------------------------
        Drops the Mongodb database specified by
        the @dbname parameter
    -----------------------------------------------------------"""

    def dropDB(self, dbname):
        if self.client:
            return self.client.drop_database(self.client[dbname])
        else:
            raise Exception("MongoDbHandler : Could not connect to db..")

    """-----------------------------------------------------------
        Creates the collection specified by the
        parameter @collname, exception if it didn't
        and returns None
    -----------------------------------------------------------"""

    def CreateCollection(self, dbname, collection_name):
        db = self.GetDb(dbname)
        if db:
            collection = db.createCollection[collection_name]

            if collection:
                """print("collection is "+collection"""
                return collection
            else:
                raise Exception("MongoDbHandler : Cannot create collection..")
        else:
            raise Exception("MongoDbHandler : Could not get db..")

    """-----------------------------------------------------------
        Gets the collection specified by the
        parameter @collname, exception if it didn't
        and returns None
    -----------------------------------------------------------"""

    def GetCollection(self, dbname, collection_name):
        db = self.GetDb(dbname)
        if db:
            collection = db[collection_name]
            if collection:
                return collection
            else:
                return None
        else:
            raise Exception("MongoDbHandler : Could not get db..")

    """
    """

    def DeleteCollection(self, dbname, collection_name):
        db = self.GetDb(dbname)
        if db:
            collection = db[collection_name]
            if collection:
                db.drop_collection(collection_name)
        else:
            raise Exception("MongoDbHandler : Could not get db..")

    """----------------------------------------------------------
        Inserts the document represented by the dictionary @objJsonToInsert
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
    ----------------------------------------------------------"""

    def InsertDoc(self, objJsonToInsert, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    result = collection.insert_one(objJsonToInsert)
                    return result
                else:
                    self.CreateCollection(dbname, collection_name)
                    result = collection.insert_one(objJsonToInsert)
                    return result
            else:
                print("MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

        """----------------------------------------------------------
        Inserts the document represented by the dictionary @objDictList
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
    ----------------------------------------------------------"""

    def InsertDocMany(self, objDictList, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    collection.insert_many(objDictList)
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Updates the document represented by the dictionary @objJsonToInsert
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
    ----------------------------------------------------------"""

    def UpdateDoc(self, objJsonToQuery, objJsonToUpdate, dbname, collection_name, upsert=False):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    collection.update_many(objJsonToQuery, {'$set': objJsonToUpdate}, upsert=upsert)
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Updates the document represented by the dictionary @objJsonToInsert
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
        @usage            :
    ----------------------------------------------------------"""

    def UpdateOneDoc(self, objJsonToQuery, objJsonToUpdate, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    objJsonToUpdate['last_modified_time'] = datetime.utcnow()
                    resp = collection.update_one(objJsonToQuery, {'$set': objJsonToUpdate})
                    return resp
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Updates the document key to
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
        @usage            :
        db.test.find()
        [{'_id':'1','quantity': 23}, {'_id':'1','quantity': 7}]
        db.test.update({'_id':'1'},{'$inc': {'quantity': 3}})
        db.test.find()
        [{'_id':'1','quantity': 26}, {'_id':'1','quantity': 7}]
    ----------------------------------------------------------"""

    def UpdateValueOfObjectInDoc(self, objJsonToQuery, objJsonToUpdate, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    resp = collection.update(objJsonToQuery, objJsonToUpdate)
                    return resp
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Updates(push) a part of document represented by the dictionary @objJsonToInsert
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
    ----------------------------------------------------------"""

    def UpdatePartOfDoc(self, objJsonToQuery, objJsonToUpdate, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    collection.update(objJsonToQuery, {'$push': objJsonToUpdate})
                    collection.update_one(objJsonToQuery, {'$set': {'last_modified_time': datetime.utcnow()}})
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Adds array of values represented by @arrayToAdd to array field in document represented by the string @fieldName
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
        @arrayToAdd : Array to add
        @fieldName : Field name of array in mongo Document
    ----------------------------------------------------------"""

    def AddElementsToArrayInDoc(self, objJsonToQuery, fieldName, arrayToAdd, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    collection.update(objJsonToQuery, {'$push': {fieldName: {'$each': arrayToAdd}}})
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))
            

    """----------------------------------------------------------
        Deletes(pulls out)  a part of document represented by the dictionary @objJsonToInsert
        into the database as specified by the parameters
        @dbname           : name of the database
        @collection_nmame : name of the collection
    ----------------------------------------------------------"""

    def DeletePartOfDoc(self, objJsonToQuery, objJsonToUpdate, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    collection.update(objJsonToQuery, {'$pull': objJsonToUpdate})
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception in DeletePartOfDoc.. " + str(e))
            

    """----------------------------------------------------------
        Gets all documents in the specified collection. This works
        like a rdbms cursor and uses lazy evaluation
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def GetAllDocs(self, dbname, collection_name):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    cursor = collection.find()
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document.." + str(e))
            

    """----------------------------------------------------------
        Gets all documents in the specified collection. This is faster
        than searching for other keys in the document because the document
        is indexed on the ObjectId.
        @dbname          : name of the database
        @collection_name : name of the collection
        @_id             : The ObjectId of the document..
    ----------------------------------------------------------"""

    def GetDocumentById(self, dbname, collection_name, objectid):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    uno_doc = collection.find_one({'_id': ObjectId(objectid)})
                    return uno_doc
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception(
                    "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a document by _id.." + str(e))
            

    """
        Gets all documents in specified collections.
        Specially added for just one function in formdata-app/views.py
    """

    def GetDocumentByIdString(self, dbname, collection_name, filter_json, projection):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    uno_doc = collection.find_one(filter_json, projection)
                    return uno_doc
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception(
                    "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a document by _id string.." + str(e))
            

    """----------------------------------------------------------
        Gets all documents from the specified collection by applying
        the filter passed in the filter_json. This works like an
        rdbms cursor and uses lazy evaluation
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def FilterDocs(self, dbname, collection_name, filter_json, projection=None, sort_list=[]):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    if sort_list:
                        cursor = collection.find(filter_json, projection).sort(sort_list)
                    else:
                        cursor = collection.find(filter_json, projection)
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))
            

    """----------------------------------------------------------
        Gets all documents from the specified collection by applying
        the filter passed in the filter_json and aggregates results using group json.
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def FilterDocsAndAggregate(self, dbname, collection_name, filter_json, group_json):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    cursor = collection.aggregate([{'$match': filter_json}, {'$group': group_json}])
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))
            

    """----------------------------------------------------------
        Gets only the matching document from the specified collection
        by applying the filter passed in the filter_json.
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def FilterOneDoc(self, dbname, collection_name, filter_json, projection=None):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    uno_doc = collection.find_one(filter_json, projection)
                    return uno_doc
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))

    """----------------------------------------------------------
        Gets the last matching document from the specified collection
        by applying the filter passed in the filter_json.
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def FilterOneLastDoc(self, dbname, collection_name, filter_json):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    uno_doc = collection.find_one(filter_json).sort({'_id':-1}).limit(1)
                    return uno_doc
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))

    """----------------------------------------------------------
        SortDocument_ByKey Sorts only the matching document from the specified key
        by applying the sort filter
        @dbname          : name of the database
        @collection_name : name of the collection
        @sortorder       : pymongo.ASCENDING 1 or pymongo.DESCENDING or -1
    ----------------------------------------------------------"""

    def SortDocument_ByKey(self, dbname, collection_name, sort_key, sort_order):
        try:
            import pymongo
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    if sort_order == 1:
                        docs = collection.find().sort(sort_key, pymongo.ASCENDING)
                    else:
                        docs = collection.find().sort(sort_key, pymongo.DESCENDING)
                    return docs
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))
            

    """----------------------------------------------------------
        Deletes only the matching document from the specified collection
        by applying the filter passed in the filter_json.
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def DeleteDoc(self, dbname, collection_name, filter_json):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    uno_doc = collection.remove(filter_json)
                    return uno_doc
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))

    """----------------------------------------------------------
        Deletes only first matching document from the specified collection
        by applying the filter passed in the filter_json.
        @dbname          : name of the database
        @collection_name : name of the collection
    ----------------------------------------------------------"""

    def DeleteOneDoc(self, dbname, collection_name, filter_json):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    result = collection.delete_one(filter_json)
                    return result.deleted_count
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))
            

    """----------------------------------------------------------
            Finds distinct entries of the key based on the filter passed.
            @dbname          : name of the database
            @collection_name : name of the collection
        ----------------------------------------------------------"""

    def FindDistinctFields(self, dbname, collection_name, distinct_key, filter_json=None):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    cursor = collection.distinct(distinct_key, filter=filter_json)
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception(
                    "MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))

    """----------------------------------------------------------
                Finds distinct entries of the key based on the filter passed.
                @dbname          : name of the database
                @collection_name : name of the collection
            ----------------------------------------------------------"""

    def FilterDocsAndAggregateWithProject(self, dbname, collection_name, match_json,
                                          project_json):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    cursor = collection.aggregate(
                        [{'$match': match_json}, {'$project': project_json}])
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception(
                    "MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))
            

    """----------------------------------------------------------
            Unwind Nested Array and Match condition passed and Project in specified collection.
            :dbname : name of the database
            :collection_name: name of collection
            :aggregate_filter: conditions to aggregate
    --------------------------------------------------------------"""

    def AggregateDocs(self, dbname, collection_name, aggregate_filter):
        try:
            db = self.GetDb(dbname)
            if db:
                collection = db[collection_name]
                if collection:
                    cursor = collection.aggregate(aggregate_filter)
                    return cursor
                else:
                    raise Exception(
                        "MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
            else:
                raise Exception(
                    "MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception getting a cursor for the document..%s" % (e))

    """----------------------------------------------------------
        Inserts the File @file_to_insert
        into the database as specified by the parameters
        @dbname           : name of the database
    ----------------------------------------------------------"""

    def InsertFile(self, file_to_insert, dbname='filedb'):
        try:
            import gridfs
            db = self.GetDb(dbname)
            if db:
                fs = gridfs.GridFS(db)
                if fs:
                    file_id = fs.put(file_to_insert)
                    return file_id
            else:
                print("MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))


    """----------------------------------------------------------
        Inserts the File @file_to_insert
        into the database as specified by the parameters
        @dbname           : name of the database
    ----------------------------------------------------------"""

    def getFileFromId(self, file_id, dbname='filedb'):
        try:
            import gridfs
            db = self.GetDb(dbname)
            if db:
                fs = gridfs.GridFS(db)
                if fs:
                    _file = fs.get(file_id)
                    return _file
            else:
                print("MongoDbHandler : Couldn't get the specified collection. Check if the collection exists.. ")
                raise Exception("MongoDbHandler : Couldn't get the specified database. Check if the database exists.. ")
        except Exception as e:
            print("MongoDbHandler : Exception inserting the document.. " + str(e))