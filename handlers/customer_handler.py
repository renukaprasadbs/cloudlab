"""    
    # Author: Bharat Gupta
    # Date: 30-09-2019
    # Description: Customer handler.py file 
    # Copyright (c) CloudLab Solution Pvt Ltd. All Rights Reserved.
"""

import uuid
import datetime
from clidealsteel import settings
from core.mongohandler import MongoHandler
from app.models import AuthUser, Customers, CustomerUsers, UserTypes

class CustomerHandler(object):
    def __init__(self):
        pass

    """
    Adds a customer into the system by using shell.
    """
    def add_customer(self,username,country_code,mobile,is_admin,mobile_user,email,password,
                    first_name,last_name,company_name,company_address,company_state,
                    company_country,is_demo,is_paid_customer,total_credits_purchased):

        user_type = UserTypes.objects.get(user_type="ADMINISTRATOR")
        auth_user = AuthUser.create_authuser(username, email, password, user_type, True)
        auth_user.first_name = first_name
        auth_user.last_name = last_name
        auth_user.countrycode = country_code
        auth_user.mobile = mobile
        auth_user.is_admin = is_admin
        auth_user.mobile_user = mobile_user
        auth_user.date_joined = datetime.datetime.now()
        auth_user.save()
        
        customer = Customers()
        customer.authuser_ptr = auth_user
        customer.uuid_cust = uuid.uuid1()
        customer.company_name=company_name
        customer.company_addr=company_address
        customer.company_state=company_state
        customer.company_country=company_country
        customer.date_add = datetime.datetime.now()
        customer.date_mod = datetime.datetime.now()
        customer.is_demo = is_demo
        customer.is_paid_customer = is_paid_customer
        customer.total_credits_purchased = total_credits_purchased
        customer.is_active = True
        customer.save()
        
        cust_user = CustomerUsers()
        cust_user.customer_users_uuid = uuid.uuid1()
        cust_user.customer_id = customer
        cust_user.auth_user = auth_user 
        cust_user.date_add = datetime.datetime.now()
        cust_user.date_mod = datetime.datetime.now()
        cust_user.save()

    """
    @Data:
        SMS allocated in integer
        Email allocated in integer
        Customer Status - Active/Inactive
    """
    def initilize_customer_credits(self, customer_id, customer_code, sms_allocated, customer_status, email_allocated):
        try:
            mongo_handler = MongoHandler()
            customer_info = {}
            customer_info["customer_id"] = customer_id 
            customer_info["customer_code"] = customer_code
            customer_info["sms_allocated"] = sms_allocated
            customer_info["customer_status"] = customer_status
            customer_info["email_allocated"] = email_allocated
            mongo_handler.InsertDoc(customer_info, settings.MONGO_DBNAME, settings.MONGO_COL_CUSTOMERS_INFO)
            return True
        except Exception as e:
            print ("{}-{}".format("Exception","Critical exception in initilize_customer_credits : "+str(e)))
            return False