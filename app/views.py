"""
    # Author: Bharat Gupta
    # Date: 26-09-2019
    # Description: Django App - app - views.py file
    # Copyright (c) CloudLab Solution Pvt Ltd. All Rights Reserved.
"""

from __future__ import division
from app.models import AuthUser
from django.contrib import auth
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

def index(request):
    if request.method == "GET":
        return render(request, 'login.html', {'hu':'hi'})

def login(request):
    if request.method == "GET":
        if "username" in request.session:
            return render(request, 'dashboard.html', {'name': 'CloudLab Ideal Steel!!'})
        else:
            return render(request, 'login.html', {'name': 'CloudLab Ideal Steel!!'})
    elif request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user:
            print ("user authenticated successfully..")
        if user is not None:
            userid = user.id
            user_auth = AuthUser.objects.get(id=userid)
            user_type = user_auth.user_type_id
            request.session["usertype"] = user_type
            if user_type == 1 or user_type == 5:
                if user.is_active:
                    auth.login(request, user)
                    request.session["user_id"] = user.id
                    request.session["username"] = username
                    return redirect('/app/dashboard')
                else:
                    return render(request, 'login.html', {'error': 'Your account has been disabled. Please contact the Administrator'})
            else:
                return render(request, "login.html", {"error": "You are not authorized to login. Please contact the Administrator"})
        elif user is None:
            return render(request, 'login.html', {'error': 'The username / password you entered does not exist'})

# @login_required(login_url="/app/login")
def logout(request):
    auth.logout(request)
    return render(request, 'login.html', {'name': 'CloudLab Ideal Steel!!'})

# @login_required(login_url="/app/login")
def dashboard(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'dashboard.html', {'userid': userid})

# @login_required(login_url="/app/login")
def enquiries(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'enquiries.html', {'userid': userid})

# @login_required(login_url="/app/login")
def add_enquiry(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'add-enquiry.html', {'userid': userid})

# @login_required(login_url="/app/login")
def view_orders(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'view-orders.html', {'userid': userid})

# @login_required(login_url="/app/login")
def list_orders(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'list-orders.html', {'userid': userid})

# @login_required(login_url="/app/login")
def invoice(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'invoice.html', {'userid': userid})

# @login_required(login_url="/app/login")
def users(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'users.html', {'userid': userid})

# @login_required(login_url="/app/login")
def customers(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'customers.html', {'userid': userid})

# @login_required(login_url="/app/login")
def register(request):
    if request.method == "GET":
        userid = 1
        # userid = request.session.get('_auth_user_id')
        return render(request, 'register.html', {'userid': userid})