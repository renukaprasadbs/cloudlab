"""    
    # Author: Bharat Gupta
    # Date: 26-09-2019
    # Description: Django App - app - urls.py file
    # Copyright (c) CloudLab Solution Pvt Ltd. All Rights Reserved.
"""

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$',views.index, name='index'),
    url(r'login$',views.login, name='login'),
    url(r'register$',views.register, name='register'),
    url(r'logout$',views.logout, name='logout'),
    url(r'dashboard$',views.dashboard, name='dashboard'),
    url(r'enquiries$',views.enquiries, name='enquiries'),
    url(r'add_enquiry$',views.add_enquiry, name='add_enquiry'),
    url(r'view_orders$',views.view_orders, name='view_orders'),
    url(r'list_orders$',views.list_orders, name='list_orders'), 
    url(r'invoice$',views.invoice, name='invoice'),
    url(r'users$',views.users, name='users'),
    url(r'customers$',views.customers, name='customers'),
]