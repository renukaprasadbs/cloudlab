$(function () {
    
    handlePageContentView = function() {
        "use strict";
        var a = "", b = "", c = handleCheckBootstrapVersion();
        3 <= c && c < 4 ? (a = "hide", b = "in") : 4 <= c && c < 5 && (a = "d-none", b = "show"), 
        $(window).on("load", function() {
            $.when($("#page-loader").addClass(a)).done(function() {
                $("#page-container").addClass(b);
            });
        });
    },
    handleCheckBootstrapVersion = function() {
        return parseInt($.fn.tooltip.Constructor.VERSION);
    }, App = function() {
        "use strict";
        var a;
        return {
            init: function(b) {
                b && (a = b),this.initPageLoad();
            },
            initPageLoad: function() {
                handlePageContentView();
            },
        };
    }();
    // ------------------------------------------------------- //
    // Sidebar
    // ------------------------------------------------------ //
    $('.sidebar-toggler').on('click', function () {
        $('.sidebar').toggleClass('shrink show');
    });



    // ------------------------------------------------------ //
    // For demo purposes, can be deleted
    // ------------------------------------------------------ //

    var stylesheet = $('link#theme-stylesheet');
    $( "<link id='new-stylesheet' rel='stylesheet'>" ).insertAfter(stylesheet);
    var alternateColour = $('link#new-stylesheet');

    if ($.cookie("theme_csspath")) {
        alternateColour.attr("href", $.cookie("theme_csspath"));
    }

    $("#colour").change(function () {

        if ($(this).val() !== '') {

            var theme_csspath = 'css/style.' + $(this).val() + '.css';

            alternateColour.attr("href", theme_csspath);

            $.cookie("theme_csspath", theme_csspath, { expires: 365, path: document.URL.substr(0, document.URL.lastIndexOf('/')) });

        }

        return false;
    });

});


Cookies.set('active', 'true');

    
$(document).ready(function() {
    App.init();
});