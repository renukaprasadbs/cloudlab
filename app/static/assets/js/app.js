(function() {
    function a(a, b) {
        function c() {
            this.constructor = a;
        }
        for (var d in b) Z.call(b, d) && (a[d] = b[d]);
        return c.prototype = b.prototype, a.prototype = new c(), a.__super__ = b.prototype, 
        a;
    }
    var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y = [].slice, Z = {}.hasOwnProperty, $ = [].indexOf || function(a) {
        for (var b = 0, c = this.length; b < c; b++) if (b in this && this[b] === a) return b;
        return -1;
    };
    for (v = {
        catchupTime: 100,
        initialRate: .03,
        minTime: 250,
        ghostTime: 100,
        maxProgressPerFrame: 20,
        easeFactor: 1.25,
        startOnPageLoad: !0,
        restartOnPushState: !0,
        restartOnRequestAfter: 500,
        target: "body",
        elements: {
            checkInterval: 100,
            selectors: [ "body" ]
        },
        eventLag: {
            minSamples: 10,
            sampleCount: 3,
            lagThreshold: 3
        },
        ajax: {
            trackMethods: [ "GET" ],
            trackWebSockets: !0,
            ignoreURLs: []
        }
    }, D = function() {
        var a;
        return null != (a = "undefined" != typeof performance && null !== performance && "function" == typeof performance.now ? performance.now() : void 0) ? a : +new Date();
    }, F = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame, 
    u = window.cancelAnimationFrame || window.mozCancelAnimationFrame, null == F && (F = function(a) {
        return setTimeout(a, 50);
    }, u = function(a) {
        return clearTimeout(a);
    }), H = function(a) {
        var b, c;
        return b = D(), (c = function() {
            var d;
            return 33 <= (d = D() - b) ? (b = D(), a(d, function() {
                return F(c);
            })) : setTimeout(c, 33 - d);
        })();
    }, G = function() {
        var a, b, c;
        return c = arguments[0], b = arguments[1], a = 3 <= arguments.length ? Y.call(arguments, 2) : [], 
        "function" == typeof c[b] ? c[b].apply(c, a) : c[b];
    }, w = function() {
        var a, b, c, d, e, f, g;
        for (b = arguments[0], f = 0, g = (d = 2 <= arguments.length ? Y.call(arguments, 1) : []).length; f < g; f++) if (c = d[f]) for (a in c) Z.call(c, a) && (e = c[a], 
        null != b[a] && "object" == typeof b[a] && null != e && "object" == typeof e ? w(b[a], e) : b[a] = e);
        return b;
    }, r = function(a) {
        var b, c, d, e, f;
        for (c = b = 0, e = 0, f = a.length; e < f; e++) d = a[e], c += Math.abs(d), b++;
        return c / b;
    }, y = function(a, b) {
        var c, d;
        if (null == a && (a = "options"), null == b && (b = !0), d = document.querySelector("[data-pace-" + a + "]")) {
            if (c = d.getAttribute("data-pace-" + a), !b) return c;
            try {
                return JSON.parse(c);
            } catch (a) {
                return "undefined" != typeof console && null !== console ? console.error("Error parsing inline pace options", a) : void 0;
            }
        }
    }, _.prototype.on = function(a, b, c, d) {
        var e;
        return null == d && (d = !1), null == this.bindings && (this.bindings = {}), null == (e = this.bindings)[a] && (e[a] = []), 
        this.bindings[a].push({
            handler: b,
            ctx: c,
            once: d
        });
    }, _.prototype.once = function(a, b, c) {
        return this.on(a, b, c, !0);
    }, _.prototype.off = function(a, b) {
        var c, d, e;
        if (null != (null != (d = this.bindings) ? d[a] : void 0)) {
            if (null == b) return delete this.bindings[a];
            for (c = 0, e = []; c < this.bindings[a].length; ) this.bindings[a][c].handler === b ? e.push(this.bindings[a].splice(c, 1)) : e.push(c++);
            return e;
        }
    }, _.prototype.trigger = function() {
        var a, b, c, d, e, f, g, h, i;
        if (c = arguments[0], a = 2 <= arguments.length ? Y.call(arguments, 1) : [], null != (g = this.bindings) && g[c]) {
            for (e = 0, i = []; e < this.bindings[c].length; ) d = (h = this.bindings[c][e]).handler, 
            b = h.ctx, f = h.once, d.apply(null != b ? b : this, a), f ? i.push(this.bindings[c].splice(e, 1)) : i.push(e++);
            return i;
        }
    }, h = _, k = window.Pace || {}, window.Pace = k, w(k, h.prototype), E = k.options = w({}, v, window.paceOptions, y()), 
    R = 0, T = (V = [ "ajax", "document", "eventLag", "elements" ]).length; R < T; R++) !0 === E[L = V[R]] && (E[L] = v[L]);
    function _() {}
    function ab(a) {
        this.source = a, this.last = this.sinceLastUpdate = 0, this.rate = E.initialRate, 
        this.catchup = 0, this.progress = this.lastProgress = 0, null != this.source && (this.progress = G(this.source, "progress"));
    }
    function bb() {
        var a, b, c = this;
        this.progress = null != (b = this.states[document.readyState]) ? b : 100, a = document.onreadystatechange, 
        document.onreadystatechange = function() {
            return null != c.states[document.readyState] && (c.progress = c.states[document.readyState]), 
            "function" == typeof a ? a.apply(null, arguments) : void 0;
        };
    }
    function cb(a) {
        this.selector = a, this.progress = 0, this.check();
    }
    function db() {
        var a = this;
        this.elements = [], z().on("request", function() {
            return a.watch.apply(a, arguments);
        });
    }
    function eb() {
        var a, b = this;
        eb.__super__.constructor.apply(this, arguments), a = function(a) {
            var c;
            return c = a.open, a.open = function(d, e, f) {
                return K(d) && b.trigger("request", {
                    type: d,
                    url: e,
                    request: a
                }), c.apply(a, arguments);
            };
        }, window.XMLHttpRequest = function(b) {
            var c;
            return c = new Q(b), a(c), c;
        };
        try {
            x(window.XMLHttpRequest, Q);
        } catch (c) {}
        if (null != P) {
            window.XDomainRequest = function() {
                var b;
                return b = new P(), a(b), b;
            };
            try {
                x(window.XDomainRequest, P);
            } catch (c) {}
        }
        if (null != O && E.ajax.trackWebSockets) {
            window.WebSocket = function(a, c) {
                var d;
                return d = null != c ? new O(a, c) : new O(a), K("socket") && b.trigger("request", {
                    type: "socket",
                    url: a,
                    protocols: c,
                    request: d
                }), d;
            };
            try {
                x(window.WebSocket, O);
            } catch (c) {}
        }
    }
    function fb() {
        this.bindings = {};
    }
    function gb() {
        this.progress = 0;
    }
    function hb() {
        return hb.__super__.constructor.apply(this, arguments);
    }
    X = Error, a(hb, X), j = hb, gb.prototype.getElement = function() {
        var a;
        if (null == this.el) {
            if (!(a = document.querySelector(E.target))) throw new j();
            this.el = document.createElement("div"), this.el.className = "pace pace-active", 
            document.body.className = document.body.className.replace(/pace-done/g, ""), document.body.className += " pace-running", 
            this.el.innerHTML = '<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>', 
            null != a.firstChild ? a.insertBefore(this.el, a.firstChild) : a.appendChild(this.el);
        }
        return this.el;
    }, gb.prototype.finish = function() {
        var a;
        return (a = this.getElement()).className = a.className.replace("pace-active", ""), 
        a.className += " pace-inactive", document.body.className = document.body.className.replace("pace-running", ""), 
        document.body.className += " pace-done";
    }, gb.prototype.update = function(a) {
        return this.progress = a, this.render();
    }, gb.prototype.destroy = function() {
        try {
            this.getElement().parentNode.removeChild(this.getElement());
        } catch (a) {
            j = a;
        }
        return this.el = void 0;
    }, gb.prototype.render = function() {
        var a, b, c, d, e, f, g;
        if (null == document.querySelector(E.target)) return !1;
        for (a = this.getElement(), d = "translate3d(" + this.progress + "%, 0, 0)", e = 0, 
        f = (g = [ "webkitTransform", "msTransform", "transform" ]).length; e < f; e++) b = g[e], 
        a.children[0].style[b] = d;
        return (!this.lastRenderedProgress || this.lastRenderedProgress | 0 !== this.progress | 0) && (a.children[0].setAttribute("data-progress-text", (0 | this.progress) + "%"), 
        100 <= this.progress ? c = "99" : (c = this.progress < 10 ? "0" : "", c += 0 | this.progress), 
        a.children[0].setAttribute("data-progress", "" + c)), this.lastRenderedProgress = this.progress;
    }, gb.prototype.done = function() {
        return 100 <= this.progress;
    }, c = gb, fb.prototype.trigger = function(a, b) {
        var c, d, e, f, g;
        if (null != this.bindings[a]) {
            for (g = [], d = 0, e = (f = this.bindings[a]).length; d < e; d++) c = f[d], g.push(c.call(this, b));
            return g;
        }
    }, fb.prototype.on = function(a, b) {
        var c;
        return null == (c = this.bindings)[a] && (c[a] = []), this.bindings[a].push(b);
    }, i = fb, Q = window.XMLHttpRequest, P = window.XDomainRequest, O = window.WebSocket, 
    x = function(a, b) {
        var c, d;
        for (c in d = [], b.prototype) try {
            null == a[c] && "function" != typeof b[c] ? "function" == typeof Object.defineProperty ? d.push(Object.defineProperty(a, c, {
                get: function() {
                    return b.prototype[c];
                },
                configurable: !0,
                enumerable: !0
            })) : d.push(a[c] = b.prototype[c]) : d.push(void 0);
        } catch (a) {}
        return d;
    }, B = [], k.ignore = function() {
        var a, b, c;
        return b = arguments[0], a = 2 <= arguments.length ? Y.call(arguments, 1) : [], 
        B.unshift("ignore"), c = b.apply(null, a), B.shift(), c;
    }, k.track = function() {
        var a, b, c;
        return b = arguments[0], a = 2 <= arguments.length ? Y.call(arguments, 1) : [], 
        B.unshift("track"), c = b.apply(null, a), B.shift(), c;
    }, K = function(a) {
        var b;
        if (null == a && (a = "GET"), "track" === B[0]) return "force";
        if (!B.length && E.ajax) {
            if ("socket" === a && E.ajax.trackWebSockets) return !0;
            if (b = a.toUpperCase(), 0 <= $.call(E.ajax.trackMethods, b)) return !0;
        }
        return !1;
    }, a(eb, i), l = eb, S = null, J = function(a) {
        var b, c, d, e;
        for (c = 0, d = (e = E.ajax.ignoreURLs).length; c < d; c++) if ("string" == typeof (b = e[c])) {
            if (-1 !== a.indexOf(b)) return !0;
        } else if (b.test(a)) return !0;
        return !1;
    }, (z = function() {
        return null == S && (S = new l()), S;
    })().on("request", function(a) {
        var c, d, e, f, g;
        return f = a.type, e = a.request, g = a.url, J(g) || k.running || !1 === E.restartOnRequestAfter && "force" !== K(f) ? void 0 : (d = arguments, 
        "boolean" == typeof (c = E.restartOnRequestAfter || 0) && (c = 0), setTimeout(function() {
            var a, c, g, h, i;
            if ("socket" === f ? e.readyState < 2 : 0 < (g = e.readyState) && g < 4) {
                for (k.restart(), i = [], a = 0, c = (h = k.sources).length; a < c; a++) {
                    if ((L = h[a]) instanceof b) {
                        L.watch.apply(L, d);
                        break;
                    }
                    i.push(void 0);
                }
                return i;
            }
        }, c));
    }), db.prototype.watch = function(a) {
        var b, c, d, e;
        return d = a.type, b = a.request, e = a.url, J(e) ? void 0 : (c = new ("socket" === d ? o : p)(b), 
        this.elements.push(c));
    }, b = db, p = function(a) {
        var b, c, d, e, f, g = this;
        if (this.progress = 0, null != window.ProgressEvent) for (a.addEventListener("progress", function(a) {
            return a.lengthComputable ? g.progress = 100 * a.loaded / a.total : g.progress = g.progress + (100 - g.progress) / 2;
        }, !1), c = 0, d = (f = [ "load", "abort", "timeout", "error" ]).length; c < d; c++) b = f[c], 
        a.addEventListener(b, function() {
            return g.progress = 100;
        }, !1); else e = a.onreadystatechange, a.onreadystatechange = function() {
            var b;
            return 0 === (b = a.readyState) || 4 === b ? g.progress = 100 : 3 === a.readyState && (g.progress = 50), 
            "function" == typeof e ? e.apply(null, arguments) : void 0;
        };
    }, o = function(a) {
        var b, c, d, e, f = this;
        for (c = this.progress = 0, d = (e = [ "error", "open" ]).length; c < d; c++) b = e[c], 
        a.addEventListener(b, function() {
            return f.progress = 100;
        }, !1);
    }, e = function(a) {
        var b, c, d, e;
        for (null == a && (a = {}), this.elements = [], null == a.selectors && (a.selectors = []), 
        c = 0, d = (e = a.selectors).length; c < d; c++) b = e[c], this.elements.push(new f(b));
    }, cb.prototype.check = function() {
        var a = this;
        return document.querySelector(this.selector) ? this.done() : setTimeout(function() {
            return a.check();
        }, E.elements.checkInterval);
    }, cb.prototype.done = function() {
        return this.progress = 100;
    }, f = cb, bb.prototype.states = {
        loading: 0,
        interactive: 50,
        complete: 100
    }, d = bb, g = function() {
        var a, b, c, d, e, f = this;
        this.progress = 0, e = [], d = a = 0, c = D(), b = setInterval(function() {
            var g;
            return g = D() - c - 50, c = D(), e.push(g), e.length > E.eventLag.sampleCount && e.shift(), 
            a = r(e), ++d >= E.eventLag.minSamples && a < E.eventLag.lagThreshold ? (f.progress = 100, 
            clearInterval(b)) : f.progress = 3 / (a + 3) * 100;
        }, 50);
    }, ab.prototype.tick = function(a, b) {
        var c;
        return null == b && (b = G(this.source, "progress")), 100 <= b && (this.done = !0), 
        b === this.last ? this.sinceLastUpdate += a : (this.sinceLastUpdate && (this.rate = (b - this.last) / this.sinceLastUpdate), 
        this.catchup = (b - this.progress) / E.catchupTime, this.sinceLastUpdate = 0, this.last = b), 
        b > this.progress && (this.progress += this.catchup * a), c = 1 - Math.pow(this.progress / 100, E.easeFactor), 
        this.progress += c * this.rate * a, this.progress = Math.min(this.lastProgress + E.maxProgressPerFrame, this.progress), 
        this.progress = Math.max(0, this.progress), this.progress = Math.min(100, this.progress), 
        this.lastProgress = this.progress, this.progress;
    }, n = ab, t = q = N = s = I = M = null, k.running = !1, A = function() {
        return E.restartOnPushState ? k.restart() : void 0;
    }, null != window.history.pushState && (U = window.history.pushState, window.history.pushState = function() {
        return A(), U.apply(window.history, arguments);
    }), null != window.history.replaceState && (W = window.history.replaceState, window.history.replaceState = function() {
        return A(), W.apply(window.history, arguments);
    }), m = {
        ajax: b,
        elements: e,
        document: d,
        eventLag: g
    }, (C = function() {
        var a, b, d, e, f, g, h, i;
        for (k.sources = M = [], b = 0, e = (g = [ "ajax", "elements", "document", "eventLag" ]).length; b < e; b++) !1 !== E[a = g[b]] && M.push(new m[a](E[a]));
        for (d = 0, f = (i = null != (h = E.extraSources) ? h : []).length; d < f; d++) L = i[d], 
        M.push(new L(E));
        return k.bar = s = new c(), I = [], N = new n();
    })(), k.stop = function() {
        return k.trigger("stop"), k.running = !1, s.destroy(), t = !0, null != q && ("function" == typeof u && u(q), 
        q = null), C();
    }, k.restart = function() {
        return k.trigger("restart"), k.stop(), k.start();
    }, k.go = function() {
        var a;
        return k.running = !0, s.render(), a = D(), t = !1, q = H(function(b, c) {
            var d, e, f, g, h, i, j, l, m, o, p, q, r, u, v;
            for (s.progress, e = o = 0, f = !0, i = p = 0, r = M.length; p < r; i = ++p) for (L = M[i], 
            m = null != I[i] ? I[i] : I[i] = [], j = q = 0, u = (h = null != (v = L.elements) ? v : [ L ]).length; q < u; j = ++q) g = h[j], 
            f &= (l = null != m[j] ? m[j] : m[j] = new n(g)).done, l.done || (e++, o += l.tick(b));
            return d = o / e, s.update(N.tick(b, d)), s.done() || f || t ? (s.update(100), k.trigger("done"), 
            setTimeout(function() {
                return s.finish(), k.running = !1, k.trigger("hide");
            }, Math.max(E.ghostTime, Math.max(E.minTime - (D() - a), 0)))) : c();
        });
    }, k.start = function(a) {
        w(E, a), k.running = !0;
        try {
            s.render();
        } catch (a) {
            j = a;
        }
        return document.querySelector(".pace") ? (k.trigger("start"), k.go()) : setTimeout(k.start, 50);
    }, "function" == typeof define && define.amd ? define([ "pace" ], function() {
        return k;
    }) : "object" == typeof exports ? module.exports = k : E.startOnPageLoad && k.start();
}).call(this), function(a, b) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a);
    } : b(a);
}("undefined" != typeof window ? window : this, function(a, b) {
    "use strict";
    function c(a) {
        return null != a && a === a.window;
    }
    var d = [], e = a.document, f = Object.getPrototypeOf, g = d.slice, h = d.concat, i = d.push, j = d.indexOf, k = {}, l = k.toString, m = k.hasOwnProperty, n = m.toString, o = n.call(Object), p = {}, q = function(a) {
        return "function" == typeof a && "number" != typeof a.nodeType;
    }, r = {
        type: !0,
        src: !0,
        nonce: !0,
        noModule: !0
    };
    function s(a, b, c) {
        var d, f, g = (c = c || e).createElement("script");
        if (g.text = a, b) for (d in r) (f = b[d] || b.getAttribute && b.getAttribute(d)) && g.setAttribute(d, f);
        c.head.appendChild(g).parentNode.removeChild(g);
    }
    function t(a) {
        return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? k[l.call(a)] || "object" : typeof a;
    }
    var u = function(a, b) {
        return new u.fn.init(a, b);
    }, v = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    function w(a) {
        var b = !!a && "length" in a && a.length, d = t(a);
        return !q(a) && !c(a) && ("array" === d || 0 === b || "number" == typeof b && 0 < b && b - 1 in a);
    }
    u.fn = u.prototype = {
        jquery: "3.4.1",
        constructor: u,
        length: 0,
        toArray: function() {
            return g.call(this);
        },
        get: function(a) {
            return null == a ? g.call(this) : a < 0 ? this[a + this.length] : this[a];
        },
        pushStack: function(a) {
            var b = u.merge(this.constructor(), a);
            return b.prevObject = this, b;
        },
        each: function(a) {
            return u.each(this, a);
        },
        map: function(a) {
            return this.pushStack(u.map(this, function(b, c) {
                return a.call(b, c, b);
            }));
        },
        slice: function() {
            return this.pushStack(g.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(a) {
            var b = this.length, c = +a + (a < 0 ? b : 0);
            return this.pushStack(0 <= c && c < b ? [ this[c] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor();
        },
        push: i,
        sort: d.sort,
        splice: d.splice
    }, u.extend = u.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || q(g) || (g = {}), 
        h === i && (g = this, h--); h < i; h++) if (null != (a = arguments[h])) for (b in a) d = a[b], 
        "__proto__" !== b && g !== d && (j && d && (u.isPlainObject(d) || (e = Array.isArray(d))) ? (c = g[b], 
        f = e && !Array.isArray(c) ? [] : e || u.isPlainObject(c) ? c : {}, e = !1, g[b] = u.extend(j, f, d)) : void 0 !== d && (g[b] = d));
        return g;
    }, u.extend({
        expando: "jQuery" + ("3.4.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw new Error(a);
        },
        noop: function() {},
        isPlainObject: function(a) {
            var b, c;
            return !(!a || "[object Object]" !== l.call(a) || (b = f(a)) && ("function" != typeof (c = m.call(b, "constructor") && b.constructor) || n.call(c) !== o));
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) return !1;
            return !0;
        },
        globalEval: function(a, b) {
            s(a, {
                nonce: b && b.nonce
            });
        },
        each: function(a, b) {
            var c, d = 0;
            if (w(a)) for (c = a.length; d < c && !1 !== b.call(a[d], d, a[d]); d++) ; else for (d in a) if (!1 === b.call(a[d], d, a[d])) break;
            return a;
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(v, "");
        },
        makeArray: function(a, b) {
            var c = b || [];
            return null != a && (w(Object(a)) ? u.merge(c, "string" == typeof a ? [ a ] : a) : i.call(c, a)), 
            c;
        },
        inArray: function(a, b, c) {
            return null == b ? -1 : j.call(b, a, c);
        },
        merge: function(a, b) {
            for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
            return a.length = e, a;
        },
        grep: function(a, b, c) {
            for (var d = [], e = 0, f = a.length, g = !c; e < f; e++) !b(a[e], e) != g && d.push(a[e]);
            return d;
        },
        map: function(a, b, c) {
            var d, e, f = 0, g = [];
            if (w(a)) for (d = a.length; f < d; f++) null != (e = b(a[f], f, c)) && g.push(e); else for (f in a) null != (e = b(a[f], f, c)) && g.push(e);
            return h.apply([], g);
        },
        guid: 1,
        support: p
    }), "function" == typeof Symbol && (u.fn[Symbol.iterator] = d[Symbol.iterator]), 
    u.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
        k["[object " + b + "]"] = b.toLowerCase();
    });
    var x = function(a) {
        function b(a, b, c) {
            var d = "0x" + b - 65536;
            return d != d || c ? b : d < 0 ? String.fromCharCode(65536 + d) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320);
        }
        function c() {
            o();
        }
        var d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w = "sizzle" + +new Date(), x = a.document, y = 0, z = 0, A = ib(), B = ib(), C = ib(), D = ib(), E = function(a, b) {
            return a === b && (n = !0), 0;
        }, F = {}.hasOwnProperty, G = [], H = G.pop, I = G.push, J = G.push, K = G.slice, L = function(a, b) {
            for (var c = 0, d = a.length; c < d; c++) if (a[c] === b) return c;
            return -1;
        }, M = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", N = "[\\x20\\t\\r\\n\\f]", O = "(?:\\\\.|[\\w-]|[^\x00-\\xa0])+", P = "\\[" + N + "*(" + O + ")(?:" + N + "*([*^$|!~]?=)" + N + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + O + "))|)" + N + "*\\]", Q = ":(" + O + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + P + ")*)|.*)\\)|)", R = new RegExp(N + "+", "g"), S = new RegExp("^" + N + "+|((?:^|[^\\\\])(?:\\\\.)*)" + N + "+$", "g"), T = new RegExp("^" + N + "*," + N + "*"), U = new RegExp("^" + N + "*([>+~]|" + N + ")" + N + "*"), V = new RegExp(N + "|>"), W = new RegExp(Q), X = new RegExp("^" + O + "$"), Y = {
            ID: new RegExp("^#(" + O + ")"),
            CLASS: new RegExp("^\\.(" + O + ")"),
            TAG: new RegExp("^(" + O + "|[*])"),
            ATTR: new RegExp("^" + P),
            PSEUDO: new RegExp("^" + Q),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + N + "*(even|odd|(([+-]|)(\\d*)n|)" + N + "*(?:([+-]|)" + N + "*(\\d+)|))" + N + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + M + ")$", "i"),
            needsContext: new RegExp("^" + N + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + N + "*((?:-\\d)?\\d*)" + N + "*\\)|)(?=[^-]|$)", "i")
        }, Z = /HTML$/i, $ = /^(?:input|select|textarea|button)$/i, _ = /^h\d$/i, ab = /^[^{]+\{\s*\[native \w/, bb = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, cb = /[+~]/, db = new RegExp("\\\\([\\da-f]{1,6}" + N + "?|(" + N + ")|.)", "ig"), eb = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, fb = function(a, b) {
            return b ? "\x00" === a ? "ï¿½" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a;
        }, gb = ub(function(a) {
            return !0 === a.disabled && "fieldset" === a.nodeName.toLowerCase();
        }, {
            dir: "parentNode",
            next: "legend"
        });
        try {
            J.apply(G = K.call(x.childNodes), x.childNodes), G[x.childNodes.length].nodeType;
        } catch (d) {
            J = {
                apply: G.length ? function(a, b) {
                    I.apply(a, K.call(b));
                } : function(a, b) {
                    for (var c = a.length, d = 0; a[c++] = b[d++]; ) ;
                    a.length = c - 1;
                }
            };
        }
        function hb(a, b, c, d) {
            var f, g, h, j, l, m, n, q = b && b.ownerDocument, t = b ? b.nodeType : 9;
            if (c = c || [], "string" != typeof a || !a || 1 !== t && 9 !== t && 11 !== t) return c;
            if (!d && ((b ? b.ownerDocument || b : x) !== p && o(b), b = b || p, r)) {
                if (11 !== t && (l = bb.exec(a))) if (f = l[1]) {
                    if (9 === t) {
                        if (!(h = b.getElementById(f))) return c;
                        if (h.id === f) return c.push(h), c;
                    } else if (q && (h = q.getElementById(f)) && v(b, h) && h.id === f) return c.push(h), 
                    c;
                } else {
                    if (l[2]) return J.apply(c, b.getElementsByTagName(a)), c;
                    if ((f = l[3]) && e.getElementsByClassName && b.getElementsByClassName) return J.apply(c, b.getElementsByClassName(f)), 
                    c;
                }
                if (e.qsa && !D[a + " "] && (!s || !s.test(a)) && (1 !== t || "object" !== b.nodeName.toLowerCase())) {
                    if (n = a, q = b, 1 === t && V.test(a)) {
                        for ((j = b.getAttribute("id")) ? j = j.replace(eb, fb) : b.setAttribute("id", j = w), 
                        g = (m = i(a)).length; g--; ) m[g] = "#" + j + " " + tb(m[g]);
                        n = m.join(","), q = cb.test(a) && rb(b.parentNode) || b;
                    }
                    try {
                        return J.apply(c, q.querySelectorAll(n)), c;
                    } catch (b) {
                        D(a, !0);
                    } finally {
                        j === w && b.removeAttribute("id");
                    }
                }
            }
            return k(a.replace(S, "$1"), b, c, d);
        }
        function ib() {
            var a = [];
            return function b(c, d) {
                return a.push(c + " ") > f.cacheLength && delete b[a.shift()], b[c + " "] = d;
            };
        }
        function jb(a) {
            return a[w] = !0, a;
        }
        function kb(a) {
            var b = p.createElement("fieldset");
            try {
                return !!a(b);
            } catch (a) {
                return !1;
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null;
            }
        }
        function lb(a, b) {
            for (var c = a.split("|"), d = c.length; d--; ) f.attrHandle[c[d]] = b;
        }
        function mb(a, b) {
            var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
            if (d) return d;
            if (c) for (;c = c.nextSibling; ) if (c === b) return -1;
            return a ? 1 : -1;
        }
        function nb(a) {
            return function(b) {
                return "input" === b.nodeName.toLowerCase() && b.type === a;
            };
        }
        function ob(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a;
            };
        }
        function pb(a) {
            return function(b) {
                return "form" in b ? b.parentNode && !1 === b.disabled ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && gb(b) === a : b.disabled === a : "label" in b && b.disabled === a;
            };
        }
        function qb(a) {
            return jb(function(b) {
                return b = +b, jb(function(c, d) {
                    for (var e, f = a([], c.length, b), g = f.length; g--; ) c[e = f[g]] && (c[e] = !(d[e] = c[e]));
                });
            });
        }
        function rb(a) {
            return a && void 0 !== a.getElementsByTagName && a;
        }
        for (d in e = hb.support = {}, h = hb.isXML = function(a) {
            var b = a.namespaceURI, c = (a.ownerDocument || a).documentElement;
            return !Z.test(b || c && c.nodeName || "HTML");
        }, o = hb.setDocument = function(a) {
            var d, g, i = a ? a.ownerDocument || a : x;
            return i !== p && 9 === i.nodeType && i.documentElement && (q = (p = i).documentElement, 
            r = !h(p), x !== p && (g = p.defaultView) && g.top !== g && (g.addEventListener ? g.addEventListener("unload", c, !1) : g.attachEvent && g.attachEvent("onunload", c)), 
            e.attributes = kb(function(a) {
                return a.className = "i", !a.getAttribute("className");
            }), e.getElementsByTagName = kb(function(a) {
                return a.appendChild(p.createComment("")), !a.getElementsByTagName("*").length;
            }), e.getElementsByClassName = ab.test(p.getElementsByClassName), e.getById = kb(function(a) {
                return q.appendChild(a).id = w, !p.getElementsByName || !p.getElementsByName(w).length;
            }), e.getById ? (f.filter.ID = function(a) {
                var c = a.replace(db, b);
                return function(a) {
                    return a.getAttribute("id") === c;
                };
            }, f.find.ID = function(a, b) {
                if (void 0 !== b.getElementById && r) {
                    var c = b.getElementById(a);
                    return c ? [ c ] : [];
                }
            }) : (f.filter.ID = function(a) {
                var c = a.replace(db, b);
                return function(a) {
                    var b = void 0 !== a.getAttributeNode && a.getAttributeNode("id");
                    return b && b.value === c;
                };
            }, f.find.ID = function(a, b) {
                if (void 0 !== b.getElementById && r) {
                    var c, d, e, f = b.getElementById(a);
                    if (f) {
                        if ((c = f.getAttributeNode("id")) && c.value === a) return [ f ];
                        for (e = b.getElementsByName(a), d = 0; f = e[d++]; ) if ((c = f.getAttributeNode("id")) && c.value === a) return [ f ];
                    }
                    return [];
                }
            }), f.find.TAG = e.getElementsByTagName ? function(a, b) {
                return void 0 !== b.getElementsByTagName ? b.getElementsByTagName(a) : e.qsa ? b.querySelectorAll(a) : void 0;
            } : function(a, b) {
                var c, d = [], e = 0, f = b.getElementsByTagName(a);
                if ("*" !== a) return f;
                for (;c = f[e++]; ) 1 === c.nodeType && d.push(c);
                return d;
            }, f.find.CLASS = e.getElementsByClassName && function(a, b) {
                if (void 0 !== b.getElementsByClassName && r) return b.getElementsByClassName(a);
            }, t = [], s = [], (e.qsa = ab.test(p.querySelectorAll)) && (kb(function(a) {
                q.appendChild(a).innerHTML = "<a id='" + w + "'></a><select id='" + w + "-\r\\' msallowcapture=''><option selected=''></option></select>", 
                a.querySelectorAll("[msallowcapture^='']").length && s.push("[*^$]=" + N + "*(?:''|\"\")"), 
                a.querySelectorAll("[selected]").length || s.push("\\[" + N + "*(?:value|" + M + ")"), 
                a.querySelectorAll("[id~=" + w + "-]").length || s.push("~="), a.querySelectorAll(":checked").length || s.push(":checked"), 
                a.querySelectorAll("a#" + w + "+*").length || s.push(".#.+[+~]");
            }), kb(function(a) {
                a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var b = p.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && s.push("name" + N + "*[*^$|!~]?="), 
                2 !== a.querySelectorAll(":enabled").length && s.push(":enabled", ":disabled"), 
                q.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && s.push(":enabled", ":disabled"), 
                a.querySelectorAll("*,:x"), s.push(",.*:");
            })), (e.matchesSelector = ab.test(u = q.matches || q.webkitMatchesSelector || q.mozMatchesSelector || q.oMatchesSelector || q.msMatchesSelector)) && kb(function(a) {
                e.disconnectedMatch = u.call(a, "*"), u.call(a, "[s!='']:x"), t.push("!=", Q);
            }), s = s.length && new RegExp(s.join("|")), t = t.length && new RegExp(t.join("|")), 
            d = ab.test(q.compareDocumentPosition), v = d || ab.test(q.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
            } : function(a, b) {
                if (b) for (;b = b.parentNode; ) if (b === a) return !0;
                return !1;
            }, E = d ? function(a, b) {
                if (a === b) return n = !0, 0;
                var c = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return c || (1 & (c = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1) || !e.sortDetached && b.compareDocumentPosition(a) === c ? a === p || a.ownerDocument === x && v(x, a) ? -1 : b === p || b.ownerDocument === x && v(x, b) ? 1 : m ? L(m, a) - L(m, b) : 0 : 4 & c ? -1 : 1);
            } : function(a, b) {
                if (a === b) return n = !0, 0;
                var c, d = 0, e = a.parentNode, f = b.parentNode, g = [ a ], h = [ b ];
                if (!e || !f) return a === p ? -1 : b === p ? 1 : e ? -1 : f ? 1 : m ? L(m, a) - L(m, b) : 0;
                if (e === f) return mb(a, b);
                for (c = a; c = c.parentNode; ) g.unshift(c);
                for (c = b; c = c.parentNode; ) h.unshift(c);
                for (;g[d] === h[d]; ) d++;
                return d ? mb(g[d], h[d]) : g[d] === x ? -1 : h[d] === x ? 1 : 0;
            }), p;
        }, hb.matches = function(a, b) {
            return hb(a, null, null, b);
        }, hb.matchesSelector = function(a, b) {
            if ((a.ownerDocument || a) !== p && o(a), e.matchesSelector && r && !D[b + " "] && (!t || !t.test(b)) && (!s || !s.test(b))) try {
                var c = u.call(a, b);
                if (c || e.disconnectedMatch || a.document && 11 !== a.document.nodeType) return c;
            } catch (a) {
                D(b, !0);
            }
            return 0 < hb(b, p, null, [ a ]).length;
        }, hb.contains = function(a, b) {
            return (a.ownerDocument || a) !== p && o(a), v(a, b);
        }, hb.attr = function(a, b) {
            (a.ownerDocument || a) !== p && o(a);
            var c = f.attrHandle[b.toLowerCase()], d = c && F.call(f.attrHandle, b.toLowerCase()) ? c(a, b, !r) : void 0;
            return void 0 !== d ? d : e.attributes || !r ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
        }, hb.escape = function(a) {
            return (a + "").replace(eb, fb);
        }, hb.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a);
        }, hb.uniqueSort = function(a) {
            var b, c = [], d = 0, f = 0;
            if (n = !e.detectDuplicates, m = !e.sortStable && a.slice(0), a.sort(E), n) {
                for (;b = a[f++]; ) b === a[f] && (d = c.push(f));
                for (;d--; ) a.splice(c[d], 1);
            }
            return m = null, a;
        }, g = hb.getText = function(a) {
            var b, c = "", d = 0, e = a.nodeType;
            if (e) {
                if (1 === e || 9 === e || 11 === e) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += g(a);
                } else if (3 === e || 4 === e) return a.nodeValue;
            } else for (;b = a[d++]; ) c += g(b);
            return c;
        }, (f = hb.selectors = {
            cacheLength: 50,
            createPseudo: jb,
            match: Y,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    return a[1] = a[1].replace(db, b), a[3] = (a[3] || a[4] || a[5] || "").replace(db, b), 
                    "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
                },
                CHILD: function(a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || hb.error(a[0]), 
                    a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && hb.error(a[0]), 
                    a;
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    return Y.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && W.test(c) && (b = i(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), 
                    a[2] = c.slice(0, b)), a.slice(0, 3));
                }
            },
            filter: {
                TAG: function(a) {
                    var c = a.replace(db, b).toLowerCase();
                    return "*" === a ? function() {
                        return !0;
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === c;
                    };
                },
                CLASS: function(a) {
                    var b = A[a + " "];
                    return b || (b = new RegExp("(^|" + N + ")" + a + "(" + N + "|$)")) && A(a, function(a) {
                        return b.test("string" == typeof a.className && a.className || void 0 !== a.getAttribute && a.getAttribute("class") || "");
                    });
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = hb.attr(d, a);
                        return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && -1 < e.indexOf(c) : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? -1 < (" " + e.replace(R, " ") + " ").indexOf(c) : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"));
                    };
                },
                CHILD: function(a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
                    return 1 === d && 0 === e ? function(a) {
                        return !!a.parentNode;
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f != g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h, t = !1;
                        if (q) {
                            if (f) {
                                for (;p; ) {
                                    for (m = b; m = m[p]; ) if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling";
                                }
                                return !0;
                            }
                            if (o = [ g ? q.firstChild : q.lastChild ], g && s) {
                                for (t = (n = (j = (k = (l = (m = q)[w] || (m[w] = {}))[m.uniqueID] || (l[m.uniqueID] = {}))[a] || [])[0] === y && j[1]) && j[2], 
                                m = n && q.childNodes[n]; m = ++n && m && m[p] || (t = n = 0) || o.pop(); ) if (1 === m.nodeType && ++t && m === b) {
                                    k[a] = [ y, n, t ];
                                    break;
                                }
                            } else if (s && (t = n = (j = (k = (l = (m = b)[w] || (m[w] = {}))[m.uniqueID] || (l[m.uniqueID] = {}))[a] || [])[0] === y && j[1]), 
                            !1 === t) for (;(m = ++n && m && m[p] || (t = n = 0) || o.pop()) && ((h ? m.nodeName.toLowerCase() !== r : 1 !== m.nodeType) || !++t || (s && ((k = (l = m[w] || (m[w] = {}))[m.uniqueID] || (l[m.uniqueID] = {}))[a] = [ y, t ]), 
                            m !== b)); ) ;
                            return (t -= e) === d || t % d == 0 && 0 <= t / d;
                        }
                    };
                },
                PSEUDO: function(a, b) {
                    var c, d = f.pseudos[a] || f.setFilters[a.toLowerCase()] || hb.error("unsupported pseudo: " + a);
                    return d[w] ? d(b) : 1 < d.length ? (c = [ a, a, "", b ], f.setFilters.hasOwnProperty(a.toLowerCase()) ? jb(function(a, c) {
                        for (var e, f = d(a, b), g = f.length; g--; ) a[e = L(a, f[g])] = !(c[e] = f[g]);
                    }) : function(a) {
                        return d(a, 0, c);
                    }) : d;
                }
            },
            pseudos: {
                not: jb(function(a) {
                    var b = [], c = [], d = j(a.replace(S, "$1"));
                    return d[w] ? jb(function(a, b, c, e) {
                        for (var f, g = d(a, null, e, []), h = a.length; h--; ) (f = g[h]) && (a[h] = !(b[h] = f));
                    }) : function(a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop();
                    };
                }),
                has: jb(function(a) {
                    return function(b) {
                        return 0 < hb(a, b).length;
                    };
                }),
                contains: jb(function(a) {
                    return a = a.replace(db, b), function(b) {
                        return -1 < (b.textContent || g(b)).indexOf(a);
                    };
                }),
                lang: jb(function(a) {
                    return X.test(a || "") || hb.error("unsupported lang: " + a), a = a.replace(db, b).toLowerCase(), 
                    function(b) {
                        var c;
                        do if (c = r ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return (c = c.toLowerCase()) === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                        return !1;
                    };
                }),
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id;
                },
                root: function(a) {
                    return a === q;
                },
                focus: function(a) {
                    return a === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
                },
                enabled: pb(!1),
                disabled: pb(!0),
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected;
                },
                selected: function(a) {
                    return a.parentNode && a.parentNode.selectedIndex, !0 === a.selected;
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1;
                    return !0;
                },
                parent: function(a) {
                    return !f.pseudos.empty(a);
                },
                header: function(a) {
                    return _.test(a.nodeName);
                },
                input: function(a) {
                    return $.test(a.nodeName);
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b;
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
                },
                first: qb(function() {
                    return [ 0 ];
                }),
                last: qb(function(a, b) {
                    return [ b - 1 ];
                }),
                eq: qb(function(a, b, c) {
                    return [ c < 0 ? c + b : c ];
                }),
                even: qb(function(a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a;
                }),
                odd: qb(function(a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a;
                }),
                lt: qb(function(a, b, c) {
                    for (var d = c < 0 ? c + b : b < c ? b : c; 0 <= --d; ) a.push(d);
                    return a;
                }),
                gt: qb(function(a, b, c) {
                    for (var d = c < 0 ? c + b : c; ++d < b; ) a.push(d);
                    return a;
                })
            }
        }).pseudos.nth = f.pseudos.eq, {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) f.pseudos[d] = nb(d);
        for (d in {
            submit: !0,
            reset: !0
        }) f.pseudos[d] = ob(d);
        function sb() {}
        function tb(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d;
        }
        function ub(a, b, c) {
            var d = b.dir, e = b.next, f = e || d, g = c && "parentNode" === f, h = z++;
            return b.first ? function(b, c, e) {
                for (;b = b[d]; ) if (1 === b.nodeType || g) return a(b, c, e);
                return !1;
            } : function(b, c, i) {
                var j, k, l, m = [ y, h ];
                if (i) {
                    for (;b = b[d]; ) if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
                } else for (;b = b[d]; ) if (1 === b.nodeType || g) if (k = (l = b[w] || (b[w] = {}))[b.uniqueID] || (l[b.uniqueID] = {}), 
                e && e === b.nodeName.toLowerCase()) b = b[d] || b; else {
                    if ((j = k[f]) && j[0] === y && j[1] === h) return m[2] = j[2];
                    if ((k[f] = m)[2] = a(b, c, i)) return !0;
                }
                return !1;
            };
        }
        function vb(a) {
            return 1 < a.length ? function(b, c, d) {
                for (var e = a.length; e--; ) if (!a[e](b, c, d)) return !1;
                return !0;
            } : a[0];
        }
        function wb(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) (f = a[h]) && (c && !c(f, d, e) || (g.push(f), 
            j && b.push(h)));
            return g;
        }
        function xb(a, b, c, d, e, f) {
            return d && !d[w] && (d = xb(d)), e && !e[w] && (e = xb(e, f)), jb(function(f, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, p = f || function(a, b, c) {
                    for (var d = 0, e = b.length; d < e; d++) hb(a, b[d], c);
                    return c;
                }(b || "*", h.nodeType ? [ h ] : h, []), q = !a || !f && b ? p : wb(p, m, a, h, i), r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) for (j = wb(r, n), d(j, [], h, i), k = j.length; k--; ) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
                if (f) {
                    if (e || a) {
                        if (e) {
                            for (j = [], k = r.length; k--; ) (l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i);
                        }
                        for (k = r.length; k--; ) (l = r[k]) && -1 < (j = e ? L(f, l) : m[k]) && (f[j] = !(g[j] = l));
                    }
                } else r = wb(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : J.apply(g, r);
            });
        }
        function yb(a) {
            for (var b, c, d, e = a.length, g = f.relative[a[0].type], h = g || f.relative[" "], i = g ? 1 : 0, j = ub(function(a) {
                return a === b;
            }, h, !0), k = ub(function(a) {
                return -1 < L(b, a);
            }, h, !0), m = [ function(a, c, d) {
                var e = !g && (d || c !== l) || ((b = c).nodeType ? j : k)(a, c, d);
                return b = null, e;
            } ]; i < e; i++) if (c = f.relative[a[i].type]) m = [ ub(vb(m), c) ]; else {
                if ((c = f.filter[a[i].type].apply(null, a[i].matches))[w]) {
                    for (d = ++i; d < e && !f.relative[a[d].type]; d++) ;
                    return xb(1 < i && vb(m), 1 < i && tb(a.slice(0, i - 1).concat({
                        value: " " === a[i - 2].type ? "*" : ""
                    })).replace(S, "$1"), c, i < d && yb(a.slice(i, d)), d < e && yb(a = a.slice(d)), d < e && tb(a));
                }
                m.push(c);
            }
            return vb(m);
        }
        return sb.prototype = f.filters = f.pseudos, f.setFilters = new sb(), i = hb.tokenize = function(a, b) {
            var c, d, e, g, h, i, j, k = B[a + " "];
            if (k) return b ? 0 : k.slice(0);
            for (h = a, i = [], j = f.preFilter; h; ) {
                for (g in c && !(d = T.exec(h)) || (d && (h = h.slice(d[0].length) || h), i.push(e = [])), 
                c = !1, (d = U.exec(h)) && (c = d.shift(), e.push({
                    value: c,
                    type: d[0].replace(S, " ")
                }), h = h.slice(c.length)), f.filter) !(d = Y[g].exec(h)) || j[g] && !(d = j[g](d)) || (c = d.shift(), 
                e.push({
                    value: c,
                    type: g,
                    matches: d
                }), h = h.slice(c.length));
                if (!c) break;
            }
            return b ? h.length : h ? hb.error(a) : B(a, i).slice(0);
        }, j = hb.compile = function(a, b) {
            var c, d, e, g, h, j, k = [], m = [], n = C[a + " "];
            if (!n) {
                for (c = (b = b || i(a)).length; c--; ) (n = yb(b[c]))[w] ? k.push(n) : m.push(n);
                (n = C(a, (d = m, g = 0 < (e = k).length, h = 0 < d.length, j = function(a, b, c, i, j) {
                    var k, m, n, q = 0, s = "0", t = a && [], u = [], v = l, w = a || h && f.find.TAG("*", j), x = y += null == v ? 1 : Math.random() || .1, z = w.length;
                    for (j && (l = b === p || b || j); s !== z && null != (k = w[s]); s++) {
                        if (h && k) {
                            for (m = 0, b || k.ownerDocument === p || (o(k), c = !r); n = d[m++]; ) if (n(k, b || p, c)) {
                                i.push(k);
                                break;
                            }
                            j && (y = x);
                        }
                        g && ((k = !n && k) && q--, a && t.push(k));
                    }
                    if (q += s, g && s !== q) {
                        for (m = 0; n = e[m++]; ) n(t, u, b, c);
                        if (a) {
                            if (0 < q) for (;s--; ) t[s] || u[s] || (u[s] = H.call(i));
                            u = wb(u);
                        }
                        J.apply(i, u), j && !a && 0 < u.length && 1 < q + e.length && hb.uniqueSort(i);
                    }
                    return j && (y = x, l = v), t;
                }, g ? jb(j) : j))).selector = a;
            }
            return n;
        }, k = hb.select = function(a, c, d, e) {
            var g, h, k, l, m, n = "function" == typeof a && a, o = !e && i(a = n.selector || a);
            if (d = d || [], 1 === o.length) {
                if (2 < (h = o[0] = o[0].slice(0)).length && "ID" === (k = h[0]).type && 9 === c.nodeType && r && f.relative[h[1].type]) {
                    if (!(c = (f.find.ID(k.matches[0].replace(db, b), c) || [])[0])) return d;
                    n && (c = c.parentNode), a = a.slice(h.shift().value.length);
                }
                for (g = Y.needsContext.test(a) ? 0 : h.length; g-- && (k = h[g], !f.relative[l = k.type]); ) if ((m = f.find[l]) && (e = m(k.matches[0].replace(db, b), cb.test(h[0].type) && rb(c.parentNode) || c))) {
                    if (h.splice(g, 1), !(a = e.length && tb(h))) return J.apply(d, e), d;
                    break;
                }
            }
            return (n || j(a, o))(e, c, !r, d, !c || cb.test(a) && rb(c.parentNode) || c), d;
        }, e.sortStable = w.split("").sort(E).join("") === w, e.detectDuplicates = !!n, 
        o(), e.sortDetached = kb(function(a) {
            return 1 & a.compareDocumentPosition(p.createElement("fieldset"));
        }), kb(function(a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href");
        }) || lb("type|href|height|width", function(a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
        }), e.attributes && kb(function(a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
        }) || lb("value", function(a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
        }), kb(function(a) {
            return null == a.getAttribute("disabled");
        }) || lb(M, function(a, b, c) {
            var d;
            if (!c) return !0 === a[b] ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
        }), hb;
    }(a);
    u.find = x, u.expr = x.selectors, u.expr[":"] = u.expr.pseudos, u.uniqueSort = u.unique = x.uniqueSort, 
    u.text = x.getText, u.isXMLDoc = x.isXML, u.contains = x.contains, u.escapeSelector = x.escape;
    function y(a, b, c) {
        for (var d = [], e = void 0 !== c; (a = a[b]) && 9 !== a.nodeType; ) if (1 === a.nodeType) {
            if (e && u(a).is(c)) break;
            d.push(a);
        }
        return d;
    }
    function z(a, b) {
        for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
        return c;
    }
    var A = u.expr.match.needsContext;
    function B(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
    }
    var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    function D(a, b, c) {
        return q(b) ? u.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c;
        }) : b.nodeType ? u.grep(a, function(a) {
            return a === b !== c;
        }) : "string" != typeof b ? u.grep(a, function(a) {
            return -1 < j.call(b, a) !== c;
        }) : u.filter(b, a, c);
    }
    u.filter = function(a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? u.find.matchesSelector(d, a) ? [ d ] : [] : u.find.matches(a, u.grep(b, function(a) {
            return 1 === a.nodeType;
        }));
    }, u.fn.extend({
        find: function(a) {
            var b, c, d = this.length, e = this;
            if ("string" != typeof a) return this.pushStack(u(a).filter(function() {
                for (b = 0; b < d; b++) if (u.contains(e[b], this)) return !0;
            }));
            for (c = this.pushStack([]), b = 0; b < d; b++) u.find(a, e[b], c);
            return 1 < d ? u.uniqueSort(c) : c;
        },
        filter: function(a) {
            return this.pushStack(D(this, a || [], !1));
        },
        not: function(a) {
            return this.pushStack(D(this, a || [], !0));
        },
        is: function(a) {
            return !!D(this, "string" == typeof a && A.test(a) ? u(a) : a || [], !1).length;
        }
    });
    var E, F = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (u.fn.init = function(a, b, c) {
        var d, f;
        if (!a) return this;
        if (c = c || E, "string" != typeof a) return a.nodeType ? (this[0] = a, this.length = 1, 
        this) : q(a) ? void 0 !== c.ready ? c.ready(a) : a(u) : u.makeArray(a, this);
        if (!(d = "<" === a[0] && ">" === a[a.length - 1] && 3 <= a.length ? [ null, a, null ] : F.exec(a)) || !d[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
        if (d[1]) {
            if (b = b instanceof u ? b[0] : b, u.merge(this, u.parseHTML(d[1], b && b.nodeType ? b.ownerDocument || b : e, !0)), 
            C.test(d[1]) && u.isPlainObject(b)) for (d in b) q(this[d]) ? this[d](b[d]) : this.attr(d, b[d]);
            return this;
        }
        return (f = e.getElementById(d[2])) && (this[0] = f, this.length = 1), this;
    }).prototype = u.fn, E = u(e);
    var G = /^(?:parents|prev(?:Until|All))/, H = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    function I(a, b) {
        for (;(a = a[b]) && 1 !== a.nodeType; ) ;
        return a;
    }
    u.fn.extend({
        has: function(a) {
            var b = u(a, this), c = b.length;
            return this.filter(function() {
                for (var a = 0; a < c; a++) if (u.contains(this, b[a])) return !0;
            });
        },
        closest: function(a, b) {
            var c, d = 0, e = this.length, f = [], g = "string" != typeof a && u(a);
            if (!A.test(a)) for (;d < e; d++) for (c = this[d]; c && c !== b; c = c.parentNode) if (c.nodeType < 11 && (g ? -1 < g.index(c) : 1 === c.nodeType && u.find.matchesSelector(c, a))) {
                f.push(c);
                break;
            }
            return this.pushStack(1 < f.length ? u.uniqueSort(f) : f);
        },
        index: function(a) {
            return a ? "string" == typeof a ? j.call(u(a), this[0]) : j.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
        },
        add: function(a, b) {
            return this.pushStack(u.uniqueSort(u.merge(this.get(), u(a, b))));
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
        }
    }), u.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null;
        },
        parents: function(a) {
            return y(a, "parentNode");
        },
        parentsUntil: function(a, b, c) {
            return y(a, "parentNode", c);
        },
        next: function(a) {
            return I(a, "nextSibling");
        },
        prev: function(a) {
            return I(a, "previousSibling");
        },
        nextAll: function(a) {
            return y(a, "nextSibling");
        },
        prevAll: function(a) {
            return y(a, "previousSibling");
        },
        nextUntil: function(a, b, c) {
            return y(a, "nextSibling", c);
        },
        prevUntil: function(a, b, c) {
            return y(a, "previousSibling", c);
        },
        siblings: function(a) {
            return z((a.parentNode || {}).firstChild, a);
        },
        children: function(a) {
            return z(a.firstChild);
        },
        contents: function(a) {
            return void 0 !== a.contentDocument ? a.contentDocument : (B(a, "template") && (a = a.content || a), 
            u.merge([], a.childNodes));
        }
    }, function(a, b) {
        u.fn[a] = function(c, d) {
            var e = u.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = u.filter(d, e)), 
            1 < this.length && (H[a] || u.uniqueSort(e), G.test(a) && e.reverse()), this.pushStack(e);
        };
    });
    var J = /[^\x20\t\r\n\f]+/g;
    function K(a) {
        return a;
    }
    function L(a) {
        throw a;
    }
    function M(a, b, c, d) {
        var e;
        try {
            a && q(e = a.promise) ? e.call(a).done(b).fail(c) : a && q(e = a.then) ? e.call(a, b, c) : b.apply(void 0, [ a ].slice(d));
        } catch (a) {
            c.apply(void 0, [ a ]);
        }
    }
    u.Callbacks = function(a) {
        var b;
        a = "string" == typeof a ? (b = {}, u.each(a.match(J) || [], function(a, c) {
            b[c] = !0;
        }), b) : u.extend({}, a);
        function c() {
            for (g = g || a.once, f = d = !0; i.length; j = -1) for (e = i.shift(); ++j < h.length; ) !1 === h[j].apply(e[0], e[1]) && a.stopOnFalse && (j = h.length, 
            e = !1);
            a.memory || (e = !1), d = !1, g && (h = e ? [] : "");
        }
        var d, e, f, g, h = [], i = [], j = -1, k = {
            add: function() {
                return h && (e && !d && (j = h.length - 1, i.push(e)), function b(c) {
                    u.each(c, function(c, d) {
                        q(d) ? a.unique && k.has(d) || h.push(d) : d && d.length && "string" !== t(d) && b(d);
                    });
                }(arguments), e && !d && c()), this;
            },
            remove: function() {
                return u.each(arguments, function(a, b) {
                    for (var c; -1 < (c = u.inArray(b, h, c)); ) h.splice(c, 1), c <= j && j--;
                }), this;
            },
            has: function(a) {
                return a ? -1 < u.inArray(a, h) : 0 < h.length;
            },
            empty: function() {
                return h = h && [], this;
            },
            disable: function() {
                return g = i = [], h = e = "", this;
            },
            disabled: function() {
                return !h;
            },
            lock: function() {
                return g = i = [], e || d || (h = e = ""), this;
            },
            locked: function() {
                return !!g;
            },
            fireWith: function(a, b) {
                return g || (b = [ a, (b = b || []).slice ? b.slice() : b ], i.push(b), d || c()), 
                this;
            },
            fire: function() {
                return k.fireWith(this, arguments), this;
            },
            fired: function() {
                return !!f;
            }
        };
        return k;
    }, u.extend({
        Deferred: function(b) {
            var c = [ [ "notify", "progress", u.Callbacks("memory"), u.Callbacks("memory"), 2 ], [ "resolve", "done", u.Callbacks("once memory"), u.Callbacks("once memory"), 0, "resolved" ], [ "reject", "fail", u.Callbacks("once memory"), u.Callbacks("once memory"), 1, "rejected" ] ], d = "pending", e = {
                state: function() {
                    return d;
                },
                always: function() {
                    return f.done(arguments).fail(arguments), this;
                },
                "catch": function(a) {
                    return e.then(null, a);
                },
                pipe: function() {
                    var a = arguments;
                    return u.Deferred(function(b) {
                        u.each(c, function(c, d) {
                            var e = q(a[d[4]]) && a[d[4]];
                            f[d[1]](function() {
                                var a = e && e.apply(this, arguments);
                                a && q(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [ a ] : arguments);
                            });
                        }), a = null;
                    }).promise();
                },
                then: function(b, d, e) {
                    var f = 0;
                    function g(b, c, d, e) {
                        return function() {
                            function h() {
                                var a, h;
                                if (!(b < f)) {
                                    if ((a = d.apply(i, j)) === c.promise()) throw new TypeError("Thenable self-resolution");
                                    h = a && ("object" == typeof a || "function" == typeof a) && a.then, q(h) ? e ? h.call(a, g(f, c, K, e), g(f, c, L, e)) : (f++, 
                                    h.call(a, g(f, c, K, e), g(f, c, L, e), g(f, c, K, c.notifyWith))) : (d !== K && (i = void 0, 
                                    j = [ a ]), (e || c.resolveWith)(i, j));
                                }
                            }
                            var i = this, j = arguments, k = e ? h : function() {
                                try {
                                    a();
                                } catch (a) {
                                    u.Deferred.exceptionHook && u.Deferred.exceptionHook(a, k.stackTrace), f <= b + 1 && (d !== L && (i = void 0, 
                                    j = [ a ]), c.rejectWith(i, j));
                                }
                            };
                            b ? k() : (u.Deferred.getStackHook && (k.stackTrace = u.Deferred.getStackHook()), 
                            a.setTimeout(k));
                        };
                    }
                    return u.Deferred(function(a) {
                        c[0][3].add(g(0, a, q(e) ? e : K, a.notifyWith)), c[1][3].add(g(0, a, q(b) ? b : K)), 
                        c[2][3].add(g(0, a, q(d) ? d : L));
                    }).promise();
                },
                promise: function(a) {
                    return null != a ? u.extend(a, e) : e;
                }
            }, f = {};
            return u.each(c, function(a, b) {
                var g = b[2], h = b[5];
                e[b[1]] = g.add, h && g.add(function() {
                    d = h;
                }, c[3 - a][2].disable, c[3 - a][3].disable, c[0][2].lock, c[0][3].lock), g.add(b[3].fire), 
                f[b[0]] = function() {
                    return f[b[0] + "With"](this === f ? void 0 : this, arguments), this;
                }, f[b[0] + "With"] = g.fireWith;
            }), e.promise(f), b && b.call(f, f), f;
        },
        when: function(a) {
            function b(a) {
                return function(b) {
                    e[a] = this, f[a] = 1 < arguments.length ? g.call(arguments) : b, --c || h.resolveWith(e, f);
                };
            }
            var c = arguments.length, d = c, e = Array(d), f = g.call(arguments), h = u.Deferred();
            if (c <= 1 && (M(a, h.done(b(d)).resolve, h.reject, !c), "pending" === h.state() || q(f[d] && f[d].then))) return h.then();
            for (;d--; ) M(f[d], b(d), h.reject);
            return h.promise();
        }
    });
    var N = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    u.Deferred.exceptionHook = function(b, c) {
        a.console && a.console.warn && b && N.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
    }, u.readyException = function(b) {
        a.setTimeout(function() {
            throw b;
        });
    };
    var O = u.Deferred();
    function P() {
        e.removeEventListener("DOMContentLoaded", P), a.removeEventListener("load", P), 
        u.ready();
    }
    u.fn.ready = function(a) {
        return O.then(a).catch(function(a) {
            u.readyException(a);
        }), this;
    }, u.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(a) {
            (!0 === a ? --u.readyWait : u.isReady) || (u.isReady = !0) !== a && 0 < --u.readyWait || O.resolveWith(e, [ u ]);
        }
    }), u.ready.then = O.then, "complete" === e.readyState || "loading" !== e.readyState && !e.documentElement.doScroll ? a.setTimeout(u.ready) : (e.addEventListener("DOMContentLoaded", P), 
    a.addEventListener("load", P));
    var Q = function(a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = null == c;
        if ("object" === t(c)) for (h in e = !0, c) Q(a, b, h, c[h], !0, f, g); else if (void 0 !== d && (e = !0, 
        q(d) || (g = !0), j && (b = g ? (b.call(a, d), null) : (j = b, function(a, b, c) {
            return j.call(u(a), c);
        })), b)) for (;h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    }, R = /^-ms-/, S = /-([a-z])/g;
    function T(a, b) {
        return b.toUpperCase();
    }
    function U(a) {
        return a.replace(R, "ms-").replace(S, T);
    }
    function V(a) {
        return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
    }
    function W() {
        this.expando = u.expando + W.uid++;
    }
    W.uid = 1, W.prototype = {
        cache: function(a) {
            var b = a[this.expando];
            return b || (b = {}, V(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
                value: b,
                configurable: !0
            }))), b;
        },
        set: function(a, b, c) {
            var d, e = this.cache(a);
            if ("string" == typeof b) e[U(b)] = c; else for (d in b) e[U(d)] = b[d];
            return e;
        },
        get: function(a, b) {
            return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][U(b)];
        },
        access: function(a, b, c) {
            return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), 
            void 0 !== c ? c : b);
        },
        remove: function(a, b) {
            var c, d = a[this.expando];
            if (void 0 !== d) {
                if (void 0 !== b) {
                    c = (b = Array.isArray(b) ? b.map(U) : (b = U(b)) in d ? [ b ] : b.match(J) || []).length;
                    for (;c--; ) delete d[b[c]];
                }
                void 0 !== b && !u.isEmptyObject(d) || (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando]);
            }
        },
        hasData: function(a) {
            var b = a[this.expando];
            return void 0 !== b && !u.isEmptyObject(b);
        }
    };
    var X = new W(), Y = new W(), Z = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, $ = /[A-Z]/g;
    function _(a, b, c) {
        var d, e;
        if (void 0 === c && 1 === a.nodeType) if (d = "data-" + b.replace($, "-$&").toLowerCase(), 
        "string" == typeof (c = a.getAttribute(d))) {
            try {
                c = "true" === (e = c) || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Z.test(e) ? JSON.parse(e) : e);
            } catch (a) {}
            Y.set(a, b, c);
        } else c = void 0;
        return c;
    }
    u.extend({
        hasData: function(a) {
            return Y.hasData(a) || X.hasData(a);
        },
        data: function(a, b, c) {
            return Y.access(a, b, c);
        },
        removeData: function(a, b) {
            Y.remove(a, b);
        },
        _data: function(a, b, c) {
            return X.access(a, b, c);
        },
        _removeData: function(a, b) {
            X.remove(a, b);
        }
    }), u.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            if (void 0 !== a) return "object" == typeof a ? this.each(function() {
                Y.set(this, a);
            }) : Q(this, function(b) {
                var c;
                if (f && void 0 === b) return void 0 !== (c = Y.get(f, a)) || void 0 !== (c = _(f, a)) ? c : void 0;
                this.each(function() {
                    Y.set(this, a, b);
                });
            }, null, b, 1 < arguments.length, null, !0);
            if (this.length && (e = Y.get(f), 1 === f.nodeType && !X.get(f, "hasDataAttrs"))) {
                for (c = g.length; c--; ) g[c] && 0 === (d = g[c].name).indexOf("data-") && (d = U(d.slice(5)), 
                _(f, d, e[d]));
                X.set(f, "hasDataAttrs", !0);
            }
            return e;
        },
        removeData: function(a) {
            return this.each(function() {
                Y.remove(this, a);
            });
        }
    }), u.extend({
        queue: function(a, b, c) {
            var d;
            if (a) return b = (b || "fx") + "queue", d = X.get(a, b), c && (!d || Array.isArray(c) ? d = X.access(a, b, u.makeArray(c)) : d.push(c)), 
            d || [];
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = u.queue(a, b), d = c.length, e = c.shift(), f = u._queueHooks(a, b);
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), 
            delete f.stop, e.call(a, function() {
                u.dequeue(a, b);
            }, f)), !d && f && f.empty.fire();
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return X.get(a, c) || X.access(a, c, {
                empty: u.Callbacks("once memory").add(function() {
                    X.remove(a, [ b + "queue", c ]);
                })
            });
        }
    }), u.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? u.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var c = u.queue(this, a, b);
                u._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && u.dequeue(this, a);
            });
        },
        dequeue: function(a) {
            return this.each(function() {
                u.dequeue(this, a);
            });
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", []);
        },
        promise: function(a, b) {
            function c() {
                --e || f.resolveWith(g, [ g ]);
            }
            var d, e = 1, f = u.Deferred(), g = this, h = this.length;
            for ("string" != typeof a && (b = a, a = void 0), a = a || "fx"; h--; ) (d = X.get(g[h], a + "queueHooks")) && d.empty && (e++, 
            d.empty.add(c));
            return c(), f.promise(b);
        }
    });
    var ab = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, bb = new RegExp("^(?:([+-])=|)(" + ab + ")([a-z%]*)$", "i"), cb = [ "Top", "Right", "Bottom", "Left" ], db = e.documentElement, eb = function(a) {
        return u.contains(a.ownerDocument, a);
    }, fb = {
        composed: !0
    };
    db.getRootNode && (eb = function(a) {
        return u.contains(a.ownerDocument, a) || a.getRootNode(fb) === a.ownerDocument;
    });
    function gb(a, b) {
        return "none" === (a = b || a).style.display || "" === a.style.display && eb(a) && "none" === u.css(a, "display");
    }
    function hb(a, b, c, d) {
        var e, f, g = {};
        for (f in b) g[f] = a.style[f], a.style[f] = b[f];
        for (f in e = c.apply(a, d || []), b) a.style[f] = g[f];
        return e;
    }
    function ib(a, b, c, d) {
        var e, f, g = 20, h = d ? function() {
            return d.cur();
        } : function() {
            return u.css(a, b, "");
        }, i = h(), j = c && c[3] || (u.cssNumber[b] ? "" : "px"), k = a.nodeType && (u.cssNumber[b] || "px" !== j && +i) && bb.exec(u.css(a, b));
        if (k && k[3] !== j) {
            for (i /= 2, j = j || k[3], k = +i || 1; g--; ) u.style(a, b, k + j), (1 - f) * (1 - (f = h() / i || .5)) <= 0 && (g = 0), 
            k /= f;
            k *= 2, u.style(a, b, k + j), c = c || [];
        }
        return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, 
        d.start = k, d.end = e)), e;
    }
    var jb = {};
    function kb(a, b) {
        for (var c, d, e, f, g, h, i = [], j = 0, k = a.length; j < k; j++) (d = a[j]).style && (c = d.style.display, 
        b ? ("none" === c && (i[j] = X.get(d, "display") || null, i[j] || (d.style.display = "")), 
        "" === d.style.display && gb(d) && (i[j] = (h = f = e = void 0, f = d.ownerDocument, 
        g = d.nodeName, (h = jb[g]) || (e = f.body.appendChild(f.createElement(g)), h = u.css(e, "display"), 
        e.parentNode.removeChild(e), "none" === h && (h = "block"), jb[g] = h)))) : "none" !== c && (i[j] = "none", 
        X.set(d, "display", c)));
        for (j = 0; j < k; j++) null != i[j] && (a[j].style.display = i[j]);
        return a;
    }
    u.fn.extend({
        show: function() {
            return kb(this, !0);
        },
        hide: function() {
            return kb(this);
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                gb(this) ? u(this).show() : u(this).hide();
            });
        }
    });
    var lb = /^(?:checkbox|radio)$/i, mb = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i, nb = /^$|^module$|\/(?:java|ecma)script/i, ob = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        thead: [ 1, "<table>", "</table>" ],
        col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        _default: [ 0, "", "" ]
    };
    function pb(a, b) {
        var c;
        return c = void 0 !== a.getElementsByTagName ? a.getElementsByTagName(b || "*") : void 0 !== a.querySelectorAll ? a.querySelectorAll(b || "*") : [], 
        void 0 === b || b && B(a, b) ? u.merge([ a ], c) : c;
    }
    function qb(a, b) {
        for (var c = 0, d = a.length; c < d; c++) X.set(a[c], "globalEval", !b || X.get(b[c], "globalEval"));
    }
    ob.optgroup = ob.option, ob.tbody = ob.tfoot = ob.colgroup = ob.caption = ob.thead, 
    ob.th = ob.td;
    var rb, sb, tb = /<|&#?\w+;/;
    function ub(a, b, c, d, e) {
        for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++) if ((f = a[n]) || 0 === f) if ("object" === t(f)) u.merge(m, f.nodeType ? [ f ] : f); else if (tb.test(f)) {
            for (g = g || l.appendChild(b.createElement("div")), h = (mb.exec(f) || [ "", "" ])[1].toLowerCase(), 
            i = ob[h] || ob._default, g.innerHTML = i[1] + u.htmlPrefilter(f) + i[2], k = i[0]; k--; ) g = g.lastChild;
            u.merge(m, g.childNodes), (g = l.firstChild).textContent = "";
        } else m.push(b.createTextNode(f));
        for (l.textContent = "", n = 0; f = m[n++]; ) if (d && -1 < u.inArray(f, d)) e && e.push(f); else if (j = eb(f), 
        g = pb(l.appendChild(f), "script"), j && qb(g), c) for (k = 0; f = g[k++]; ) nb.test(f.type || "") && c.push(f);
        return l;
    }
    rb = e.createDocumentFragment().appendChild(e.createElement("div")), (sb = e.createElement("input")).setAttribute("type", "radio"), 
    sb.setAttribute("checked", "checked"), sb.setAttribute("name", "t"), rb.appendChild(sb), 
    p.checkClone = rb.cloneNode(!0).cloneNode(!0).lastChild.checked, rb.innerHTML = "<textarea>x</textarea>", 
    p.noCloneChecked = !!rb.cloneNode(!0).lastChild.defaultValue;
    var vb = /^key/, wb = /^(?:mouse|pointer|contextmenu|drag|drop)|click/, xb = /^([^.]*)(?:\.(.+)|)/;
    function yb() {
        return !0;
    }
    function zb() {
        return !1;
    }
    function Ab(a, b) {
        return a === function() {
            try {
                return e.activeElement;
            } catch (a) {}
        }() == ("focus" === b);
    }
    function Bb(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            for (h in "string" != typeof c && (d = d || c, c = void 0), b) Bb(a, h, c, d, b[h], f);
            return a;
        }
        if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, 
        d = void 0) : (e = d, d = c, c = void 0)), !1 === e) e = zb; else if (!e) return a;
        return 1 === f && (g = e, (e = function(a) {
            return u().off(a), g.apply(this, arguments);
        }).guid = g.guid || (g.guid = u.guid++)), a.each(function() {
            u.event.add(this, b, e, d, c);
        });
    }
    function Cb(a, b, c) {
        c ? (X.set(a, b, !1), u.event.add(a, b, {
            namespace: !1,
            handler: function(a) {
                var d, e, f = X.get(this, b);
                if (1 & a.isTrigger && this[b]) {
                    if (f.length) (u.event.special[b] || {}).delegateType && a.stopPropagation(); else if (f = g.call(arguments), 
                    X.set(this, b, f), d = c(this, b), this[b](), f !== (e = X.get(this, b)) || d ? X.set(this, b, !1) : e = {}, 
                    f !== e) return a.stopImmediatePropagation(), a.preventDefault(), e.value;
                } else f.length && (X.set(this, b, {
                    value: u.event.trigger(u.extend(f[0], u.Event.prototype), f.slice(1), this)
                }), a.stopImmediatePropagation());
            }
        })) : void 0 === X.get(a, b) && u.event.add(a, b, yb);
    }
    u.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = X.get(a);
            if (q) for (c.handler && (c = (f = c).handler, e = f.selector), e && u.find.matchesSelector(db, e), 
            c.guid || (c.guid = u.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function(b) {
                return void 0 !== u && u.event.triggered !== b.type ? u.event.dispatch.apply(a, arguments) : void 0;
            }), j = (b = (b || "").match(J) || [ "" ]).length; j--; ) n = p = (h = xb.exec(b[j]) || [])[1], 
            o = (h[2] || "").split(".").sort(), n && (l = u.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, 
            l = u.event.special[n] || {}, k = u.extend({
                type: n,
                origType: p,
                data: d,
                handler: c,
                guid: c.guid,
                selector: e,
                needsContext: e && u.expr.match.needsContext.test(e),
                namespace: o.join(".")
            }, f), (m = i[n]) || ((m = i[n] = []).delegateCount = 0, l.setup && !1 !== l.setup.call(a, d, o, g) || a.addEventListener && a.addEventListener(n, g)), 
            l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), 
            u.event.global[n] = !0);
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = X.hasData(a) && X.get(a);
            if (q && (i = q.events)) {
                for (j = (b = (b || "").match(J) || [ "" ]).length; j--; ) if (n = p = (h = xb.exec(b[j]) || [])[1], 
                o = (h[2] || "").split(".").sort(), n) {
                    for (l = u.event.special[n] || {}, m = i[n = (d ? l.delegateType : l.bindType) || n] || [], 
                    h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length; f--; ) k = m[f], 
                    !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), 
                    k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                    g && !m.length && (l.teardown && !1 !== l.teardown.call(a, o, q.handle) || u.removeEvent(a, n, q.handle), 
                    delete i[n]);
                } else for (n in i) u.event.remove(a, n + b[j], c, d, !0);
                u.isEmptyObject(i) && X.remove(a, "handle events");
            }
        },
        dispatch: function(a) {
            var b, c, d, e, f, g, h = u.event.fix(a), i = new Array(arguments.length), j = (X.get(this, "events") || {})[h.type] || [], k = u.event.special[h.type] || {};
            for (i[0] = h, b = 1; b < arguments.length; b++) i[b] = arguments[b];
            if (h.delegateTarget = this, !k.preDispatch || !1 !== k.preDispatch.call(this, h)) {
                for (g = u.event.handlers.call(this, h, j), b = 0; (e = g[b++]) && !h.isPropagationStopped(); ) for (h.currentTarget = e.elem, 
                c = 0; (f = e.handlers[c++]) && !h.isImmediatePropagationStopped(); ) h.rnamespace && !1 !== f.namespace && !h.rnamespace.test(f.namespace) || (h.handleObj = f, 
                h.data = f.data, void 0 !== (d = ((u.event.special[f.origType] || {}).handle || f.handler).apply(e.elem, i)) && !1 === (h.result = d) && (h.preventDefault(), 
                h.stopPropagation()));
                return k.postDispatch && k.postDispatch.call(this, h), h.result;
            }
        },
        handlers: function(a, b) {
            var c, d, e, f, g, h = [], i = b.delegateCount, j = a.target;
            if (i && j.nodeType && !("click" === a.type && 1 <= a.button)) for (;j !== this; j = j.parentNode || this) if (1 === j.nodeType && ("click" !== a.type || !0 !== j.disabled)) {
                for (f = [], g = {}, c = 0; c < i; c++) void 0 === g[e = (d = b[c]).selector + " "] && (g[e] = d.needsContext ? -1 < u(e, this).index(j) : u.find(e, this, null, [ j ]).length), 
                g[e] && f.push(d);
                f.length && h.push({
                    elem: j,
                    handlers: f
                });
            }
            return j = this, i < b.length && h.push({
                elem: j,
                handlers: b.slice(i)
            }), h;
        },
        addProp: function(a, b) {
            Object.defineProperty(u.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: q(b) ? function() {
                    if (this.originalEvent) return b(this.originalEvent);
                } : function() {
                    if (this.originalEvent) return this.originalEvent[a];
                },
                set: function(b) {
                    Object.defineProperty(this, a, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: b
                    });
                }
            });
        },
        fix: function(a) {
            return a[u.expando] ? a : new u.Event(a);
        },
        special: {
            load: {
                noBubble: !0
            },
            click: {
                setup: function(a) {
                    var b = this || a;
                    return lb.test(b.type) && b.click && B(b, "input") && Cb(b, "click", yb), !1;
                },
                trigger: function(a) {
                    var b = this || a;
                    return lb.test(b.type) && b.click && B(b, "input") && Cb(b, "click"), !0;
                },
                _default: function(a) {
                    var b = a.target;
                    return lb.test(b.type) && b.click && B(b, "input") && X.get(b, "click") || B(b, "a");
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
                }
            }
        }
    }, u.removeEvent = function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c);
    }, u.Event = function(a, b) {
        if (!(this instanceof u.Event)) return new u.Event(a, b);
        a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && !1 === a.returnValue ? yb : zb, 
        this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, 
        this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, 
        b && u.extend(this, b), this.timeStamp = a && a.timeStamp || Date.now(), this[u.expando] = !0;
    }, u.Event.prototype = {
        constructor: u.Event,
        isDefaultPrevented: zb,
        isPropagationStopped: zb,
        isImmediatePropagationStopped: zb,
        isSimulated: !1,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = yb, a && !this.isSimulated && a.preventDefault();
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = yb, a && !this.isSimulated && a.stopPropagation();
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = yb, a && !this.isSimulated && a.stopImmediatePropagation(), 
            this.stopPropagation();
        }
    }, u.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        code: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(a) {
            var b = a.button;
            return null == a.which && vb.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && wb.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which;
        }
    }, u.event.addProp), u.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        u.event.special[a] = {
            setup: function() {
                return Cb(this, a, Ab), !1;
            },
            trigger: function() {
                return Cb(this, a), !0;
            },
            delegateType: b
        };
    }), u.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        u.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = a.relatedTarget, e = a.handleObj;
                return d && (d === this || u.contains(this, d)) || (a.type = e.origType, c = e.handler.apply(this, arguments), 
                a.type = b), c;
            }
        };
    }), u.fn.extend({
        on: function(a, b, c, d) {
            return Bb(this, a, b, c, d);
        },
        one: function(a, b, c, d) {
            return Bb(this, a, b, c, d, 1);
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, u(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), 
            this;
            if ("object" != typeof a) return !1 !== b && "function" != typeof b || (c = b, b = void 0), 
            !1 === c && (c = zb), this.each(function() {
                u.event.remove(this, a, c, b);
            });
            for (e in a) this.off(e, b, a[e]);
            return this;
        }
    });
    var Db = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi, Eb = /<script|<style|<link/i, Fb = /checked\s*(?:[^=]|=\s*.checked.)/i, Gb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    function Hb(a, b) {
        return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr") && u(a).children("tbody")[0] || a;
    }
    function Ib(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a;
    }
    function Jb(a) {
        return "true/" === (a.type || "").slice(0, 5) ? a.type = a.type.slice(5) : a.removeAttribute("type"), 
        a;
    }
    function Kb(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (X.hasData(a) && (f = X.access(a), g = X.set(b, f), j = f.events)) for (e in delete g.handle, 
            g.events = {}, j) for (c = 0, d = j[e].length; c < d; c++) u.event.add(b, e, j[e][c]);
            Y.hasData(a) && (h = Y.access(a), i = u.extend({}, h), Y.set(b, i));
        }
    }
    function Lb(a, b, c, d) {
        b = h.apply([], b);
        var e, f, g, i, j, k, l = 0, m = a.length, n = m - 1, o = b[0], r = q(o);
        if (r || 1 < m && "string" == typeof o && !p.checkClone && Fb.test(o)) return a.each(function(e) {
            var f = a.eq(e);
            r && (b[0] = o.call(this, e, f.html())), Lb(f, b, c, d);
        });
        if (m && (f = (e = ub(b, a[0].ownerDocument, !1, a, d)).firstChild, 1 === e.childNodes.length && (e = f), 
        f || d)) {
            for (i = (g = u.map(pb(e, "script"), Ib)).length; l < m; l++) j = e, l !== n && (j = u.clone(j, !0, !0), 
            i && u.merge(g, pb(j, "script"))), c.call(a[l], j, l);
            if (i) for (k = g[g.length - 1].ownerDocument, u.map(g, Jb), l = 0; l < i; l++) j = g[l], 
            nb.test(j.type || "") && !X.access(j, "globalEval") && u.contains(k, j) && (j.src && "module" !== (j.type || "").toLowerCase() ? u._evalUrl && !j.noModule && u._evalUrl(j.src, {
                nonce: j.nonce || j.getAttribute("nonce")
            }) : s(j.textContent.replace(Gb, ""), j, k));
        }
        return a;
    }
    function Mb(a, b, c) {
        for (var d, e = b ? u.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || u.cleanData(pb(d)), 
        d.parentNode && (c && eb(d) && qb(pb(d, "script")), d.parentNode.removeChild(d));
        return a;
    }
    u.extend({
        htmlPrefilter: function(a) {
            return a.replace(Db, "<$1></$2>");
        },
        clone: function(a, b, c) {
            var d, e, f, g, h, i, j, k = a.cloneNode(!0), l = eb(a);
            if (!(p.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || u.isXMLDoc(a))) for (g = pb(k), 
            d = 0, e = (f = pb(a)).length; d < e; d++) h = f[d], "input" === (j = (i = g[d]).nodeName.toLowerCase()) && lb.test(h.type) ? i.checked = h.checked : "input" !== j && "textarea" !== j || (i.defaultValue = h.defaultValue);
            if (b) if (c) for (f = f || pb(a), g = g || pb(k), d = 0, e = f.length; d < e; d++) Kb(f[d], g[d]); else Kb(a, k);
            return 0 < (g = pb(k, "script")).length && qb(g, !l && pb(a, "script")), k;
        },
        cleanData: function(a) {
            for (var b, c, d, e = u.event.special, f = 0; void 0 !== (c = a[f]); f++) if (V(c)) {
                if (b = c[X.expando]) {
                    if (b.events) for (d in b.events) e[d] ? u.event.remove(c, d) : u.removeEvent(c, d, b.handle);
                    c[X.expando] = void 0;
                }
                c[Y.expando] && (c[Y.expando] = void 0);
            }
        }
    }), u.fn.extend({
        detach: function(a) {
            return Mb(this, a, !0);
        },
        remove: function(a) {
            return Mb(this, a);
        },
        text: function(a) {
            return Q(this, function(a) {
                return void 0 === a ? u.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a);
                });
            }, null, a, arguments.length);
        },
        append: function() {
            return Lb(this, arguments, function(a) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Hb(this, a).appendChild(a);
            });
        },
        prepend: function() {
            return Lb(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Hb(this, a);
                    b.insertBefore(a, b.firstChild);
                }
            });
        },
        before: function() {
            return Lb(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this);
            });
        },
        after: function() {
            return Lb(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
            });
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (u.cleanData(pb(a, !1)), 
            a.textContent = "");
            return this;
        },
        clone: function(a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function() {
                return u.clone(this, a, b);
            });
        },
        html: function(a) {
            return Q(this, function(a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
                if ("string" == typeof a && !Eb.test(a) && !ob[(mb.exec(a) || [ "", "" ])[1].toLowerCase()]) {
                    a = u.htmlPrefilter(a);
                    try {
                        for (;c < d; c++) 1 === (b = this[c] || {}).nodeType && (u.cleanData(pb(b, !1)), 
                        b.innerHTML = a);
                        b = 0;
                    } catch (a) {}
                }
                b && this.empty().append(a);
            }, null, a, arguments.length);
        },
        replaceWith: function() {
            var a = [];
            return Lb(this, arguments, function(b) {
                var c = this.parentNode;
                u.inArray(this, a) < 0 && (u.cleanData(pb(this)), c && c.replaceChild(b, this));
            }, a);
        }
    }), u.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        u.fn[a] = function(a) {
            for (var c, d = [], e = u(a), f = e.length - 1, g = 0; g <= f; g++) c = g === f ? this : this.clone(!0), 
            u(e[g])[b](c), i.apply(d, c.get());
            return this.pushStack(d);
        };
    });
    var Nb, Ob, Pb, Qb, Rb, Sb, Tb, Ub = new RegExp("^(" + ab + ")(?!px)[a-z%]+$", "i"), Vb = function(b) {
        var c = b.ownerDocument.defaultView;
        return c && c.opener || (c = a), c.getComputedStyle(b);
    }, Wb = new RegExp(cb.join("|"), "i");
    function Xb(a, b, c) {
        var d, e, f, g, h = a.style;
        return (c = c || Vb(a)) && ("" !== (g = c.getPropertyValue(b) || c[b]) || eb(a) || (g = u.style(a, b)), 
        !p.pixelBoxStyles() && Ub.test(g) && Wb.test(b) && (d = h.width, e = h.minWidth, 
        f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, 
        h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g;
    }
    function Yb(a, b) {
        return {
            get: function() {
                if (!a()) return (this.get = b).apply(this, arguments);
                delete this.get;
            }
        };
    }
    function Zb() {
        if (Tb) {
            Sb.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", 
            Tb.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", 
            db.appendChild(Sb).appendChild(Tb);
            var b = a.getComputedStyle(Tb);
            Nb = "1%" !== b.top, Rb = 12 === $b(b.marginLeft), Tb.style.right = "60%", Qb = 36 === $b(b.right), 
            Ob = 36 === $b(b.width), Tb.style.position = "absolute", Pb = 12 === $b(Tb.offsetWidth / 3), 
            db.removeChild(Sb), Tb = null;
        }
    }
    function $b(a) {
        return Math.round(parseFloat(a));
    }
    Sb = e.createElement("div"), (Tb = e.createElement("div")).style && (Tb.style.backgroundClip = "content-box", 
    Tb.cloneNode(!0).style.backgroundClip = "", p.clearCloneStyle = "content-box" === Tb.style.backgroundClip, 
    u.extend(p, {
        boxSizingReliable: function() {
            return Zb(), Ob;
        },
        pixelBoxStyles: function() {
            return Zb(), Qb;
        },
        pixelPosition: function() {
            return Zb(), Nb;
        },
        reliableMarginLeft: function() {
            return Zb(), Rb;
        },
        scrollboxSize: function() {
            return Zb(), Pb;
        }
    }));
    var _b = [ "Webkit", "Moz", "ms" ], ac = e.createElement("div").style, bc = {};
    function cc(a) {
        return u.cssProps[a] || bc[a] || (a in ac ? a : bc[a] = function(a) {
            for (var b = a[0].toUpperCase() + a.slice(1), c = _b.length; c--; ) if ((a = _b[c] + b) in ac) return a;
        }(a) || a);
    }
    var dc = /^(none|table(?!-c[ea]).+)/, ec = /^--/, fc = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, gc = {
        letterSpacing: "0",
        fontWeight: "400"
    };
    function hc(a, b, c) {
        var d = bb.exec(b);
        return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
    }
    function ic(a, b, c, d, e, f) {
        var g = "width" === b ? 1 : 0, h = 0, i = 0;
        if (c === (d ? "border" : "content")) return 0;
        for (;g < 4; g += 2) "margin" === c && (i += u.css(a, c + cb[g], !0, e)), d ? ("content" === c && (i -= u.css(a, "padding" + cb[g], !0, e)), 
        "margin" !== c && (i -= u.css(a, "border" + cb[g] + "Width", !0, e))) : (i += u.css(a, "padding" + cb[g], !0, e), 
        "padding" !== c ? i += u.css(a, "border" + cb[g] + "Width", !0, e) : h += u.css(a, "border" + cb[g] + "Width", !0, e));
        return !d && 0 <= f && (i += Math.max(0, Math.ceil(a["offset" + b[0].toUpperCase() + b.slice(1)] - f - i - h - .5)) || 0), 
        i;
    }
    function jc(a, b, c) {
        var d = Vb(a), e = (!p.boxSizingReliable() || c) && "border-box" === u.css(a, "boxSizing", !1, d), f = e, g = Xb(a, b, d), h = "offset" + b[0].toUpperCase() + b.slice(1);
        if (Ub.test(g)) {
            if (!c) return g;
            g = "auto";
        }
        return (!p.boxSizingReliable() && e || "auto" === g || !parseFloat(g) && "inline" === u.css(a, "display", !1, d)) && a.getClientRects().length && (e = "border-box" === u.css(a, "boxSizing", !1, d), 
        (f = h in a) && (g = a[h])), (g = parseFloat(g) || 0) + ic(a, b, c || (e ? "border" : "content"), f, d, g) + "px";
    }
    function kc(a, b, c, d, e) {
        return new kc.prototype.init(a, b, c, d, e);
    }
    u.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Xb(a, "opacity");
                        return "" === c ? "1" : c;
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            gridArea: !0,
            gridColumn: !0,
            gridColumnEnd: !0,
            gridColumnStart: !0,
            gridRow: !0,
            gridRowEnd: !0,
            gridRowStart: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function(a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = U(b), i = ec.test(b), j = a.style;
                if (i || (b = cc(h)), g = u.cssHooks[b] || u.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : j[b];
                "string" == (f = typeof c) && (e = bb.exec(c)) && e[1] && (c = ib(a, b, e), f = "number"), 
                null != c && c == c && ("number" !== f || i || (c += e && e[3] || (u.cssNumber[h] ? "" : "px")), 
                p.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"), 
                g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i ? j.setProperty(b, c) : j[b] = c));
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = U(b);
            return ec.test(b) || (b = cc(h)), (g = u.cssHooks[b] || u.cssHooks[h]) && "get" in g && (e = g.get(a, !0, c)), 
            void 0 === e && (e = Xb(a, b, d)), "normal" === e && b in gc && (e = gc[b]), "" === c || c ? (f = parseFloat(e), 
            !0 === c || isFinite(f) ? f || 0 : e) : e;
        }
    }), u.each([ "height", "width" ], function(a, b) {
        u.cssHooks[b] = {
            get: function(a, c, d) {
                if (c) return !dc.test(u.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? jc(a, b, d) : hb(a, fc, function() {
                    return jc(a, b, d);
                });
            },
            set: function(a, c, d) {
                var e, f = Vb(a), g = !p.scrollboxSize() && "absolute" === f.position, h = (g || d) && "border-box" === u.css(a, "boxSizing", !1, f), i = d ? ic(a, b, d, h, f) : 0;
                return h && g && (i -= Math.ceil(a["offset" + b[0].toUpperCase() + b.slice(1)] - parseFloat(f[b]) - ic(a, b, "border", !1, f) - .5)), 
                i && (e = bb.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = u.css(a, b)), 
                hc(0, c, i);
            }
        };
    }), u.cssHooks.marginLeft = Yb(p.reliableMarginLeft, function(a, b) {
        if (b) return (parseFloat(Xb(a, "marginLeft")) || a.getBoundingClientRect().left - hb(a, {
            marginLeft: 0
        }, function() {
            return a.getBoundingClientRect().left;
        })) + "px";
    }), u.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        u.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [ c ]; d < 4; d++) e[a + cb[d] + b] = f[d] || f[d - 2] || f[0];
                return e;
            }
        }, "margin" !== a && (u.cssHooks[a + b].set = hc);
    }), u.fn.extend({
        css: function(a, b) {
            return Q(this, function(a, b, c) {
                var d, e, f = {}, g = 0;
                if (Array.isArray(b)) {
                    for (d = Vb(a), e = b.length; g < e; g++) f[b[g]] = u.css(a, b[g], !1, d);
                    return f;
                }
                return void 0 !== c ? u.style(a, b, c) : u.css(a, b);
            }, a, b, 1 < arguments.length);
        }
    }), ((u.Tween = kc).prototype = {
        constructor: kc,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || u.easing._default, this.options = b, 
            this.start = this.now = this.cur(), this.end = d, this.unit = f || (u.cssNumber[c] ? "" : "px");
        },
        cur: function() {
            var a = kc.propHooks[this.prop];
            return a && a.get ? a.get(this) : kc.propHooks._default.get(this);
        },
        run: function(a) {
            var b, c = kc.propHooks[this.prop];
            return this.options.duration ? this.pos = b = u.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, 
            this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), 
            c && c.set ? c.set(this) : kc.propHooks._default.set(this), this;
        }
    }).init.prototype = kc.prototype, (kc.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = u.css(a.elem, a.prop, "")) && "auto" !== b ? b : 0;
            },
            set: function(a) {
                u.fx.step[a.prop] ? u.fx.step[a.prop](a) : 1 !== a.elem.nodeType || !u.cssHooks[a.prop] && null == a.elem.style[cc(a.prop)] ? a.elem[a.prop] = a.now : u.style(a.elem, a.prop, a.now + a.unit);
            }
        }
    }).scrollTop = kc.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
        }
    }, u.easing = {
        linear: function(a) {
            return a;
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2;
        },
        _default: "swing"
    }, u.fx = kc.prototype.init, u.fx.step = {};
    var lc, mc, nc, oc, pc = /^(?:toggle|show|hide)$/, qc = /queueHooks$/;
    function rc() {
        mc && (!1 === e.hidden && a.requestAnimationFrame ? a.requestAnimationFrame(rc) : a.setTimeout(rc, u.fx.interval), 
        u.fx.tick());
    }
    function sc() {
        return a.setTimeout(function() {
            lc = void 0;
        }), lc = Date.now();
    }
    function tc(a, b) {
        var c, d = 0, e = {
            height: a
        };
        for (b = b ? 1 : 0; d < 4; d += 2 - b) e["margin" + (c = cb[d])] = e["padding" + c] = a;
        return b && (e.opacity = e.width = a), e;
    }
    function uc(a, b, c) {
        for (var d, e = (vc.tweeners[b] || []).concat(vc.tweeners["*"]), f = 0, g = e.length; f < g; f++) if (d = e[f].call(c, b, a)) return d;
    }
    function vc(a, b, c) {
        var d, e, f = 0, g = vc.prefilters.length, h = u.Deferred().always(function() {
            delete i.elem;
        }), i = function() {
            if (e) return !1;
            for (var b = lc || sc(), c = Math.max(0, j.startTime + j.duration - b), d = 1 - (c / j.duration || 0), f = 0, g = j.tweens.length; f < g; f++) j.tweens[f].run(d);
            return h.notifyWith(a, [ j, d, c ]), d < 1 && g ? c : (g || h.notifyWith(a, [ j, 1, 0 ]), 
            h.resolveWith(a, [ j ]), !1);
        }, j = h.promise({
            elem: a,
            props: u.extend({}, b),
            opts: u.extend(!0, {
                specialEasing: {},
                easing: u.easing._default
            }, c),
            originalProperties: b,
            originalOptions: c,
            startTime: lc || sc(),
            duration: c.duration,
            tweens: [],
            createTween: function(b, c) {
                var d = u.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                return j.tweens.push(d), d;
            },
            stop: function(b) {
                var c = 0, d = b ? j.tweens.length : 0;
                if (e) return this;
                for (e = !0; c < d; c++) j.tweens[c].run(1);
                return b ? (h.notifyWith(a, [ j, 1, 0 ]), h.resolveWith(a, [ j, b ])) : h.rejectWith(a, [ j, b ]), 
                this;
            }
        }), k = j.props;
        for ((function(a, b) {
            var c, d, e, f, g;
            for (c in a) if (e = b[d = U(c)], f = a[c], Array.isArray(f) && (e = f[1], f = a[c] = f[0]), 
            c !== d && (a[d] = f, delete a[c]), (g = u.cssHooks[d]) && "expand" in g) for (c in f = g.expand(f), 
            delete a[d], f) c in a || (a[c] = f[c], b[c] = e); else b[d] = e;
        }(k, j.opts.specialEasing)); f < g; f++) if (d = vc.prefilters[f].call(j, a, k, j.opts)) return q(d.stop) && (u._queueHooks(j.elem, j.opts.queue).stop = d.stop.bind(d)), 
        d;
        return u.map(k, uc, j), q(j.opts.start) && j.opts.start.call(a, j), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always), 
        u.fx.timer(u.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j;
    }
    u.Animation = u.extend(vc, {
        tweeners: {
            "*": [ function(a, b) {
                var c = this.createTween(a, b);
                return ib(c.elem, a, bb.exec(b), c), c;
            } ]
        },
        tweener: function(a, b) {
            for (var c, d = 0, e = (a = q(a) ? (b = a, [ "*" ]) : a.match(J)).length; d < e; d++) c = a[d], 
            vc.tweeners[c] = vc.tweeners[c] || [], vc.tweeners[c].unshift(b);
        },
        prefilters: [ function(a, b, c) {
            var d, e, f, g, h, i, j, k, l = "width" in b || "height" in b, m = this, n = {}, o = a.style, p = a.nodeType && gb(a), q = X.get(a, "fxshow");
            for (d in c.queue || (null == (g = u._queueHooks(a, "fx")).unqueued && (g.unqueued = 0, 
            h = g.empty.fire, g.empty.fire = function() {
                g.unqueued || h();
            }), g.unqueued++, m.always(function() {
                m.always(function() {
                    g.unqueued--, u.queue(a, "fx").length || g.empty.fire();
                });
            })), b) if (e = b[d], pc.test(e)) {
                if (delete b[d], f = f || "toggle" === e, e === (p ? "hide" : "show")) {
                    if ("show" !== e || !q || void 0 === q[d]) continue;
                    p = !0;
                }
                n[d] = q && q[d] || u.style(a, d);
            }
            if ((i = !u.isEmptyObject(b)) || !u.isEmptyObject(n)) for (d in l && 1 === a.nodeType && (c.overflow = [ o.overflow, o.overflowX, o.overflowY ], 
            null == (j = q && q.display) && (j = X.get(a, "display")), "none" === (k = u.css(a, "display")) && (j ? k = j : (kb([ a ], !0), 
            j = a.style.display || j, k = u.css(a, "display"), kb([ a ]))), ("inline" === k || "inline-block" === k && null != j) && "none" === u.css(a, "float") && (i || (m.done(function() {
                o.display = j;
            }), null == j && (k = o.display, j = "none" === k ? "" : k)), o.display = "inline-block")), 
            c.overflow && (o.overflow = "hidden", m.always(function() {
                o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2];
            })), i = !1, n) i || (q ? "hidden" in q && (p = q.hidden) : q = X.access(a, "fxshow", {
                display: j
            }), f && (q.hidden = !p), p && kb([ a ], !0), m.done(function() {
                for (d in p || kb([ a ]), X.remove(a, "fxshow"), n) u.style(a, d, n[d]);
            })), i = uc(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, 
            i.start = 0));
        } ],
        prefilter: function(a, b) {
            b ? vc.prefilters.unshift(a) : vc.prefilters.push(a);
        }
    }), u.speed = function(a, b, c) {
        var d = a && "object" == typeof a ? u.extend({}, a) : {
            complete: c || !c && b || q(a) && a,
            duration: a,
            easing: c && b || b && !q(b) && b
        };
        return u.fx.off ? d.duration = 0 : "number" != typeof d.duration && (d.duration in u.fx.speeds ? d.duration = u.fx.speeds[d.duration] : d.duration = u.fx.speeds._default), 
        null != d.queue && !0 !== d.queue || (d.queue = "fx"), d.old = d.complete, d.complete = function() {
            q(d.old) && d.old.call(this), d.queue && u.dequeue(this, d.queue);
        }, d;
    }, u.fn.extend({
        fadeTo: function(a, b, c, d) {
            return this.filter(gb).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d);
        },
        animate: function(a, b, c, d) {
            function e() {
                var b = vc(this, u.extend({}, a), g);
                (f || X.get(this, "finish")) && b.stop(!0);
            }
            var f = u.isEmptyObject(a), g = u.speed(b, c, d);
            return e.finish = e, f || !1 === g.queue ? this.each(e) : this.queue(g.queue, e);
        },
        stop: function(a, b, c) {
            function d(a) {
                var b = a.stop;
                delete a.stop, b(c);
            }
            return "string" != typeof a && (c = b, b = a, a = void 0), b && !1 !== a && this.queue(a || "fx", []), 
            this.each(function() {
                var b = !0, e = null != a && a + "queueHooks", f = u.timers, g = X.get(this);
                if (e) g[e] && g[e].stop && d(g[e]); else for (e in g) g[e] && g[e].stop && qc.test(e) && d(g[e]);
                for (e = f.length; e--; ) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), 
                b = !1, f.splice(e, 1));
                !b && c || u.dequeue(this, a);
            });
        },
        finish: function(a) {
            return !1 !== a && (a = a || "fx"), this.each(function() {
                var b, c = X.get(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = u.timers, g = d ? d.length : 0;
                for (c.finish = !0, u.queue(this, a, []), e && e.stop && e.stop.call(this, !0), 
                b = f.length; b--; ) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), 
                f.splice(b, 1));
                for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish;
            });
        }
    }), u.each([ "toggle", "show", "hide" ], function(a, b) {
        var c = u.fn[b];
        u.fn[b] = function(a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(tc(b, !0), a, d, e);
        };
    }), u.each({
        slideDown: tc("show"),
        slideUp: tc("hide"),
        slideToggle: tc("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        u.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d);
        };
    }), u.timers = [], u.fx.tick = function() {
        var a, b = 0, c = u.timers;
        for (lc = Date.now(); b < c.length; b++) (a = c[b])() || c[b] !== a || c.splice(b--, 1);
        c.length || u.fx.stop(), lc = void 0;
    }, u.fx.timer = function(a) {
        u.timers.push(a), u.fx.start();
    }, u.fx.interval = 13, u.fx.start = function() {
        mc || (mc = !0, rc());
    }, u.fx.stop = function() {
        mc = null;
    }, u.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, u.fn.delay = function(b, c) {
        return b = u.fx && u.fx.speeds[b] || b, c = c || "fx", this.queue(c, function(c, d) {
            var e = a.setTimeout(c, b);
            d.stop = function() {
                a.clearTimeout(e);
            };
        });
    }, nc = e.createElement("input"), oc = e.createElement("select").appendChild(e.createElement("option")), 
    nc.type = "checkbox", p.checkOn = "" !== nc.value, p.optSelected = oc.selected, 
    (nc = e.createElement("input")).value = "t", nc.type = "radio", p.radioValue = "t" === nc.value;
    var wc, xc = u.expr.attrHandle;
    u.fn.extend({
        attr: function(a, b) {
            return Q(this, u.attr, a, b, 1 < arguments.length);
        },
        removeAttr: function(a) {
            return this.each(function() {
                u.removeAttr(this, a);
            });
        }
    }), u.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return void 0 === a.getAttribute ? u.prop(a, b, c) : (1 === f && u.isXMLDoc(a) || (e = u.attrHooks[b.toLowerCase()] || (u.expr.match.bool.test(b) ? wc : void 0)), 
            void 0 !== c ? null === c ? void u.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), 
            c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : null == (d = u.find.attr(a, b)) ? void 0 : d);
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!p.radioValue && "radio" === b && B(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b;
                    }
                }
            }
        },
        removeAttr: function(a, b) {
            var c, d = 0, e = b && b.match(J);
            if (e && 1 === a.nodeType) for (;c = e[d++]; ) a.removeAttribute(c);
        }
    }), wc = {
        set: function(a, b, c) {
            return !1 === b ? u.removeAttr(a, c) : a.setAttribute(c, c), c;
        }
    }, u.each(u.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = xc[b] || u.find.attr;
        xc[b] = function(a, b, d) {
            var e, f, g = b.toLowerCase();
            return d || (f = xc[g], xc[g] = e, e = null != c(a, b, d) ? g : null, xc[g] = f), 
            e;
        };
    });
    var yc = /^(?:input|select|textarea|button)$/i, zc = /^(?:a|area)$/i;
    function Ac(a) {
        return (a.match(J) || []).join(" ");
    }
    function Bc(a) {
        return a.getAttribute && a.getAttribute("class") || "";
    }
    function Cc(a) {
        return Array.isArray(a) ? a : "string" == typeof a && a.match(J) || [];
    }
    u.fn.extend({
        prop: function(a, b) {
            return Q(this, u.prop, a, b, 1 < arguments.length);
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[u.propFix[a] || a];
            });
        }
    }), u.extend({
        prop: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return 1 === f && u.isXMLDoc(a) || (b = u.propFix[b] || b, 
            e = u.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b];
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = u.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : yc.test(a.nodeName) || zc.test(a.nodeName) && a.href ? 0 : -1;
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), p.optSelected || (u.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && b.parentNode && b.parentNode.selectedIndex, null;
        },
        set: function(a) {
            var b = a.parentNode;
            b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
        }
    }), u.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        u.propFix[this.toLowerCase()] = this;
    }), u.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (q(a)) return this.each(function(b) {
                u(this).addClass(a.call(this, b, Bc(this)));
            });
            if ((b = Cc(a)).length) for (;c = this[i++]; ) if (e = Bc(c), d = 1 === c.nodeType && " " + Ac(e) + " ") {
                for (g = 0; f = b[g++]; ) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                e !== (h = Ac(d)) && c.setAttribute("class", h);
            }
            return this;
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (q(a)) return this.each(function(b) {
                u(this).removeClass(a.call(this, b, Bc(this)));
            });
            if (!arguments.length) return this.attr("class", "");
            if ((b = Cc(a)).length) for (;c = this[i++]; ) if (e = Bc(c), d = 1 === c.nodeType && " " + Ac(e) + " ") {
                for (g = 0; f = b[g++]; ) for (;-1 < d.indexOf(" " + f + " "); ) d = d.replace(" " + f + " ", " ");
                e !== (h = Ac(d)) && c.setAttribute("class", h);
            }
            return this;
        },
        toggleClass: function(a, b) {
            var c = typeof a, d = "string" == c || Array.isArray(a);
            return "boolean" == typeof b && d ? b ? this.addClass(a) : this.removeClass(a) : q(a) ? this.each(function(c) {
                u(this).toggleClass(a.call(this, c, Bc(this), b), b);
            }) : this.each(function() {
                var b, e, f, g;
                if (d) for (e = 0, f = u(this), g = Cc(a); b = g[e++]; ) f.hasClass(b) ? f.removeClass(b) : f.addClass(b); else void 0 !== a && "boolean" != c || ((b = Bc(this)) && X.set(this, "__className__", b), 
                this.setAttribute && this.setAttribute("class", b || !1 === a ? "" : X.get(this, "__className__") || ""));
            });
        },
        hasClass: function(a) {
            var b, c, d = 0;
            for (b = " " + a + " "; c = this[d++]; ) if (1 === c.nodeType && -1 < (" " + Ac(Bc(c)) + " ").indexOf(b)) return !0;
            return !1;
        }
    });
    var Dc = /\r/g;
    u.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0];
            return arguments.length ? (d = q(a), this.each(function(c) {
                var e;
                1 === this.nodeType && (null == (e = d ? a.call(this, c, u(this).val()) : a) ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = u.map(e, function(a) {
                    return null == a ? "" : a + "";
                })), (b = u.valHooks[this.type] || u.valHooks[this.nodeName.toLowerCase()]) && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e));
            })) : e ? (b = u.valHooks[e.type] || u.valHooks[e.nodeName.toLowerCase()]) && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : "string" == typeof (c = e.value) ? c.replace(Dc, "") : null == c ? "" : c : void 0;
        }
    }), u.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = u.find.attr(a, "value");
                    return null != b ? b : Ac(u.text(a));
                }
            },
            select: {
                get: function(a) {
                    var b, c, d, e = a.options, f = a.selectedIndex, g = "select-one" === a.type, h = g ? null : [], i = g ? f + 1 : e.length;
                    for (d = f < 0 ? i : g ? f : 0; d < i; d++) if (((c = e[d]).selected || d === f) && !c.disabled && (!c.parentNode.disabled || !B(c.parentNode, "optgroup"))) {
                        if (b = u(c).val(), g) return b;
                        h.push(b);
                    }
                    return h;
                },
                set: function(a, b) {
                    for (var c, d, e = a.options, f = u.makeArray(b), g = e.length; g--; ) ((d = e[g]).selected = -1 < u.inArray(u.valHooks.option.get(d), f)) && (c = !0);
                    return c || (a.selectedIndex = -1), f;
                }
            }
        }
    }), u.each([ "radio", "checkbox" ], function() {
        u.valHooks[this] = {
            set: function(a, b) {
                if (Array.isArray(b)) return a.checked = -1 < u.inArray(u(a).val(), b);
            }
        }, p.checkOn || (u.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value;
        });
    }), p.focusin = "onfocusin" in a;
    function Ec(a) {
        a.stopPropagation();
    }
    var Fc = /^(?:focusinfocus|focusoutblur)$/;
    u.extend(u.event, {
        trigger: function(b, d, f, g) {
            var h, i, j, k, l, n, o, p, r = [ f || e ], s = m.call(b, "type") ? b.type : b, t = m.call(b, "namespace") ? b.namespace.split(".") : [];
            if (i = p = j = f = f || e, 3 !== f.nodeType && 8 !== f.nodeType && !Fc.test(s + u.event.triggered) && (-1 < s.indexOf(".") && (s = (t = s.split(".")).shift(), 
            t.sort()), l = s.indexOf(":") < 0 && "on" + s, (b = b[u.expando] ? b : new u.Event(s, "object" == typeof b && b)).isTrigger = g ? 2 : 3, 
            b.namespace = t.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + t.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, 
            b.result = void 0, b.target || (b.target = f), d = null == d ? [ b ] : u.makeArray(d, [ b ]), 
            o = u.event.special[s] || {}, g || !o.trigger || !1 !== o.trigger.apply(f, d))) {
                if (!g && !o.noBubble && !c(f)) {
                    for (k = o.delegateType || s, Fc.test(k + s) || (i = i.parentNode); i; i = i.parentNode) r.push(i), 
                    j = i;
                    j === (f.ownerDocument || e) && r.push(j.defaultView || j.parentWindow || a);
                }
                for (h = 0; (i = r[h++]) && !b.isPropagationStopped(); ) p = i, b.type = 1 < h ? k : o.bindType || s, 
                (n = (X.get(i, "events") || {})[b.type] && X.get(i, "handle")) && n.apply(i, d), 
                (n = l && i[l]) && n.apply && V(i) && (b.result = n.apply(i, d), !1 === b.result && b.preventDefault());
                return b.type = s, g || b.isDefaultPrevented() || o._default && !1 !== o._default.apply(r.pop(), d) || !V(f) || l && q(f[s]) && !c(f) && ((j = f[l]) && (f[l] = null), 
                u.event.triggered = s, b.isPropagationStopped() && p.addEventListener(s, Ec), f[s](), 
                b.isPropagationStopped() && p.removeEventListener(s, Ec), u.event.triggered = void 0, 
                j && (f[l] = j)), b.result;
            }
        },
        simulate: function(a, b, c) {
            var d = u.extend(new u.Event(), c, {
                type: a,
                isSimulated: !0
            });
            u.event.trigger(d, null, b);
        }
    }), u.fn.extend({
        trigger: function(a, b) {
            return this.each(function() {
                u.event.trigger(a, b, this);
            });
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) return u.event.trigger(a, b, c, !0);
        }
    }), p.focusin || u.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        function c(a) {
            u.event.simulate(b, a.target, u.event.fix(a));
        }
        u.event.special[b] = {
            setup: function() {
                var d = this.ownerDocument || this, e = X.access(d, b);
                e || d.addEventListener(a, c, !0), X.access(d, b, (e || 0) + 1);
            },
            teardown: function() {
                var d = this.ownerDocument || this, e = X.access(d, b) - 1;
                e ? X.access(d, b, e) : (d.removeEventListener(a, c, !0), X.remove(d, b));
            }
        };
    });
    var Gc = a.location, Hc = Date.now(), Ic = /\?/;
    u.parseXML = function(b) {
        var c;
        if (!b || "string" != typeof b) return null;
        try {
            c = new a.DOMParser().parseFromString(b, "text/xml");
        } catch (b) {
            c = void 0;
        }
        return c && !c.getElementsByTagName("parsererror").length || u.error("Invalid XML: " + b), 
        c;
    };
    var Jc = /\[\]$/, Kc = /\r?\n/g, Lc = /^(?:submit|button|image|reset|file)$/i, Mc = /^(?:input|select|textarea|keygen)/i;
    function Nc(a, b, c, d) {
        var e;
        if (Array.isArray(b)) u.each(b, function(b, e) {
            c || Jc.test(a) ? d(a, e) : Nc(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d);
        }); else if (c || "object" !== t(b)) d(a, b); else for (e in b) Nc(a + "[" + e + "]", b[e], c, d);
    }
    u.param = function(a, b) {
        function c(a, b) {
            var c = q(b) ? b() : b;
            e[e.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
        }
        var d, e = [];
        if (null == a) return "";
        if (Array.isArray(a) || a.jquery && !u.isPlainObject(a)) u.each(a, function() {
            c(this.name, this.value);
        }); else for (d in a) Nc(d, a[d], b, c);
        return e.join("&");
    }, u.fn.extend({
        serialize: function() {
            return u.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                var a = u.prop(this, "elements");
                return a ? u.makeArray(a) : this;
            }).filter(function() {
                var a = this.type;
                return this.name && !u(this).is(":disabled") && Mc.test(this.nodeName) && !Lc.test(a) && (this.checked || !lb.test(a));
            }).map(function(a, b) {
                var c = u(this).val();
                return null == c ? null : Array.isArray(c) ? u.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(Kc, "\r\n")
                    };
                }) : {
                    name: b.name,
                    value: c.replace(Kc, "\r\n")
                };
            }).get();
        }
    });
    var Oc = /%20/g, Pc = /#.*$/, Qc = /([?&])_=[^&]*/, Rc = /^(.*?):[ \t]*([^\r\n]*)$/gm, Sc = /^(?:GET|HEAD)$/, Tc = /^\/\//, Uc = {}, Vc = {}, Wc = "*/".concat("*"), Xc = e.createElement("a");
    function Yc(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0, f = b.toLowerCase().match(J) || [];
            if (q(c)) for (;d = f[e++]; ) "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c);
        };
    }
    function Zc(a, b, c, d) {
        var e = {}, f = a === Vc;
        function g(h) {
            var i;
            return e[h] = !0, u.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), 
                g(j), !1);
            }), i;
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*");
    }
    function $c(a, b) {
        var c, d, e = u.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((e[c] ? a : d = d || {})[c] = b[c]);
        return d && u.extend(!0, a, d), a;
    }
    Xc.href = Gc.href, u.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Gc.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Gc.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Wc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": u.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? $c($c(a, u.ajaxSettings), b) : $c(u.ajaxSettings, a);
        },
        ajaxPrefilter: Yc(Uc),
        ajaxTransport: Yc(Vc),
        ajax: function(b, c) {
            "object" == typeof b && (c = b, b = void 0), c = c || {};
            var d, f, g, h, i, j, k, l, m, n, o = u.ajaxSetup({}, c), p = o.context || o, q = o.context && (p.nodeType || p.jquery) ? u(p) : u.event, r = u.Deferred(), s = u.Callbacks("once memory"), t = o.statusCode || {}, v = {}, w = {}, x = "canceled", y = {
                readyState: 0,
                getResponseHeader: function(a) {
                    var b;
                    if (k) {
                        if (!h) for (h = {}; b = Rc.exec(g); ) h[b[1].toLowerCase() + " "] = (h[b[1].toLowerCase() + " "] || []).concat(b[2]);
                        b = h[a.toLowerCase() + " "];
                    }
                    return null == b ? null : b.join(", ");
                },
                getAllResponseHeaders: function() {
                    return k ? g : null;
                },
                setRequestHeader: function(a, b) {
                    return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), 
                    this;
                },
                overrideMimeType: function(a) {
                    return null == k && (o.mimeType = a), this;
                },
                statusCode: function(a) {
                    var b;
                    if (a) if (k) y.always(a[y.status]); else for (b in a) t[b] = [ t[b], a[b] ];
                    return this;
                },
                abort: function(a) {
                    var b = a || x;
                    return d && d.abort(b), z(0, b), this;
                }
            };
            if (r.promise(y), o.url = ((b || o.url || Gc.href) + "").replace(Tc, Gc.protocol + "//"), 
            o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || "*").toLowerCase().match(J) || [ "" ], 
            null == o.crossDomain) {
                j = e.createElement("a");
                try {
                    j.href = o.url, j.href = j.href, o.crossDomain = Xc.protocol + "//" + Xc.host != j.protocol + "//" + j.host;
                } catch (b) {
                    o.crossDomain = !0;
                }
            }
            if (o.data && o.processData && "string" != typeof o.data && (o.data = u.param(o.data, o.traditional)), 
            Zc(Uc, o, c, y), k) return y;
            for (m in (l = u.event && o.global) && 0 == u.active++ && u.event.trigger("ajaxStart"), 
            o.type = o.type.toUpperCase(), o.hasContent = !Sc.test(o.type), f = o.url.replace(Pc, ""), 
            o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(Oc, "+")) : (n = o.url.slice(f.length), 
            o.data && (o.processData || "string" == typeof o.data) && (f += (Ic.test(f) ? "&" : "?") + o.data, 
            delete o.data), !1 === o.cache && (f = f.replace(Qc, "$1"), n = (Ic.test(f) ? "&" : "?") + "_=" + Hc++ + n), 
            o.url = f + n), o.ifModified && (u.lastModified[f] && y.setRequestHeader("If-Modified-Since", u.lastModified[f]), 
            u.etag[f] && y.setRequestHeader("If-None-Match", u.etag[f])), (o.data && o.hasContent && !1 !== o.contentType || c.contentType) && y.setRequestHeader("Content-Type", o.contentType), 
            y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Wc + "; q=0.01" : "") : o.accepts["*"]), 
            o.headers) y.setRequestHeader(m, o.headers[m]);
            if (o.beforeSend && (!1 === o.beforeSend.call(p, y, o) || k)) return y.abort();
            if (x = "abort", s.add(o.complete), y.done(o.success), y.fail(o.error), d = Zc(Vc, o, c, y)) {
                if (y.readyState = 1, l && q.trigger("ajaxSend", [ y, o ]), k) return y;
                o.async && 0 < o.timeout && (i = a.setTimeout(function() {
                    y.abort("timeout");
                }, o.timeout));
                try {
                    k = !1, d.send(v, z);
                } catch (b) {
                    if (k) throw b;
                    z(-1, b);
                }
            } else z(-1, "No Transport");
            function z(b, c, e, h) {
                var j, m, n, v, w, x = c;
                k || (k = !0, i && a.clearTimeout(i), d = void 0, g = h || "", y.readyState = 0 < b ? 4 : 0, 
                j = 200 <= b && b < 300 || 304 === b, e && (v = function(a, b, c) {
                    for (var d, e, f, g, h = a.contents, i = a.dataTypes; "*" === i[0]; ) i.shift(), 
                    void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
                    if (d) for (e in h) if (h[e] && h[e].test(d)) {
                        i.unshift(e);
                        break;
                    }
                    if (i[0] in c) f = i[0]; else {
                        for (e in c) {
                            if (!i[0] || a.converters[e + " " + i[0]]) {
                                f = e;
                                break;
                            }
                            g = g || e;
                        }
                        f = f || g;
                    }
                    if (f) return f !== i[0] && i.unshift(f), c[f];
                }(o, y, e)), v = function(a, b, c, d) {
                    var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
                    if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
                    for (f = k.shift(); f; ) if (a.responseFields[f] && (c[a.responseFields[f]] = b), 
                    !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift()) if ("*" === f) f = i; else if ("*" !== i && i !== f) {
                        if (!(g = j[i + " " + f] || j["* " + f])) for (e in j) if ((h = e.split(" "))[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                            !0 === g ? g = j[e] : !0 !== j[e] && (f = h[0], k.unshift(h[1]));
                            break;
                        }
                        if (!0 !== g) if (g && a.throws) b = g(b); else try {
                            b = g(b);
                        } catch (a) {
                            return {
                                state: "parsererror",
                                error: g ? a : "No conversion from " + i + " to " + f
                            };
                        }
                    }
                    return {
                        state: "success",
                        data: b
                    };
                }(o, v, y, j), j ? (o.ifModified && ((w = y.getResponseHeader("Last-Modified")) && (u.lastModified[f] = w), 
                (w = y.getResponseHeader("etag")) && (u.etag[f] = w)), 204 === b || "HEAD" === o.type ? x = "nocontent" : 304 === b ? x = "notmodified" : (x = v.state, 
                m = v.data, j = !(n = v.error))) : (n = x, !b && x || (x = "error", b < 0 && (b = 0))), 
                y.status = b, y.statusText = (c || x) + "", j ? r.resolveWith(p, [ m, x, y ]) : r.rejectWith(p, [ y, x, n ]), 
                y.statusCode(t), t = void 0, l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [ y, o, j ? m : n ]), 
                s.fireWith(p, [ y, x ]), l && (q.trigger("ajaxComplete", [ y, o ]), --u.active || u.event.trigger("ajaxStop")));
            }
            return y;
        },
        getJSON: function(a, b, c) {
            return u.get(a, b, c, "json");
        },
        getScript: function(a, b) {
            return u.get(a, void 0, b, "script");
        }
    }), u.each([ "get", "post" ], function(a, b) {
        u[b] = function(a, c, d, e) {
            return q(c) && (e = e || d, d = c, c = void 0), u.ajax(u.extend({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            }, u.isPlainObject(a) && a));
        };
    }), u._evalUrl = function(a, b) {
        return u.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            converters: {
                "text script": function() {}
            },
            dataFilter: function(a) {
                u.globalEval(a, b);
            }
        });
    }, u.fn.extend({
        wrapAll: function(a) {
            var b;
            return this[0] && (q(a) && (a = a.call(this[0])), b = u(a, this[0].ownerDocument).eq(0).clone(!0), 
            this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                for (var a = this; a.firstElementChild; ) a = a.firstElementChild;
                return a;
            }).append(this)), this;
        },
        wrapInner: function(a) {
            return q(a) ? this.each(function(b) {
                u(this).wrapInner(a.call(this, b));
            }) : this.each(function() {
                var b = u(this), c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a);
            });
        },
        wrap: function(a) {
            var b = q(a);
            return this.each(function(c) {
                u(this).wrapAll(b ? a.call(this, c) : a);
            });
        },
        unwrap: function(a) {
            return this.parent(a).not("body").each(function() {
                u(this).replaceWith(this.childNodes);
            }), this;
        }
    }), u.expr.pseudos.hidden = function(a) {
        return !u.expr.pseudos.visible(a);
    }, u.expr.pseudos.visible = function(a) {
        return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
    }, u.ajaxSettings.xhr = function() {
        try {
            return new a.XMLHttpRequest();
        } catch (b) {}
    };
    var _c = {
        0: 200,
        1223: 204
    }, ad = u.ajaxSettings.xhr();
    p.cors = !!ad && "withCredentials" in ad, p.ajax = ad = !!ad, u.ajaxTransport(function(b) {
        var c, d;
        if (p.cors || ad && !b.crossDomain) return {
            send: function(e, f) {
                var g, h = b.xhr();
                if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields) for (g in b.xhrFields) h[g] = b.xhrFields[g];
                for (g in b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), 
                e) h.setRequestHeader(g, e[g]);
                c = function(a) {
                    return function() {
                        c && (c = d = h.onload = h.onerror = h.onabort = h.ontimeout = h.onreadystatechange = null, 
                        "abort" === a ? h.abort() : "error" === a ? "number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText) : f(_c[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? {
                            binary: h.response
                        } : {
                            text: h.responseText
                        }, h.getAllResponseHeaders()));
                    };
                }, h.onload = c(), d = h.onerror = h.ontimeout = c("error"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function() {
                    4 === h.readyState && a.setTimeout(function() {
                        c && d();
                    });
                }, c = c("abort");
                try {
                    h.send(b.hasContent && b.data || null);
                } catch (e) {
                    if (c) throw e;
                }
            },
            abort: function() {
                c && c();
            }
        };
    }), u.ajaxPrefilter(function(a) {
        a.crossDomain && (a.contents.script = !1);
    }), u.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(a) {
                return u.globalEval(a), a;
            }
        }
    }), u.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET");
    }), u.ajaxTransport("script", function(a) {
        var b, c;
        if (a.crossDomain || a.scriptAttrs) return {
            send: function(d, f) {
                b = u("<script>").attr(a.scriptAttrs || {}).prop({
                    charset: a.scriptCharset,
                    src: a.url
                }).on("load error", c = function(a) {
                    b.remove(), c = null, a && f("error" === a.type ? 404 : 200, a.type);
                }), e.head.appendChild(b[0]);
            },
            abort: function() {
                c && c();
            }
        };
    });
    var bd, cd = [], dd = /(=)\?(?=&|$)|\?\?/;
    u.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = cd.pop() || u.expando + "_" + Hc++;
            return this[a] = !0, a;
        }
    }), u.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = !1 !== b.jsonp && (dd.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && dd.test(b.data) && "data");
        if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = q(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, 
        h ? b[h] = b[h].replace(dd, "$1" + e) : !1 !== b.jsonp && (b.url += (Ic.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), 
        b.converters["script json"] = function() {
            return g || u.error(e + " was not called"), g[0];
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
            g = arguments;
        }, d.always(function() {
            void 0 === f ? u(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, 
            cd.push(e)), g && q(f) && f(g[0]), g = f = void 0;
        }), "script";
    }), p.createHTMLDocument = ((bd = e.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 
    2 === bd.childNodes.length), u.parseHTML = function(a, b, c) {
        return "string" != typeof a ? [] : ("boolean" == typeof b && (c = b, b = !1), b || (p.createHTMLDocument ? ((d = (b = e.implementation.createHTMLDocument("")).createElement("base")).href = e.location.href, 
        b.head.appendChild(d)) : b = e), g = !c && [], (f = C.exec(a)) ? [ b.createElement(f[1]) ] : (f = ub([ a ], b, g), 
        g && g.length && u(g).remove(), u.merge([], f.childNodes)));
        var d, f, g;
    }, u.fn.load = function(a, b, c) {
        var d, e, f, g = this, h = a.indexOf(" ");
        return -1 < h && (d = Ac(a.slice(h)), a = a.slice(0, h)), q(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), 
        0 < g.length && u.ajax({
            url: a,
            type: e || "GET",
            dataType: "html",
            data: b
        }).done(function(a) {
            f = arguments, g.html(d ? u("<div>").append(u.parseHTML(a)).find(d) : a);
        }).always(c && function(a, b) {
            g.each(function() {
                c.apply(this, f || [ a.responseText, b, a ]);
            });
        }), this;
    }, u.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(a, b) {
        u.fn[b] = function(a) {
            return this.on(b, a);
        };
    }), u.expr.pseudos.animated = function(a) {
        return u.grep(u.timers, function(b) {
            return a === b.elem;
        }).length;
    }, u.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j = u.css(a, "position"), k = u(a), l = {};
            "static" === j && (a.style.position = "relative"), h = k.offset(), f = u.css(a, "top"), 
            i = u.css(a, "left"), e = ("absolute" === j || "fixed" === j) && -1 < (f + i).indexOf("auto") ? (g = (d = k.position()).top, 
            d.left) : (g = parseFloat(f) || 0, parseFloat(i) || 0), q(b) && (b = b.call(a, c, u.extend({}, h))), 
            null != b.top && (l.top = b.top - h.top + g), null != b.left && (l.left = b.left - h.left + e), 
            "using" in b ? b.using.call(a, l) : k.css(l);
        }
    }, u.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(b) {
                u.offset.setOffset(this, a, b);
            });
            var b, c, d = this[0];
            return d ? d.getClientRects().length ? (b = d.getBoundingClientRect(), c = d.ownerDocument.defaultView, 
            {
                top: b.top + c.pageYOffset,
                left: b.left + c.pageXOffset
            }) : {
                top: 0,
                left: 0
            } : void 0;
        },
        position: function() {
            if (this[0]) {
                var a, b, c, d = this[0], e = {
                    top: 0,
                    left: 0
                };
                if ("fixed" === u.css(d, "position")) b = d.getBoundingClientRect(); else {
                    for (b = this.offset(), c = d.ownerDocument, a = d.offsetParent || c.documentElement; a && (a === c.body || a === c.documentElement) && "static" === u.css(a, "position"); ) a = a.parentNode;
                    a && a !== d && 1 === a.nodeType && ((e = u(a).offset()).top += u.css(a, "borderTopWidth", !0), 
                    e.left += u.css(a, "borderLeftWidth", !0));
                }
                return {
                    top: b.top - e.top - u.css(d, "marginTop", !0),
                    left: b.left - e.left - u.css(d, "marginLeft", !0)
                };
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var a = this.offsetParent; a && "static" === u.css(a, "position"); ) a = a.offsetParent;
                return a || db;
            });
        }
    }), u.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var d = "pageYOffset" === b;
        u.fn[a] = function(e) {
            return Q(this, function(a, e, f) {
                var g;
                if (c(a) ? g = a : 9 === a.nodeType && (g = a.defaultView), void 0 === f) return g ? g[b] : a[e];
                g ? g.scrollTo(d ? g.pageXOffset : f, d ? f : g.pageYOffset) : a[e] = f;
            }, a, e, arguments.length);
        };
    }), u.each([ "top", "left" ], function(a, b) {
        u.cssHooks[b] = Yb(p.pixelPosition, function(a, c) {
            if (c) return c = Xb(a, b), Ub.test(c) ? u(a).position()[b] + "px" : c;
        });
    }), u.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        u.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(d, e) {
            u.fn[e] = function(f, g) {
                var h = arguments.length && (d || "boolean" != typeof f), i = d || (!0 === f || !0 === g ? "margin" : "border");
                return Q(this, function(b, d, f) {
                    var g;
                    return c(b) ? 0 === e.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (g = b.documentElement, 
                    Math.max(b.body["scroll" + a], g["scroll" + a], b.body["offset" + a], g["offset" + a], g["client" + a])) : void 0 === f ? u.css(b, d, i) : u.style(b, d, f, i);
                }, b, h ? f : void 0, h);
            };
        });
    }), u.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(a, b) {
        u.fn[b] = function(a, c) {
            return 0 < arguments.length ? this.on(b, null, a, c) : this.trigger(b);
        };
    }), u.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a);
        }
    }), u.fn.extend({
        bind: function(a, b, c) {
            return this.on(a, null, b, c);
        },
        unbind: function(a, b) {
            return this.off(a, null, b);
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d);
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
        }
    }), u.proxy = function(a, b) {
        var c, d, e;
        if ("string" == typeof b && (c = a[b], b = a, a = c), q(a)) return d = g.call(arguments, 2), 
        (e = function() {
            return a.apply(b || this, d.concat(g.call(arguments)));
        }).guid = a.guid = a.guid || u.guid++, e;
    }, u.holdReady = function(a) {
        a ? u.readyWait++ : u.ready(!0);
    }, u.isArray = Array.isArray, u.parseJSON = JSON.parse, u.nodeName = B, u.isFunction = q, 
    u.isWindow = c, u.camelCase = U, u.type = t, u.now = Date.now, u.isNumeric = function(a) {
        var b = u.type(a);
        return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
    }, "function" == typeof define && define.amd && define("jquery", [], function() {
        return u;
    });
    var ed = a.jQuery, fd = a.$;
    return u.noConflict = function(b) {
        return a.$ === u && (a.$ = fd), b && a.jQuery === u && (a.jQuery = ed), u;
    }, b || (a.jQuery = a.$ = u), u;
}), function(a) {
    "function" == typeof define && define.amd ? define([ "jquery" ], a) : a(jQuery);
}(function(a) {
    function b() {
        this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, 
        this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", 
        this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", 
        this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", 
        this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", 
        this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            dayNamesMin: [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        }, this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: !1,
            hideIfNoPrevNext: !1,
            navigationAsDateFormat: !1,
            gotoCurrent: !1,
            changeMonth: !1,
            changeYear: !1,
            yearRange: "c-10:c+10",
            showOtherMonths: !1,
            selectOtherMonths: !1,
            showWeek: !1,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: !0,
            showButtonPanel: !1,
            autoSize: !1,
            disabled: !1
        }, a.extend(this._defaults, this.regional[""]), this.regional.en = a.extend(!0, {}, this.regional[""]), 
        this.regional["en-US"] = a.extend(!0, {}, this.regional.en), this.dpDiv = c(a("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));
    }
    function c(b) {
        var c = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return b.on("mouseout", c, function() {
            a(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && a(this).removeClass("ui-datepicker-prev-hover"), 
            -1 !== this.className.indexOf("ui-datepicker-next") && a(this).removeClass("ui-datepicker-next-hover");
        }).on("mouseover", c, d);
    }
    function d() {
        a.datepicker._isDisabledDatepicker(cb.inline ? cb.dpDiv.parent()[0] : cb.input[0]) || (a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), 
        a(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && a(this).addClass("ui-datepicker-prev-hover"), 
        -1 !== this.className.indexOf("ui-datepicker-next") && a(this).addClass("ui-datepicker-next-hover"));
    }
    function e(b, c) {
        for (var d in a.extend(b, c), c) null == c[d] && (b[d] = c[d]);
        return b;
    }
    function f(a) {
        return function() {
            var b = this.element.val();
            a.apply(this, arguments), this._refresh(), b !== this.element.val() && this._trigger("change");
        };
    }
    a.ui = a.ui || {}, a.ui.version = "1.12.1";
    var g, h, i, j, k, l, m, n, o, p, q, r = 0, s = Array.prototype.slice;
    function t(a, b, c) {
        return [ parseFloat(a[0]) * (o.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (o.test(a[1]) ? c / 100 : 1) ];
    }
    function u(b, c) {
        return parseInt(a.css(b, c), 10) || 0;
    }
    a.cleanData = (q = a.cleanData, function(b) {
        var c, d, e;
        for (e = 0; null != (d = b[e]); e++) try {
            (c = a._data(d, "events")) && c.remove && a(d).triggerHandler("remove");
        } catch (b) {}
        q(b);
    }), a.widget = function(b, c, d) {
        var e, f, g, h = {}, i = b.split(".")[0], j = i + "-" + (b = b.split(".")[1]);
        return d || (d = c, c = a.Widget), a.isArray(d) && (d = a.extend.apply(null, [ {} ].concat(d))), 
        a.expr[":"][j.toLowerCase()] = function(b) {
            return !!a.data(b, j);
        }, a[i] = a[i] || {}, e = a[i][b], f = a[i][b] = function(a, b) {
            return this._createWidget ? void (arguments.length && this._createWidget(a, b)) : new f(a, b);
        }, a.extend(f, e, {
            version: d.version,
            _proto: a.extend({}, d),
            _childConstructors: []
        }), (g = new c()).options = a.widget.extend({}, g.options), a.each(d, function(b, d) {
            return a.isFunction(d) ? void (h[b] = function() {
                var a, b = this._super, c = this._superApply;
                return this._super = e, this._superApply = f, a = d.apply(this, arguments), this._super = b, 
                this._superApply = c, a;
            }) : void (h[b] = d);
            function e() {
                return c.prototype[b].apply(this, arguments);
            }
            function f(a) {
                return c.prototype[b].apply(this, a);
            }
        }), f.prototype = a.widget.extend(g, {
            widgetEventPrefix: e && g.widgetEventPrefix || b
        }, h, {
            constructor: f,
            namespace: i,
            widgetName: b,
            widgetFullName: j
        }), e ? (a.each(e._childConstructors, function(b, c) {
            var d = c.prototype;
            a.widget(d.namespace + "." + d.widgetName, f, c._proto);
        }), delete e._childConstructors) : c._childConstructors.push(f), a.widget.bridge(b, f), 
        f;
    }, a.widget.extend = function(b) {
        for (var c, d, e = s.call(arguments, 1), f = 0, g = e.length; f < g; f++) for (c in e[f]) d = e[f][c], 
        e[f].hasOwnProperty(c) && void 0 !== d && (b[c] = a.isPlainObject(d) ? a.isPlainObject(b[c]) ? a.widget.extend({}, b[c], d) : a.widget.extend({}, d) : d);
        return b;
    }, a.widget.bridge = function(b, c) {
        var d = c.prototype.widgetFullName || b;
        a.fn[b] = function(e) {
            var f = "string" == typeof e, g = s.call(arguments, 1), h = this;
            return f ? this.length || "instance" !== e ? this.each(function() {
                var c, f = a.data(this, d);
                return "instance" === e ? (h = f, !1) : f ? a.isFunction(f[e]) && "_" !== e.charAt(0) ? (c = f[e].apply(f, g)) !== f && void 0 !== c ? (h = c && c.jquery ? h.pushStack(c.get()) : c, 
                !1) : void 0 : a.error("no such method '" + e + "' for " + b + " widget instance") : a.error("cannot call methods on " + b + " prior to initialization; attempted to call method '" + e + "'");
            }) : h = void 0 : (g.length && (e = a.widget.extend.apply(null, [ e ].concat(g))), 
            this.each(function() {
                var b = a.data(this, d);
                b ? (b.option(e || {}), b._init && b._init()) : a.data(this, d, new c(e, this));
            })), h;
        };
    }, a.Widget = function() {}, a.Widget._childConstructors = [], a.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            classes: {},
            disabled: !1,
            create: null
        },
        _createWidget: function(b, c) {
            c = a(c || this.defaultElement || this)[0], this.element = a(c), this.uuid = r++, 
            this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = a(), this.hoverable = a(), 
            this.focusable = a(), this.classesElementLookup = {}, c !== this && (a.data(c, this.widgetFullName, this), 
            this._on(!0, this.element, {
                remove: function(a) {
                    a.target === c && this.destroy();
                }
            }), this.document = a(c.style ? c.ownerDocument : c.document || c), this.window = a(this.document[0].defaultView || this.document[0].parentWindow)), 
            this.options = a.widget.extend({}, this.options, this._getCreateOptions(), b), this._create(), 
            this.options.disabled && this._setOptionDisabled(this.options.disabled), this._trigger("create", null, this._getCreateEventData()), 
            this._init();
        },
        _getCreateOptions: function() {
            return {};
        },
        _getCreateEventData: a.noop,
        _create: a.noop,
        _init: a.noop,
        destroy: function() {
            var b = this;
            this._destroy(), a.each(this.classesElementLookup, function(a, c) {
                b._removeClass(c, a);
            }), this.element.off(this.eventNamespace).removeData(this.widgetFullName), this.widget().off(this.eventNamespace).removeAttr("aria-disabled"), 
            this.bindings.off(this.eventNamespace);
        },
        _destroy: a.noop,
        widget: function() {
            return this.element;
        },
        option: function(b, c) {
            var d, e, f, g = b;
            if (0 === arguments.length) return a.widget.extend({}, this.options);
            if ("string" == typeof b) if (g = {}, b = (d = b.split(".")).shift(), d.length) {
                for (e = g[b] = a.widget.extend({}, this.options[b]), f = 0; d.length - 1 > f; f++) e[d[f]] = e[d[f]] || {}, 
                e = e[d[f]];
                if (b = d.pop(), 1 === arguments.length) return void 0 === e[b] ? null : e[b];
                e[b] = c;
            } else {
                if (1 === arguments.length) return void 0 === this.options[b] ? null : this.options[b];
                g[b] = c;
            }
            return this._setOptions(g), this;
        },
        _setOptions: function(a) {
            var b;
            for (b in a) this._setOption(b, a[b]);
            return this;
        },
        _setOption: function(a, b) {
            return "classes" === a && this._setOptionClasses(b), this.options[a] = b, "disabled" === a && this._setOptionDisabled(b), 
            this;
        },
        _setOptionClasses: function(b) {
            var c, d, e;
            for (c in b) e = this.classesElementLookup[c], b[c] !== this.options.classes[c] && e && e.length && (d = a(e.get()), 
            this._removeClass(e, c), d.addClass(this._classes({
                element: d,
                keys: c,
                classes: b,
                add: !0
            })));
        },
        _setOptionDisabled: function(a) {
            this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!a), 
            a && (this._removeClass(this.hoverable, null, "ui-state-hover"), this._removeClass(this.focusable, null, "ui-state-focus"));
        },
        enable: function() {
            return this._setOptions({
                disabled: !1
            });
        },
        disable: function() {
            return this._setOptions({
                disabled: !0
            });
        },
        _classes: function(b) {
            function c(c, f) {
                var g, h;
                for (h = 0; c.length > h; h++) g = e.classesElementLookup[c[h]] || a(), g = b.add ? a(a.unique(g.get().concat(b.element.get()))) : a(g.not(b.element).get()), 
                e.classesElementLookup[c[h]] = g, d.push(c[h]), f && b.classes[c[h]] && d.push(b.classes[c[h]]);
            }
            var d = [], e = this;
            return b = a.extend({
                element: this.element,
                classes: this.options.classes || {}
            }, b), this._on(b.element, {
                remove: "_untrackClassesElement"
            }), b.keys && c(b.keys.match(/\S+/g) || [], !0), b.extra && c(b.extra.match(/\S+/g) || []), 
            d.join(" ");
        },
        _untrackClassesElement: function(b) {
            var c = this;
            a.each(c.classesElementLookup, function(d, e) {
                -1 !== a.inArray(b.target, e) && (c.classesElementLookup[d] = a(e.not(b.target).get()));
            });
        },
        _removeClass: function(a, b, c) {
            return this._toggleClass(a, b, c, !1);
        },
        _addClass: function(a, b, c) {
            return this._toggleClass(a, b, c, !0);
        },
        _toggleClass: function(a, b, c, d) {
            d = "boolean" == typeof d ? d : c;
            var e = "string" == typeof a || null === a, f = {
                extra: e ? b : c,
                keys: e ? a : b,
                element: e ? this.element : a,
                add: d
            };
            return f.element.toggleClass(this._classes(f), d), this;
        },
        _on: function(b, c, d) {
            var e, f = this;
            "boolean" != typeof b && (d = c, c = b, b = !1), d ? (c = e = a(c), this.bindings = this.bindings.add(c)) : (d = c, 
            c = this.element, e = this.widget()), a.each(d, function(d, g) {
                function h() {
                    return b || !0 !== f.options.disabled && !a(this).hasClass("ui-state-disabled") ? ("string" == typeof g ? f[g] : g).apply(f, arguments) : void 0;
                }
                "string" != typeof g && (h.guid = g.guid = g.guid || h.guid || a.guid++);
                var i = d.match(/^([\w:-]*)\s*(.*)$/), j = i[1] + f.eventNamespace, k = i[2];
                k ? e.on(j, k, h) : c.on(j, h);
            });
        },
        _off: function(b, c) {
            c = (c || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, 
            b.off(c).off(c), this.bindings = a(this.bindings.not(b).get()), this.focusable = a(this.focusable.not(b).get()), 
            this.hoverable = a(this.hoverable.not(b).get());
        },
        _delay: function(a, b) {
            var c = this;
            return setTimeout(function() {
                return ("string" == typeof a ? c[a] : a).apply(c, arguments);
            }, b || 0);
        },
        _hoverable: function(b) {
            this.hoverable = this.hoverable.add(b), this._on(b, {
                mouseenter: function(b) {
                    this._addClass(a(b.currentTarget), null, "ui-state-hover");
                },
                mouseleave: function(b) {
                    this._removeClass(a(b.currentTarget), null, "ui-state-hover");
                }
            });
        },
        _focusable: function(b) {
            this.focusable = this.focusable.add(b), this._on(b, {
                focusin: function(b) {
                    this._addClass(a(b.currentTarget), null, "ui-state-focus");
                },
                focusout: function(b) {
                    this._removeClass(a(b.currentTarget), null, "ui-state-focus");
                }
            });
        },
        _trigger: function(b, c, d) {
            var e, f, g = this.options[b];
            if (d = d || {}, (c = a.Event(c)).type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase(), 
            c.target = this.element[0], f = c.originalEvent) for (e in f) e in c || (c[e] = f[e]);
            return this.element.trigger(c, d), !(a.isFunction(g) && !1 === g.apply(this.element[0], [ c ].concat(d)) || c.isDefaultPrevented());
        }
    }, a.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(b, c) {
        a.Widget.prototype["_" + b] = function(d, e, f) {
            "string" == typeof e && (e = {
                effect: e
            });
            var g, h = e ? !0 === e || "number" == typeof e ? c : e.effect || c : b;
            "number" == typeof (e = e || {}) && (e = {
                duration: e
            }), g = !a.isEmptyObject(e), e.complete = f, e.delay && d.delay(e.delay), g && a.effects && a.effects.effect[h] ? d[b](e) : h !== b && d[h] ? d[h](e.duration, e.easing, f) : d.queue(function(c) {
                a(this)[b](), f && f.call(d[0]), c();
            });
        };
    }), a.widget, i = Math.max, j = Math.abs, k = /left|center|right/, l = /top|center|bottom/, 
    m = /[\+\-]\d+(\.[\d]+)?%?/, n = /^\w+/, o = /%$/, p = a.fn.position, a.position = {
        scrollbarWidth: function() {
            if (void 0 !== h) return h;
            var b, c, d = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), e = d.children()[0];
            return a("body").append(d), b = e.offsetWidth, d.css("overflow", "scroll"), b === (c = e.offsetWidth) && (c = d[0].clientWidth), 
            d.remove(), h = b - c;
        },
        getScrollInfo: function(b) {
            var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = "scroll" === c || "auto" === c && b.width < b.element[0].scrollWidth;
            return {
                width: "scroll" === d || "auto" === d && b.height < b.element[0].scrollHeight ? a.position.scrollbarWidth() : 0,
                height: e ? a.position.scrollbarWidth() : 0
            };
        },
        getWithinInfo: function(b) {
            var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && 9 === c[0].nodeType;
            return {
                element: c,
                isWindow: d,
                isDocument: e,
                offset: !d && !e ? a(b).offset() : {
                    left: 0,
                    top: 0
                },
                scrollLeft: c.scrollLeft(),
                scrollTop: c.scrollTop(),
                width: c.outerWidth(),
                height: c.outerHeight()
            };
        }
    }, a.fn.position = function(b) {
        if (!b || !b.of) return p.apply(this, arguments);
        b = a.extend({}, b);
        var c, d, e, f, g, h, o, q, r = a(b.of), s = a.position.getWithinInfo(b.within), v = a.position.getScrollInfo(s), w = (b.collision || "flip").split(" "), x = {};
        return h = 9 === (q = (o = r)[0]).nodeType ? {
            width: o.width(),
            height: o.height(),
            offset: {
                top: 0,
                left: 0
            }
        } : a.isWindow(q) ? {
            width: o.width(),
            height: o.height(),
            offset: {
                top: o.scrollTop(),
                left: o.scrollLeft()
            }
        } : q.preventDefault ? {
            width: 0,
            height: 0,
            offset: {
                top: q.pageY,
                left: q.pageX
            }
        } : {
            width: o.outerWidth(),
            height: o.outerHeight(),
            offset: o.offset()
        }, r[0].preventDefault && (b.at = "left top"), d = h.width, e = h.height, f = h.offset, 
        g = a.extend({}, f), a.each([ "my", "at" ], function() {
            var a, c, d = (b[this] || "").split(" ");
            1 === d.length && (d = k.test(d[0]) ? d.concat([ "center" ]) : l.test(d[0]) ? [ "center" ].concat(d) : [ "center", "center" ]), 
            d[0] = k.test(d[0]) ? d[0] : "center", d[1] = l.test(d[1]) ? d[1] : "center", a = m.exec(d[0]), 
            c = m.exec(d[1]), x[this] = [ a ? a[0] : 0, c ? c[0] : 0 ], b[this] = [ n.exec(d[0])[0], n.exec(d[1])[0] ];
        }), 1 === w.length && (w[1] = w[0]), "right" === b.at[0] ? g.left += d : "center" === b.at[0] && (g.left += d / 2), 
        "bottom" === b.at[1] ? g.top += e : "center" === b.at[1] && (g.top += e / 2), c = t(x.at, d, e), 
        g.left += c[0], g.top += c[1], this.each(function() {
            var h, k, l = a(this), m = l.outerWidth(), n = l.outerHeight(), o = u(this, "marginLeft"), p = u(this, "marginTop"), q = m + o + u(this, "marginRight") + v.width, y = n + p + u(this, "marginBottom") + v.height, z = a.extend({}, g), A = t(x.my, l.outerWidth(), l.outerHeight());
            "right" === b.my[0] ? z.left -= m : "center" === b.my[0] && (z.left -= m / 2), "bottom" === b.my[1] ? z.top -= n : "center" === b.my[1] && (z.top -= n / 2), 
            z.left += A[0], z.top += A[1], h = {
                marginLeft: o,
                marginTop: p
            }, a.each([ "left", "top" ], function(f, g) {
                a.ui.position[w[f]] && a.ui.position[w[f]][g](z, {
                    targetWidth: d,
                    targetHeight: e,
                    elemWidth: m,
                    elemHeight: n,
                    collisionPosition: h,
                    collisionWidth: q,
                    collisionHeight: y,
                    offset: [ c[0] + A[0], c[1] + A[1] ],
                    my: b.my,
                    at: b.at,
                    within: s,
                    elem: l
                });
            }), b.using && (k = function(a) {
                var c = f.left - z.left, g = c + d - m, h = f.top - z.top, k = h + e - n, o = {
                    target: {
                        element: r,
                        left: f.left,
                        top: f.top,
                        width: d,
                        height: e
                    },
                    element: {
                        element: l,
                        left: z.left,
                        top: z.top,
                        width: m,
                        height: n
                    },
                    horizontal: g < 0 ? "left" : 0 < c ? "right" : "center",
                    vertical: k < 0 ? "top" : 0 < h ? "bottom" : "middle"
                };
                d < m && d > j(c + g) && (o.horizontal = "center"), e < n && e > j(h + k) && (o.vertical = "middle"), 
                o.important = i(j(c), j(g)) > i(j(h), j(k)) ? "horizontal" : "vertical", b.using.call(this, a, o);
            }), l.offset(a.extend(z, {
                using: k
            }));
        });
    }, a.ui.position = {
        fit: {
            left: function(a, b) {
                var c, d = b.within, e = d.isWindow ? d.scrollLeft : d.offset.left, f = d.width, g = a.left - b.collisionPosition.marginLeft, h = e - g, j = g + b.collisionWidth - f - e;
                b.collisionWidth > f ? 0 < h && j <= 0 ? (c = a.left + h + b.collisionWidth - f - e, 
                a.left += h - c) : a.left = 0 < j && h <= 0 ? e : j < h ? e + f - b.collisionWidth : e : 0 < h ? a.left += h : 0 < j ? a.left -= j : a.left = i(a.left - g, a.left);
            },
            top: function(a, b) {
                var c, d = b.within, e = d.isWindow ? d.scrollTop : d.offset.top, f = b.within.height, g = a.top - b.collisionPosition.marginTop, h = e - g, j = g + b.collisionHeight - f - e;
                b.collisionHeight > f ? 0 < h && j <= 0 ? (c = a.top + h + b.collisionHeight - f - e, 
                a.top += h - c) : a.top = 0 < j && h <= 0 ? e : j < h ? e + f - b.collisionHeight : e : 0 < h ? a.top += h : 0 < j ? a.top -= j : a.top = i(a.top - g, a.top);
            }
        },
        flip: {
            left: function(a, b) {
                var c, d, e = b.within, f = e.offset.left + e.scrollLeft, g = e.width, h = e.isWindow ? e.scrollLeft : e.offset.left, i = a.left - b.collisionPosition.marginLeft, k = i - h, l = i + b.collisionWidth - g - h, m = "left" === b.my[0] ? -b.elemWidth : "right" === b.my[0] ? b.elemWidth : 0, n = "left" === b.at[0] ? b.targetWidth : "right" === b.at[0] ? -b.targetWidth : 0, o = -2 * b.offset[0];
                k < 0 ? ((c = a.left + m + n + o + b.collisionWidth - g - f) < 0 || j(k) > c) && (a.left += m + n + o) : 0 < l && (0 < (d = a.left - b.collisionPosition.marginLeft + m + n + o - h) || l > j(d)) && (a.left += m + n + o);
            },
            top: function(a, b) {
                var c, d, e = b.within, f = e.offset.top + e.scrollTop, g = e.height, h = e.isWindow ? e.scrollTop : e.offset.top, i = a.top - b.collisionPosition.marginTop, k = i - h, l = i + b.collisionHeight - g - h, m = "top" === b.my[1] ? -b.elemHeight : "bottom" === b.my[1] ? b.elemHeight : 0, n = "top" === b.at[1] ? b.targetHeight : "bottom" === b.at[1] ? -b.targetHeight : 0, o = -2 * b.offset[1];
                k < 0 ? ((d = a.top + m + n + o + b.collisionHeight - g - f) < 0 || j(k) > d) && (a.top += m + n + o) : 0 < l && (0 < (c = a.top - b.collisionPosition.marginTop + m + n + o - h) || l > j(c)) && (a.top += m + n + o);
            }
        },
        flipfit: {
            left: function() {
                a.ui.position.flip.left.apply(this, arguments), a.ui.position.fit.left.apply(this, arguments);
            },
            top: function() {
                a.ui.position.flip.top.apply(this, arguments), a.ui.position.fit.top.apply(this, arguments);
            }
        }
    }, a.ui.position, a.extend(a.expr[":"], {
        data: a.expr.createPseudo ? a.expr.createPseudo(function(b) {
            return function(c) {
                return !!a.data(c, b);
            };
        }) : function(b, c, d) {
            return !!a.data(b, d[3]);
        }
    }), a.fn.extend({
        disableSelection: (g = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown", 
        function() {
            return this.on(g + ".ui-disableSelection", function(a) {
                a.preventDefault();
            });
        }),
        enableSelection: function() {
            return this.off(".ui-disableSelection");
        }
    });
    var v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q = "ui-effects-", R = "ui-effects-style", S = "ui-effects-animated", T = a;
    function U(b, c, d, e) {
        return a.isPlainObject(b) && (b = (c = b).effect), b = {
            effect: b
        }, null == c && (c = {}), a.isFunction(c) && (e = c, d = null, c = {}), "number" != typeof c && !a.fx.speeds[c] || (e = d, 
        d = c, c = {}), a.isFunction(d) && (e = d, d = null), c && a.extend(b, c), d = d || c.duration, 
        b.duration = a.fx.off ? 0 : "number" == typeof d ? d : d in a.fx.speeds ? a.fx.speeds[d] : a.fx.speeds._default, 
        b.complete = e || c.complete, b;
    }
    function V(b) {
        return !b || "number" == typeof b || a.fx.speeds[b] || "string" == typeof b && !a.effects.effect[b] || a.isFunction(b) || "object" == typeof b && !b.effect;
    }
    function W(a, b) {
        var c = b.outerWidth(), d = b.outerHeight(), e = /^rect\((-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto)\)$/.exec(a) || [ "", 0, c, d, 0 ];
        return {
            top: parseFloat(e[1]) || 0,
            right: "auto" === e[2] ? c : parseFloat(e[2]),
            bottom: "auto" === e[3] ? d : parseFloat(e[3]),
            left: parseFloat(e[4]) || 0
        };
    }
    function X(b) {
        var c, d, e = b.ownerDocument.defaultView ? b.ownerDocument.defaultView.getComputedStyle(b, null) : b.currentStyle, f = {};
        if (e && e.length && e[0] && e[e[0]]) for (d = e.length; d--; ) "string" == typeof e[c = e[d]] && (f[a.camelCase(c)] = e[c]); else for (c in e) "string" == typeof e[c] && (f[c] = e[c]);
        return f;
    }
    function Y(a, b, c) {
        var d = M[b.type] || {};
        return null == a ? c || !b.def ? null : b.def : (a = d.floor ? ~~a : parseFloat(a), 
        isNaN(a) ? b.def : d.mod ? (a + d.mod) % d.mod : a < 0 ? 0 : d.max < a ? d.max : a);
    }
    function Z(a) {
        var b = K(), c = b._rgba = [];
        return a = a.toLowerCase(), P(J, function(d, e) {
            var f, g = e.re.exec(a), h = g && e.parse(g), i = e.space || "rgba";
            return h ? (f = b[i](h), b[L[i].cache] = f[L[i].cache], c = b._rgba = f._rgba, !1) : G;
        }), c.length ? ("0,0,0,0" === c.join() && F.extend(c, H.transparent), b) : H[a];
    }
    function $(a, b, c) {
        return 6 * (c = (c + 1) % 1) < 1 ? a + 6 * (b - a) * c : 2 * c < 1 ? b : 3 * c < 2 ? a + 6 * (b - a) * (2 / 3 - c) : a;
    }
    a.effects = {
        effect: {}
    }, I = /^([\-+])=\s*(\d+\.?\d*)/, J = [ {
        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        parse: function(a) {
            return [ a[1], a[2], a[3], a[4] ];
        }
    }, {
        re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        parse: function(a) {
            return [ 2.55 * a[1], 2.55 * a[2], 2.55 * a[3], a[4] ];
        }
    }, {
        re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
        parse: function(a) {
            return [ parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16) ];
        }
    }, {
        re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
        parse: function(a) {
            return [ parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16) ];
        }
    }, {
        re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
        space: "hsla",
        parse: function(a) {
            return [ a[1], a[2] / 100, a[3] / 100, a[4] ];
        }
    } ], K = (F = T).Color = function(a, b, c, d) {
        return new F.Color.fn.parse(a, b, c, d);
    }, L = {
        rgba: {
            props: {
                red: {
                    idx: 0,
                    type: "byte"
                },
                green: {
                    idx: 1,
                    type: "byte"
                },
                blue: {
                    idx: 2,
                    type: "byte"
                }
            }
        },
        hsla: {
            props: {
                hue: {
                    idx: 0,
                    type: "degrees"
                },
                saturation: {
                    idx: 1,
                    type: "percent"
                },
                lightness: {
                    idx: 2,
                    type: "percent"
                }
            }
        }
    }, M = {
        "byte": {
            floor: !0,
            max: 255
        },
        percent: {
            max: 1
        },
        degrees: {
            mod: 360,
            floor: !0
        }
    }, N = K.support = {}, O = F("<p>")[0], P = F.each, O.style.cssText = "background-color:rgba(1,1,1,.5)", 
    N.rgba = -1 < O.style.backgroundColor.indexOf("rgba"), P(L, function(a, b) {
        b.cache = "_" + a, b.props.alpha = {
            idx: 3,
            type: "percent",
            def: 1
        };
    }), K.fn = F.extend(K.prototype, {
        parse: function(a, b, c, d) {
            if (a === G) return this._rgba = [ null, null, null, null ], this;
            (a.jquery || a.nodeType) && (a = F(a).css(b), b = G);
            var e = this, f = F.type(a), g = this._rgba = [];
            return b !== G && (a = [ a, b, c, d ], f = "array"), "string" === f ? this.parse(Z(a) || H._default) : "array" === f ? (P(L.rgba.props, function(b, c) {
                g[c.idx] = Y(a[c.idx], c);
            }), this) : "object" === f ? (P(L, a instanceof K ? function(b, c) {
                a[c.cache] && (e[c.cache] = a[c.cache].slice());
            } : function(b, c) {
                var d = c.cache;
                P(c.props, function(b, f) {
                    if (!e[d] && c.to) {
                        if ("alpha" === b || null == a[b]) return;
                        e[d] = c.to(e._rgba);
                    }
                    e[d][f.idx] = Y(a[b], f, !0);
                }), e[d] && F.inArray(null, e[d].slice(0, 3)) < 0 && (e[d][3] = 1, c.from && (e._rgba = c.from(e[d])));
            }), this) : G;
        },
        is: function(a) {
            var b = K(a), c = !0, d = this;
            return P(L, function(a, e) {
                var f, g = b[e.cache];
                return g && (f = d[e.cache] || e.to && e.to(d._rgba) || [], P(e.props, function(a, b) {
                    return null != g[b.idx] ? c = g[b.idx] === f[b.idx] : G;
                })), c;
            }), c;
        },
        _space: function() {
            var a = [], b = this;
            return P(L, function(c, d) {
                b[d.cache] && a.push(c);
            }), a.pop();
        },
        transition: function(a, b) {
            var c = K(a), d = c._space(), e = L[d], f = 0 === this.alpha() ? K("transparent") : this, g = f[e.cache] || e.to(f._rgba), h = g.slice();
            return c = c[e.cache], P(e.props, function(a, d) {
                var e = d.idx, f = g[e], i = c[e], j = M[d.type] || {};
                null !== i && (null === f ? h[e] = i : (j.mod && (j.mod / 2 < i - f ? f += j.mod : j.mod / 2 < f - i && (f -= j.mod)), 
                h[e] = Y((i - f) * b + f, d)));
            }), this[d](h);
        },
        blend: function(a) {
            if (1 === this._rgba[3]) return this;
            var b = this._rgba.slice(), c = b.pop(), d = K(a)._rgba;
            return K(F.map(b, function(a, b) {
                return (1 - c) * d[b] + c * a;
            }));
        },
        toRgbaString: function() {
            var a = "rgba(", b = F.map(this._rgba, function(a, b) {
                return null == a ? 2 < b ? 1 : 0 : a;
            });
            return 1 === b[3] && (b.pop(), a = "rgb("), a + b.join() + ")";
        },
        toHslaString: function() {
            var a = "hsla(", b = F.map(this.hsla(), function(a, b) {
                return null == a && (a = 2 < b ? 1 : 0), b && b < 3 && (a = Math.round(100 * a) + "%"), 
                a;
            });
            return 1 === b[3] && (b.pop(), a = "hsl("), a + b.join() + ")";
        },
        toHexString: function(a) {
            var b = this._rgba.slice(), c = b.pop();
            return a && b.push(~~(255 * c)), "#" + F.map(b, function(a) {
                return 1 === (a = (a || 0).toString(16)).length ? "0" + a : a;
            }).join("");
        },
        toString: function() {
            return 0 === this._rgba[3] ? "transparent" : this.toRgbaString();
        }
    }), K.fn.parse.prototype = K.fn, L.hsla.to = function(a) {
        if (null == a[0] || null == a[1] || null == a[2]) return [ null, null, null, a[3] ];
        var b, c, d = a[0] / 255, e = a[1] / 255, f = a[2] / 255, g = a[3], h = Math.max(d, e, f), i = Math.min(d, e, f), j = h - i, k = h + i, l = .5 * k;
        return b = i === h ? 0 : d === h ? 60 * (e - f) / j + 360 : e === h ? 60 * (f - d) / j + 120 : 60 * (d - e) / j + 240, 
        c = 0 == j ? 0 : l <= .5 ? j / k : j / (2 - k), [ Math.round(b) % 360, c, l, null == g ? 1 : g ];
    }, L.hsla.from = function(a) {
        if (null == a[0] || null == a[1] || null == a[2]) return [ null, null, null, a[3] ];
        var b = a[0] / 360, c = a[1], d = a[2], e = a[3], f = d <= .5 ? d * (1 + c) : d + c - d * c, g = 2 * d - f;
        return [ Math.round(255 * $(g, f, b + 1 / 3)), Math.round(255 * $(g, f, b)), Math.round(255 * $(g, f, b - 1 / 3)), e ];
    }, P(L, function(a, b) {
        var c = b.props, d = b.cache, e = b.to, f = b.from;
        K.fn[a] = function(a) {
            if (e && !this[d] && (this[d] = e(this._rgba)), a === G) return this[d].slice();
            var b, g = F.type(a), h = "array" === g || "object" === g ? a : arguments, i = this[d].slice();
            return P(c, function(a, b) {
                var c = h["object" === g ? a : b.idx];
                null == c && (c = i[b.idx]), i[b.idx] = Y(c, b);
            }), f ? ((b = K(f(i)))[d] = i, b) : K(i);
        }, P(c, function(b, c) {
            K.fn[b] || (K.fn[b] = function(d) {
                var e, f = F.type(d), g = "alpha" === b ? this._hsla ? "hsla" : "rgba" : a, h = this[g](), i = h[c.idx];
                return "undefined" === f ? i : ("function" === f && (d = d.call(this, i), f = F.type(d)), 
                null == d && c.empty ? this : ("string" === f && (e = I.exec(d)) && (d = i + parseFloat(e[2]) * ("+" === e[1] ? 1 : -1)), 
                h[c.idx] = d, this[g](h)));
            });
        });
    }), K.hook = function(a) {
        var b = a.split(" ");
        P(b, function(a, b) {
            F.cssHooks[b] = {
                set: function(a, c) {
                    var d, e, f = "";
                    if ("transparent" !== c && ("string" !== F.type(c) || (d = Z(c)))) {
                        if (c = K(d || c), !N.rgba && 1 !== c._rgba[3]) {
                            for (e = "backgroundColor" === b ? a.parentNode : a; ("" === f || "transparent" === f) && e && e.style; ) try {
                                f = F.css(e, "backgroundColor"), e = e.parentNode;
                            } catch (a) {}
                            c = c.blend(f && "transparent" !== f ? f : "_default");
                        }
                        c = c.toRgbaString();
                    }
                    try {
                        a.style[b] = c;
                    } catch (a) {}
                }
            }, F.fx.step[b] = function(a) {
                a.colorInit || (a.start = K(a.elem, b), a.end = K(a.end), a.colorInit = !0), F.cssHooks[b].set(a.elem, a.start.transition(a.end, a.pos));
            };
        });
    }, K.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"), 
    F.cssHooks.borderColor = {
        expand: function(a) {
            var b = {};
            return P([ "Top", "Right", "Bottom", "Left" ], function(c, d) {
                b["border" + d + "Color"] = a;
            }), b;
        }
    }, H = F.Color.names = {
        aqua: "#00ffff",
        black: "#000000",
        blue: "#0000ff",
        fuchsia: "#ff00ff",
        gray: "#808080",
        green: "#008000",
        lime: "#00ff00",
        maroon: "#800000",
        navy: "#000080",
        olive: "#808000",
        purple: "#800080",
        red: "#ff0000",
        silver: "#c0c0c0",
        teal: "#008080",
        white: "#ffffff",
        yellow: "#ffff00",
        transparent: [ null, null, null, 0 ],
        _default: "#ffffff"
    }, D = [ "add", "remove", "toggle" ], E = {
        border: 1,
        borderBottom: 1,
        borderColor: 1,
        borderLeft: 1,
        borderRight: 1,
        borderTop: 1,
        borderWidth: 1,
        margin: 1,
        padding: 1
    }, a.each([ "borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle" ], function(b, c) {
        a.fx.step[c] = function(a) {
            ("none" !== a.end && !a.setAttr || 1 === a.pos && !a.setAttr) && (T.style(a.elem, c, a.end), 
            a.setAttr = !0);
        };
    }), a.fn.addBack || (a.fn.addBack = function(a) {
        return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
    }), a.effects.animateClass = function(b, c, d, e) {
        var f = a.speed(c, d, e);
        return this.queue(function() {
            var c, d = a(this), e = d.attr("class") || "", g = f.children ? d.find("*").addBack() : d;
            g = g.map(function() {
                return {
                    el: a(this),
                    start: X(this)
                };
            }), (c = function() {
                a.each(D, function(a, c) {
                    b[c] && d[c + "Class"](b[c]);
                });
            })(), g = g.map(function() {
                return this.end = X(this.el[0]), this.diff = function(b, c) {
                    var d, e, f = {};
                    for (d in c) e = c[d], b[d] !== e && (E[d] || !a.fx.step[d] && isNaN(parseFloat(e)) || (f[d] = e));
                    return f;
                }(this.start, this.end), this;
            }), d.attr("class", e), g = g.map(function() {
                var b = this, c = a.Deferred(), d = a.extend({}, f, {
                    queue: !1,
                    complete: function() {
                        c.resolve(b);
                    }
                });
                return this.el.animate(this.diff, d), c.promise();
            }), a.when.apply(a, g.get()).done(function() {
                c(), a.each(arguments, function() {
                    var b = this.el;
                    a.each(this.diff, function(a) {
                        b.css(a, "");
                    });
                }), f.complete.call(d[0]);
            });
        });
    }, a.fn.extend({
        addClass: (C = a.fn.addClass, function(b, c, d, e) {
            return c ? a.effects.animateClass.call(this, {
                add: b
            }, c, d, e) : C.apply(this, arguments);
        }),
        removeClass: (B = a.fn.removeClass, function(b, c, d, e) {
            return 1 < arguments.length ? a.effects.animateClass.call(this, {
                remove: b
            }, c, d, e) : B.apply(this, arguments);
        }),
        toggleClass: (A = a.fn.toggleClass, function(b, c, d, e, f) {
            return "boolean" == typeof c || void 0 === c ? d ? a.effects.animateClass.call(this, c ? {
                add: b
            } : {
                remove: b
            }, d, e, f) : A.apply(this, arguments) : a.effects.animateClass.call(this, {
                toggle: b
            }, c, d, e);
        }),
        switchClass: function(b, c, d, e, f) {
            return a.effects.animateClass.call(this, {
                add: c,
                remove: b
            }, d, e, f);
        }
    }), a.expr && a.expr.filters && a.expr.filters.animated && (a.expr.filters.animated = (z = a.expr.filters.animated, 
    function(b) {
        return !!a(b).data(S) || z(b);
    })), !1 !== a.uiBackCompat && a.extend(a.effects, {
        save: function(a, b) {
            for (var c = 0, d = b.length; c < d; c++) null !== b[c] && a.data(Q + b[c], a[0].style[b[c]]);
        },
        restore: function(a, b) {
            for (var c, d = 0, e = b.length; d < e; d++) null !== b[d] && (c = a.data(Q + b[d]), 
            a.css(b[d], c));
        },
        setMode: function(a, b) {
            return "toggle" === b && (b = a.is(":hidden") ? "show" : "hide"), b;
        },
        createWrapper: function(b) {
            if (b.parent().is(".ui-effects-wrapper")) return b.parent();
            var c = {
                width: b.outerWidth(!0),
                height: b.outerHeight(!0),
                "float": b.css("float")
            }, d = a("<div></div>").addClass("ui-effects-wrapper").css({
                fontSize: "100%",
                background: "transparent",
                border: "none",
                margin: 0,
                padding: 0
            }), e = {
                width: b.width(),
                height: b.height()
            }, f = document.activeElement;
            try {
                f.id;
            } catch (d) {
                f = document.body;
            }
            return b.wrap(d), b[0] !== f && !a.contains(b[0], f) || a(f).trigger("focus"), d = b.parent(), 
            "static" === b.css("position") ? (d.css({
                position: "relative"
            }), b.css({
                position: "relative"
            })) : (a.extend(c, {
                position: b.css("position"),
                zIndex: b.css("z-index")
            }), a.each([ "top", "left", "bottom", "right" ], function(a, d) {
                c[d] = b.css(d), isNaN(parseInt(c[d], 10)) && (c[d] = "auto");
            }), b.css({
                position: "relative",
                top: 0,
                left: 0,
                right: "auto",
                bottom: "auto"
            })), b.css(e), d.css(c).show();
        },
        removeWrapper: function(b) {
            var c = document.activeElement;
            return b.parent().is(".ui-effects-wrapper") && (b.parent().replaceWith(b), b[0] !== c && !a.contains(b[0], c) || a(c).trigger("focus")), 
            b;
        }
    }), a.extend(a.effects, {
        version: "1.12.1",
        define: function(b, c, d) {
            return d || (d = c, c = "effect"), a.effects.effect[b] = d, a.effects.effect[b].mode = c, 
            d;
        },
        scaledDimensions: function(a, b, c) {
            if (0 === b) return {
                height: 0,
                width: 0,
                outerHeight: 0,
                outerWidth: 0
            };
            var d = "horizontal" !== c ? (b || 100) / 100 : 1, e = "vertical" !== c ? (b || 100) / 100 : 1;
            return {
                height: a.height() * e,
                width: a.width() * d,
                outerHeight: a.outerHeight() * e,
                outerWidth: a.outerWidth() * d
            };
        },
        clipToBox: function(a) {
            return {
                width: a.clip.right - a.clip.left,
                height: a.clip.bottom - a.clip.top,
                left: a.clip.left,
                top: a.clip.top
            };
        },
        unshift: function(a, b, c) {
            var d = a.queue();
            1 < b && d.splice.apply(d, [ 1, 0 ].concat(d.splice(b, c))), a.dequeue();
        },
        saveStyle: function(a) {
            a.data(R, a[0].style.cssText);
        },
        restoreStyle: function(a) {
            a[0].style.cssText = a.data(R) || "", a.removeData(R);
        },
        mode: function(a, b) {
            var c = a.is(":hidden");
            return "toggle" === b && (b = c ? "show" : "hide"), (c ? "hide" === b : "show" === b) && (b = "none"), 
            b;
        },
        getBaseline: function(a, b) {
            var c, d;
            switch (a[0]) {
              case "top":
                c = 0;
                break;

              case "middle":
                c = .5;
                break;

              case "bottom":
                c = 1;
                break;

              default:
                c = a[0] / b.height;
            }
            switch (a[1]) {
              case "left":
                d = 0;
                break;

              case "center":
                d = .5;
                break;

              case "right":
                d = 1;
                break;

              default:
                d = a[1] / b.width;
            }
            return {
                x: d,
                y: c
            };
        },
        createPlaceholder: function(b) {
            var c, d = b.css("position"), e = b.position();
            return b.css({
                marginTop: b.css("marginTop"),
                marginBottom: b.css("marginBottom"),
                marginLeft: b.css("marginLeft"),
                marginRight: b.css("marginRight")
            }).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()), /^(static|relative)/.test(d) && (d = "absolute", 
            c = a("<" + b[0].nodeName + ">").insertAfter(b).css({
                display: /^(inline|ruby)/.test(b.css("display")) ? "inline-block" : "block",
                visibility: "hidden",
                marginTop: b.css("marginTop"),
                marginBottom: b.css("marginBottom"),
                marginLeft: b.css("marginLeft"),
                marginRight: b.css("marginRight"),
                "float": b.css("float")
            }).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).addClass("ui-effects-placeholder"), 
            b.data(Q + "placeholder", c)), b.css({
                position: d,
                left: e.left,
                top: e.top
            }), c;
        },
        removePlaceholder: function(a) {
            var b = Q + "placeholder", c = a.data(b);
            c && (c.remove(), a.removeData(b));
        },
        cleanUp: function(b) {
            a.effects.restoreStyle(b), a.effects.removePlaceholder(b);
        },
        setTransition: function(b, c, d, e) {
            return e = e || {}, a.each(c, function(a, c) {
                var f = b.cssUnit(c);
                0 < f[0] && (e[c] = f[0] * d + f[1]);
            }), e;
        }
    }), a.fn.extend({
        effect: function() {
            function b(b) {
                function c() {
                    a.isFunction(i) && i.call(g[0]), a.isFunction(b) && b();
                }
                var g = a(this);
                d.mode = k.shift(), !1 === a.uiBackCompat || f ? "none" === d.mode ? (g[j](), c()) : e.call(g[0], d, function() {
                    g.removeData(S), a.effects.cleanUp(g), "hide" === d.mode && g.hide(), c();
                }) : (g.is(":hidden") ? "hide" === j : "show" === j) ? (g[j](), c()) : e.call(g[0], d, c);
            }
            function c(b) {
                var c = a(this), d = a.effects.mode(c, j) || f;
                c.data(S, !0), k.push(d), f && ("show" === d || d === f && "hide" === d) && c.show(), 
                f && "none" === d || a.effects.saveStyle(c), a.isFunction(b) && b();
            }
            var d = U.apply(this, arguments), e = a.effects.effect[d.effect], f = e.mode, g = d.queue, h = g || "fx", i = d.complete, j = d.mode, k = [];
            return a.fx.off || !e ? j ? this[j](d.duration, i) : this.each(function() {
                i && i.call(this);
            }) : !1 === g ? this.each(c).each(b) : this.queue(h, c).queue(h, b);
        },
        show: (y = a.fn.show, function(a) {
            if (V(a)) return y.apply(this, arguments);
            var b = U.apply(this, arguments);
            return b.mode = "show", this.effect.call(this, b);
        }),
        hide: (x = a.fn.hide, function(a) {
            if (V(a)) return x.apply(this, arguments);
            var b = U.apply(this, arguments);
            return b.mode = "hide", this.effect.call(this, b);
        }),
        toggle: (w = a.fn.toggle, function(a) {
            if (V(a) || "boolean" == typeof a) return w.apply(this, arguments);
            var b = U.apply(this, arguments);
            return b.mode = "toggle", this.effect.call(this, b);
        }),
        cssUnit: function(b) {
            var c = this.css(b), d = [];
            return a.each([ "em", "px", "%", "pt" ], function(a, b) {
                0 < c.indexOf(b) && (d = [ parseFloat(c), b ]);
            }), d;
        },
        cssClip: function(a) {
            return a ? this.css("clip", "rect(" + a.top + "px " + a.right + "px " + a.bottom + "px " + a.left + "px)") : W(this.css("clip"), this);
        },
        transfer: function(b, c) {
            var d = a(this), e = a(b.to), f = "fixed" === e.css("position"), g = a("body"), h = f ? g.scrollTop() : 0, i = f ? g.scrollLeft() : 0, j = e.offset(), k = {
                top: j.top - h,
                left: j.left - i,
                height: e.innerHeight(),
                width: e.innerWidth()
            }, l = d.offset(), m = a("<div class='ui-effects-transfer'></div>").appendTo("body").addClass(b.className).css({
                top: l.top - h,
                left: l.left - i,
                height: d.innerHeight(),
                width: d.innerWidth(),
                position: f ? "fixed" : "absolute"
            }).animate(k, b.duration, b.easing, function() {
                m.remove(), a.isFunction(c) && c();
            });
        }
    }), a.fx.step.clip = function(b) {
        b.clipInit || (b.start = a(b.elem).cssClip(), "string" == typeof b.end && (b.end = W(b.end, b.elem)), 
        b.clipInit = !0), a(b.elem).cssClip({
            top: b.pos * (b.end.top - b.start.top) + b.start.top,
            right: b.pos * (b.end.right - b.start.right) + b.start.right,
            bottom: b.pos * (b.end.bottom - b.start.bottom) + b.start.bottom,
            left: b.pos * (b.end.left - b.start.left) + b.start.left
        });
    }, v = {}, a.each([ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function(a, b) {
        v[b] = function(b) {
            return Math.pow(b, a + 2);
        };
    }), a.extend(v, {
        Sine: function(a) {
            return 1 - Math.cos(a * Math.PI / 2);
        },
        Circ: function(a) {
            return 1 - Math.sqrt(1 - a * a);
        },
        Elastic: function(a) {
            return 0 === a || 1 === a ? a : -Math.pow(2, 8 * (a - 1)) * Math.sin((80 * (a - 1) - 7.5) * Math.PI / 15);
        },
        Back: function(a) {
            return a * a * (3 * a - 2);
        },
        Bounce: function(a) {
            for (var b, c = 4; ((b = Math.pow(2, --c)) - 1) / 11 > a; ) ;
            return 1 / Math.pow(4, 3 - c) - 7.5625 * Math.pow((3 * b - 2) / 22 - a, 2);
        }
    }), a.each(v, function(b, c) {
        a.easing["easeIn" + b] = c, a.easing["easeOut" + b] = function(a) {
            return 1 - c(1 - a);
        }, a.easing["easeInOut" + b] = function(a) {
            return a < .5 ? c(2 * a) / 2 : 1 - c(-2 * a + 2) / 2;
        };
    });
    var _, ab;
    a.effects;
    a.effects.define("blind", "hide", function(b, c) {
        var d = {
            up: [ "bottom", "top" ],
            vertical: [ "bottom", "top" ],
            down: [ "top", "bottom" ],
            left: [ "right", "left" ],
            horizontal: [ "right", "left" ],
            right: [ "left", "right" ]
        }, e = a(this), f = b.direction || "up", g = e.cssClip(), h = {
            clip: a.extend({}, g)
        }, i = a.effects.createPlaceholder(e);
        h.clip[d[f][0]] = h.clip[d[f][1]], "show" === b.mode && (e.cssClip(h.clip), i && i.css(a.effects.clipToBox(h)), 
        h.clip = g), i && i.animate(a.effects.clipToBox(h), b.duration, b.easing), e.animate(h, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), a.effects.define("bounce", function(b, c) {
        var d, e, f, g = a(this), h = b.mode, i = "hide" === h, j = "show" === h, k = b.direction || "up", l = b.distance, m = b.times || 5, n = 2 * m + (j || i ? 1 : 0), o = b.duration / n, p = b.easing, q = "up" === k || "down" === k ? "top" : "left", r = "up" === k || "left" === k, s = 0, t = g.queue().length;
        for (a.effects.createPlaceholder(g), f = g.css(q), l = l || g["top" == q ? "outerHeight" : "outerWidth"]() / 3, 
        j && ((e = {
            opacity: 1
        })[q] = f, g.css("opacity", 0).css(q, r ? 2 * -l : 2 * l).animate(e, o, p)), i && (l /= Math.pow(2, m - 1)), 
        (e = {})[q] = f; s < m; s++) (d = {})[q] = (r ? "-=" : "+=") + l, g.animate(d, o, p).animate(e, o, p), 
        l = i ? 2 * l : l / 2;
        i && ((d = {
            opacity: 0
        })[q] = (r ? "-=" : "+=") + l, g.animate(d, o, p)), g.queue(c), a.effects.unshift(g, t, 1 + n);
    }), a.effects.define("clip", "hide", function(b, c) {
        var d, e = {}, f = a(this), g = b.direction || "vertical", h = "both" === g, i = h || "horizontal" === g, j = h || "vertical" === g;
        d = f.cssClip(), e.clip = {
            top: j ? (d.bottom - d.top) / 2 : d.top,
            right: i ? (d.right - d.left) / 2 : d.right,
            bottom: j ? (d.bottom - d.top) / 2 : d.bottom,
            left: i ? (d.right - d.left) / 2 : d.left
        }, a.effects.createPlaceholder(f), "show" === b.mode && (f.cssClip(e.clip), e.clip = d), 
        f.animate(e, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), a.effects.define("drop", "hide", function(b, c) {
        var d, e = a(this), f = "show" === b.mode, g = b.direction || "left", h = "up" === g || "down" === g ? "top" : "left", i = "up" === g || "left" === g ? "-=" : "+=", j = "+=" == i ? "-=" : "+=", k = {
            opacity: 0
        };
        a.effects.createPlaceholder(e), d = b.distance || e["top" == h ? "outerHeight" : "outerWidth"](!0) / 2, 
        k[h] = i + d, f && (e.css(k), k[h] = j + d, k.opacity = 1), e.animate(k, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), a.effects.define("explode", "hide", function(b, c) {
        function d() {
            r.push(this), r.length === k * l && (m.css({
                visibility: "visible"
            }), a(r).remove(), c());
        }
        var e, f, g, h, i, j, k = b.pieces ? Math.round(Math.sqrt(b.pieces)) : 3, l = k, m = a(this), n = "show" === b.mode, o = m.show().css("visibility", "hidden").offset(), p = Math.ceil(m.outerWidth() / l), q = Math.ceil(m.outerHeight() / k), r = [];
        for (e = 0; e < k; e++) for (h = o.top + e * q, j = e - (k - 1) / 2, f = 0; f < l; f++) g = o.left + f * p, 
        i = f - (l - 1) / 2, m.clone().appendTo("body").wrap("<div></div>").css({
            position: "absolute",
            visibility: "visible",
            left: -f * p,
            top: -e * q
        }).parent().addClass("ui-effects-explode").css({
            position: "absolute",
            overflow: "hidden",
            width: p,
            height: q,
            left: g + (n ? i * p : 0),
            top: h + (n ? j * q : 0),
            opacity: n ? 0 : 1
        }).animate({
            left: g + (n ? 0 : i * p),
            top: h + (n ? 0 : j * q),
            opacity: n ? 1 : 0
        }, b.duration || 500, b.easing, d);
    }), a.effects.define("fade", "toggle", function(b, c) {
        var d = "show" === b.mode;
        a(this).css("opacity", d ? 0 : 1).animate({
            opacity: d ? 1 : 0
        }, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), a.effects.define("fold", "hide", function(b, c) {
        var d = a(this), e = b.mode, f = "show" === e, g = "hide" === e, h = b.size || 15, i = /([0-9]+)%/.exec(h), j = !!b.horizFirst ? [ "right", "bottom" ] : [ "bottom", "right" ], k = b.duration / 2, l = a.effects.createPlaceholder(d), m = d.cssClip(), n = {
            clip: a.extend({}, m)
        }, o = {
            clip: a.extend({}, m)
        }, p = [ m[j[0]], m[j[1]] ], q = d.queue().length;
        i && (h = parseInt(i[1], 10) / 100 * p[g ? 0 : 1]), n.clip[j[0]] = h, o.clip[j[0]] = h, 
        o.clip[j[1]] = 0, f && (d.cssClip(o.clip), l && l.css(a.effects.clipToBox(o)), o.clip = m), 
        d.queue(function(c) {
            l && l.animate(a.effects.clipToBox(n), k, b.easing).animate(a.effects.clipToBox(o), k, b.easing), 
            c();
        }).animate(n, k, b.easing).animate(o, k, b.easing).queue(c), a.effects.unshift(d, q, 4);
    }), a.effects.define("highlight", "show", function(b, c) {
        var d = a(this), e = {
            backgroundColor: d.css("backgroundColor")
        };
        "hide" === b.mode && (e.opacity = 0), a.effects.saveStyle(d), d.css({
            backgroundImage: "none",
            backgroundColor: b.color || "#ffff99"
        }).animate(e, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), a.effects.define("size", function(b, c) {
        var d, e, f, g = a(this), h = [ "fontSize" ], i = [ "borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom" ], j = [ "borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight" ], k = b.mode, l = "effect" !== k, m = b.scale || "both", n = b.origin || [ "middle", "center" ], o = g.css("position"), p = g.position(), q = a.effects.scaledDimensions(g), r = b.from || q, s = b.to || a.effects.scaledDimensions(g, 0);
        a.effects.createPlaceholder(g), "show" === k && (f = r, r = s, s = f), e = {
            from: {
                y: r.height / q.height,
                x: r.width / q.width
            },
            to: {
                y: s.height / q.height,
                x: s.width / q.width
            }
        }, "box" !== m && "both" !== m || (e.from.y !== e.to.y && (r = a.effects.setTransition(g, i, e.from.y, r), 
        s = a.effects.setTransition(g, i, e.to.y, s)), e.from.x !== e.to.x && (r = a.effects.setTransition(g, j, e.from.x, r), 
        s = a.effects.setTransition(g, j, e.to.x, s))), "content" !== m && "both" !== m || e.from.y === e.to.y || (r = a.effects.setTransition(g, h, e.from.y, r), 
        s = a.effects.setTransition(g, h, e.to.y, s)), n && (d = a.effects.getBaseline(n, q), 
        r.top = (q.outerHeight - r.outerHeight) * d.y + p.top, r.left = (q.outerWidth - r.outerWidth) * d.x + p.left, 
        s.top = (q.outerHeight - s.outerHeight) * d.y + p.top, s.left = (q.outerWidth - s.outerWidth) * d.x + p.left), 
        g.css(r), "content" !== m && "both" !== m || (i = i.concat([ "marginTop", "marginBottom" ]).concat(h), 
        j = j.concat([ "marginLeft", "marginRight" ]), g.find("*[width]").each(function() {
            var c = a(this), d = a.effects.scaledDimensions(c), f = {
                height: d.height * e.from.y,
                width: d.width * e.from.x,
                outerHeight: d.outerHeight * e.from.y,
                outerWidth: d.outerWidth * e.from.x
            }, g = {
                height: d.height * e.to.y,
                width: d.width * e.to.x,
                outerHeight: d.height * e.to.y,
                outerWidth: d.width * e.to.x
            };
            e.from.y !== e.to.y && (f = a.effects.setTransition(c, i, e.from.y, f), g = a.effects.setTransition(c, i, e.to.y, g)), 
            e.from.x !== e.to.x && (f = a.effects.setTransition(c, j, e.from.x, f), g = a.effects.setTransition(c, j, e.to.x, g)), 
            l && a.effects.saveStyle(c), c.css(f), c.animate(g, b.duration, b.easing, function() {
                l && a.effects.restoreStyle(c);
            });
        })), g.animate(s, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                var b = g.offset();
                0 === s.opacity && g.css("opacity", r.opacity), l || (g.css("position", "static" === o ? "relative" : o).offset(b), 
                a.effects.saveStyle(g)), c();
            }
        });
    }), a.effects.define("scale", function(b, c) {
        var d = a(this), e = b.mode, f = parseInt(b.percent, 10) || (0 === parseInt(b.percent, 10) || "effect" !== e ? 0 : 100), g = a.extend(!0, {
            from: a.effects.scaledDimensions(d),
            to: a.effects.scaledDimensions(d, f, b.direction || "both"),
            origin: b.origin || [ "middle", "center" ]
        }, b);
        b.fade && (g.from.opacity = 1, g.to.opacity = 0), a.effects.effect.size.call(this, g, c);
    }), a.effects.define("puff", "hide", function(b, c) {
        var d = a.extend(!0, {}, b, {
            fade: !0,
            percent: parseInt(b.percent, 10) || 150
        });
        a.effects.effect.scale.call(this, d, c);
    }), a.effects.define("pulsate", "show", function(b, c) {
        var d = a(this), e = b.mode, f = "show" === e, g = f || "hide" === e, h = 2 * (b.times || 5) + (g ? 1 : 0), i = b.duration / h, j = 0, k = 1, l = d.queue().length;
        for (!f && d.is(":visible") || (d.css("opacity", 0).show(), j = 1); k < h; k++) d.animate({
            opacity: j
        }, i, b.easing), j = 1 - j;
        d.animate({
            opacity: j
        }, i, b.easing), d.queue(c), a.effects.unshift(d, l, 1 + h);
    }), a.effects.define("shake", function(b, c) {
        var d = 1, e = a(this), f = b.direction || "left", g = b.distance || 20, h = b.times || 3, i = 2 * h + 1, j = Math.round(b.duration / i), k = "up" === f || "down" === f ? "top" : "left", l = "up" === f || "left" === f, m = {}, n = {}, o = {}, p = e.queue().length;
        for (a.effects.createPlaceholder(e), m[k] = (l ? "-=" : "+=") + g, n[k] = (l ? "+=" : "-=") + 2 * g, 
        o[k] = (l ? "-=" : "+=") + 2 * g, e.animate(m, j, b.easing); d < h; d++) e.animate(n, j, b.easing).animate(o, j, b.easing);
        e.animate(n, j, b.easing).animate(m, j / 2, b.easing).queue(c), a.effects.unshift(e, p, 1 + i);
    }), a.effects.define("slide", "show", function(b, c) {
        var d, e, f = a(this), g = {
            up: [ "bottom", "top" ],
            down: [ "top", "bottom" ],
            left: [ "right", "left" ],
            right: [ "left", "right" ]
        }, h = b.mode, i = b.direction || "left", j = "up" === i || "down" === i ? "top" : "left", k = "up" === i || "left" === i, l = b.distance || f["top" == j ? "outerHeight" : "outerWidth"](!0), m = {};
        a.effects.createPlaceholder(f), d = f.cssClip(), e = f.position()[j], m[j] = (k ? -1 : 1) * l + e, 
        m.clip = f.cssClip(), m.clip[g[i][1]] = m.clip[g[i][0]], "show" === h && (f.cssClip(m.clip), 
        f.css(j, m[j]), m.clip = d, m[j] = e), f.animate(m, {
            queue: !1,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    }), !1 !== a.uiBackCompat && a.effects.define("transfer", function(b, c) {
        a(this).transfer(b, c);
    }), a.ui.focusable = function(b, c) {
        var d, e, f, g, h, i = b.nodeName.toLowerCase();
        return "area" === i ? (e = (d = b.parentNode).name, !(!b.href || !e || "map" !== d.nodeName.toLowerCase()) && 0 < (f = a("img[usemap='#" + e + "']")).length && f.is(":visible")) : (/^(input|select|textarea|button|object)$/.test(i) ? (g = !b.disabled) && (h = a(b).closest("fieldset")[0]) && (g = !h.disabled) : g = "a" === i && b.href || c, 
        g && a(b).is(":visible") && function(a) {
            for (var b = a.css("visibility"); "inherit" === b; ) b = (a = a.parent()).css("visibility");
            return "hidden" !== b;
        }(a(b)));
    }, a.extend(a.expr[":"], {
        focusable: function(b) {
            return a.ui.focusable(b, null != a.attr(b, "tabindex"));
        }
    }), a.ui.focusable, a.fn.form = function() {
        return "string" == typeof this[0].form ? this.closest("form") : a(this[0].form);
    }, a.ui.formResetMixin = {
        _formResetHandler: function() {
            var b = a(this);
            setTimeout(function() {
                var c = b.data("ui-form-reset-instances");
                a.each(c, function() {
                    this.refresh();
                });
            });
        },
        _bindFormResetHandler: function() {
            if (this.form = this.element.form(), this.form.length) {
                var a = this.form.data("ui-form-reset-instances") || [];
                a.length || this.form.on("reset.ui-form-reset", this._formResetHandler), a.push(this), 
                this.form.data("ui-form-reset-instances", a);
            }
        },
        _unbindFormResetHandler: function() {
            if (this.form.length) {
                var b = this.form.data("ui-form-reset-instances");
                b.splice(a.inArray(this, b), 1), b.length ? this.form.data("ui-form-reset-instances", b) : this.form.removeData("ui-form-reset-instances").off("reset.ui-form-reset");
            }
        }
    }, "1.7" === a.fn.jquery.substring(0, 3) && (a.each([ "Width", "Height" ], function(b, c) {
        function d(b, c, d, f) {
            return a.each(e, function() {
                c -= parseFloat(a.css(b, "padding" + this)) || 0, d && (c -= parseFloat(a.css(b, "border" + this + "Width")) || 0), 
                f && (c -= parseFloat(a.css(b, "margin" + this)) || 0);
            }), c;
        }
        var e = "Width" === c ? [ "Left", "Right" ] : [ "Top", "Bottom" ], f = c.toLowerCase(), g = {
            innerWidth: a.fn.innerWidth,
            innerHeight: a.fn.innerHeight,
            outerWidth: a.fn.outerWidth,
            outerHeight: a.fn.outerHeight
        };
        a.fn["inner" + c] = function(b) {
            return void 0 === b ? g["inner" + c].call(this) : this.each(function() {
                a(this).css(f, d(this, b) + "px");
            });
        }, a.fn["outer" + c] = function(b, e) {
            return "number" != typeof b ? g["outer" + c].call(this, b) : this.each(function() {
                a(this).css(f, d(this, b, !0, e) + "px");
            });
        };
    }), a.fn.addBack = function(a) {
        return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
    }), a.ui.keyCode = {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    }, a.ui.escapeSelector = (ab = /([!"#$%&'()*+,.\/:;<=>?@[\]^`{|}~])/g, function(a) {
        return a.replace(ab, "\\$1");
    }), a.fn.labels = function() {
        var b, c, d, e, f;
        return this[0].labels && this[0].labels.length ? this.pushStack(this[0].labels) : (e = this.eq(0).parents("label"), 
        (d = this.attr("id")) && (f = (b = this.eq(0).parents().last()).add(b.length ? b.siblings() : this.siblings()), 
        c = "label[for='" + a.ui.escapeSelector(d) + "']", e = e.add(f.find(c).addBack(c))), 
        this.pushStack(e));
    }, a.fn.scrollParent = function(b) {
        var c = this.css("position"), d = "absolute" === c, e = b ? /(auto|scroll|hidden)/ : /(auto|scroll)/, f = this.parents().filter(function() {
            var b = a(this);
            return (!d || "static" !== b.css("position")) && e.test(b.css("overflow") + b.css("overflow-y") + b.css("overflow-x"));
        }).eq(0);
        return "fixed" !== c && f.length ? f : a(this[0].ownerDocument || document);
    }, a.extend(a.expr[":"], {
        tabbable: function(b) {
            var c = a.attr(b, "tabindex"), d = null != c;
            return (!d || 0 <= c) && a.ui.focusable(b, d);
        }
    }), a.fn.extend({
        uniqueId: (_ = 0, function() {
            return this.each(function() {
                this.id || (this.id = "ui-id-" + ++_);
            });
        }),
        removeUniqueId: function() {
            return this.each(function() {
                /^ui-id-\d+$/.test(this.id) && a(this).removeAttr("id");
            });
        }
    }), a.widget("ui.accordion", {
        version: "1.12.1",
        options: {
            active: 0,
            animate: {},
            classes: {
                "ui-accordion-header": "ui-corner-top",
                "ui-accordion-header-collapsed": "ui-corner-all",
                "ui-accordion-content": "ui-corner-bottom"
            },
            collapsible: !1,
            event: "click",
            header: "> li > :first-child, > :not(li):even",
            heightStyle: "auto",
            icons: {
                activeHeader: "ui-icon-triangle-1-s",
                header: "ui-icon-triangle-1-e"
            },
            activate: null,
            beforeActivate: null
        },
        hideProps: {
            borderTopWidth: "hide",
            borderBottomWidth: "hide",
            paddingTop: "hide",
            paddingBottom: "hide",
            height: "hide"
        },
        showProps: {
            borderTopWidth: "show",
            borderBottomWidth: "show",
            paddingTop: "show",
            paddingBottom: "show",
            height: "show"
        },
        _create: function() {
            var b = this.options;
            this.prevShow = this.prevHide = a(), this._addClass("ui-accordion", "ui-widget ui-helper-reset"), 
            this.element.attr("role", "tablist"), b.collapsible || !1 !== b.active && null != b.active || (b.active = 0), 
            this._processPanels(), b.active < 0 && (b.active += this.headers.length), this._refresh();
        },
        _getCreateEventData: function() {
            return {
                header: this.active,
                panel: this.active.length ? this.active.next() : a()
            };
        },
        _createIcons: function() {
            var b, c, d = this.options.icons;
            d && (b = a("<span>"), this._addClass(b, "ui-accordion-header-icon", "ui-icon " + d.header), 
            b.prependTo(this.headers), c = this.active.children(".ui-accordion-header-icon"), 
            this._removeClass(c, d.header)._addClass(c, null, d.activeHeader)._addClass(this.headers, "ui-accordion-icons"));
        },
        _destroyIcons: function() {
            this._removeClass(this.headers, "ui-accordion-icons"), this.headers.children(".ui-accordion-header-icon").remove();
        },
        _destroy: function() {
            var a;
            this.element.removeAttr("role"), this.headers.removeAttr("role aria-expanded aria-selected aria-controls tabIndex").removeUniqueId(), 
            this._destroyIcons(), a = this.headers.next().css("display", "").removeAttr("role aria-hidden aria-labelledby").removeUniqueId(), 
            "content" !== this.options.heightStyle && a.css("height", "");
        },
        _setOption: function(a, b) {
            return "active" === a ? void this._activate(b) : ("event" === a && (this.options.event && this._off(this.headers, this.options.event), 
            this._setupEvents(b)), this._super(a, b), "collapsible" !== a || b || !1 !== this.options.active || this._activate(0), 
            void ("icons" === a && (this._destroyIcons(), b && this._createIcons())));
        },
        _setOptionDisabled: function(a) {
            this._super(a), this.element.attr("aria-disabled", a), this._toggleClass(null, "ui-state-disabled", !!a), 
            this._toggleClass(this.headers.add(this.headers.next()), null, "ui-state-disabled", !!a);
        },
        _keydown: function(b) {
            if (!b.altKey && !b.ctrlKey) {
                var c = a.ui.keyCode, d = this.headers.length, e = this.headers.index(b.target), f = !1;
                switch (b.keyCode) {
                  case c.RIGHT:
                  case c.DOWN:
                    f = this.headers[(e + 1) % d];
                    break;

                  case c.LEFT:
                  case c.UP:
                    f = this.headers[(e - 1 + d) % d];
                    break;

                  case c.SPACE:
                  case c.ENTER:
                    this._eventHandler(b);
                    break;

                  case c.HOME:
                    f = this.headers[0];
                    break;

                  case c.END:
                    f = this.headers[d - 1];
                }
                f && (a(b.target).attr("tabIndex", -1), a(f).attr("tabIndex", 0), a(f).trigger("focus"), 
                b.preventDefault());
            }
        },
        _panelKeyDown: function(b) {
            b.keyCode === a.ui.keyCode.UP && b.ctrlKey && a(b.currentTarget).prev().trigger("focus");
        },
        refresh: function() {
            var b = this.options;
            this._processPanels(), !1 === b.active && !0 === b.collapsible || !this.headers.length ? (b.active = !1, 
            this.active = a()) : !1 === b.active ? this._activate(0) : this.active.length && !a.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (b.active = !1, 
            this.active = a()) : this._activate(Math.max(0, b.active - 1)) : b.active = this.headers.index(this.active), 
            this._destroyIcons(), this._refresh();
        },
        _processPanels: function() {
            var a = this.headers, b = this.panels;
            this.headers = this.element.find(this.options.header), this._addClass(this.headers, "ui-accordion-header ui-accordion-header-collapsed", "ui-state-default"), 
            this.panels = this.headers.next().filter(":not(.ui-accordion-content-active)").hide(), 
            this._addClass(this.panels, "ui-accordion-content", "ui-helper-reset ui-widget-content"), 
            b && (this._off(a.not(this.headers)), this._off(b.not(this.panels)));
        },
        _refresh: function() {
            var b, c = this.options, d = c.heightStyle, e = this.element.parent();
            this.active = this._findActive(c.active), this._addClass(this.active, "ui-accordion-header-active", "ui-state-active")._removeClass(this.active, "ui-accordion-header-collapsed"), 
            this._addClass(this.active.next(), "ui-accordion-content-active"), this.active.next().show(), 
            this.headers.attr("role", "tab").each(function() {
                var b = a(this), c = b.uniqueId().attr("id"), d = b.next(), e = d.uniqueId().attr("id");
                b.attr("aria-controls", e), d.attr("aria-labelledby", c);
            }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
                "aria-selected": "false",
                "aria-expanded": "false",
                tabIndex: -1
            }).next().attr({
                "aria-hidden": "true"
            }).hide(), this.active.length ? this.active.attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            }).next().attr({
                "aria-hidden": "false"
            }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(c.event), 
            "fill" === d ? (b = e.height(), this.element.siblings(":visible").each(function() {
                var c = a(this), d = c.css("position");
                "absolute" !== d && "fixed" !== d && (b -= c.outerHeight(!0));
            }), this.headers.each(function() {
                b -= a(this).outerHeight(!0);
            }), this.headers.next().each(function() {
                a(this).height(Math.max(0, b - a(this).innerHeight() + a(this).height()));
            }).css("overflow", "auto")) : "auto" === d && (b = 0, this.headers.next().each(function() {
                var c = a(this).is(":visible");
                c || a(this).show(), b = Math.max(b, a(this).css("height", "").height()), c || a(this).hide();
            }).height(b));
        },
        _activate: function(b) {
            var c = this._findActive(b)[0];
            c !== this.active[0] && (c = c || this.active[0], this._eventHandler({
                target: c,
                currentTarget: c,
                preventDefault: a.noop
            }));
        },
        _findActive: function(b) {
            return "number" == typeof b ? this.headers.eq(b) : a();
        },
        _setupEvents: function(b) {
            var c = {
                keydown: "_keydown"
            };
            b && a.each(b.split(" "), function(a, b) {
                c[b] = "_eventHandler";
            }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, c), 
            this._on(this.headers.next(), {
                keydown: "_panelKeyDown"
            }), this._hoverable(this.headers), this._focusable(this.headers);
        },
        _eventHandler: function(b) {
            var c, d, e = this.options, f = this.active, g = a(b.currentTarget), h = g[0] === f[0], i = h && e.collapsible, j = i ? a() : g.next(), k = f.next(), l = {
                oldHeader: f,
                oldPanel: k,
                newHeader: i ? a() : g,
                newPanel: j
            };
            b.preventDefault(), h && !e.collapsible || !1 === this._trigger("beforeActivate", b, l) || (e.active = !i && this.headers.index(g), 
            this.active = h ? a() : g, this._toggle(l), this._removeClass(f, "ui-accordion-header-active", "ui-state-active"), 
            e.icons && (c = f.children(".ui-accordion-header-icon"), this._removeClass(c, null, e.icons.activeHeader)._addClass(c, null, e.icons.header)), 
            h || (this._removeClass(g, "ui-accordion-header-collapsed")._addClass(g, "ui-accordion-header-active", "ui-state-active"), 
            e.icons && (d = g.children(".ui-accordion-header-icon"), this._removeClass(d, null, e.icons.header)._addClass(d, null, e.icons.activeHeader)), 
            this._addClass(g.next(), "ui-accordion-content-active")));
        },
        _toggle: function(b) {
            var c = b.newPanel, d = this.prevShow.length ? this.prevShow : b.oldPanel;
            this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = c, this.prevHide = d, 
            this.options.animate ? this._animate(c, d, b) : (d.hide(), c.show(), this._toggleComplete(b)), 
            d.attr({
                "aria-hidden": "true"
            }), d.prev().attr({
                "aria-selected": "false",
                "aria-expanded": "false"
            }), c.length && d.length ? d.prev().attr({
                tabIndex: -1,
                "aria-expanded": "false"
            }) : c.length && this.headers.filter(function() {
                return 0 === parseInt(a(this).attr("tabIndex"), 10);
            }).attr("tabIndex", -1), c.attr("aria-hidden", "false").prev().attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            });
        },
        _animate: function(a, b, c) {
            function d() {
                h._toggleComplete(c);
            }
            var e, f, g, h = this, i = 0, j = a.css("box-sizing"), k = a.length && (!b.length || a.index() < b.index()), l = this.options.animate || {}, m = k && l.down || l;
            return "number" == typeof m && (g = m), "string" == typeof m && (f = m), f = f || m.easing || l.easing, 
            g = g || m.duration || l.duration, b.length ? a.length ? (e = a.show().outerHeight(), 
            b.animate(this.hideProps, {
                duration: g,
                easing: f,
                step: function(a, b) {
                    b.now = Math.round(a);
                }
            }), void a.hide().animate(this.showProps, {
                duration: g,
                easing: f,
                complete: d,
                step: function(a, c) {
                    c.now = Math.round(a), "height" !== c.prop ? "content-box" === j && (i += c.now) : "content" !== h.options.heightStyle && (c.now = Math.round(e - b.outerHeight() - i), 
                    i = 0);
                }
            })) : b.animate(this.hideProps, g, f, d) : a.animate(this.showProps, g, f, d);
        },
        _toggleComplete: function(a) {
            var b = a.oldPanel, c = b.prev();
            this._removeClass(b, "ui-accordion-content-active"), this._removeClass(c, "ui-accordion-header-active")._addClass(c, "ui-accordion-header-collapsed"), 
            b.length && (b.parent()[0].className = b.parent()[0].className), this._trigger("activate", null, a);
        }
    }), a.ui.safeActiveElement = function(a) {
        var b;
        try {
            b = a.activeElement;
        } catch (c) {
            b = a.body;
        }
        return (b = b || a.body).nodeName || (b = a.body), b;
    }, a.widget("ui.menu", {
        version: "1.12.1",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {
                submenu: "ui-icon-caret-1-e"
            },
            items: "> *",
            menus: "ul",
            position: {
                my: "left top",
                at: "right top"
            },
            role: "menu",
            blur: null,
            focus: null,
            select: null
        },
        _create: function() {
            this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().attr({
                role: this.options.role,
                tabIndex: 0
            }), this._addClass("ui-menu", "ui-widget ui-widget-content"), this._on({
                "mousedown .ui-menu-item": function(a) {
                    a.preventDefault();
                },
                "click .ui-menu-item": function(b) {
                    var c = a(b.target), d = a(a.ui.safeActiveElement(this.document[0]));
                    !this.mouseHandled && c.not(".ui-state-disabled").length && (this.select(b), b.isPropagationStopped() || (this.mouseHandled = !0), 
                    c.has(".ui-menu").length ? this.expand(b) : !this.element.is(":focus") && d.closest(".ui-menu").length && (this.element.trigger("focus", [ !0 ]), 
                    this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)));
                },
                "mouseenter .ui-menu-item": function(b) {
                    if (!this.previousFilter) {
                        var c = a(b.target).closest(".ui-menu-item"), d = a(b.currentTarget);
                        c[0] === d[0] && (this._removeClass(d.siblings().children(".ui-state-active"), null, "ui-state-active"), 
                        this.focus(b, d));
                    }
                },
                mouseleave: "collapseAll",
                "mouseleave .ui-menu": "collapseAll",
                focus: function(a, b) {
                    var c = this.active || this.element.find(this.options.items).eq(0);
                    b || this.focus(a, c);
                },
                blur: function(b) {
                    this._delay(function() {
                        a.contains(this.element[0], a.ui.safeActiveElement(this.document[0])) || this.collapseAll(b);
                    });
                },
                keydown: "_keydown"
            }), this.refresh(), this._on(this.document, {
                click: function(a) {
                    this._closeOnDocumentClick(a) && this.collapseAll(a), this.mouseHandled = !1;
                }
            });
        },
        _destroy: function() {
            var b = this.element.find(".ui-menu-item").removeAttr("role aria-disabled").children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled tabIndex").removeUniqueId().show(), 
            b.children().each(function() {
                var b = a(this);
                b.data("ui-menu-submenu-caret") && b.remove();
            });
        },
        _keydown: function(b) {
            var c, d, e, f, g = !0;
            switch (b.keyCode) {
              case a.ui.keyCode.PAGE_UP:
                this.previousPage(b);
                break;

              case a.ui.keyCode.PAGE_DOWN:
                this.nextPage(b);
                break;

              case a.ui.keyCode.HOME:
                this._move("first", "first", b);
                break;

              case a.ui.keyCode.END:
                this._move("last", "last", b);
                break;

              case a.ui.keyCode.UP:
                this.previous(b);
                break;

              case a.ui.keyCode.DOWN:
                this.next(b);
                break;

              case a.ui.keyCode.LEFT:
                this.collapse(b);
                break;

              case a.ui.keyCode.RIGHT:
                this.active && !this.active.is(".ui-state-disabled") && this.expand(b);
                break;

              case a.ui.keyCode.ENTER:
              case a.ui.keyCode.SPACE:
                this._activate(b);
                break;

              case a.ui.keyCode.ESCAPE:
                this.collapse(b);
                break;

              default:
                g = !1, d = this.previousFilter || "", f = !1, e = 96 <= b.keyCode && b.keyCode <= 105 ? "" + (b.keyCode - 96) : String.fromCharCode(b.keyCode), 
                clearTimeout(this.filterTimer), e === d ? f = !0 : e = d + e, c = this._filterMenuItems(e), 
                (c = f && -1 !== c.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : c).length || (e = String.fromCharCode(b.keyCode), 
                c = this._filterMenuItems(e)), c.length ? (this.focus(b, c), this.previousFilter = e, 
                this.filterTimer = this._delay(function() {
                    delete this.previousFilter;
                }, 1e3)) : delete this.previousFilter;
            }
            g && b.preventDefault();
        },
        _activate: function(a) {
            this.active && !this.active.is(".ui-state-disabled") && (this.active.children("[aria-haspopup='true']").length ? this.expand(a) : this.select(a));
        },
        refresh: function() {
            var b, c, d, e, f = this, g = this.options.icons.submenu, h = this.element.find(this.options.menus);
            this._toggleClass("ui-menu-icons", null, !!this.element.find(".ui-icon").length), 
            c = h.filter(":not(.ui-menu)").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function() {
                var b = a(this), c = b.prev(), d = a("<span>").data("ui-menu-submenu-caret", !0);
                f._addClass(d, "ui-menu-icon", "ui-icon " + g), c.attr("aria-haspopup", "true").prepend(d), 
                b.attr("aria-labelledby", c.attr("id"));
            }), this._addClass(c, "ui-menu", "ui-widget ui-widget-content ui-front"), (b = h.add(this.element).find(this.options.items)).not(".ui-menu-item").each(function() {
                var b = a(this);
                f._isDivider(b) && f._addClass(b, "ui-menu-divider", "ui-widget-content");
            }), e = (d = b.not(".ui-menu-item, .ui-menu-divider")).children().not(".ui-menu").uniqueId().attr({
                tabIndex: -1,
                role: this._itemRole()
            }), this._addClass(d, "ui-menu-item")._addClass(e, "ui-menu-item-wrapper"), b.filter(".ui-state-disabled").attr("aria-disabled", "true"), 
            this.active && !a.contains(this.element[0], this.active[0]) && this.blur();
        },
        _itemRole: function() {
            return {
                menu: "menuitem",
                listbox: "option"
            }[this.options.role];
        },
        _setOption: function(a, b) {
            if ("icons" === a) {
                var c = this.element.find(".ui-menu-icon");
                this._removeClass(c, null, this.options.icons.submenu)._addClass(c, null, b.submenu);
            }
            this._super(a, b);
        },
        _setOptionDisabled: function(a) {
            this._super(a), this.element.attr("aria-disabled", a + ""), this._toggleClass(null, "ui-state-disabled", !!a);
        },
        focus: function(a, b) {
            var c, d, e;
            this.blur(a, a && "focus" === a.type), this._scrollIntoView(b), this.active = b.first(), 
            d = this.active.children(".ui-menu-item-wrapper"), this._addClass(d, null, "ui-state-active"), 
            this.options.role && this.element.attr("aria-activedescendant", d.attr("id")), e = this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper"), 
            this._addClass(e, null, "ui-state-active"), a && "keydown" === a.type ? this._close() : this.timer = this._delay(function() {
                this._close();
            }, this.delay), (c = b.children(".ui-menu")).length && a && /^mouse/.test(a.type) && this._startOpening(c), 
            this.activeMenu = b.parent(), this._trigger("focus", a, {
                item: b
            });
        },
        _scrollIntoView: function(b) {
            var c, d, e, f, g, h;
            this._hasScroll() && (c = parseFloat(a.css(this.activeMenu[0], "borderTopWidth")) || 0, 
            d = parseFloat(a.css(this.activeMenu[0], "paddingTop")) || 0, e = b.offset().top - this.activeMenu.offset().top - c - d, 
            f = this.activeMenu.scrollTop(), g = this.activeMenu.height(), h = b.outerHeight(), 
            e < 0 ? this.activeMenu.scrollTop(f + e) : g < e + h && this.activeMenu.scrollTop(f + e - g + h));
        },
        blur: function(a, b) {
            b || clearTimeout(this.timer), this.active && (this._removeClass(this.active.children(".ui-menu-item-wrapper"), null, "ui-state-active"), 
            this._trigger("blur", a, {
                item: this.active
            }), this.active = null);
        },
        _startOpening: function(a) {
            clearTimeout(this.timer), "true" === a.attr("aria-hidden") && (this.timer = this._delay(function() {
                this._close(), this._open(a);
            }, this.delay));
        },
        _open: function(b) {
            var c = a.extend({
                of: this.active
            }, this.options.position);
            clearTimeout(this.timer), this.element.find(".ui-menu").not(b.parents(".ui-menu")).hide().attr("aria-hidden", "true"), 
            b.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(c);
        },
        collapseAll: function(b, c) {
            clearTimeout(this.timer), this.timer = this._delay(function() {
                var d = c ? this.element : a(b && b.target).closest(this.element.find(".ui-menu"));
                d.length || (d = this.element), this._close(d), this.blur(b), this._removeClass(d.find(".ui-state-active"), null, "ui-state-active"), 
                this.activeMenu = d;
            }, this.delay);
        },
        _close: function(a) {
            (a = a || (this.active ? this.active.parent() : this.element)).find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false");
        },
        _closeOnDocumentClick: function(b) {
            return !a(b.target).closest(".ui-menu").length;
        },
        _isDivider: function(a) {
            return !/[^\-\u2014\u2013\s]/.test(a.text());
        },
        collapse: function(a) {
            var b = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            b && b.length && (this._close(), this.focus(a, b));
        },
        expand: function(a) {
            var b = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
            b && b.length && (this._open(b.parent()), this._delay(function() {
                this.focus(a, b);
            }));
        },
        next: function(a) {
            this._move("next", "first", a);
        },
        previous: function(a) {
            this._move("prev", "last", a);
        },
        isFirstItem: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length;
        },
        isLastItem: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length;
        },
        _move: function(a, b, c) {
            var d;
            this.active && (d = "first" === a || "last" === a ? this.active["first" === a ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[a + "All"](".ui-menu-item").eq(0)), 
            d && d.length && this.active || (d = this.activeMenu.find(this.options.items)[b]()), 
            this.focus(c, d);
        },
        nextPage: function(b) {
            var c, d, e;
            return this.active ? void (this.isLastItem() || (this._hasScroll() ? (d = this.active.offset().top, 
            e = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
                return (c = a(this)).offset().top - d - e < 0;
            }), this.focus(b, c)) : this.focus(b, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]()))) : void this.next(b);
        },
        previousPage: function(b) {
            var c, d, e;
            return this.active ? void (this.isFirstItem() || (this._hasScroll() ? (d = this.active.offset().top, 
            e = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
                return 0 < (c = a(this)).offset().top - d + e;
            }), this.focus(b, c)) : this.focus(b, this.activeMenu.find(this.options.items).first()))) : void this.next(b);
        },
        _hasScroll: function() {
            return this.element.outerHeight() < this.element.prop("scrollHeight");
        },
        select: function(b) {
            this.active = this.active || a(b.target).closest(".ui-menu-item");
            var c = {
                item: this.active
            };
            this.active.has(".ui-menu").length || this.collapseAll(b, !0), this._trigger("select", b, c);
        },
        _filterMenuItems: function(b) {
            var c = b.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"), d = RegExp("^" + c, "i");
            return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function() {
                return d.test(a.trim(a(this).children(".ui-menu-item-wrapper").text()));
            });
        }
    }), a.widget("ui.autocomplete", {
        version: "1.12.1",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function() {
            var b, c, d, e = this.element[0].nodeName.toLowerCase(), f = "textarea" === e, g = "input" === e;
            this.isMultiLine = f || !g && this._isContentEditable(this.element), this.valueMethod = this.element[f || g ? "val" : "text"], 
            this.isNewMenu = !0, this._addClass("ui-autocomplete-input"), this.element.attr("autocomplete", "off"), 
            this._on(this.element, {
                keydown: function(e) {
                    if (this.element.prop("readOnly")) c = d = b = !0; else {
                        c = d = b = !1;
                        var f = a.ui.keyCode;
                        switch (e.keyCode) {
                          case f.PAGE_UP:
                            b = !0, this._move("previousPage", e);
                            break;

                          case f.PAGE_DOWN:
                            b = !0, this._move("nextPage", e);
                            break;

                          case f.UP:
                            b = !0, this._keyEvent("previous", e);
                            break;

                          case f.DOWN:
                            b = !0, this._keyEvent("next", e);
                            break;

                          case f.ENTER:
                            this.menu.active && (b = !0, e.preventDefault(), this.menu.select(e));
                            break;

                          case f.TAB:
                            this.menu.active && this.menu.select(e);
                            break;

                          case f.ESCAPE:
                            this.menu.element.is(":visible") && (this.isMultiLine || this._value(this.term), 
                            this.close(e), e.preventDefault());
                            break;

                          default:
                            c = !0, this._searchTimeout(e);
                        }
                    }
                },
                keypress: function(d) {
                    if (b) return b = !1, void (this.isMultiLine && !this.menu.element.is(":visible") || d.preventDefault());
                    if (!c) {
                        var e = a.ui.keyCode;
                        switch (d.keyCode) {
                          case e.PAGE_UP:
                            this._move("previousPage", d);
                            break;

                          case e.PAGE_DOWN:
                            this._move("nextPage", d);
                            break;

                          case e.UP:
                            this._keyEvent("previous", d);
                            break;

                          case e.DOWN:
                            this._keyEvent("next", d);
                        }
                    }
                },
                input: function(a) {
                    return d ? (d = !1, void a.preventDefault()) : void this._searchTimeout(a);
                },
                focus: function() {
                    this.selectedItem = null, this.previous = this._value();
                },
                blur: function(a) {
                    return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching), 
                    this.close(a), void this._change(a));
                }
            }), this._initSource(), this.menu = a("<ul>").appendTo(this._appendTo()).menu({
                role: null
            }).hide().menu("instance"), this._addClass(this.menu.element, "ui-autocomplete", "ui-front"), 
            this._on(this.menu.element, {
                mousedown: function(b) {
                    b.preventDefault(), this.cancelBlur = !0, this._delay(function() {
                        delete this.cancelBlur, this.element[0] !== a.ui.safeActiveElement(this.document[0]) && this.element.trigger("focus");
                    });
                },
                menufocus: function(b, c) {
                    var d, e;
                    return this.isNewMenu && (this.isNewMenu = !1, b.originalEvent && /^mouse/.test(b.originalEvent.type)) ? (this.menu.blur(), 
                    void this.document.one("mousemove", function() {
                        a(b.target).trigger(b.originalEvent);
                    })) : (e = c.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", b, {
                        item: e
                    }) && b.originalEvent && /^key/.test(b.originalEvent.type) && this._value(e.value), 
                    void ((d = c.item.attr("aria-label") || e.value) && a.trim(d).length && (this.liveRegion.children().hide(), 
                    a("<div>").text(d).appendTo(this.liveRegion))));
                },
                menuselect: function(b, c) {
                    var d = c.item.data("ui-autocomplete-item"), e = this.previous;
                    this.element[0] !== a.ui.safeActiveElement(this.document[0]) && (this.element.trigger("focus"), 
                    this.previous = e, this._delay(function() {
                        this.previous = e, this.selectedItem = d;
                    })), !1 !== this._trigger("select", b, {
                        item: d
                    }) && this._value(d.value), this.term = this._value(), this.close(b), this.selectedItem = d;
                }
            }), this.liveRegion = a("<div>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).appendTo(this.document[0].body), this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"), 
            this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete");
                }
            });
        },
        _destroy: function() {
            clearTimeout(this.searching), this.element.removeAttr("autocomplete"), this.menu.element.remove(), 
            this.liveRegion.remove();
        },
        _setOption: function(a, b) {
            this._super(a, b), "source" === a && this._initSource(), "appendTo" === a && this.menu.element.appendTo(this._appendTo()), 
            "disabled" === a && b && this.xhr && this.xhr.abort();
        },
        _isEventTargetInWidget: function(b) {
            var c = this.menu.element[0];
            return b.target === this.element[0] || b.target === c || a.contains(c, b.target);
        },
        _closeOnClickOutside: function(a) {
            this._isEventTargetInWidget(a) || this.close();
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            return (b = b && (b.jquery || b.nodeType ? a(b) : this.document.find(b).eq(0))) && b[0] || (b = this.element.closest(".ui-front, dialog")), 
            b.length || (b = this.document[0].body), b;
        },
        _initSource: function() {
            var b, c, d = this;
            a.isArray(this.options.source) ? (b = this.options.source, this.source = function(c, d) {
                d(a.ui.autocomplete.filter(b, c.term));
            }) : "string" == typeof this.options.source ? (c = this.options.source, this.source = function(b, e) {
                d.xhr && d.xhr.abort(), d.xhr = a.ajax({
                    url: c,
                    data: b,
                    dataType: "json",
                    success: function(a) {
                        e(a);
                    },
                    error: function() {
                        e([]);
                    }
                });
            }) : this.source = this.options.source;
        },
        _searchTimeout: function(a) {
            clearTimeout(this.searching), this.searching = this._delay(function() {
                var b = this.term === this._value(), c = this.menu.element.is(":visible"), d = a.altKey || a.ctrlKey || a.metaKey || a.shiftKey;
                b && (!b || c || d) || (this.selectedItem = null, this.search(null, a));
            }, this.options.delay);
        },
        search: function(a, b) {
            return a = null != a ? a : this._value(), this.term = this._value(), a.length < this.options.minLength ? this.close(b) : !1 !== this._trigger("search", b) ? this._search(a) : void 0;
        },
        _search: function(a) {
            this.pending++, this._addClass("ui-autocomplete-loading"), this.cancelSearch = !1, 
            this.source({
                term: a
            }, this._response());
        },
        _response: function() {
            var b = ++this.requestIndex;
            return a.proxy(function(a) {
                b === this.requestIndex && this.__response(a), this.pending--, this.pending || this._removeClass("ui-autocomplete-loading");
            }, this);
        },
        __response: function(a) {
            a = a && this._normalize(a), this._trigger("response", null, {
                content: a
            }), !this.options.disabled && a && a.length && !this.cancelSearch ? (this._suggest(a), 
            this._trigger("open")) : this._close();
        },
        close: function(a) {
            this.cancelSearch = !0, this._close(a);
        },
        _close: function(a) {
            this._off(this.document, "mousedown"), this.menu.element.is(":visible") && (this.menu.element.hide(), 
            this.menu.blur(), this.isNewMenu = !0, this._trigger("close", a));
        },
        _change: function(a) {
            this.previous !== this._value() && this._trigger("change", a, {
                item: this.selectedItem
            });
        },
        _normalize: function(b) {
            return b.length && b[0].label && b[0].value ? b : a.map(b, function(b) {
                return "string" == typeof b ? {
                    label: b,
                    value: b
                } : a.extend({}, b, {
                    label: b.label || b.value,
                    value: b.value || b.label
                });
            });
        },
        _suggest: function(b) {
            var c = this.menu.element.empty();
            this._renderMenu(c, b), this.isNewMenu = !0, this.menu.refresh(), c.show(), this._resizeMenu(), 
            c.position(a.extend({
                of: this.element
            }, this.options.position)), this.options.autoFocus && this.menu.next(), this._on(this.document, {
                mousedown: "_closeOnClickOutside"
            });
        },
        _resizeMenu: function() {
            var a = this.menu.element;
            a.outerWidth(Math.max(a.width("").outerWidth() + 1, this.element.outerWidth()));
        },
        _renderMenu: function(b, c) {
            var d = this;
            a.each(c, function(a, c) {
                d._renderItemData(b, c);
            });
        },
        _renderItemData: function(a, b) {
            return this._renderItem(a, b).data("ui-autocomplete-item", b);
        },
        _renderItem: function(b, c) {
            return a("<li>").append(a("<div>").text(c.label)).appendTo(b);
        },
        _move: function(a, b) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(a) || this.menu.isLastItem() && /^next/.test(a) ? (this.isMultiLine || this._value(this.term), 
            void this.menu.blur()) : void this.menu[a](b) : void this.search(null, b);
        },
        widget: function() {
            return this.menu.element;
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments);
        },
        _keyEvent: function(a, b) {
            this.isMultiLine && !this.menu.element.is(":visible") || (this._move(a, b), b.preventDefault());
        },
        _isContentEditable: function(a) {
            if (!a.length) return !1;
            var b = a.prop("contentEditable");
            return "inherit" === b ? this._isContentEditable(a.parent()) : "true" === b;
        }
    }), a.extend(a.ui.autocomplete, {
        escapeRegex: function(a) {
            return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
        },
        filter: function(b, c) {
            var d = RegExp(a.ui.autocomplete.escapeRegex(c), "i");
            return a.grep(b, function(a) {
                return d.test(a.label || a.value || a);
            });
        }
    }), a.widget("ui.autocomplete", a.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function(a) {
                    return a + (1 < a ? " results are" : " result is") + " available, use up and down arrow keys to navigate.";
                }
            }
        },
        __response: function(b) {
            var c;
            this._superApply(arguments), this.options.disabled || this.cancelSearch || (c = b && b.length ? this.options.messages.results(b.length) : this.options.messages.noResults, 
            this.liveRegion.children().hide(), a("<div>").text(c).appendTo(this.liveRegion));
        }
    }), a.ui.autocomplete;
    var bb, cb, db = /ui-corner-([a-z]){2,6}/g;
    a.widget("ui.controlgroup", {
        version: "1.12.1",
        defaultElement: "<div>",
        options: {
            direction: "horizontal",
            disabled: null,
            onlyVisible: !0,
            items: {
                button: "input[type=button], input[type=submit], input[type=reset], button, a",
                controlgroupLabel: ".ui-controlgroup-label",
                checkboxradio: "input[type='checkbox'], input[type='radio']",
                selectmenu: "select",
                spinner: ".ui-spinner-input"
            }
        },
        _create: function() {
            this._enhance();
        },
        _enhance: function() {
            this.element.attr("role", "toolbar"), this.refresh();
        },
        _destroy: function() {
            this._callChildMethod("destroy"), this.childWidgets.removeData("ui-controlgroup-data"), 
            this.element.removeAttr("role"), this.options.items.controlgroupLabel && this.element.find(this.options.items.controlgroupLabel).find(".ui-controlgroup-label-contents").contents().unwrap();
        },
        _initWidgets: function() {
            var b = this, c = [];
            a.each(this.options.items, function(d, e) {
                var f, g = {};
                return e ? "controlgroupLabel" === d ? ((f = b.element.find(e)).each(function() {
                    var b = a(this);
                    b.children(".ui-controlgroup-label-contents").length || b.contents().wrapAll("<span class='ui-controlgroup-label-contents'></span>");
                }), b._addClass(f, null, "ui-widget ui-widget-content ui-state-default"), void (c = c.concat(f.get()))) : void (a.fn[d] && (g = b["_" + d + "Options"] ? b["_" + d + "Options"]("middle") : {
                    classes: {}
                }, b.element.find(e).each(function() {
                    var e = a(this), f = e[d]("instance"), h = a.widget.extend({}, g);
                    if ("button" !== d || !e.parent(".ui-spinner").length) {
                        (f = f || e[d]()[d]("instance")) && (h.classes = b._resolveClassesValues(h.classes, f)), 
                        e[d](h);
                        var i = e[d]("widget");
                        a.data(i[0], "ui-controlgroup-data", f || e[d]("instance")), c.push(i[0]);
                    }
                }))) : void 0;
            }), this.childWidgets = a(a.unique(c)), this._addClass(this.childWidgets, "ui-controlgroup-item");
        },
        _callChildMethod: function(b) {
            this.childWidgets.each(function() {
                var c = a(this).data("ui-controlgroup-data");
                c && c[b] && c[b]();
            });
        },
        _updateCornerClass: function(a, b) {
            var c = this._buildSimpleOptions(b, "label").classes.label;
            this._removeClass(a, null, "ui-corner-top ui-corner-bottom ui-corner-left ui-corner-right ui-corner-all"), 
            this._addClass(a, null, c);
        },
        _buildSimpleOptions: function(a, b) {
            var c = "vertical" === this.options.direction, d = {
                classes: {}
            };
            return d.classes[b] = {
                middle: "",
                first: "ui-corner-" + (c ? "top" : "left"),
                last: "ui-corner-" + (c ? "bottom" : "right"),
                only: "ui-corner-all"
            }[a], d;
        },
        _spinnerOptions: function(a) {
            var b = this._buildSimpleOptions(a, "ui-spinner");
            return b.classes["ui-spinner-up"] = "", b.classes["ui-spinner-down"] = "", b;
        },
        _buttonOptions: function(a) {
            return this._buildSimpleOptions(a, "ui-button");
        },
        _checkboxradioOptions: function(a) {
            return this._buildSimpleOptions(a, "ui-checkboxradio-label");
        },
        _selectmenuOptions: function(a) {
            var b = "vertical" === this.options.direction;
            return {
                width: b && "auto",
                classes: {
                    middle: {
                        "ui-selectmenu-button-open": "",
                        "ui-selectmenu-button-closed": ""
                    },
                    first: {
                        "ui-selectmenu-button-open": "ui-corner-" + (b ? "top" : "tl"),
                        "ui-selectmenu-button-closed": "ui-corner-" + (b ? "top" : "left")
                    },
                    last: {
                        "ui-selectmenu-button-open": b ? "" : "ui-corner-tr",
                        "ui-selectmenu-button-closed": "ui-corner-" + (b ? "bottom" : "right")
                    },
                    only: {
                        "ui-selectmenu-button-open": "ui-corner-top",
                        "ui-selectmenu-button-closed": "ui-corner-all"
                    }
                }[a]
            };
        },
        _resolveClassesValues: function(b, c) {
            var d = {};
            return a.each(b, function(e) {
                var f = c.options.classes[e] || "";
                f = a.trim(f.replace(db, "")), d[e] = (f + " " + b[e]).replace(/\s+/g, " ");
            }), d;
        },
        _setOption: function(a, b) {
            return "direction" === a && this._removeClass("ui-controlgroup-" + this.options.direction), 
            this._super(a, b), "disabled" === a ? void this._callChildMethod(b ? "disable" : "enable") : void this.refresh();
        },
        refresh: function() {
            var b, c = this;
            this._addClass("ui-controlgroup ui-controlgroup-" + this.options.direction), "horizontal" === this.options.direction && this._addClass(null, "ui-helper-clearfix"), 
            this._initWidgets(), b = this.childWidgets, this.options.onlyVisible && (b = b.filter(":visible")), 
            b.length && (a.each([ "first", "last" ], function(a, d) {
                var e = b[d]().data("ui-controlgroup-data");
                if (e && c["_" + e.widgetName + "Options"]) {
                    var f = c["_" + e.widgetName + "Options"](1 === b.length ? "only" : d);
                    f.classes = c._resolveClassesValues(f.classes, e), e.element[e.widgetName](f);
                } else c._updateCornerClass(b[d](), d);
            }), this._callChildMethod("refresh"));
        }
    }), a.widget("ui.checkboxradio", [ a.ui.formResetMixin, {
        version: "1.12.1",
        options: {
            disabled: null,
            label: null,
            icon: !0,
            classes: {
                "ui-checkboxradio-label": "ui-corner-all",
                "ui-checkboxradio-icon": "ui-corner-all"
            }
        },
        _getCreateOptions: function() {
            var b, c, d = this, e = this._super() || {};
            return this._readType(), c = this.element.labels(), this.label = a(c[c.length - 1]), 
            this.label.length || a.error("No label found for checkboxradio widget"), this.originalLabel = "", 
            this.label.contents().not(this.element[0]).each(function() {
                d.originalLabel += 3 === this.nodeType ? a(this).text() : this.outerHTML;
            }), this.originalLabel && (e.label = this.originalLabel), null != (b = this.element[0].disabled) && (e.disabled = b), 
            e;
        },
        _create: function() {
            var a = this.element[0].checked;
            this._bindFormResetHandler(), null == this.options.disabled && (this.options.disabled = this.element[0].disabled), 
            this._setOption("disabled", this.options.disabled), this._addClass("ui-checkboxradio", "ui-helper-hidden-accessible"), 
            this._addClass(this.label, "ui-checkboxradio-label", "ui-button ui-widget"), "radio" === this.type && this._addClass(this.label, "ui-checkboxradio-radio-label"), 
            this.options.label && this.options.label !== this.originalLabel ? this._updateLabel() : this.originalLabel && (this.options.label = this.originalLabel), 
            this._enhance(), a && (this._addClass(this.label, "ui-checkboxradio-checked", "ui-state-active"), 
            this.icon && this._addClass(this.icon, null, "ui-state-hover")), this._on({
                change: "_toggleClasses",
                focus: function() {
                    this._addClass(this.label, null, "ui-state-focus ui-visual-focus");
                },
                blur: function() {
                    this._removeClass(this.label, null, "ui-state-focus ui-visual-focus");
                }
            });
        },
        _readType: function() {
            var b = this.element[0].nodeName.toLowerCase();
            this.type = this.element[0].type, "input" === b && /radio|checkbox/.test(this.type) || a.error("Can't create checkboxradio on element.nodeName=" + b + " and element.type=" + this.type);
        },
        _enhance: function() {
            this._updateIcon(this.element[0].checked);
        },
        widget: function() {
            return this.label;
        },
        _getRadioGroup: function() {
            var b = this.element[0].name, c = "input[name='" + a.ui.escapeSelector(b) + "']";
            return b ? (this.form.length ? a(this.form[0].elements).filter(c) : a(c).filter(function() {
                return 0 === a(this).form().length;
            })).not(this.element) : a([]);
        },
        _toggleClasses: function() {
            var b = this.element[0].checked;
            this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", b), 
            this.options.icon && "checkbox" === this.type && this._toggleClass(this.icon, null, "ui-icon-check ui-state-checked", b)._toggleClass(this.icon, null, "ui-icon-blank", !b), 
            "radio" === this.type && this._getRadioGroup().each(function() {
                var b = a(this).checkboxradio("instance");
                b && b._removeClass(b.label, "ui-checkboxradio-checked", "ui-state-active");
            });
        },
        _destroy: function() {
            this._unbindFormResetHandler(), this.icon && (this.icon.remove(), this.iconSpace.remove());
        },
        _setOption: function(a, b) {
            return "label" !== a || b ? (this._super(a, b), "disabled" === a ? (this._toggleClass(this.label, null, "ui-state-disabled", b), 
            void (this.element[0].disabled = b)) : void this.refresh()) : void 0;
        },
        _updateIcon: function(b) {
            var c = "ui-icon ui-icon-background ";
            this.options.icon ? (this.icon || (this.icon = a("<span>"), this.iconSpace = a("<span> </span>"), 
            this._addClass(this.iconSpace, "ui-checkboxradio-icon-space")), "checkbox" === this.type ? (c += b ? "ui-icon-check ui-state-checked" : "ui-icon-blank", 
            this._removeClass(this.icon, null, b ? "ui-icon-blank" : "ui-icon-check")) : c += "ui-icon-blank", 
            this._addClass(this.icon, "ui-checkboxradio-icon", c), b || this._removeClass(this.icon, null, "ui-icon-check ui-state-checked"), 
            this.icon.prependTo(this.label).after(this.iconSpace)) : void 0 !== this.icon && (this.icon.remove(), 
            this.iconSpace.remove(), delete this.icon);
        },
        _updateLabel: function() {
            var a = this.label.contents().not(this.element[0]);
            this.icon && (a = a.not(this.icon[0])), this.iconSpace && (a = a.not(this.iconSpace[0])), 
            a.remove(), this.label.append(this.options.label);
        },
        refresh: function() {
            var a = this.element[0].checked, b = this.element[0].disabled;
            this._updateIcon(a), this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", a), 
            null !== this.options.label && this._updateLabel(), b !== this.options.disabled && this._setOptions({
                disabled: b
            });
        }
    } ]), a.ui.checkboxradio, a.widget("ui.button", {
        version: "1.12.1",
        defaultElement: "<button>",
        options: {
            classes: {
                "ui-button": "ui-corner-all"
            },
            disabled: null,
            icon: null,
            iconPosition: "beginning",
            label: null,
            showLabel: !0
        },
        _getCreateOptions: function() {
            var a, b = this._super() || {};
            return this.isInput = this.element.is("input"), null != (a = this.element[0].disabled) && (b.disabled = a), 
            this.originalLabel = this.isInput ? this.element.val() : this.element.html(), this.originalLabel && (b.label = this.originalLabel), 
            b;
        },
        _create: function() {
            !this.option.showLabel & !this.options.icon && (this.options.showLabel = !0), null == this.options.disabled && (this.options.disabled = this.element[0].disabled || !1), 
            this.hasTitle = !!this.element.attr("title"), this.options.label && this.options.label !== this.originalLabel && (this.isInput ? this.element.val(this.options.label) : this.element.html(this.options.label)), 
            this._addClass("ui-button", "ui-widget"), this._setOption("disabled", this.options.disabled), 
            this._enhance(), this.element.is("a") && this._on({
                keyup: function(b) {
                    b.keyCode === a.ui.keyCode.SPACE && (b.preventDefault(), this.element[0].click ? this.element[0].click() : this.element.trigger("click"));
                }
            });
        },
        _enhance: function() {
            this.element.is("button") || this.element.attr("role", "button"), this.options.icon && (this._updateIcon("icon", this.options.icon), 
            this._updateTooltip());
        },
        _updateTooltip: function() {
            this.title = this.element.attr("title"), this.options.showLabel || this.title || this.element.attr("title", this.options.label);
        },
        _updateIcon: function(b, c) {
            var d = "iconPosition" !== b, e = d ? this.options.iconPosition : c, f = "top" === e || "bottom" === e;
            this.icon ? d && this._removeClass(this.icon, null, this.options.icon) : (this.icon = a("<span>"), 
            this._addClass(this.icon, "ui-button-icon", "ui-icon"), this.options.showLabel || this._addClass("ui-button-icon-only")), 
            d && this._addClass(this.icon, null, c), this._attachIcon(e), f ? (this._addClass(this.icon, null, "ui-widget-icon-block"), 
            this.iconSpace && this.iconSpace.remove()) : (this.iconSpace || (this.iconSpace = a("<span> </span>"), 
            this._addClass(this.iconSpace, "ui-button-icon-space")), this._removeClass(this.icon, null, "ui-wiget-icon-block"), 
            this._attachIconSpace(e));
        },
        _destroy: function() {
            this.element.removeAttr("role"), this.icon && this.icon.remove(), this.iconSpace && this.iconSpace.remove(), 
            this.hasTitle || this.element.removeAttr("title");
        },
        _attachIconSpace: function(a) {
            this.icon[/^(?:end|bottom)/.test(a) ? "before" : "after"](this.iconSpace);
        },
        _attachIcon: function(a) {
            this.element[/^(?:end|bottom)/.test(a) ? "append" : "prepend"](this.icon);
        },
        _setOptions: function(a) {
            var b = void 0 === a.showLabel ? this.options.showLabel : a.showLabel, c = void 0 === a.icon ? this.options.icon : a.icon;
            b || c || (a.showLabel = !0), this._super(a);
        },
        _setOption: function(a, b) {
            "icon" === a && (b ? this._updateIcon(a, b) : this.icon && (this.icon.remove(), 
            this.iconSpace && this.iconSpace.remove())), "iconPosition" === a && this._updateIcon(a, b), 
            "showLabel" === a && (this._toggleClass("ui-button-icon-only", null, !b), this._updateTooltip()), 
            "label" === a && (this.isInput ? this.element.val(b) : (this.element.html(b), this.icon && (this._attachIcon(this.options.iconPosition), 
            this._attachIconSpace(this.options.iconPosition)))), this._super(a, b), "disabled" === a && (this._toggleClass(null, "ui-state-disabled", b), 
            (this.element[0].disabled = b) && this.element.blur());
        },
        refresh: function() {
            var a = this.element.is("input, button") ? this.element[0].disabled : this.element.hasClass("ui-button-disabled");
            a !== this.options.disabled && this._setOptions({
                disabled: a
            }), this._updateTooltip();
        }
    }), !1 !== a.uiBackCompat && (a.widget("ui.button", a.ui.button, {
        options: {
            text: !0,
            icons: {
                primary: null,
                secondary: null
            }
        },
        _create: function() {
            this.options.showLabel && !this.options.text && (this.options.showLabel = this.options.text), 
            !this.options.showLabel && this.options.text && (this.options.text = this.options.showLabel), 
            this.options.icon || !this.options.icons.primary && !this.options.icons.secondary ? this.options.icon && (this.options.icons.primary = this.options.icon) : this.options.icons.primary ? this.options.icon = this.options.icons.primary : (this.options.icon = this.options.icons.secondary, 
            this.options.iconPosition = "end"), this._super();
        },
        _setOption: function(a, b) {
            return "text" === a ? void this._super("showLabel", b) : ("showLabel" === a && (this.options.text = b), 
            "icon" === a && (this.options.icons.primary = b), "icons" === a && (b.primary ? (this._super("icon", b.primary), 
            this._super("iconPosition", "beginning")) : b.secondary && (this._super("icon", b.secondary), 
            this._super("iconPosition", "end"))), void this._superApply(arguments));
        }
    }), a.fn.button = (bb = a.fn.button, function() {
        return !this.length || this.length && "INPUT" !== this[0].tagName || this.length && "INPUT" === this[0].tagName && "checkbox" !== this.attr("type") && "radio" !== this.attr("type") ? bb.apply(this, arguments) : (a.ui.checkboxradio || a.error("Checkboxradio widget missing"), 
        0 === arguments.length ? this.checkboxradio({
            icon: !1
        }) : this.checkboxradio.apply(this, arguments));
    }), a.fn.buttonset = function() {
        return a.ui.controlgroup || a.error("Controlgroup widget missing"), "option" === arguments[0] && "items" === arguments[1] && arguments[2] ? this.controlgroup.apply(this, [ arguments[0], "items.button", arguments[2] ]) : "option" === arguments[0] && "items" === arguments[1] ? this.controlgroup.apply(this, [ arguments[0], "items.button" ]) : ("object" == typeof arguments[0] && arguments[0].items && (arguments[0].items = {
            button: arguments[0].items
        }), this.controlgroup.apply(this, arguments));
    }), a.ui.button, a.extend(a.ui, {
        datepicker: {
            version: "1.12.1"
        }
    }), a.extend(b.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        _widgetDatepicker: function() {
            return this.dpDiv;
        },
        setDefaults: function(a) {
            return e(this._defaults, a || {}), this;
        },
        _attachDatepicker: function(b, c) {
            var d, e, f;
            e = "div" === (d = b.nodeName.toLowerCase()) || "span" === d, b.id || (this.uuid += 1, 
            b.id = "dp" + this.uuid), (f = this._newInst(a(b), e)).settings = a.extend({}, c || {}), 
            "input" === d ? this._connectDatepicker(b, f) : e && this._inlineDatepicker(b, f);
        },
        _newInst: function(b, d) {
            return {
                id: b[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"),
                input: b,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: d,
                dpDiv: d ? c(a("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
            };
        },
        _connectDatepicker: function(b, c) {
            var d = a(b);
            c.append = a([]), c.trigger = a([]), d.hasClass(this.markerClassName) || (this._attachments(d, c), 
            d.addClass(this.markerClassName).on("keydown", this._doKeyDown).on("keypress", this._doKeyPress).on("keyup", this._doKeyUp), 
            this._autoSize(c), a.data(b, "datepicker", c), c.settings.disabled && this._disableDatepicker(b));
        },
        _attachments: function(b, c) {
            var d, e, f, g = this._get(c, "appendText"), h = this._get(c, "isRTL");
            c.append && c.append.remove(), g && (c.append = a("<span class='" + this._appendClass + "'>" + g + "</span>"), 
            b[h ? "before" : "after"](c.append)), b.off("focus", this._showDatepicker), c.trigger && c.trigger.remove(), 
            "focus" !== (d = this._get(c, "showOn")) && "both" !== d || b.on("focus", this._showDatepicker), 
            "button" !== d && "both" !== d || (e = this._get(c, "buttonText"), f = this._get(c, "buttonImage"), 
            c.trigger = a(this._get(c, "buttonImageOnly") ? a("<img/>").addClass(this._triggerClass).attr({
                src: f,
                alt: e,
                title: e
            }) : a("<button type='button'></button>").addClass(this._triggerClass).html(f ? a("<img/>").attr({
                src: f,
                alt: e,
                title: e
            }) : e)), b[h ? "before" : "after"](c.trigger), c.trigger.on("click", function() {
                return a.datepicker._datepickerShowing && a.datepicker._lastInput === b[0] ? a.datepicker._hideDatepicker() : (a.datepicker._datepickerShowing && a.datepicker._lastInput !== b[0] && a.datepicker._hideDatepicker(), 
                a.datepicker._showDatepicker(b[0])), !1;
            }));
        },
        _autoSize: function(a) {
            if (this._get(a, "autoSize") && !a.inline) {
                var b, c, d, e, f = new Date(2009, 11, 20), g = this._get(a, "dateFormat");
                g.match(/[DM]/) && (b = function(a) {
                    for (e = d = c = 0; a.length > e; e++) a[e].length > c && (c = a[e].length, d = e);
                    return d;
                }, f.setMonth(b(this._get(a, g.match(/MM/) ? "monthNames" : "monthNamesShort"))), 
                f.setDate(b(this._get(a, g.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - f.getDay())), 
                a.input.attr("size", this._formatDate(a, f).length);
            }
        },
        _inlineDatepicker: function(b, c) {
            var d = a(b);
            d.hasClass(this.markerClassName) || (d.addClass(this.markerClassName).append(c.dpDiv), 
            a.data(b, "datepicker", c), this._setDate(c, this._getDefaultDate(c), !0), this._updateDatepicker(c), 
            this._updateAlternate(c), c.settings.disabled && this._disableDatepicker(b), c.dpDiv.css("display", "block"));
        },
        _dialogDatepicker: function(b, c, d, f, g) {
            var h, i, j, k, l, m = this._dialogInst;
            return m || (this.uuid += 1, h = "dp" + this.uuid, this._dialogInput = a("<input type='text' id='" + h + "' style='position: absolute; top: -100px; width: 0px;'/>"), 
            this._dialogInput.on("keydown", this._doKeyDown), a("body").append(this._dialogInput), 
            (m = this._dialogInst = this._newInst(this._dialogInput, !1)).settings = {}, a.data(this._dialogInput[0], "datepicker", m)), 
            e(m.settings, f || {}), c = c && c.constructor === Date ? this._formatDate(m, c) : c, 
            this._dialogInput.val(c), this._pos = g ? g.length ? g : [ g.pageX, g.pageY ] : null, 
            this._pos || (i = document.documentElement.clientWidth, j = document.documentElement.clientHeight, 
            k = document.documentElement.scrollLeft || document.body.scrollLeft, l = document.documentElement.scrollTop || document.body.scrollTop, 
            this._pos = [ i / 2 - 100 + k, j / 2 - 150 + l ]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), 
            m.settings.onSelect = d, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), 
            this._showDatepicker(this._dialogInput[0]), a.blockUI && a.blockUI(this.dpDiv), 
            a.data(this._dialogInput[0], "datepicker", m), this;
        },
        _destroyDatepicker: function(b) {
            var c, d = a(b), e = a.data(b, "datepicker");
            d.hasClass(this.markerClassName) && (c = b.nodeName.toLowerCase(), a.removeData(b, "datepicker"), 
            "input" === c ? (e.append.remove(), e.trigger.remove(), d.removeClass(this.markerClassName).off("focus", this._showDatepicker).off("keydown", this._doKeyDown).off("keypress", this._doKeyPress).off("keyup", this._doKeyUp)) : "div" !== c && "span" !== c || d.removeClass(this.markerClassName).empty(), 
            cb === e && (cb = null));
        },
        _enableDatepicker: function(b) {
            var c, d, e = a(b), f = a.data(b, "datepicker");
            e.hasClass(this.markerClassName) && ("input" === (c = b.nodeName.toLowerCase()) ? (b.disabled = !1, 
            f.trigger.filter("button").each(function() {
                this.disabled = !1;
            }).end().filter("img").css({
                opacity: "1.0",
                cursor: ""
            })) : "div" !== c && "span" !== c || ((d = e.children("." + this._inlineClass)).children().removeClass("ui-state-disabled"), 
            d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), 
            this._disabledInputs = a.map(this._disabledInputs, function(a) {
                return a === b ? null : a;
            }));
        },
        _disableDatepicker: function(b) {
            var c, d, e = a(b), f = a.data(b, "datepicker");
            e.hasClass(this.markerClassName) && ("input" === (c = b.nodeName.toLowerCase()) ? (b.disabled = !0, 
            f.trigger.filter("button").each(function() {
                this.disabled = !0;
            }).end().filter("img").css({
                opacity: "0.5",
                cursor: "default"
            })) : "div" !== c && "span" !== c || ((d = e.children("." + this._inlineClass)).children().addClass("ui-state-disabled"), 
            d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), 
            this._disabledInputs = a.map(this._disabledInputs, function(a) {
                return a === b ? null : a;
            }), this._disabledInputs[this._disabledInputs.length] = b);
        },
        _isDisabledDatepicker: function(a) {
            if (!a) return !1;
            for (var b = 0; this._disabledInputs.length > b; b++) if (this._disabledInputs[b] === a) return !0;
            return !1;
        },
        _getInst: function(b) {
            try {
                return a.data(b, "datepicker");
            } catch (b) {
                throw "Missing instance data for this datepicker";
            }
        },
        _optionDatepicker: function(b, c, d) {
            var f, g, h, i, j = this._getInst(b);
            return 2 === arguments.length && "string" == typeof c ? "defaults" === c ? a.extend({}, a.datepicker._defaults) : j ? "all" === c ? a.extend({}, j.settings) : this._get(j, c) : null : (f = c || {}, 
            "string" == typeof c && ((f = {})[c] = d), void (j && (this._curInst === j && this._hideDatepicker(), 
            g = this._getDateDatepicker(b, !0), h = this._getMinMaxDate(j, "min"), i = this._getMinMaxDate(j, "max"), 
            e(j.settings, f), null !== h && void 0 !== f.dateFormat && void 0 === f.minDate && (j.settings.minDate = this._formatDate(j, h)), 
            null !== i && void 0 !== f.dateFormat && void 0 === f.maxDate && (j.settings.maxDate = this._formatDate(j, i)), 
            "disabled" in f && (f.disabled ? this._disableDatepicker(b) : this._enableDatepicker(b)), 
            this._attachments(a(b), j), this._autoSize(j), this._setDate(j, g), this._updateAlternate(j), 
            this._updateDatepicker(j))));
        },
        _changeDatepicker: function(a, b, c) {
            this._optionDatepicker(a, b, c);
        },
        _refreshDatepicker: function(a) {
            var b = this._getInst(a);
            b && this._updateDatepicker(b);
        },
        _setDateDatepicker: function(a, b) {
            var c = this._getInst(a);
            c && (this._setDate(c, b), this._updateDatepicker(c), this._updateAlternate(c));
        },
        _getDateDatepicker: function(a, b) {
            var c = this._getInst(a);
            return c && !c.inline && this._setDateFromField(c, b), c ? this._getDate(c) : null;
        },
        _doKeyDown: function(b) {
            var c, d, e, f = a.datepicker._getInst(b.target), g = !0, h = f.dpDiv.is(".ui-datepicker-rtl");
            if (f._keyEvent = !0, a.datepicker._datepickerShowing) switch (b.keyCode) {
              case 9:
                a.datepicker._hideDatepicker(), g = !1;
                break;

              case 13:
                return (e = a("td." + a.datepicker._dayOverClass + ":not(." + a.datepicker._currentClass + ")", f.dpDiv))[0] && a.datepicker._selectDay(b.target, f.selectedMonth, f.selectedYear, e[0]), 
                (c = a.datepicker._get(f, "onSelect")) ? (d = a.datepicker._formatDate(f), c.apply(f.input ? f.input[0] : null, [ d, f ])) : a.datepicker._hideDatepicker(), 
                !1;

              case 27:
                a.datepicker._hideDatepicker();
                break;

              case 33:
                a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(f, "stepBigMonths") : -a.datepicker._get(f, "stepMonths"), "M");
                break;

              case 34:
                a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(f, "stepBigMonths") : +a.datepicker._get(f, "stepMonths"), "M");
                break;

              case 35:
                (b.ctrlKey || b.metaKey) && a.datepicker._clearDate(b.target), g = b.ctrlKey || b.metaKey;
                break;

              case 36:
                (b.ctrlKey || b.metaKey) && a.datepicker._gotoToday(b.target), g = b.ctrlKey || b.metaKey;
                break;

              case 37:
                (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, h ? 1 : -1, "D"), 
                g = b.ctrlKey || b.metaKey, b.originalEvent.altKey && a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(f, "stepBigMonths") : -a.datepicker._get(f, "stepMonths"), "M");
                break;

              case 38:
                (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, -7, "D"), g = b.ctrlKey || b.metaKey;
                break;

              case 39:
                (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, h ? -1 : 1, "D"), 
                g = b.ctrlKey || b.metaKey, b.originalEvent.altKey && a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(f, "stepBigMonths") : +a.datepicker._get(f, "stepMonths"), "M");
                break;

              case 40:
                (b.ctrlKey || b.metaKey) && a.datepicker._adjustDate(b.target, 7, "D"), g = b.ctrlKey || b.metaKey;
                break;

              default:
                g = !1;
            } else 36 === b.keyCode && b.ctrlKey ? a.datepicker._showDatepicker(this) : g = !1;
            g && (b.preventDefault(), b.stopPropagation());
        },
        _doKeyPress: function(b) {
            var c, d, e = a.datepicker._getInst(b.target);
            return a.datepicker._get(e, "constrainInput") ? (c = a.datepicker._possibleChars(a.datepicker._get(e, "dateFormat")), 
            d = String.fromCharCode(null == b.charCode ? b.keyCode : b.charCode), b.ctrlKey || b.metaKey || d < " " || !c || -1 < c.indexOf(d)) : void 0;
        },
        _doKeyUp: function(b) {
            var c = a.datepicker._getInst(b.target);
            if (c.input.val() !== c.lastVal) try {
                a.datepicker.parseDate(a.datepicker._get(c, "dateFormat"), c.input ? c.input.val() : null, a.datepicker._getFormatConfig(c)) && (a.datepicker._setDateFromField(c), 
                a.datepicker._updateAlternate(c), a.datepicker._updateDatepicker(c));
            } catch (b) {}
            return !0;
        },
        _showDatepicker: function(b) {
            var c, d, f, g, h, i, j;
            "input" !== (b = b.target || b).nodeName.toLowerCase() && (b = a("input", b.parentNode)[0]), 
            a.datepicker._isDisabledDatepicker(b) || a.datepicker._lastInput === b || (c = a.datepicker._getInst(b), 
            a.datepicker._curInst && a.datepicker._curInst !== c && (a.datepicker._curInst.dpDiv.stop(!0, !0), 
            c && a.datepicker._datepickerShowing && a.datepicker._hideDatepicker(a.datepicker._curInst.input[0])), 
            !1 !== (f = (d = a.datepicker._get(c, "beforeShow")) ? d.apply(b, [ b, c ]) : {}) && (e(c.settings, f), 
            c.lastVal = null, a.datepicker._lastInput = b, a.datepicker._setDateFromField(c), 
            a.datepicker._inDialog && (b.value = ""), a.datepicker._pos || (a.datepicker._pos = a.datepicker._findPos(b), 
            a.datepicker._pos[1] += b.offsetHeight), g = !1, a(b).parents().each(function() {
                return !(g |= "fixed" === a(this).css("position"));
            }), h = {
                left: a.datepicker._pos[0],
                top: a.datepicker._pos[1]
            }, a.datepicker._pos = null, c.dpDiv.empty(), c.dpDiv.css({
                position: "absolute",
                display: "block",
                top: "-1000px"
            }), a.datepicker._updateDatepicker(c), h = a.datepicker._checkOffset(c, h, g), c.dpDiv.css({
                position: a.datepicker._inDialog && a.blockUI ? "static" : g ? "fixed" : "absolute",
                display: "none",
                left: h.left + "px",
                top: h.top + "px"
            }), c.inline || (i = a.datepicker._get(c, "showAnim"), j = a.datepicker._get(c, "duration"), 
            c.dpDiv.css("z-index", function(a) {
                for (var b, c; a.length && a[0] !== document; ) {
                    if (("absolute" === (b = a.css("position")) || "relative" === b || "fixed" === b) && (c = parseInt(a.css("zIndex"), 10), 
                    !isNaN(c) && 0 !== c)) return c;
                    a = a.parent();
                }
                return 0;
            }(a(b)) + 1), a.datepicker._datepickerShowing = !0, a.effects && a.effects.effect[i] ? c.dpDiv.show(i, a.datepicker._get(c, "showOptions"), j) : c.dpDiv[i || "show"](i ? j : null), 
            a.datepicker._shouldFocusInput(c) && c.input.trigger("focus"), a.datepicker._curInst = c)));
        },
        _updateDatepicker: function(b) {
            this.maxRows = 4, (cb = b).dpDiv.empty().append(this._generateHTML(b)), this._attachHandlers(b);
            var c, e = this._getNumberOfMonths(b), f = e[1], g = b.dpDiv.find("." + this._dayOverClass + " a");
            0 < g.length && d.apply(g.get(0)), b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), 
            1 < f && b.dpDiv.addClass("ui-datepicker-multi-" + f).css("width", 17 * f + "em"), 
            b.dpDiv[(1 !== e[0] || 1 !== e[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), 
            b.dpDiv[(this._get(b, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), 
            b === a.datepicker._curInst && a.datepicker._datepickerShowing && a.datepicker._shouldFocusInput(b) && b.input.trigger("focus"), 
            b.yearshtml && (c = b.yearshtml, setTimeout(function() {
                c === b.yearshtml && b.yearshtml && b.dpDiv.find("select.ui-datepicker-year:first").replaceWith(b.yearshtml), 
                c = b.yearshtml = null;
            }, 0));
        },
        _shouldFocusInput: function(a) {
            return a.input && a.input.is(":visible") && !a.input.is(":disabled") && !a.input.is(":focus");
        },
        _checkOffset: function(b, c, d) {
            var e = b.dpDiv.outerWidth(), f = b.dpDiv.outerHeight(), g = b.input ? b.input.outerWidth() : 0, h = b.input ? b.input.outerHeight() : 0, i = document.documentElement.clientWidth + (d ? 0 : a(document).scrollLeft()), j = document.documentElement.clientHeight + (d ? 0 : a(document).scrollTop());
            return c.left -= this._get(b, "isRTL") ? e - g : 0, c.left -= d && c.left === b.input.offset().left ? a(document).scrollLeft() : 0, 
            c.top -= d && c.top === b.input.offset().top + h ? a(document).scrollTop() : 0, 
            c.left -= Math.min(c.left, c.left + e > i && e < i ? Math.abs(c.left + e - i) : 0), 
            c.top -= Math.min(c.top, c.top + f > j && f < j ? Math.abs(f + h) : 0), c;
        },
        _findPos: function(b) {
            for (var c, d = this._getInst(b), e = this._get(d, "isRTL"); b && ("hidden" === b.type || 1 !== b.nodeType || a.expr.filters.hidden(b)); ) b = b[e ? "previousSibling" : "nextSibling"];
            return [ (c = a(b).offset()).left, c.top ];
        },
        _hideDatepicker: function(b) {
            var c, d, e, f, g = this._curInst;
            !g || b && g !== a.data(b, "datepicker") || this._datepickerShowing && (c = this._get(g, "showAnim"), 
            d = this._get(g, "duration"), e = function() {
                a.datepicker._tidyDialog(g);
            }, a.effects && (a.effects.effect[c] || a.effects[c]) ? g.dpDiv.hide(c, a.datepicker._get(g, "showOptions"), d, e) : g.dpDiv["slideDown" === c ? "slideUp" : "fadeIn" === c ? "fadeOut" : "hide"](c ? d : null, e), 
            c || e(), this._datepickerShowing = !1, (f = this._get(g, "onClose")) && f.apply(g.input ? g.input[0] : null, [ g.input ? g.input.val() : "", g ]), 
            this._lastInput = null, this._inDialog && (this._dialogInput.css({
                position: "absolute",
                left: "0",
                top: "-100px"
            }), a.blockUI && (a.unblockUI(), a("body").append(this.dpDiv))), this._inDialog = !1);
        },
        _tidyDialog: function(a) {
            a.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar");
        },
        _checkExternalClick: function(b) {
            if (a.datepicker._curInst) {
                var c = a(b.target), d = a.datepicker._getInst(c[0]);
                (c[0].id === a.datepicker._mainDivId || 0 !== c.parents("#" + a.datepicker._mainDivId).length || c.hasClass(a.datepicker.markerClassName) || c.closest("." + a.datepicker._triggerClass).length || !a.datepicker._datepickerShowing || a.datepicker._inDialog && a.blockUI) && (!c.hasClass(a.datepicker.markerClassName) || a.datepicker._curInst === d) || a.datepicker._hideDatepicker();
            }
        },
        _adjustDate: function(b, c, d) {
            var e = a(b), f = this._getInst(e[0]);
            this._isDisabledDatepicker(e[0]) || (this._adjustInstDate(f, c + ("M" === d ? this._get(f, "showCurrentAtPos") : 0), d), 
            this._updateDatepicker(f));
        },
        _gotoToday: function(b) {
            var c, d = a(b), e = this._getInst(d[0]);
            this._get(e, "gotoCurrent") && e.currentDay ? (e.selectedDay = e.currentDay, e.drawMonth = e.selectedMonth = e.currentMonth, 
            e.drawYear = e.selectedYear = e.currentYear) : (c = new Date(), e.selectedDay = c.getDate(), 
            e.drawMonth = e.selectedMonth = c.getMonth(), e.drawYear = e.selectedYear = c.getFullYear()), 
            this._notifyChange(e), this._adjustDate(d);
        },
        _selectMonthYear: function(b, c, d) {
            var e = a(b), f = this._getInst(e[0]);
            f["selected" + ("M" === d ? "Month" : "Year")] = f["draw" + ("M" === d ? "Month" : "Year")] = parseInt(c.options[c.selectedIndex].value, 10), 
            this._notifyChange(f), this._adjustDate(e);
        },
        _selectDay: function(b, c, d, e) {
            var f, g = a(b);
            a(e).hasClass(this._unselectableClass) || this._isDisabledDatepicker(g[0]) || ((f = this._getInst(g[0])).selectedDay = f.currentDay = a("a", e).html(), 
            f.selectedMonth = f.currentMonth = c, f.selectedYear = f.currentYear = d, this._selectDate(b, this._formatDate(f, f.currentDay, f.currentMonth, f.currentYear)));
        },
        _clearDate: function(b) {
            var c = a(b);
            this._selectDate(c, "");
        },
        _selectDate: function(b, c) {
            var d, e = a(b), f = this._getInst(e[0]);
            c = null != c ? c : this._formatDate(f), f.input && f.input.val(c), this._updateAlternate(f), 
            (d = this._get(f, "onSelect")) ? d.apply(f.input ? f.input[0] : null, [ c, f ]) : f.input && f.input.trigger("change"), 
            f.inline ? this._updateDatepicker(f) : (this._hideDatepicker(), this._lastInput = f.input[0], 
            "object" != typeof f.input[0] && f.input.trigger("focus"), this._lastInput = null);
        },
        _updateAlternate: function(b) {
            var c, d, e, f = this._get(b, "altField");
            f && (c = this._get(b, "altFormat") || this._get(b, "dateFormat"), d = this._getDate(b), 
            e = this.formatDate(c, d, this._getFormatConfig(b)), a(f).val(e));
        },
        noWeekends: function(a) {
            var b = a.getDay();
            return [ 0 < b && b < 6, "" ];
        },
        iso8601Week: function(a) {
            var b, c = new Date(a.getTime());
            return c.setDate(c.getDate() + 4 - (c.getDay() || 7)), b = c.getTime(), c.setMonth(0), 
            c.setDate(1), Math.floor(Math.round((b - c) / 864e5) / 7) + 1;
        },
        parseDate: function(b, c, d) {
            if (null == b || null == c) throw "Invalid arguments";
            if ("" === (c = "object" == typeof c ? "" + c : c + "")) return null;
            function e(a) {
                var c = b.length > i + 1 && b.charAt(i + 1) === a;
                return c && i++, c;
            }
            function f(a) {
                var b = e(a), d = "@" === a ? 14 : "!" === a ? 20 : "y" === a && b ? 4 : "o" === a ? 3 : 2, f = RegExp("^\\d{" + ("y" === a ? d : 1) + "," + d + "}"), g = c.substring(m).match(f);
                if (!g) throw "Missing number at position " + m;
                return m += g[0].length, parseInt(g[0], 10);
            }
            function g(b, d, f) {
                var g = -1, h = a.map(e(b) ? f : d, function(a, b) {
                    return [ [ b, a ] ];
                }).sort(function(a, b) {
                    return -(a[1].length - b[1].length);
                });
                if (a.each(h, function(a, b) {
                    var d = b[1];
                    return c.substr(m, d.length).toLowerCase() === d.toLowerCase() ? (g = b[0], m += d.length, 
                    !1) : void 0;
                }), -1 !== g) return g + 1;
                throw "Unknown name at position " + m;
            }
            function h() {
                if (c.charAt(m) !== b.charAt(i)) throw "Unexpected literal at position " + m;
                m++;
            }
            var i, j, k, l, m = 0, n = (d ? d.shortYearCutoff : null) || this._defaults.shortYearCutoff, o = "string" != typeof n ? n : new Date().getFullYear() % 100 + parseInt(n, 10), p = (d ? d.dayNamesShort : null) || this._defaults.dayNamesShort, q = (d ? d.dayNames : null) || this._defaults.dayNames, r = (d ? d.monthNamesShort : null) || this._defaults.monthNamesShort, s = (d ? d.monthNames : null) || this._defaults.monthNames, t = -1, u = -1, v = -1, w = -1, x = !1;
            for (i = 0; b.length > i; i++) if (x) "'" !== b.charAt(i) || e("'") ? h() : x = !1; else switch (b.charAt(i)) {
              case "d":
                v = f("d");
                break;

              case "D":
                g("D", p, q);
                break;

              case "o":
                w = f("o");
                break;

              case "m":
                u = f("m");
                break;

              case "M":
                u = g("M", r, s);
                break;

              case "y":
                t = f("y");
                break;

              case "@":
                t = (l = new Date(f("@"))).getFullYear(), u = l.getMonth() + 1, v = l.getDate();
                break;

              case "!":
                t = (l = new Date((f("!") - this._ticksTo1970) / 1e4)).getFullYear(), u = l.getMonth() + 1, 
                v = l.getDate();
                break;

              case "'":
                e("'") ? h() : x = !0;
                break;

              default:
                h();
            }
            if (c.length > m && (k = c.substr(m), !/^\s+/.test(k))) throw "Extra/unparsed characters found in date: " + k;
            if (-1 === t ? t = new Date().getFullYear() : t < 100 && (t += new Date().getFullYear() - new Date().getFullYear() % 100 + (t <= o ? 0 : -100)), 
            -1 < w) for (u = 1, v = w; !(v <= (j = this._getDaysInMonth(t, u - 1))); ) u++, 
            v -= j;
            if ((l = this._daylightSavingAdjust(new Date(t, u - 1, v))).getFullYear() !== t || l.getMonth() + 1 !== u || l.getDate() !== v) throw "Invalid date";
            return l;
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: 864e9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
        formatDate: function(a, b, c) {
            if (!b) return "";
            function d(b) {
                var c = a.length > g + 1 && a.charAt(g + 1) === b;
                return c && g++, c;
            }
            function e(a, b, c) {
                var e = "" + b;
                if (d(a)) for (;c > e.length; ) e = "0" + e;
                return e;
            }
            function f(a, b, c, e) {
                return d(a) ? e[b] : c[b];
            }
            var g, h = (c ? c.dayNamesShort : null) || this._defaults.dayNamesShort, i = (c ? c.dayNames : null) || this._defaults.dayNames, j = (c ? c.monthNamesShort : null) || this._defaults.monthNamesShort, k = (c ? c.monthNames : null) || this._defaults.monthNames, l = "", m = !1;
            if (b) for (g = 0; a.length > g; g++) if (m) "'" !== a.charAt(g) || d("'") ? l += a.charAt(g) : m = !1; else switch (a.charAt(g)) {
              case "d":
                l += e("d", b.getDate(), 2);
                break;

              case "D":
                l += f("D", b.getDay(), h, i);
                break;

              case "o":
                l += e("o", Math.round((new Date(b.getFullYear(), b.getMonth(), b.getDate()).getTime() - new Date(b.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                break;

              case "m":
                l += e("m", b.getMonth() + 1, 2);
                break;

              case "M":
                l += f("M", b.getMonth(), j, k);
                break;

              case "y":
                l += d("y") ? b.getFullYear() : (b.getFullYear() % 100 < 10 ? "0" : "") + b.getFullYear() % 100;
                break;

              case "@":
                l += b.getTime();
                break;

              case "!":
                l += 1e4 * b.getTime() + this._ticksTo1970;
                break;

              case "'":
                d("'") ? l += "'" : m = !0;
                break;

              default:
                l += a.charAt(g);
            }
            return l;
        },
        _possibleChars: function(a) {
            function b(b) {
                var d = a.length > c + 1 && a.charAt(c + 1) === b;
                return d && c++, d;
            }
            var c, d = "", e = !1;
            for (c = 0; a.length > c; c++) if (e) "'" !== a.charAt(c) || b("'") ? d += a.charAt(c) : e = !1; else switch (a.charAt(c)) {
              case "d":
              case "m":
              case "y":
              case "@":
                d += "0123456789";
                break;

              case "D":
              case "M":
                return null;

              case "'":
                b("'") ? d += "'" : e = !0;
                break;

              default:
                d += a.charAt(c);
            }
            return d;
        },
        _get: function(a, b) {
            return void 0 !== a.settings[b] ? a.settings[b] : this._defaults[b];
        },
        _setDateFromField: function(a, b) {
            if (a.input.val() !== a.lastVal) {
                var c = this._get(a, "dateFormat"), d = a.lastVal = a.input ? a.input.val() : null, e = this._getDefaultDate(a), f = e, g = this._getFormatConfig(a);
                try {
                    f = this.parseDate(c, d, g) || e;
                } catch (a) {
                    d = b ? "" : d;
                }
                a.selectedDay = f.getDate(), a.drawMonth = a.selectedMonth = f.getMonth(), a.drawYear = a.selectedYear = f.getFullYear(), 
                a.currentDay = d ? f.getDate() : 0, a.currentMonth = d ? f.getMonth() : 0, a.currentYear = d ? f.getFullYear() : 0, 
                this._adjustInstDate(a);
            }
        },
        _getDefaultDate: function(a) {
            return this._restrictMinMax(a, this._determineDate(a, this._get(a, "defaultDate"), new Date()));
        },
        _determineDate: function(b, c, d) {
            var e, f, g = null == c || "" === c ? d : "string" == typeof c ? function(c) {
                try {
                    return a.datepicker.parseDate(a.datepicker._get(b, "dateFormat"), c, a.datepicker._getFormatConfig(b));
                } catch (c) {}
                for (var d = (c.toLowerCase().match(/^c/) ? a.datepicker._getDate(b) : null) || new Date(), e = d.getFullYear(), f = d.getMonth(), g = d.getDate(), h = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, i = h.exec(c); i; ) {
                    switch (i[2] || "d") {
                      case "d":
                      case "D":
                        g += parseInt(i[1], 10);
                        break;

                      case "w":
                      case "W":
                        g += 7 * parseInt(i[1], 10);
                        break;

                      case "m":
                      case "M":
                        f += parseInt(i[1], 10), g = Math.min(g, a.datepicker._getDaysInMonth(e, f));
                        break;

                      case "y":
                      case "Y":
                        e += parseInt(i[1], 10), g = Math.min(g, a.datepicker._getDaysInMonth(e, f));
                    }
                    i = h.exec(c);
                }
                return new Date(e, f, g);
            }(c) : "number" == typeof c ? isNaN(c) ? d : (e = c, (f = new Date()).setDate(f.getDate() + e), 
            f) : new Date(c.getTime());
            return (g = g && "Invalid Date" == "" + g ? d : g) && (g.setHours(0), g.setMinutes(0), 
            g.setSeconds(0), g.setMilliseconds(0)), this._daylightSavingAdjust(g);
        },
        _daylightSavingAdjust: function(a) {
            return a ? (a.setHours(12 < a.getHours() ? a.getHours() + 2 : 0), a) : null;
        },
        _setDate: function(a, b, c) {
            var d = !b, e = a.selectedMonth, f = a.selectedYear, g = this._restrictMinMax(a, this._determineDate(a, b, new Date()));
            a.selectedDay = a.currentDay = g.getDate(), a.drawMonth = a.selectedMonth = a.currentMonth = g.getMonth(), 
            a.drawYear = a.selectedYear = a.currentYear = g.getFullYear(), e === a.selectedMonth && f === a.selectedYear || c || this._notifyChange(a), 
            this._adjustInstDate(a), a.input && a.input.val(d ? "" : this._formatDate(a));
        },
        _getDate: function(a) {
            return !a.currentYear || a.input && "" === a.input.val() ? null : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
        },
        _attachHandlers: function(b) {
            var c = this._get(b, "stepMonths"), d = "#" + b.id.replace(/\\\\/g, "\\");
            b.dpDiv.find("[data-handler]").map(function() {
                var b = {
                    prev: function() {
                        a.datepicker._adjustDate(d, -c, "M");
                    },
                    next: function() {
                        a.datepicker._adjustDate(d, +c, "M");
                    },
                    hide: function() {
                        a.datepicker._hideDatepicker();
                    },
                    today: function() {
                        a.datepicker._gotoToday(d);
                    },
                    selectDay: function() {
                        return a.datepicker._selectDay(d, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), 
                        !1;
                    },
                    selectMonth: function() {
                        return a.datepicker._selectMonthYear(d, this, "M"), !1;
                    },
                    selectYear: function() {
                        return a.datepicker._selectMonthYear(d, this, "Y"), !1;
                    }
                };
                a(this).on(this.getAttribute("data-event"), b[this.getAttribute("data-handler")]);
            });
        },
        _generateHTML: function(a) {
            var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O = new Date(), P = this._daylightSavingAdjust(new Date(O.getFullYear(), O.getMonth(), O.getDate())), Q = this._get(a, "isRTL"), R = this._get(a, "showButtonPanel"), S = this._get(a, "hideIfNoPrevNext"), T = this._get(a, "navigationAsDateFormat"), U = this._getNumberOfMonths(a), V = this._get(a, "showCurrentAtPos"), W = this._get(a, "stepMonths"), X = 1 !== U[0] || 1 !== U[1], Y = this._daylightSavingAdjust(a.currentDay ? new Date(a.currentYear, a.currentMonth, a.currentDay) : new Date(9999, 9, 9)), Z = this._getMinMaxDate(a, "min"), $ = this._getMinMaxDate(a, "max"), _ = a.drawMonth - V, ab = a.drawYear;
            if (_ < 0 && (_ += 12, ab--), $) for (b = this._daylightSavingAdjust(new Date($.getFullYear(), $.getMonth() - U[0] * U[1] + 1, $.getDate())), 
            b = Z && b < Z ? Z : b; this._daylightSavingAdjust(new Date(ab, _, 1)) > b; ) --_ < 0 && (_ = 11, 
            ab--);
            for (a.drawMonth = _, a.drawYear = ab, c = this._get(a, "prevText"), c = T ? this.formatDate(c, this._daylightSavingAdjust(new Date(ab, _ - W, 1)), this._getFormatConfig(a)) : c, 
            d = this._canAdjustMonth(a, -1, ab, _) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + c + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "e" : "w") + "'>" + c + "</span></a>" : S ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + c + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "e" : "w") + "'>" + c + "</span></a>", 
            e = this._get(a, "nextText"), e = T ? this.formatDate(e, this._daylightSavingAdjust(new Date(ab, _ + W, 1)), this._getFormatConfig(a)) : e, 
            f = this._canAdjustMonth(a, 1, ab, _) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + e + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "w" : "e") + "'>" + e + "</span></a>" : S ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + e + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "w" : "e") + "'>" + e + "</span></a>", 
            g = this._get(a, "currentText"), h = this._get(a, "gotoCurrent") && a.currentDay ? Y : P, 
            g = T ? this.formatDate(g, h, this._getFormatConfig(a)) : g, i = a.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(a, "closeText") + "</button>", 
            j = R ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (Q ? i : "") + (this._isInRange(a, h) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + g + "</button>" : "") + (Q ? "" : i) + "</div>" : "", 
            k = parseInt(this._get(a, "firstDay"), 10), k = isNaN(k) ? 0 : k, l = this._get(a, "showWeek"), 
            m = this._get(a, "dayNames"), n = this._get(a, "dayNamesMin"), o = this._get(a, "monthNames"), 
            p = this._get(a, "monthNamesShort"), q = this._get(a, "beforeShowDay"), r = this._get(a, "showOtherMonths"), 
            s = this._get(a, "selectOtherMonths"), t = this._getDefaultDate(a), u = "", w = 0; U[0] > w; w++) {
                for (x = "", this.maxRows = 4, y = 0; U[1] > y; y++) {
                    if (z = this._daylightSavingAdjust(new Date(ab, _, a.selectedDay)), A = " ui-corner-all", 
                    B = "", X) {
                        if (B += "<div class='ui-datepicker-group", 1 < U[1]) switch (y) {
                          case 0:
                            B += " ui-datepicker-group-first", A = " ui-corner-" + (Q ? "right" : "left");
                            break;

                          case U[1] - 1:
                            B += " ui-datepicker-group-last", A = " ui-corner-" + (Q ? "left" : "right");
                            break;

                          default:
                            B += " ui-datepicker-group-middle", A = "";
                        }
                        B += "'>";
                    }
                    for (B += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + A + "'>" + (/all|left/.test(A) && 0 === w ? Q ? f : d : "") + (/all|right/.test(A) && 0 === w ? Q ? d : f : "") + this._generateMonthYearHeader(a, _, ab, Z, $, 0 < w || 0 < y, o, p) + "</div><table class='ui-datepicker-calendar'><thead><tr>", 
                    C = l ? "<th class='ui-datepicker-week-col'>" + this._get(a, "weekHeader") + "</th>" : "", 
                    v = 0; v < 7; v++) C += "<th scope='col'" + (5 <= (v + k + 6) % 7 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + m[D = (v + k) % 7] + "'>" + n[D] + "</span></th>";
                    for (B += C + "</tr></thead><tbody>", E = this._getDaysInMonth(ab, _), ab === a.selectedYear && _ === a.selectedMonth && (a.selectedDay = Math.min(a.selectedDay, E)), 
                    F = (this._getFirstDayOfMonth(ab, _) - k + 7) % 7, G = Math.ceil((F + E) / 7), H = X && this.maxRows > G ? this.maxRows : G, 
                    this.maxRows = H, I = this._daylightSavingAdjust(new Date(ab, _, 1 - F)), J = 0; J < H; J++) {
                        for (B += "<tr>", K = l ? "<td class='ui-datepicker-week-col'>" + this._get(a, "calculateWeek")(I) + "</td>" : "", 
                        v = 0; v < 7; v++) L = q ? q.apply(a.input ? a.input[0] : null, [ I ]) : [ !0, "" ], 
                        N = (M = I.getMonth() !== _) && !s || !L[0] || Z && I < Z || $ && $ < I, K += "<td class='" + (5 <= (v + k + 6) % 7 ? " ui-datepicker-week-end" : "") + (M ? " ui-datepicker-other-month" : "") + (I.getTime() === z.getTime() && _ === a.selectedMonth && a._keyEvent || t.getTime() === I.getTime() && t.getTime() === z.getTime() ? " " + this._dayOverClass : "") + (N ? " " + this._unselectableClass + " ui-state-disabled" : "") + (M && !r ? "" : " " + L[1] + (I.getTime() === Y.getTime() ? " " + this._currentClass : "") + (I.getTime() === P.getTime() ? " ui-datepicker-today" : "")) + "'" + (M && !r || !L[2] ? "" : " title='" + L[2].replace(/'/g, "&#39;") + "'") + (N ? "" : " data-handler='selectDay' data-event='click' data-month='" + I.getMonth() + "' data-year='" + I.getFullYear() + "'") + ">" + (M && !r ? "&#xa0;" : N ? "<span class='ui-state-default'>" + I.getDate() + "</span>" : "<a class='ui-state-default" + (I.getTime() === P.getTime() ? " ui-state-highlight" : "") + (I.getTime() === Y.getTime() ? " ui-state-active" : "") + (M ? " ui-priority-secondary" : "") + "' href='#'>" + I.getDate() + "</a>") + "</td>", 
                        I.setDate(I.getDate() + 1), I = this._daylightSavingAdjust(I);
                        B += K + "</tr>";
                    }
                    11 < ++_ && (_ = 0, ab++), x += B += "</tbody></table>" + (X ? "</div>" + (0 < U[0] && y === U[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : "");
                }
                u += x;
            }
            return u += j, a._keyEvent = !1, u;
        },
        _generateMonthYearHeader: function(a, b, c, d, e, f, g, h) {
            var i, j, k, l, m, n, o, p, q = this._get(a, "changeMonth"), r = this._get(a, "changeYear"), s = this._get(a, "showMonthAfterYear"), t = "<div class='ui-datepicker-title'>", u = "";
            if (f || !q) u += "<span class='ui-datepicker-month'>" + g[b] + "</span>"; else {
                for (i = d && d.getFullYear() === c, j = e && e.getFullYear() === c, u += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", 
                k = 0; k < 12; k++) (!i || k >= d.getMonth()) && (!j || e.getMonth() >= k) && (u += "<option value='" + k + "'" + (k === b ? " selected='selected'" : "") + ">" + h[k] + "</option>");
                u += "</select>";
            }
            if (s || (t += u + (!f && q && r ? "" : "&#xa0;")), !a.yearshtml) if (a.yearshtml = "", 
            f || !r) t += "<span class='ui-datepicker-year'>" + c + "</span>"; else {
                for (l = this._get(a, "yearRange").split(":"), m = new Date().getFullYear(), o = (n = function(a) {
                    var b = a.match(/c[+\-].*/) ? c + parseInt(a.substring(1), 10) : a.match(/[+\-].*/) ? m + parseInt(a, 10) : parseInt(a, 10);
                    return isNaN(b) ? m : b;
                })(l[0]), p = Math.max(o, n(l[1] || "")), o = d ? Math.max(o, d.getFullYear()) : o, 
                p = e ? Math.min(p, e.getFullYear()) : p, a.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; o <= p; o++) a.yearshtml += "<option value='" + o + "'" + (o === c ? " selected='selected'" : "") + ">" + o + "</option>";
                a.yearshtml += "</select>", t += a.yearshtml, a.yearshtml = null;
            }
            return t += this._get(a, "yearSuffix"), s && (t += (!f && q && r ? "" : "&#xa0;") + u), 
            t + "</div>";
        },
        _adjustInstDate: function(a, b, c) {
            var d = a.selectedYear + ("Y" === c ? b : 0), e = a.selectedMonth + ("M" === c ? b : 0), f = Math.min(a.selectedDay, this._getDaysInMonth(d, e)) + ("D" === c ? b : 0), g = this._restrictMinMax(a, this._daylightSavingAdjust(new Date(d, e, f)));
            a.selectedDay = g.getDate(), a.drawMonth = a.selectedMonth = g.getMonth(), a.drawYear = a.selectedYear = g.getFullYear(), 
            "M" !== c && "Y" !== c || this._notifyChange(a);
        },
        _restrictMinMax: function(a, b) {
            var c = this._getMinMaxDate(a, "min"), d = this._getMinMaxDate(a, "max"), e = c && b < c ? c : b;
            return d && d < e ? d : e;
        },
        _notifyChange: function(a) {
            var b = this._get(a, "onChangeMonthYear");
            b && b.apply(a.input ? a.input[0] : null, [ a.selectedYear, a.selectedMonth + 1, a ]);
        },
        _getNumberOfMonths: function(a) {
            var b = this._get(a, "numberOfMonths");
            return null == b ? [ 1, 1 ] : "number" == typeof b ? [ 1, b ] : b;
        },
        _getMinMaxDate: function(a, b) {
            return this._determineDate(a, this._get(a, b + "Date"), null);
        },
        _getDaysInMonth: function(a, b) {
            return 32 - this._daylightSavingAdjust(new Date(a, b, 32)).getDate();
        },
        _getFirstDayOfMonth: function(a, b) {
            return new Date(a, b, 1).getDay();
        },
        _canAdjustMonth: function(a, b, c, d) {
            var e = this._getNumberOfMonths(a), f = this._daylightSavingAdjust(new Date(c, d + (b < 0 ? b : e[0] * e[1]), 1));
            return b < 0 && f.setDate(this._getDaysInMonth(f.getFullYear(), f.getMonth())), 
            this._isInRange(a, f);
        },
        _isInRange: function(a, b) {
            var c, d, e = this._getMinMaxDate(a, "min"), f = this._getMinMaxDate(a, "max"), g = null, h = null, i = this._get(a, "yearRange");
            return i && (c = i.split(":"), d = new Date().getFullYear(), g = parseInt(c[0], 10), 
            h = parseInt(c[1], 10), c[0].match(/[+\-].*/) && (g += d), c[1].match(/[+\-].*/) && (h += d)), 
            (!e || b.getTime() >= e.getTime()) && (!f || b.getTime() <= f.getTime()) && (!g || b.getFullYear() >= g) && (!h || h >= b.getFullYear());
        },
        _getFormatConfig: function(a) {
            var b = this._get(a, "shortYearCutoff");
            return {
                shortYearCutoff: b = "string" != typeof b ? b : new Date().getFullYear() % 100 + parseInt(b, 10),
                dayNamesShort: this._get(a, "dayNamesShort"),
                dayNames: this._get(a, "dayNames"),
                monthNamesShort: this._get(a, "monthNamesShort"),
                monthNames: this._get(a, "monthNames")
            };
        },
        _formatDate: function(a, b, c, d) {
            b || (a.currentDay = a.selectedDay, a.currentMonth = a.selectedMonth, a.currentYear = a.selectedYear);
            var e = b ? "object" == typeof b ? b : this._daylightSavingAdjust(new Date(d, c, b)) : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
            return this.formatDate(this._get(a, "dateFormat"), e, this._getFormatConfig(a));
        }
    }), a.fn.datepicker = function(b) {
        if (!this.length) return this;
        a.datepicker.initialized || (a(document).on("mousedown", a.datepicker._checkExternalClick), 
        a.datepicker.initialized = !0), 0 === a("#" + a.datepicker._mainDivId).length && a("body").append(a.datepicker.dpDiv);
        var c = Array.prototype.slice.call(arguments, 1);
        return "string" != typeof b || "isDisabled" !== b && "getDate" !== b && "widget" !== b ? "option" === b && 2 === arguments.length && "string" == typeof arguments[1] ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this[0] ].concat(c)) : this.each(function() {
            "string" == typeof b ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this ].concat(c)) : a.datepicker._attachDatepicker(this, b);
        }) : a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this[0] ].concat(c));
    }, a.datepicker = new b(), a.datepicker.initialized = !1, a.datepicker.uuid = new Date().getTime(), 
    a.datepicker.version = "1.12.1", a.datepicker, a.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
    var eb = !1;
    a(document).on("mouseup", function() {
        eb = !1;
    }), a.widget("ui.mouse", {
        version: "1.12.1",
        options: {
            cancel: "input, textarea, button, select, option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var b = this;
            this.element.on("mousedown." + this.widgetName, function(a) {
                return b._mouseDown(a);
            }).on("click." + this.widgetName, function(c) {
                return !0 === a.data(c.target, b.widgetName + ".preventClickEvent") ? (a.removeData(c.target, b.widgetName + ".preventClickEvent"), 
                c.stopImmediatePropagation(), !1) : void 0;
            }), this.started = !1;
        },
        _mouseDestroy: function() {
            this.element.off("." + this.widgetName), this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate);
        },
        _mouseDown: function(b) {
            if (!eb) {
                this._mouseMoved = !1, this._mouseStarted && this._mouseUp(b), this._mouseDownEvent = b;
                var c = this, d = 1 === b.which, e = !("string" != typeof this.options.cancel || !b.target.nodeName) && a(b.target).closest(this.options.cancel).length;
                return d && !e && this._mouseCapture(b) && (this.mouseDelayMet = !this.options.delay, 
                this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                    c.mouseDelayMet = !0;
                }, this.options.delay)), this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = !1 !== this._mouseStart(b), 
                !this._mouseStarted) ? b.preventDefault() : (!0 === a.data(b.target, this.widgetName + ".preventClickEvent") && a.removeData(b.target, this.widgetName + ".preventClickEvent"), 
                this._mouseMoveDelegate = function(a) {
                    return c._mouseMove(a);
                }, this._mouseUpDelegate = function(a) {
                    return c._mouseUp(a);
                }, this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate), 
                b.preventDefault(), eb = !0)), !0;
            }
        },
        _mouseMove: function(b) {
            if (this._mouseMoved) {
                if (a.ui.ie && (!document.documentMode || document.documentMode < 9) && !b.button) return this._mouseUp(b);
                if (!b.which) if (b.originalEvent.altKey || b.originalEvent.ctrlKey || b.originalEvent.metaKey || b.originalEvent.shiftKey) this.ignoreMissingWhich = !0; else if (!this.ignoreMissingWhich) return this._mouseUp(b);
            }
            return (b.which || b.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(b), 
            b.preventDefault()) : (this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, b), 
            this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b)), !this._mouseStarted);
        },
        _mouseUp: function(b) {
            this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate), 
            this._mouseStarted && (this._mouseStarted = !1, b.target === this._mouseDownEvent.target && a.data(b.target, this.widgetName + ".preventClickEvent", !0), 
            this._mouseStop(b)), this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer), 
            delete this._mouseDelayTimer), this.ignoreMissingWhich = !1, eb = !1, b.preventDefault();
        },
        _mouseDistanceMet: function(a) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance;
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet;
        },
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return !0;
        }
    }), a.ui.plugin = {
        add: function(b, c, d) {
            var e, f = a.ui[b].prototype;
            for (e in d) f.plugins[e] = f.plugins[e] || [], f.plugins[e].push([ c, d[e] ]);
        },
        call: function(a, b, c, d) {
            var e, f = a.plugins[b];
            if (f && (d || a.element[0].parentNode && 11 !== a.element[0].parentNode.nodeType)) for (e = 0; f.length > e; e++) a.options[f[e][0]] && f[e][1].apply(a.element, c);
        }
    }, a.ui.safeBlur = function(b) {
        b && "body" !== b.nodeName.toLowerCase() && a(b).trigger("blur");
    }, a.widget("ui.draggable", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1,
            drag: null,
            start: null,
            stop: null
        },
        _create: function() {
            "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this._addClass("ui-draggable"), 
            this._setHandleClassName(), this._mouseInit();
        },
        _setOption: function(a, b) {
            this._super(a, b), "handle" === a && (this._removeHandleClassName(), this._setHandleClassName());
        },
        _destroy: function() {
            return (this.helper || this.element).is(".ui-draggable-dragging") ? void (this.destroyOnClear = !0) : (this._removeHandleClassName(), 
            void this._mouseDestroy());
        },
        _mouseCapture: function(b) {
            var c = this.options;
            return !(this.helper || c.disabled || 0 < a(b.target).closest(".ui-resizable-handle").length) && (this.handle = this._getHandle(b), 
            !!this.handle && (this._blurActiveElement(b), this._blockFrames(!0 === c.iframeFix ? "iframe" : c.iframeFix), 
            !0));
        },
        _blockFrames: function(b) {
            this.iframeBlocks = this.document.find(b).map(function() {
                var b = a(this);
                return a("<div>").css("position", "absolute").appendTo(b.parent()).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).offset(b.offset())[0];
            });
        },
        _unblockFrames: function() {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
        },
        _blurActiveElement: function(b) {
            var c = a.ui.safeActiveElement(this.document[0]);
            a(b.target).closest(c).length || a.ui.safeBlur(c);
        },
        _mouseStart: function(b) {
            var c = this.options;
            return this.helper = this._createHelper(b), this._addClass(this.helper, "ui-draggable-dragging"), 
            this._cacheHelperProportions(), a.ui.ddmanager && (a.ui.ddmanager.current = this), 
            this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), 
            this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = 0 < this.helper.parents().filter(function() {
                return "fixed" === a(this).css("position");
            }).length, this.positionAbs = this.element.offset(), this._refreshOffsets(b), this.originalPosition = this.position = this._generatePosition(b, !1), 
            this.originalPageX = b.pageX, this.originalPageY = b.pageY, c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt), 
            this._setContainment(), !1 === this._trigger("start", b) ? (this._clear(), !1) : (this._cacheHelperProportions(), 
            a.ui.ddmanager && !c.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this._mouseDrag(b, !0), 
            a.ui.ddmanager && a.ui.ddmanager.dragStart(this, b), !0);
        },
        _refreshOffsets: function(a) {
            this.offset = {
                top: this.positionAbs.top - this.margins.top,
                left: this.positionAbs.left - this.margins.left,
                scroll: !1,
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }, this.offset.click = {
                left: a.pageX - this.offset.left,
                top: a.pageY - this.offset.top
            };
        },
        _mouseDrag: function(b, c) {
            if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(b, !0), 
            this.positionAbs = this._convertPositionTo("absolute"), !c) {
                var d = this._uiHash();
                if (!1 === this._trigger("drag", b, d)) return this._mouseUp(new a.Event("mouseup", b)), 
                !1;
                this.position = d.position;
            }
            return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", 
            a.ui.ddmanager && a.ui.ddmanager.drag(this, b), !1;
        },
        _mouseStop: function(b) {
            var c = this, d = !1;
            return a.ui.ddmanager && !this.options.dropBehaviour && (d = a.ui.ddmanager.drop(this, b)), 
            this.dropped && (d = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !d || "valid" === this.options.revert && d || !0 === this.options.revert || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d) ? a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                !1 !== c._trigger("stop", b) && c._clear();
            }) : !1 !== this._trigger("stop", b) && this._clear(), !1;
        },
        _mouseUp: function(b) {
            return this._unblockFrames(), a.ui.ddmanager && a.ui.ddmanager.dragStop(this, b), 
            this.handleElement.is(b.target) && this.element.trigger("focus"), a.ui.mouse.prototype._mouseUp.call(this, b);
        },
        cancel: function() {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new a.Event("mouseup", {
                target: this.element[0]
            })) : this._clear(), this;
        },
        _getHandle: function(b) {
            return !this.options.handle || !!a(b.target).closest(this.element.find(this.options.handle)).length;
        },
        _setHandleClassName: function() {
            this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, 
            this._addClass(this.handleElement, "ui-draggable-handle");
        },
        _removeHandleClassName: function() {
            this._removeClass(this.handleElement, "ui-draggable-handle");
        },
        _createHelper: function(b) {
            var c = this.options, d = a.isFunction(c.helper), e = d ? a(c.helper.apply(this.element[0], [ b ])) : "clone" === c.helper ? this.element.clone().removeAttr("id") : this.element;
            return e.parents("body").length || e.appendTo("parent" === c.appendTo ? this.element[0].parentNode : c.appendTo), 
            d && e[0] === this.element[0] && this._setPositionRelative(), e[0] === this.element[0] || /(fixed|absolute)/.test(e.css("position")) || e.css("position", "absolute"), 
            e;
        },
        _setPositionRelative: function() {
            /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative");
        },
        _adjustOffsetFromHelper: function(b) {
            "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
                left: +b[0],
                top: +b[1] || 0
            }), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), 
            "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top);
        },
        _isRootNode: function(a) {
            return /(html|body)/i.test(a.tagName) || a === this.document[0];
        },
        _getParentOffset: function() {
            var b = this.offsetParent.offset(), c = this.document[0];
            return "absolute" === this.cssPosition && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), 
            b.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (b = {
                top: 0,
                left: 0
            }), {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            };
        },
        _getRelativeOffset: function() {
            if ("relative" !== this.cssPosition) return {
                top: 0,
                left: 0
            };
            var a = this.element.position(), b = this._isRootNode(this.scrollParent[0]);
            return {
                top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (b ? 0 : this.scrollParent.scrollTop()),
                left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (b ? 0 : this.scrollParent.scrollLeft())
            };
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            };
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            };
        },
        _setContainment: function() {
            var b, c, d, e = this.options, f = this.document[0];
            return this.relativeContainer = null, e.containment ? "window" === e.containment ? void (this.containment = [ a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]) : "document" === e.containment ? void (this.containment = [ 0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]) : e.containment.constructor === Array ? void (this.containment = e.containment) : ("parent" === e.containment && (e.containment = this.helper[0].parentNode), 
            void ((d = (c = a(e.containment))[0]) && (b = /(scroll|auto)/.test(c.css("overflow")), 
            this.containment = [ (parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom ], 
            this.relativeContainer = c))) : void (this.containment = null);
        },
        _convertPositionTo: function(a, b) {
            b = b || this.position;
            var c = "absolute" === a ? 1 : -1, d = this._isRootNode(this.scrollParent[0]);
            return {
                top: b.top + this.offset.relative.top * c + this.offset.parent.top * c - ("fixed" === this.cssPosition ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c,
                left: b.left + this.offset.relative.left * c + this.offset.parent.left * c - ("fixed" === this.cssPosition ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c
            };
        },
        _generatePosition: function(a, b) {
            var c, d, e, f, g = this.options, h = this._isRootNode(this.scrollParent[0]), i = a.pageX, j = a.pageY;
            return h && this.offset.scroll || (this.offset.scroll = {
                top: this.scrollParent.scrollTop(),
                left: this.scrollParent.scrollLeft()
            }), b && (this.containment && (c = this.relativeContainer ? (d = this.relativeContainer.offset(), 
            [ this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top ]) : this.containment, 
            a.pageX - this.offset.click.left < c[0] && (i = c[0] + this.offset.click.left), 
            a.pageY - this.offset.click.top < c[1] && (j = c[1] + this.offset.click.top), a.pageX - this.offset.click.left > c[2] && (i = c[2] + this.offset.click.left), 
            a.pageY - this.offset.click.top > c[3] && (j = c[3] + this.offset.click.top)), g.grid && (e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, 
            j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e, 
            f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, 
            i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f), 
            "y" === g.axis && (i = this.originalPageX), "x" === g.axis && (j = this.originalPageY)), 
            {
                top: j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top),
                left: i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left)
            };
        },
        _clear: function() {
            this._removeClass(this.helper, "ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), 
            this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy();
        },
        _trigger: function(b, c, d) {
            return d = d || this._uiHash(), a.ui.plugin.call(this, b, [ c, d, this ], !0), /^(drag|start|stop)/.test(b) && (this.positionAbs = this._convertPositionTo("absolute"), 
            d.offset = this.positionAbs), a.Widget.prototype._trigger.call(this, b, c, d);
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            };
        }
    }), a.ui.plugin.add("draggable", "connectToSortable", {
        start: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.sortables = [], a(d.options.connectToSortable).each(function() {
                var c = a(this).sortable("instance");
                c && !c.options.disabled && (d.sortables.push(c), c.refreshPositions(), c._trigger("activate", b, e));
            });
        },
        stop: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.cancelHelperRemoval = !1, a.each(d.sortables, function() {
                var a = this;
                a.isOver ? (a.isOver = 0, d.cancelHelperRemoval = !0, a.cancelHelperRemoval = !1, 
                a._storedCSS = {
                    position: a.placeholder.css("position"),
                    top: a.placeholder.css("top"),
                    left: a.placeholder.css("left")
                }, a._mouseStop(b), a.options.helper = a.options._helper) : (a.cancelHelperRemoval = !0, 
                a._trigger("deactivate", b, e));
            });
        },
        drag: function(b, c, d) {
            a.each(d.sortables, function() {
                var e = !1, f = this;
                f.positionAbs = d.positionAbs, f.helperProportions = d.helperProportions, f.offset.click = d.offset.click, 
                f._intersectsWith(f.containerCache) && (e = !0, a.each(d.sortables, function() {
                    return this.positionAbs = d.positionAbs, this.helperProportions = d.helperProportions, 
                    this.offset.click = d.offset.click, this !== f && this._intersectsWith(this.containerCache) && a.contains(f.element[0], this.element[0]) && (e = !1), 
                    e;
                })), e ? (f.isOver || (f.isOver = 1, d._parent = c.helper.parent(), f.currentItem = c.helper.appendTo(f.element).data("ui-sortable-item", !0), 
                f.options._helper = f.options.helper, f.options.helper = function() {
                    return c.helper[0];
                }, b.target = f.currentItem[0], f._mouseCapture(b, !0), f._mouseStart(b, !0, !0), 
                f.offset.click.top = d.offset.click.top, f.offset.click.left = d.offset.click.left, 
                f.offset.parent.left -= d.offset.parent.left - f.offset.parent.left, f.offset.parent.top -= d.offset.parent.top - f.offset.parent.top, 
                d._trigger("toSortable", b), d.dropped = f.element, a.each(d.sortables, function() {
                    this.refreshPositions();
                }), d.currentItem = d.element, f.fromOutside = d), f.currentItem && (f._mouseDrag(b), 
                c.position = f.position)) : f.isOver && (f.isOver = 0, f.cancelHelperRemoval = !0, 
                f.options._revert = f.options.revert, f.options.revert = !1, f._trigger("out", b, f._uiHash(f)), 
                f._mouseStop(b, !0), f.options.revert = f.options._revert, f.options.helper = f.options._helper, 
                f.placeholder && f.placeholder.remove(), c.helper.appendTo(d._parent), d._refreshOffsets(b), 
                c.position = d._generatePosition(b, !0), d._trigger("fromSortable", b), d.dropped = !1, 
                a.each(d.sortables, function() {
                    this.refreshPositions();
                }));
            });
        }
    }), a.ui.plugin.add("draggable", "cursor", {
        start: function(b, c, d) {
            var e = a("body"), f = d.options;
            e.css("cursor") && (f._cursor = e.css("cursor")), e.css("cursor", f.cursor);
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._cursor && a("body").css("cursor", e._cursor);
        }
    }), a.ui.plugin.add("draggable", "opacity", {
        start: function(b, c, d) {
            var e = a(c.helper), f = d.options;
            e.css("opacity") && (f._opacity = e.css("opacity")), e.css("opacity", f.opacity);
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._opacity && a(c.helper).css("opacity", e._opacity);
        }
    }), a.ui.plugin.add("draggable", "scroll", {
        start: function(a, b, c) {
            c.scrollParentNotHidden || (c.scrollParentNotHidden = c.helper.scrollParent(!1)), 
            c.scrollParentNotHidden[0] !== c.document[0] && "HTML" !== c.scrollParentNotHidden[0].tagName && (c.overflowOffset = c.scrollParentNotHidden.offset());
        },
        drag: function(b, c, d) {
            var e = d.options, f = !1, g = d.scrollParentNotHidden[0], h = d.document[0];
            g !== h && "HTML" !== g.tagName ? (e.axis && "x" === e.axis || (d.overflowOffset.top + g.offsetHeight - b.pageY < e.scrollSensitivity ? g.scrollTop = f = g.scrollTop + e.scrollSpeed : b.pageY - d.overflowOffset.top < e.scrollSensitivity && (g.scrollTop = f = g.scrollTop - e.scrollSpeed)), 
            e.axis && "y" === e.axis || (d.overflowOffset.left + g.offsetWidth - b.pageX < e.scrollSensitivity ? g.scrollLeft = f = g.scrollLeft + e.scrollSpeed : b.pageX - d.overflowOffset.left < e.scrollSensitivity && (g.scrollLeft = f = g.scrollLeft - e.scrollSpeed))) : (e.axis && "x" === e.axis || (b.pageY - a(h).scrollTop() < e.scrollSensitivity ? f = a(h).scrollTop(a(h).scrollTop() - e.scrollSpeed) : a(window).height() - (b.pageY - a(h).scrollTop()) < e.scrollSensitivity && (f = a(h).scrollTop(a(h).scrollTop() + e.scrollSpeed))), 
            e.axis && "y" === e.axis || (b.pageX - a(h).scrollLeft() < e.scrollSensitivity ? f = a(h).scrollLeft(a(h).scrollLeft() - e.scrollSpeed) : a(window).width() - (b.pageX - a(h).scrollLeft()) < e.scrollSensitivity && (f = a(h).scrollLeft(a(h).scrollLeft() + e.scrollSpeed)))), 
            !1 !== f && a.ui.ddmanager && !e.dropBehaviour && a.ui.ddmanager.prepareOffsets(d, b);
        }
    }), a.ui.plugin.add("draggable", "snap", {
        start: function(b, c, d) {
            var e = d.options;
            d.snapElements = [], a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function() {
                var b = a(this), c = b.offset();
                this !== d.element[0] && d.snapElements.push({
                    item: this,
                    width: b.outerWidth(),
                    height: b.outerHeight(),
                    top: c.top,
                    left: c.left
                });
            });
        },
        drag: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o = d.options, p = o.snapTolerance, q = c.offset.left, r = q + d.helperProportions.width, s = c.offset.top, t = s + d.helperProportions.height;
            for (m = d.snapElements.length - 1; 0 <= m; m--) j = (i = d.snapElements[m].left - d.margins.left) + d.snapElements[m].width, 
            l = (k = d.snapElements[m].top - d.margins.top) + d.snapElements[m].height, r < i - p || j + p < q || t < k - p || l + p < s || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item) ? (d.snapElements[m].snapping && d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), {
                snapItem: d.snapElements[m].item
            })), d.snapElements[m].snapping = !1) : ("inner" !== o.snapMode && (e = p >= Math.abs(k - t), 
            f = p >= Math.abs(l - s), g = p >= Math.abs(i - r), h = p >= Math.abs(j - q), e && (c.position.top = d._convertPositionTo("relative", {
                top: k - d.helperProportions.height,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i - d.helperProportions.width
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j
            }).left)), n = e || f || g || h, "outer" !== o.snapMode && (e = p >= Math.abs(k - s), 
            f = p >= Math.abs(l - t), g = p >= Math.abs(i - q), h = p >= Math.abs(j - r), e && (c.position.top = d._convertPositionTo("relative", {
                top: k,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l - d.helperProportions.height,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j - d.helperProportions.width
            }).left)), !d.snapElements[m].snapping && (e || f || g || h || n) && d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), {
                snapItem: d.snapElements[m].item
            })), d.snapElements[m].snapping = e || f || g || h || n);
        }
    }), a.ui.plugin.add("draggable", "stack", {
        start: function(b, c, d) {
            var e, f = d.options, g = a.makeArray(a(f.stack)).sort(function(b, c) {
                return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0);
            });
            g.length && (e = parseInt(a(g[0]).css("zIndex"), 10) || 0, a(g).each(function(b) {
                a(this).css("zIndex", e + b);
            }), this.css("zIndex", e + g.length));
        }
    }), a.ui.plugin.add("draggable", "zIndex", {
        start: function(b, c, d) {
            var e = a(c.helper), f = d.options;
            e.css("zIndex") && (f._zIndex = e.css("zIndex")), e.css("zIndex", f.zIndex);
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._zIndex && a(c.helper).css("zIndex", e._zIndex);
        }
    }), a.ui.draggable, a.widget("ui.resizable", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            classes: {
                "ui-resizable-se": "ui-icon ui-icon-gripsmall-diagonal-se"
            },
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 90,
            resize: null,
            start: null,
            stop: null
        },
        _num: function(a) {
            return parseFloat(a) || 0;
        },
        _isNumber: function(a) {
            return !isNaN(parseFloat(a));
        },
        _hasScroll: function(b, c) {
            if ("hidden" === a(b).css("overflow")) return !1;
            var d = c && "left" === c ? "scrollLeft" : "scrollTop", e = !1;
            return 0 < b[d] || (b[d] = 1, e = 0 < b[d], b[d] = 0, e);
        },
        _create: function() {
            var b, c = this.options, d = this;
            this._addClass("ui-resizable"), a.extend(this, {
                _aspectRatio: !!c.aspectRatio,
                aspectRatio: c.aspectRatio,
                originalElement: this.element,
                _proportionallyResizeElements: [],
                _helper: c.helper || c.ghost || c.animate ? c.helper || "ui-resizable-helper" : null
            }), this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i) && (this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                position: this.element.css("position"),
                width: this.element.outerWidth(),
                height: this.element.outerHeight(),
                top: this.element.css("top"),
                left: this.element.css("left")
            })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), 
            this.elementIsWrapper = !0, b = {
                marginTop: this.originalElement.css("marginTop"),
                marginRight: this.originalElement.css("marginRight"),
                marginBottom: this.originalElement.css("marginBottom"),
                marginLeft: this.originalElement.css("marginLeft")
            }, this.element.css(b), this.originalElement.css("margin", 0), this.originalResizeStyle = this.originalElement.css("resize"), 
            this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                position: "static",
                zoom: 1,
                display: "block"
            })), this.originalElement.css(b), this._proportionallyResize()), this._setupHandles(), 
            c.autoHide && a(this.element).on("mouseenter", function() {
                c.disabled || (d._removeClass("ui-resizable-autohide"), d._handles.show());
            }).on("mouseleave", function() {
                c.disabled || d.resizing || (d._addClass("ui-resizable-autohide"), d._handles.hide());
            }), this._mouseInit();
        },
        _destroy: function() {
            this._mouseDestroy();
            function b(b) {
                a(b).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove();
            }
            var c;
            return this.elementIsWrapper && (b(this.element), c = this.element, this.originalElement.css({
                position: c.css("position"),
                width: c.outerWidth(),
                height: c.outerHeight(),
                top: c.css("top"),
                left: c.css("left")
            }).insertAfter(c), c.remove()), this.originalElement.css("resize", this.originalResizeStyle), 
            b(this.originalElement), this;
        },
        _setOption: function(a, b) {
            switch (this._super(a, b), a) {
              case "handles":
                this._removeHandles(), this._setupHandles();
            }
        },
        _setupHandles: function() {
            var b, c, d, e, f, g = this.options, h = this;
            if (this.handles = g.handles || (a(".ui-resizable-handle", this.element).length ? {
                n: ".ui-resizable-n",
                e: ".ui-resizable-e",
                s: ".ui-resizable-s",
                w: ".ui-resizable-w",
                se: ".ui-resizable-se",
                sw: ".ui-resizable-sw",
                ne: ".ui-resizable-ne",
                nw: ".ui-resizable-nw"
            } : "e,s,se"), this._handles = a(), this.handles.constructor === String) for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), 
            d = this.handles.split(","), this.handles = {}, c = 0; d.length > c; c++) e = "ui-resizable-" + (b = a.trim(d[c])), 
            f = a("<div>"), this._addClass(f, "ui-resizable-handle " + e), f.css({
                zIndex: g.zIndex
            }), this.handles[b] = ".ui-resizable-" + b, this.element.append(f);
            this._renderAxis = function(b) {
                var c, d, e, f;
                for (c in b = b || this.element, this.handles) this.handles[c].constructor === String ? this.handles[c] = this.element.children(this.handles[c]).first().show() : (this.handles[c].jquery || this.handles[c].nodeType) && (this.handles[c] = a(this.handles[c]), 
                this._on(this.handles[c], {
                    mousedown: h._mouseDown
                })), this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i) && (d = a(this.handles[c], this.element), 
                f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth(), e = [ "padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left" ].join(""), 
                b.css(e, f), this._proportionallyResize()), this._handles = this._handles.add(this.handles[c]);
            }, this._renderAxis(this.element), this._handles = this._handles.add(this.element.find(".ui-resizable-handle")), 
            this._handles.disableSelection(), this._handles.on("mouseover", function() {
                h.resizing || (this.className && (f = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), 
                h.axis = f && f[1] ? f[1] : "se");
            }), g.autoHide && (this._handles.hide(), this._addClass("ui-resizable-autohide"));
        },
        _removeHandles: function() {
            this._handles.remove();
        },
        _mouseCapture: function(b) {
            var c, d, e = !1;
            for (c in this.handles) (d = a(this.handles[c])[0]) !== b.target && !a.contains(d, b.target) || (e = !0);
            return !this.options.disabled && e;
        },
        _mouseStart: function(b) {
            var c, d, e, f = this.options, g = this.element;
            return this.resizing = !0, this._renderProxy(), c = this._num(this.helper.css("left")), 
            d = this._num(this.helper.css("top")), f.containment && (c += a(f.containment).scrollLeft() || 0, 
            d += a(f.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: c,
                top: d
            }, this.size = this._helper ? {
                width: this.helper.width(),
                height: this.helper.height()
            } : {
                width: g.width(),
                height: g.height()
            }, this.originalSize = this._helper ? {
                width: g.outerWidth(),
                height: g.outerHeight()
            } : {
                width: g.width(),
                height: g.height()
            }, this.sizeDiff = {
                width: g.outerWidth() - g.width(),
                height: g.outerHeight() - g.height()
            }, this.originalPosition = {
                left: c,
                top: d
            }, this.originalMousePosition = {
                left: b.pageX,
                top: b.pageY
            }, this.aspectRatio = "number" == typeof f.aspectRatio ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1, 
            e = a(".ui-resizable-" + this.axis).css("cursor"), a("body").css("cursor", "auto" === e ? this.axis + "-resize" : e), 
            this._addClass("ui-resizable-resizing"), this._propagate("start", b), !0;
        },
        _mouseDrag: function(b) {
            var c, d, e = this.originalMousePosition, f = this.axis, g = b.pageX - e.left || 0, h = b.pageY - e.top || 0, i = this._change[f];
            return this._updatePrevProperties(), i && (c = i.apply(this, [ b, g, h ]), this._updateVirtualBoundaries(b.shiftKey), 
            (this._aspectRatio || b.shiftKey) && (c = this._updateRatio(c, b)), c = this._respectSize(c, b), 
            this._updateCache(c), this._propagate("resize", b), d = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), 
            a.isEmptyObject(d) || (this._updatePrevProperties(), this._trigger("resize", b, this.ui()), 
            this._applyChanges())), !1;
        },
        _mouseStop: function(b) {
            this.resizing = !1;
            var c, d, e, f, g, h, i, j = this.options, k = this;
            return this._helper && (e = (d = (c = this._proportionallyResizeElements).length && /textarea/i.test(c[0].nodeName)) && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height, 
            f = d ? 0 : k.sizeDiff.width, g = {
                width: k.helper.width() - f,
                height: k.helper.height() - e
            }, h = parseFloat(k.element.css("left")) + (k.position.left - k.originalPosition.left) || null, 
            i = parseFloat(k.element.css("top")) + (k.position.top - k.originalPosition.top) || null, 
            j.animate || this.element.css(a.extend(g, {
                top: i,
                left: h
            })), k.helper.height(k.size.height), k.helper.width(k.size.width), this._helper && !j.animate && this._proportionallyResize()), 
            a("body").css("cursor", "auto"), this._removeClass("ui-resizable-resizing"), this._propagate("stop", b), 
            this._helper && this.helper.remove(), !1;
        },
        _updatePrevProperties: function() {
            this.prevPosition = {
                top: this.position.top,
                left: this.position.left
            }, this.prevSize = {
                width: this.size.width,
                height: this.size.height
            };
        },
        _applyChanges: function() {
            var a = {};
            return this.position.top !== this.prevPosition.top && (a.top = this.position.top + "px"), 
            this.position.left !== this.prevPosition.left && (a.left = this.position.left + "px"), 
            this.size.width !== this.prevSize.width && (a.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (a.height = this.size.height + "px"), 
            this.helper.css(a), a;
        },
        _updateVirtualBoundaries: function(a) {
            var b, c, d, e, f, g = this.options;
            f = {
                minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0,
                maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : 1 / 0,
                minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0,
                maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : 1 / 0
            }, (this._aspectRatio || a) && (b = f.minHeight * this.aspectRatio, d = f.minWidth / this.aspectRatio, 
            c = f.maxHeight * this.aspectRatio, e = f.maxWidth / this.aspectRatio, b > f.minWidth && (f.minWidth = b), 
            d > f.minHeight && (f.minHeight = d), f.maxWidth > c && (f.maxWidth = c), f.maxHeight > e && (f.maxHeight = e)), 
            this._vBoundaries = f;
        },
        _updateCache: function(a) {
            this.offset = this.helper.offset(), this._isNumber(a.left) && (this.position.left = a.left), 
            this._isNumber(a.top) && (this.position.top = a.top), this._isNumber(a.height) && (this.size.height = a.height), 
            this._isNumber(a.width) && (this.size.width = a.width);
        },
        _updateRatio: function(a) {
            var b = this.position, c = this.size, d = this.axis;
            return this._isNumber(a.height) ? a.width = a.height * this.aspectRatio : this._isNumber(a.width) && (a.height = a.width / this.aspectRatio), 
            "sw" === d && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === d && (a.top = b.top + (c.height - a.height), 
            a.left = b.left + (c.width - a.width)), a;
        },
        _respectSize: function(a) {
            var b = this._vBoundaries, c = this.axis, d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width, e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height, f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width, g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height, h = this.originalPosition.left + this.originalSize.width, i = this.originalPosition.top + this.originalSize.height, j = /sw|nw|w/.test(c), k = /nw|ne|n/.test(c);
            return f && (a.width = b.minWidth), g && (a.height = b.minHeight), d && (a.width = b.maxWidth), 
            e && (a.height = b.maxHeight), f && j && (a.left = h - b.minWidth), d && j && (a.left = h - b.maxWidth), 
            g && k && (a.top = i - b.minHeight), e && k && (a.top = i - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, 
            a;
        },
        _getPaddingPlusBorderDimensions: function(a) {
            for (var b = 0, c = [], d = [ a.css("borderTopWidth"), a.css("borderRightWidth"), a.css("borderBottomWidth"), a.css("borderLeftWidth") ], e = [ a.css("paddingTop"), a.css("paddingRight"), a.css("paddingBottom"), a.css("paddingLeft") ]; b < 4; b++) c[b] = parseFloat(d[b]) || 0, 
            c[b] += parseFloat(e[b]) || 0;
            return {
                height: c[0] + c[2],
                width: c[1] + c[3]
            };
        },
        _proportionallyResize: function() {
            if (this._proportionallyResizeElements.length) for (var a, b = 0, c = this.helper || this.element; this._proportionallyResizeElements.length > b; b++) a = this._proportionallyResizeElements[b], 
            this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(a)), 
            a.css({
                height: c.height() - this.outerDimensions.height || 0,
                width: c.width() - this.outerDimensions.width || 0
            });
        },
        _renderProxy: function() {
            var b = this.element, c = this.options;
            this.elementOffset = b.offset(), this._helper ? (this.helper = this.helper || a("<div style='overflow:hidden;'></div>"), 
            this._addClass(this.helper, this._helper), this.helper.css({
                width: this.element.outerWidth(),
                height: this.element.outerHeight(),
                position: "absolute",
                left: this.elementOffset.left + "px",
                top: this.elementOffset.top + "px",
                zIndex: ++c.zIndex
            }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element;
        },
        _change: {
            e: function(a, b) {
                return {
                    width: this.originalSize.width + b
                };
            },
            w: function(a, b) {
                var c = this.originalSize;
                return {
                    left: this.originalPosition.left + b,
                    width: c.width - b
                };
            },
            n: function(a, b, c) {
                var d = this.originalSize;
                return {
                    top: this.originalPosition.top + c,
                    height: d.height - c
                };
            },
            s: function(a, b, c) {
                return {
                    height: this.originalSize.height + c
                };
            },
            se: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [ b, c, d ]));
            },
            sw: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [ b, c, d ]));
            },
            ne: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [ b, c, d ]));
            },
            nw: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [ b, c, d ]));
            }
        },
        _propagate: function(b, c) {
            a.ui.plugin.call(this, b, [ c, this.ui() ]), "resize" !== b && this._trigger(b, c, this.ui());
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            };
        }
    }), a.ui.plugin.add("resizable", "animate", {
        stop: function(b) {
            var c = a(this).resizable("instance"), d = c.options, e = c._proportionallyResizeElements, f = e.length && /textarea/i.test(e[0].nodeName), g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height, h = f ? 0 : c.sizeDiff.width, i = {
                width: c.size.width - h,
                height: c.size.height - g
            }, j = parseFloat(c.element.css("left")) + (c.position.left - c.originalPosition.left) || null, k = parseFloat(c.element.css("top")) + (c.position.top - c.originalPosition.top) || null;
            c.element.animate(a.extend(i, k && j ? {
                top: k,
                left: j
            } : {}), {
                duration: d.animateDuration,
                easing: d.animateEasing,
                step: function() {
                    var d = {
                        width: parseFloat(c.element.css("width")),
                        height: parseFloat(c.element.css("height")),
                        top: parseFloat(c.element.css("top")),
                        left: parseFloat(c.element.css("left"))
                    };
                    e && e.length && a(e[0]).css({
                        width: d.width,
                        height: d.height
                    }), c._updateCache(d), c._propagate("resize", b);
                }
            });
        }
    }), a.ui.plugin.add("resizable", "containment", {
        start: function() {
            var b, c, d, e, f, g, h, i = a(this).resizable("instance"), j = i.options, k = i.element, l = j.containment, m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l;
            m && (i.containerElement = a(m), /document/.test(l) || l === document ? (i.containerOffset = {
                left: 0,
                top: 0
            }, i.containerPosition = {
                left: 0,
                top: 0
            }, i.parentData = {
                element: a(document),
                left: 0,
                top: 0,
                width: a(document).width(),
                height: a(document).height() || document.body.parentNode.scrollHeight
            }) : (b = a(m), c = [], a([ "Top", "Right", "Left", "Bottom" ]).each(function(a, d) {
                c[a] = i._num(b.css("padding" + d));
            }), i.containerOffset = b.offset(), i.containerPosition = b.position(), i.containerSize = {
                height: b.innerHeight() - c[3],
                width: b.innerWidth() - c[1]
            }, d = i.containerOffset, e = i.containerSize.height, f = i.containerSize.width, 
            g = i._hasScroll(m, "left") ? m.scrollWidth : f, h = i._hasScroll(m) ? m.scrollHeight : e, 
            i.parentData = {
                element: m,
                left: d.left,
                top: d.top,
                width: g,
                height: h
            }));
        },
        resize: function(b) {
            var c, d, e, f, g = a(this).resizable("instance"), h = g.options, i = g.containerOffset, j = g.position, k = g._aspectRatio || b.shiftKey, l = {
                top: 0,
                left: 0
            }, m = g.containerElement, n = !0;
            m[0] !== document && /static/.test(m.css("position")) && (l = i), j.left < (g._helper ? i.left : 0) && (g.size.width = g.size.width + (g._helper ? g.position.left - i.left : g.position.left - l.left), 
            k && (g.size.height = g.size.width / g.aspectRatio, n = !1), g.position.left = h.helper ? i.left : 0), 
            j.top < (g._helper ? i.top : 0) && (g.size.height = g.size.height + (g._helper ? g.position.top - i.top : g.position.top), 
            k && (g.size.width = g.size.height * g.aspectRatio, n = !1), g.position.top = g._helper ? i.top : 0), 
            e = g.containerElement.get(0) === g.element.parent().get(0), f = /relative|absolute/.test(g.containerElement.css("position")), 
            e && f ? (g.offset.left = g.parentData.left + g.position.left, g.offset.top = g.parentData.top + g.position.top) : (g.offset.left = g.element.offset().left, 
            g.offset.top = g.element.offset().top), c = Math.abs(g.sizeDiff.width + (g._helper ? g.offset.left - l.left : g.offset.left - i.left)), 
            d = Math.abs(g.sizeDiff.height + (g._helper ? g.offset.top - l.top : g.offset.top - i.top)), 
            c + g.size.width >= g.parentData.width && (g.size.width = g.parentData.width - c, 
            k && (g.size.height = g.size.width / g.aspectRatio, n = !1)), d + g.size.height >= g.parentData.height && (g.size.height = g.parentData.height - d, 
            k && (g.size.width = g.size.height * g.aspectRatio, n = !1)), n || (g.position.left = g.prevPosition.left, 
            g.position.top = g.prevPosition.top, g.size.width = g.prevSize.width, g.size.height = g.prevSize.height);
        },
        stop: function() {
            var b = a(this).resizable("instance"), c = b.options, d = b.containerOffset, e = b.containerPosition, f = b.containerElement, g = a(b.helper), h = g.offset(), i = g.outerWidth() - b.sizeDiff.width, j = g.outerHeight() - b.sizeDiff.height;
            b._helper && !c.animate && /relative/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            }), b._helper && !c.animate && /static/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            });
        }
    }), a.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var b = a(this).resizable("instance").options;
            a(b.alsoResize).each(function() {
                var b = a(this);
                b.data("ui-resizable-alsoresize", {
                    width: parseFloat(b.width()),
                    height: parseFloat(b.height()),
                    left: parseFloat(b.css("left")),
                    top: parseFloat(b.css("top"))
                });
            });
        },
        resize: function(b, c) {
            var d = a(this).resizable("instance"), e = d.options, f = d.originalSize, g = d.originalPosition, h = {
                height: d.size.height - f.height || 0,
                width: d.size.width - f.width || 0,
                top: d.position.top - g.top || 0,
                left: d.position.left - g.left || 0
            };
            a(e.alsoResize).each(function() {
                var b = a(this), d = a(this).data("ui-resizable-alsoresize"), e = {}, f = b.parents(c.originalElement[0]).length ? [ "width", "height" ] : [ "width", "height", "top", "left" ];
                a.each(f, function(a, b) {
                    var c = (d[b] || 0) + (h[b] || 0);
                    c && 0 <= c && (e[b] = c || null);
                }), b.css(e);
            });
        },
        stop: function() {
            a(this).removeData("ui-resizable-alsoresize");
        }
    }), a.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var b = a(this).resizable("instance"), c = b.size;
            b.ghost = b.originalElement.clone(), b.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: c.height,
                width: c.width,
                margin: 0,
                left: 0,
                top: 0
            }), b._addClass(b.ghost, "ui-resizable-ghost"), !1 !== a.uiBackCompat && "string" == typeof b.options.ghost && b.ghost.addClass(this.options.ghost), 
            b.ghost.appendTo(b.helper);
        },
        resize: function() {
            var b = a(this).resizable("instance");
            b.ghost && b.ghost.css({
                position: "relative",
                height: b.size.height,
                width: b.size.width
            });
        },
        stop: function() {
            var b = a(this).resizable("instance");
            b.ghost && b.helper && b.helper.get(0).removeChild(b.ghost.get(0));
        }
    }), a.ui.plugin.add("resizable", "grid", {
        resize: function() {
            var b, c = a(this).resizable("instance"), d = c.options, e = c.size, f = c.originalSize, g = c.originalPosition, h = c.axis, i = "number" == typeof d.grid ? [ d.grid, d.grid ] : d.grid, j = i[0] || 1, k = i[1] || 1, l = Math.round((e.width - f.width) / j) * j, m = Math.round((e.height - f.height) / k) * k, n = f.width + l, o = f.height + m, p = d.maxWidth && n > d.maxWidth, q = d.maxHeight && o > d.maxHeight, r = d.minWidth && d.minWidth > n, s = d.minHeight && d.minHeight > o;
            d.grid = i, r && (n += j), s && (o += k), p && (n -= j), q && (o -= k), /^(se|s|e)$/.test(h) ? (c.size.width = n, 
            c.size.height = o) : /^(ne)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.top = g.top - m) : /^(sw)$/.test(h) ? (c.size.width = n, 
            c.size.height = o, c.position.left = g.left - l) : ((o - k <= 0 || n - j <= 0) && (b = c._getPaddingPlusBorderDimensions(this)), 
            0 < o - k ? (c.size.height = o, c.position.top = g.top - m) : (o = k - b.height, 
            c.size.height = o, c.position.top = g.top + f.height - o), 0 < n - j ? (c.size.width = n, 
            c.position.left = g.left - l) : (n = j - b.width, c.size.width = n, c.position.left = g.left + f.width - n));
        }
    }), a.ui.resizable, a.widget("ui.dialog", {
        version: "1.12.1",
        options: {
            appendTo: "body",
            autoOpen: !0,
            buttons: [],
            classes: {
                "ui-dialog": "ui-corner-all",
                "ui-dialog-titlebar": "ui-corner-all"
            },
            closeOnEscape: !0,
            closeText: "Close",
            draggable: !0,
            hide: null,
            height: "auto",
            maxHeight: null,
            maxWidth: null,
            minHeight: 150,
            minWidth: 150,
            modal: !1,
            position: {
                my: "center",
                at: "center",
                of: window,
                collision: "fit",
                using: function(b) {
                    var c = a(this).css(b).offset().top;
                    c < 0 && a(this).css("top", b.top - c);
                }
            },
            resizable: !0,
            show: null,
            title: null,
            width: 300,
            beforeClose: null,
            close: null,
            drag: null,
            dragStart: null,
            dragStop: null,
            focus: null,
            open: null,
            resize: null,
            resizeStart: null,
            resizeStop: null
        },
        sizeRelatedOptions: {
            buttons: !0,
            height: !0,
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0,
            width: !0
        },
        resizableRelatedOptions: {
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0
        },
        _create: function() {
            this.originalCss = {
                display: this.element[0].style.display,
                width: this.element[0].style.width,
                minHeight: this.element[0].style.minHeight,
                maxHeight: this.element[0].style.maxHeight,
                height: this.element[0].style.height
            }, this.originalPosition = {
                parent: this.element.parent(),
                index: this.element.parent().children().index(this.element)
            }, this.originalTitle = this.element.attr("title"), null == this.options.title && null != this.originalTitle && (this.options.title = this.originalTitle), 
            this.options.disabled && (this.options.disabled = !1), this._createWrapper(), this.element.show().removeAttr("title").appendTo(this.uiDialog), 
            this._addClass("ui-dialog-content", "ui-widget-content"), this._createTitlebar(), 
            this._createButtonPane(), this.options.draggable && a.fn.draggable && this._makeDraggable(), 
            this.options.resizable && a.fn.resizable && this._makeResizable(), this._isOpen = !1, 
            this._trackFocus();
        },
        _init: function() {
            this.options.autoOpen && this.open();
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            return b && (b.jquery || b.nodeType) ? a(b) : this.document.find(b || "body").eq(0);
        },
        _destroy: function() {
            var a, b = this.originalPosition;
            this._untrackInstance(), this._destroyOverlay(), this.element.removeUniqueId().css(this.originalCss).detach(), 
            this.uiDialog.remove(), this.originalTitle && this.element.attr("title", this.originalTitle), 
            (a = b.parent.children().eq(b.index)).length && a[0] !== this.element[0] ? a.before(this.element) : b.parent.append(this.element);
        },
        widget: function() {
            return this.uiDialog;
        },
        disable: a.noop,
        enable: a.noop,
        close: function(b) {
            var c = this;
            this._isOpen && !1 !== this._trigger("beforeClose", b) && (this._isOpen = !1, this._focusedElement = null, 
            this._destroyOverlay(), this._untrackInstance(), this.opener.filter(":focusable").trigger("focus").length || a.ui.safeBlur(a.ui.safeActiveElement(this.document[0])), 
            this._hide(this.uiDialog, this.options.hide, function() {
                c._trigger("close", b);
            }));
        },
        isOpen: function() {
            return this._isOpen;
        },
        moveToTop: function() {
            this._moveToTop();
        },
        _moveToTop: function(b, c) {
            var d = !1, e = this.uiDialog.siblings(".ui-front:visible").map(function() {
                return +a(this).css("z-index");
            }).get(), f = Math.max.apply(null, e);
            return f >= +this.uiDialog.css("z-index") && (this.uiDialog.css("z-index", f + 1), 
            d = !0), d && !c && this._trigger("focus", b), d;
        },
        open: function() {
            var b = this;
            return this._isOpen ? void (this._moveToTop() && this._focusTabbable()) : (this._isOpen = !0, 
            this.opener = a(a.ui.safeActiveElement(this.document[0])), this._size(), this._position(), 
            this._createOverlay(), this._moveToTop(null, !0), this.overlay && this.overlay.css("z-index", this.uiDialog.css("z-index") - 1), 
            this._show(this.uiDialog, this.options.show, function() {
                b._focusTabbable(), b._trigger("focus");
            }), this._makeFocusTarget(), void this._trigger("open"));
        },
        _focusTabbable: function() {
            var a = this._focusedElement;
            (a = a || this.element.find("[autofocus]")).length || (a = this.element.find(":tabbable")), 
            a.length || (a = this.uiDialogButtonPane.find(":tabbable")), a.length || (a = this.uiDialogTitlebarClose.filter(":tabbable")), 
            a.length || (a = this.uiDialog), a.eq(0).trigger("focus");
        },
        _keepFocus: function(b) {
            function c() {
                var b = a.ui.safeActiveElement(this.document[0]);
                this.uiDialog[0] === b || a.contains(this.uiDialog[0], b) || this._focusTabbable();
            }
            b.preventDefault(), c.call(this), this._delay(c);
        },
        _createWrapper: function() {
            this.uiDialog = a("<div>").hide().attr({
                tabIndex: -1,
                role: "dialog"
            }).appendTo(this._appendTo()), this._addClass(this.uiDialog, "ui-dialog", "ui-widget ui-widget-content ui-front"), 
            this._on(this.uiDialog, {
                keydown: function(b) {
                    if (this.options.closeOnEscape && !b.isDefaultPrevented() && b.keyCode && b.keyCode === a.ui.keyCode.ESCAPE) return b.preventDefault(), 
                    void this.close(b);
                    if (b.keyCode === a.ui.keyCode.TAB && !b.isDefaultPrevented()) {
                        var c = this.uiDialog.find(":tabbable"), d = c.filter(":first"), e = c.filter(":last");
                        b.target !== e[0] && b.target !== this.uiDialog[0] || b.shiftKey ? b.target !== d[0] && b.target !== this.uiDialog[0] || !b.shiftKey || (this._delay(function() {
                            e.trigger("focus");
                        }), b.preventDefault()) : (this._delay(function() {
                            d.trigger("focus");
                        }), b.preventDefault());
                    }
                },
                mousedown: function(a) {
                    this._moveToTop(a) && this._focusTabbable();
                }
            }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({
                "aria-describedby": this.element.uniqueId().attr("id")
            });
        },
        _createTitlebar: function() {
            var b;
            this.uiDialogTitlebar = a("<div>"), this._addClass(this.uiDialogTitlebar, "ui-dialog-titlebar", "ui-widget-header ui-helper-clearfix"), 
            this._on(this.uiDialogTitlebar, {
                mousedown: function(b) {
                    a(b.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.trigger("focus");
                }
            }), this.uiDialogTitlebarClose = a("<button type='button'></button>").button({
                label: a("<a>").text(this.options.closeText).html(),
                icon: "ui-icon-closethick",
                showLabel: !1
            }).appendTo(this.uiDialogTitlebar), this._addClass(this.uiDialogTitlebarClose, "ui-dialog-titlebar-close"), 
            this._on(this.uiDialogTitlebarClose, {
                click: function(a) {
                    a.preventDefault(), this.close(a);
                }
            }), b = a("<span>").uniqueId().prependTo(this.uiDialogTitlebar), this._addClass(b, "ui-dialog-title"), 
            this._title(b), this.uiDialogTitlebar.prependTo(this.uiDialog), this.uiDialog.attr({
                "aria-labelledby": b.attr("id")
            });
        },
        _title: function(a) {
            this.options.title ? a.text(this.options.title) : a.html("&#160;");
        },
        _createButtonPane: function() {
            this.uiDialogButtonPane = a("<div>"), this._addClass(this.uiDialogButtonPane, "ui-dialog-buttonpane", "ui-widget-content ui-helper-clearfix"), 
            this.uiButtonSet = a("<div>").appendTo(this.uiDialogButtonPane), this._addClass(this.uiButtonSet, "ui-dialog-buttonset"), 
            this._createButtons();
        },
        _createButtons: function() {
            var b = this, c = this.options.buttons;
            return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), a.isEmptyObject(c) || a.isArray(c) && !c.length ? void this._removeClass(this.uiDialog, "ui-dialog-buttons") : (a.each(c, function(c, d) {
                var e, f;
                d = a.isFunction(d) ? {
                    click: d,
                    text: c
                } : d, d = a.extend({
                    type: "button"
                }, d), e = d.click, f = {
                    icon: d.icon,
                    iconPosition: d.iconPosition,
                    showLabel: d.showLabel,
                    icons: d.icons,
                    text: d.text
                }, delete d.click, delete d.icon, delete d.iconPosition, delete d.showLabel, delete d.icons, 
                "boolean" == typeof d.text && delete d.text, a("<button></button>", d).button(f).appendTo(b.uiButtonSet).on("click", function() {
                    e.apply(b.element[0], arguments);
                });
            }), this._addClass(this.uiDialog, "ui-dialog-buttons"), void this.uiDialogButtonPane.appendTo(this.uiDialog));
        },
        _makeDraggable: function() {
            function b(a) {
                return {
                    position: a.position,
                    offset: a.offset
                };
            }
            var c = this, d = this.options;
            this.uiDialog.draggable({
                cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function(d, e) {
                    c._addClass(a(this), "ui-dialog-dragging"), c._blockFrames(), c._trigger("dragStart", d, b(e));
                },
                drag: function(a, d) {
                    c._trigger("drag", a, b(d));
                },
                stop: function(e, f) {
                    var g = f.offset.left - c.document.scrollLeft(), h = f.offset.top - c.document.scrollTop();
                    d.position = {
                        my: "left top",
                        at: "left" + (0 <= g ? "+" : "") + g + " top" + (0 <= h ? "+" : "") + h,
                        of: c.window
                    }, c._removeClass(a(this), "ui-dialog-dragging"), c._unblockFrames(), c._trigger("dragStop", e, b(f));
                }
            });
        },
        _makeResizable: function() {
            function b(a) {
                return {
                    originalPosition: a.originalPosition,
                    originalSize: a.originalSize,
                    position: a.position,
                    size: a.size
                };
            }
            var c = this, d = this.options, e = d.resizable, f = this.uiDialog.css("position"), g = "string" == typeof e ? e : "n,e,s,w,se,sw,ne,nw";
            this.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                containment: "document",
                alsoResize: this.element,
                maxWidth: d.maxWidth,
                maxHeight: d.maxHeight,
                minWidth: d.minWidth,
                minHeight: this._minHeight(),
                handles: g,
                start: function(d, e) {
                    c._addClass(a(this), "ui-dialog-resizing"), c._blockFrames(), c._trigger("resizeStart", d, b(e));
                },
                resize: function(a, d) {
                    c._trigger("resize", a, b(d));
                },
                stop: function(e, f) {
                    var g = c.uiDialog.offset(), h = g.left - c.document.scrollLeft(), i = g.top - c.document.scrollTop();
                    d.height = c.uiDialog.height(), d.width = c.uiDialog.width(), d.position = {
                        my: "left top",
                        at: "left" + (0 <= h ? "+" : "") + h + " top" + (0 <= i ? "+" : "") + i,
                        of: c.window
                    }, c._removeClass(a(this), "ui-dialog-resizing"), c._unblockFrames(), c._trigger("resizeStop", e, b(f));
                }
            }).css("position", f);
        },
        _trackFocus: function() {
            this._on(this.widget(), {
                focusin: function(b) {
                    this._makeFocusTarget(), this._focusedElement = a(b.target);
                }
            });
        },
        _makeFocusTarget: function() {
            this._untrackInstance(), this._trackingInstances().unshift(this);
        },
        _untrackInstance: function() {
            var b = this._trackingInstances(), c = a.inArray(this, b);
            -1 !== c && b.splice(c, 1);
        },
        _trackingInstances: function() {
            var a = this.document.data("ui-dialog-instances");
            return a || (a = [], this.document.data("ui-dialog-instances", a)), a;
        },
        _minHeight: function() {
            var a = this.options;
            return "auto" === a.height ? a.minHeight : Math.min(a.minHeight, a.height);
        },
        _position: function() {
            var a = this.uiDialog.is(":visible");
            a || this.uiDialog.show(), this.uiDialog.position(this.options.position), a || this.uiDialog.hide();
        },
        _setOptions: function(b) {
            var c = this, d = !1, e = {};
            a.each(b, function(a, b) {
                c._setOption(a, b), a in c.sizeRelatedOptions && (d = !0), a in c.resizableRelatedOptions && (e[a] = b);
            }), d && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", e);
        },
        _setOption: function(b, c) {
            var d, e, f = this.uiDialog;
            "disabled" !== b && (this._super(b, c), "appendTo" === b && this.uiDialog.appendTo(this._appendTo()), 
            "buttons" === b && this._createButtons(), "closeText" === b && this.uiDialogTitlebarClose.button({
                label: a("<a>").text("" + this.options.closeText).html()
            }), "draggable" === b && ((d = f.is(":data(ui-draggable)")) && !c && f.draggable("destroy"), 
            !d && c && this._makeDraggable()), "position" === b && this._position(), "resizable" === b && ((e = f.is(":data(ui-resizable)")) && !c && f.resizable("destroy"), 
            e && "string" == typeof c && f.resizable("option", "handles", c), e || !1 === c || this._makeResizable()), 
            "title" === b && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")));
        },
        _size: function() {
            var a, b, c, d = this.options;
            this.element.show().css({
                width: "auto",
                minHeight: 0,
                maxHeight: "none",
                height: 0
            }), d.minWidth > d.width && (d.width = d.minWidth), a = this.uiDialog.css({
                height: "auto",
                width: d.width
            }).outerHeight(), b = Math.max(0, d.minHeight - a), c = "number" == typeof d.maxHeight ? Math.max(0, d.maxHeight - a) : "none", 
            "auto" === d.height ? this.element.css({
                minHeight: b,
                maxHeight: c,
                height: "auto"
            }) : this.element.height(Math.max(0, d.height - a)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight());
        },
        _blockFrames: function() {
            this.iframeBlocks = this.document.find("iframe").map(function() {
                var b = a(this);
                return a("<div>").css({
                    position: "absolute",
                    width: b.outerWidth(),
                    height: b.outerHeight()
                }).appendTo(b.parent()).offset(b.offset())[0];
            });
        },
        _unblockFrames: function() {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
        },
        _allowInteraction: function(b) {
            return !!a(b.target).closest(".ui-dialog").length || !!a(b.target).closest(".ui-datepicker").length;
        },
        _createOverlay: function() {
            if (this.options.modal) {
                var b = !0;
                this._delay(function() {
                    b = !1;
                }), this.document.data("ui-dialog-overlays") || this._on(this.document, {
                    focusin: function(a) {
                        b || this._allowInteraction(a) || (a.preventDefault(), this._trackingInstances()[0]._focusTabbable());
                    }
                }), this.overlay = a("<div>").appendTo(this._appendTo()), this._addClass(this.overlay, null, "ui-widget-overlay ui-front"), 
                this._on(this.overlay, {
                    mousedown: "_keepFocus"
                }), this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1);
            }
        },
        _destroyOverlay: function() {
            if (this.options.modal && this.overlay) {
                var a = this.document.data("ui-dialog-overlays") - 1;
                a ? this.document.data("ui-dialog-overlays", a) : (this._off(this.document, "focusin"), 
                this.document.removeData("ui-dialog-overlays")), this.overlay.remove(), this.overlay = null;
            }
        }
    }), !1 !== a.uiBackCompat && a.widget("ui.dialog", a.ui.dialog, {
        options: {
            dialogClass: ""
        },
        _createWrapper: function() {
            this._super(), this.uiDialog.addClass(this.options.dialogClass);
        },
        _setOption: function(a, b) {
            "dialogClass" === a && this.uiDialog.removeClass(this.options.dialogClass).addClass(b), 
            this._superApply(arguments);
        }
    }), a.ui.dialog, a.widget("ui.droppable", {
        version: "1.12.1",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            addClasses: !0,
            greedy: !1,
            scope: "default",
            tolerance: "intersect",
            activate: null,
            deactivate: null,
            drop: null,
            out: null,
            over: null
        },
        _create: function() {
            var b, c = this.options, d = c.accept;
            this.isover = !1, this.isout = !0, this.accept = a.isFunction(d) ? d : function(a) {
                return a.is(d);
            }, this.proportions = function() {
                return arguments.length ? void (b = arguments[0]) : b || (b = {
                    width: this.element[0].offsetWidth,
                    height: this.element[0].offsetHeight
                });
            }, this._addToManager(c.scope), c.addClasses && this._addClass("ui-droppable");
        },
        _addToManager: function(b) {
            a.ui.ddmanager.droppables[b] = a.ui.ddmanager.droppables[b] || [], a.ui.ddmanager.droppables[b].push(this);
        },
        _splice: function(a) {
            for (var b = 0; a.length > b; b++) a[b] === this && a.splice(b, 1);
        },
        _destroy: function() {
            var b = a.ui.ddmanager.droppables[this.options.scope];
            this._splice(b);
        },
        _setOption: function(b, c) {
            if ("accept" === b) this.accept = a.isFunction(c) ? c : function(a) {
                return a.is(c);
            }; else if ("scope" === b) {
                var d = a.ui.ddmanager.droppables[this.options.scope];
                this._splice(d), this._addToManager(c);
            }
            this._super(b, c);
        },
        _activate: function(b) {
            var c = a.ui.ddmanager.current;
            this._addActiveClass(), c && this._trigger("activate", b, this.ui(c));
        },
        _deactivate: function(b) {
            var c = a.ui.ddmanager.current;
            this._removeActiveClass(), c && this._trigger("deactivate", b, this.ui(c));
        },
        _over: function(b) {
            var c = a.ui.ddmanager.current;
            c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this._addHoverClass(), 
            this._trigger("over", b, this.ui(c)));
        },
        _out: function(b) {
            var c = a.ui.ddmanager.current;
            c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this._removeHoverClass(), 
            this._trigger("out", b, this.ui(c)));
        },
        _drop: function(b, c) {
            var d = c || a.ui.ddmanager.current, e = !1;
            return !(!d || (d.currentItem || d.element)[0] === this.element[0]) && (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                var c = a(this).droppable("instance");
                return c.options.greedy && !c.options.disabled && c.options.scope === d.options.scope && c.accept.call(c.element[0], d.currentItem || d.element) && gb(d, a.extend(c, {
                    offset: c.element.offset()
                }), c.options.tolerance, b) ? !(e = !0) : void 0;
            }), !e && !!this.accept.call(this.element[0], d.currentItem || d.element) && (this._removeActiveClass(), 
            this._removeHoverClass(), this._trigger("drop", b, this.ui(d)), this.element));
        },
        ui: function(a) {
            return {
                draggable: a.currentItem || a.element,
                helper: a.helper,
                position: a.position,
                offset: a.positionAbs
            };
        },
        _addHoverClass: function() {
            this._addClass("ui-droppable-hover");
        },
        _removeHoverClass: function() {
            this._removeClass("ui-droppable-hover");
        },
        _addActiveClass: function() {
            this._addClass("ui-droppable-active");
        },
        _removeActiveClass: function() {
            this._removeClass("ui-droppable-active");
        }
    });
    var fb, gb = a.ui.intersect = function(a, b, c, d) {
        if (!b.offset) return !1;
        var e = (a.positionAbs || a.position.absolute).left + a.margins.left, f = (a.positionAbs || a.position.absolute).top + a.margins.top, g = e + a.helperProportions.width, h = f + a.helperProportions.height, i = b.offset.left, j = b.offset.top, k = i + b.proportions().width, l = j + b.proportions().height;
        switch (c) {
          case "fit":
            return i <= e && g <= k && j <= f && h <= l;

          case "intersect":
            return e + a.helperProportions.width / 2 > i && k > g - a.helperProportions.width / 2 && f + a.helperProportions.height / 2 > j && l > h - a.helperProportions.height / 2;

          case "pointer":
            return hb(d.pageY, j, b.proportions().height) && hb(d.pageX, i, b.proportions().width);

          case "touch":
            return (j <= f && f <= l || j <= h && h <= l || f < j && l < h) && (i <= e && e <= k || i <= g && g <= k || e < i && k < g);

          default:
            return !1;
        }
    };
    function hb(a, b, c) {
        return b <= a && a < b + c;
    }
    !(a.ui.ddmanager = {
        current: null,
        droppables: {
            "default": []
        },
        prepareOffsets: function(b, c) {
            var d, e, f = a.ui.ddmanager.droppables[b.options.scope] || [], g = c ? c.type : null, h = (b.currentItem || b.element).find(":data(ui-droppable)").addBack();
            a: for (d = 0; f.length > d; d++) if (!(f[d].options.disabled || b && !f[d].accept.call(f[d].element[0], b.currentItem || b.element))) {
                for (e = 0; h.length > e; e++) if (h[e] === f[d].element[0]) {
                    f[d].proportions().height = 0;
                    continue a;
                }
                f[d].visible = "none" !== f[d].element.css("display"), f[d].visible && ("mousedown" === g && f[d]._activate.call(f[d], c), 
                f[d].offset = f[d].element.offset(), f[d].proportions({
                    width: f[d].element[0].offsetWidth,
                    height: f[d].element[0].offsetHeight
                }));
            }
        },
        drop: function(b, c) {
            var d = !1;
            return a.each((a.ui.ddmanager.droppables[b.options.scope] || []).slice(), function() {
                this.options && (!this.options.disabled && this.visible && gb(b, this, this.options.tolerance, c) && (d = this._drop.call(this, c) || d), 
                !this.options.disabled && this.visible && this.accept.call(this.element[0], b.currentItem || b.element) && (this.isout = !0, 
                this.isover = !1, this._deactivate.call(this, c)));
            }), d;
        },
        dragStart: function(b, c) {
            b.element.parentsUntil("body").on("scroll.droppable", function() {
                b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c);
            });
        },
        drag: function(b, c) {
            b.options.refreshPositions && a.ui.ddmanager.prepareOffsets(b, c), a.each(a.ui.ddmanager.droppables[b.options.scope] || [], function() {
                if (!this.options.disabled && !this.greedyChild && this.visible) {
                    var d, e, f, g = gb(b, this, this.options.tolerance, c), h = !g && this.isover ? "isout" : g && !this.isover ? "isover" : null;
                    h && (this.options.greedy && (e = this.options.scope, (f = this.element.parents(":data(ui-droppable)").filter(function() {
                        return a(this).droppable("instance").options.scope === e;
                    })).length && ((d = a(f[0]).droppable("instance")).greedyChild = "isover" === h)), 
                    d && "isover" === h && (d.isover = !1, d.isout = !0, d._out.call(d, c)), this[h] = !0, 
                    this["isout" === h ? "isover" : "isout"] = !1, this["isover" === h ? "_over" : "_out"].call(this, c), 
                    d && "isout" === h && (d.isout = !1, d.isover = !0, d._over.call(d, c)));
                }
            });
        },
        dragStop: function(b, c) {
            b.element.parentsUntil("body").off("scroll.droppable"), b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c);
        }
    }) !== a.uiBackCompat && a.widget("ui.droppable", a.ui.droppable, {
        options: {
            hoverClass: !1,
            activeClass: !1
        },
        _addActiveClass: function() {
            this._super(), this.options.activeClass && this.element.addClass(this.options.activeClass);
        },
        _removeActiveClass: function() {
            this._super(), this.options.activeClass && this.element.removeClass(this.options.activeClass);
        },
        _addHoverClass: function() {
            this._super(), this.options.hoverClass && this.element.addClass(this.options.hoverClass);
        },
        _removeHoverClass: function() {
            this._super(), this.options.hoverClass && this.element.removeClass(this.options.hoverClass);
        }
    }), a.ui.droppable, a.widget("ui.progressbar", {
        version: "1.12.1",
        options: {
            classes: {
                "ui-progressbar": "ui-corner-all",
                "ui-progressbar-value": "ui-corner-left",
                "ui-progressbar-complete": "ui-corner-right"
            },
            max: 100,
            value: 0,
            change: null,
            complete: null
        },
        min: 0,
        _create: function() {
            this.oldValue = this.options.value = this._constrainedValue(), this.element.attr({
                role: "progressbar",
                "aria-valuemin": this.min
            }), this._addClass("ui-progressbar", "ui-widget ui-widget-content"), this.valueDiv = a("<div>").appendTo(this.element), 
            this._addClass(this.valueDiv, "ui-progressbar-value", "ui-widget-header"), this._refreshValue();
        },
        _destroy: function() {
            this.element.removeAttr("role aria-valuemin aria-valuemax aria-valuenow"), this.valueDiv.remove();
        },
        value: function(a) {
            return void 0 === a ? this.options.value : (this.options.value = this._constrainedValue(a), 
            void this._refreshValue());
        },
        _constrainedValue: function(a) {
            return void 0 === a && (a = this.options.value), this.indeterminate = !1 === a, 
            "number" != typeof a && (a = 0), !this.indeterminate && Math.min(this.options.max, Math.max(this.min, a));
        },
        _setOptions: function(a) {
            var b = a.value;
            delete a.value, this._super(a), this.options.value = this._constrainedValue(b), 
            this._refreshValue();
        },
        _setOption: function(a, b) {
            "max" === a && (b = Math.max(this.min, b)), this._super(a, b);
        },
        _setOptionDisabled: function(a) {
            this._super(a), this.element.attr("aria-disabled", a), this._toggleClass(null, "ui-state-disabled", !!a);
        },
        _percentage: function() {
            return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min);
        },
        _refreshValue: function() {
            var b = this.options.value, c = this._percentage();
            this.valueDiv.toggle(this.indeterminate || b > this.min).width(c.toFixed(0) + "%"), 
            this._toggleClass(this.valueDiv, "ui-progressbar-complete", null, b === this.options.max)._toggleClass("ui-progressbar-indeterminate", null, this.indeterminate), 
            this.indeterminate ? (this.element.removeAttr("aria-valuenow"), this.overlayDiv || (this.overlayDiv = a("<div>").appendTo(this.valueDiv), 
            this._addClass(this.overlayDiv, "ui-progressbar-overlay"))) : (this.element.attr({
                "aria-valuemax": this.options.max,
                "aria-valuenow": b
            }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== b && (this.oldValue = b, 
            this._trigger("change")), b === this.options.max && this._trigger("complete");
        }
    }), a.widget("ui.selectable", a.ui.mouse, {
        version: "1.12.1",
        options: {
            appendTo: "body",
            autoRefresh: !0,
            distance: 0,
            filter: "*",
            tolerance: "touch",
            selected: null,
            selecting: null,
            start: null,
            stop: null,
            unselected: null,
            unselecting: null
        },
        _create: function() {
            var b = this;
            this._addClass("ui-selectable"), this.dragged = !1, this.refresh = function() {
                b.elementPos = a(b.element[0]).offset(), b.selectees = a(b.options.filter, b.element[0]), 
                b._addClass(b.selectees, "ui-selectee"), b.selectees.each(function() {
                    var c = a(this), d = c.offset(), e = {
                        left: d.left - b.elementPos.left,
                        top: d.top - b.elementPos.top
                    };
                    a.data(this, "selectable-item", {
                        element: this,
                        $element: c,
                        left: e.left,
                        top: e.top,
                        right: e.left + c.outerWidth(),
                        bottom: e.top + c.outerHeight(),
                        startselected: !1,
                        selected: c.hasClass("ui-selected"),
                        selecting: c.hasClass("ui-selecting"),
                        unselecting: c.hasClass("ui-unselecting")
                    });
                });
            }, this.refresh(), this._mouseInit(), this.helper = a("<div>"), this._addClass(this.helper, "ui-selectable-helper");
        },
        _destroy: function() {
            this.selectees.removeData("selectable-item"), this._mouseDestroy();
        },
        _mouseStart: function(b) {
            var c = this, d = this.options;
            this.opos = [ b.pageX, b.pageY ], this.elementPos = a(this.element[0]).offset(), 
            this.options.disabled || (this.selectees = a(d.filter, this.element[0]), this._trigger("start", b), 
            a(d.appendTo).append(this.helper), this.helper.css({
                left: b.pageX,
                top: b.pageY,
                width: 0,
                height: 0
            }), d.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
                var d = a.data(this, "selectable-item");
                d.startselected = !0, b.metaKey || b.ctrlKey || (c._removeClass(d.$element, "ui-selected"), 
                d.selected = !1, c._addClass(d.$element, "ui-unselecting"), d.unselecting = !0, 
                c._trigger("unselecting", b, {
                    unselecting: d.element
                }));
            }), a(b.target).parents().addBack().each(function() {
                var d, e = a.data(this, "selectable-item");
                return e ? (d = !b.metaKey && !b.ctrlKey || !e.$element.hasClass("ui-selected"), 
                c._removeClass(e.$element, d ? "ui-unselecting" : "ui-selected")._addClass(e.$element, d ? "ui-selecting" : "ui-unselecting"), 
                e.unselecting = !d, e.selecting = d, (e.selected = d) ? c._trigger("selecting", b, {
                    selecting: e.element
                }) : c._trigger("unselecting", b, {
                    unselecting: e.element
                }), !1) : void 0;
            }));
        },
        _mouseDrag: function(b) {
            if (this.dragged = !0, !this.options.disabled) {
                var c, d = this, e = this.options, f = this.opos[0], g = this.opos[1], h = b.pageX, i = b.pageY;
                return h < f && (c = h, h = f, f = c), i < g && (c = i, i = g, g = c), this.helper.css({
                    left: f,
                    top: g,
                    width: h - f,
                    height: i - g
                }), this.selectees.each(function() {
                    var c = a.data(this, "selectable-item"), j = !1, k = {};
                    c && c.element !== d.element[0] && (k.left = c.left + d.elementPos.left, k.right = c.right + d.elementPos.left, 
                    k.top = c.top + d.elementPos.top, k.bottom = c.bottom + d.elementPos.top, "touch" === e.tolerance ? j = !(k.left > h || f > k.right || k.top > i || g > k.bottom) : "fit" === e.tolerance && (j = k.left > f && h > k.right && k.top > g && i > k.bottom), 
                    j ? (c.selected && (d._removeClass(c.$element, "ui-selected"), c.selected = !1), 
                    c.unselecting && (d._removeClass(c.$element, "ui-unselecting"), c.unselecting = !1), 
                    c.selecting || (d._addClass(c.$element, "ui-selecting"), c.selecting = !0, d._trigger("selecting", b, {
                        selecting: c.element
                    }))) : (c.selecting && ((b.metaKey || b.ctrlKey) && c.startselected ? (d._removeClass(c.$element, "ui-selecting"), 
                    c.selecting = !1, d._addClass(c.$element, "ui-selected"), c.selected = !0) : (d._removeClass(c.$element, "ui-selecting"), 
                    c.selecting = !1, c.startselected && (d._addClass(c.$element, "ui-unselecting"), 
                    c.unselecting = !0), d._trigger("unselecting", b, {
                        unselecting: c.element
                    }))), c.selected && (b.metaKey || b.ctrlKey || c.startselected || (d._removeClass(c.$element, "ui-selected"), 
                    c.selected = !1, d._addClass(c.$element, "ui-unselecting"), c.unselecting = !0, 
                    d._trigger("unselecting", b, {
                        unselecting: c.element
                    })))));
                }), !1;
            }
        },
        _mouseStop: function(b) {
            var c = this;
            return this.dragged = !1, a(".ui-unselecting", this.element[0]).each(function() {
                var d = a.data(this, "selectable-item");
                c._removeClass(d.$element, "ui-unselecting"), d.unselecting = !1, d.startselected = !1, 
                c._trigger("unselected", b, {
                    unselected: d.element
                });
            }), a(".ui-selecting", this.element[0]).each(function() {
                var d = a.data(this, "selectable-item");
                c._removeClass(d.$element, "ui-selecting")._addClass(d.$element, "ui-selected"), 
                d.selecting = !1, d.selected = !0, d.startselected = !0, c._trigger("selected", b, {
                    selected: d.element
                });
            }), this._trigger("stop", b), this.helper.remove(), !1;
        }
    }), a.widget("ui.selectmenu", [ a.ui.formResetMixin, {
        version: "1.12.1",
        defaultElement: "<select>",
        options: {
            appendTo: null,
            classes: {
                "ui-selectmenu-button-open": "ui-corner-top",
                "ui-selectmenu-button-closed": "ui-corner-all"
            },
            disabled: null,
            icons: {
                button: "ui-icon-triangle-1-s"
            },
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            width: !1,
            change: null,
            close: null,
            focus: null,
            open: null,
            select: null
        },
        _create: function() {
            var b = this.element.uniqueId().attr("id");
            this.ids = {
                element: b,
                button: b + "-button",
                menu: b + "-menu"
            }, this._drawButton(), this._drawMenu(), this._bindFormResetHandler(), this._rendered = !1, 
            this.menuItems = a();
        },
        _drawButton: function() {
            var b, c = this, d = this._parseOption(this.element.find("option:selected"), this.element[0].selectedIndex);
            this.labels = this.element.labels().attr("for", this.ids.button), this._on(this.labels, {
                click: function(a) {
                    this.button.focus(), a.preventDefault();
                }
            }), this.element.hide(), this.button = a("<span>", {
                tabindex: this.options.disabled ? -1 : 0,
                id: this.ids.button,
                role: "combobox",
                "aria-expanded": "false",
                "aria-autocomplete": "list",
                "aria-owns": this.ids.menu,
                "aria-haspopup": "true",
                title: this.element.attr("title")
            }).insertAfter(this.element), this._addClass(this.button, "ui-selectmenu-button ui-selectmenu-button-closed", "ui-button ui-widget"), 
            b = a("<span>").appendTo(this.button), this._addClass(b, "ui-selectmenu-icon", "ui-icon " + this.options.icons.button), 
            this.buttonItem = this._renderButtonItem(d).appendTo(this.button), !1 !== this.options.width && this._resizeButton(), 
            this._on(this.button, this._buttonEvents), this.button.one("focusin", function() {
                c._rendered || c._refreshMenu();
            });
        },
        _drawMenu: function() {
            var b = this;
            this.menu = a("<ul>", {
                "aria-hidden": "true",
                "aria-labelledby": this.ids.button,
                id: this.ids.menu
            }), this.menuWrap = a("<div>").append(this.menu), this._addClass(this.menuWrap, "ui-selectmenu-menu", "ui-front"), 
            this.menuWrap.appendTo(this._appendTo()), this.menuInstance = this.menu.menu({
                classes: {
                    "ui-menu": "ui-corner-bottom"
                },
                role: "listbox",
                select: function(a, c) {
                    a.preventDefault(), b._setSelection(), b._select(c.item.data("ui-selectmenu-item"), a);
                },
                focus: function(a, c) {
                    var d = c.item.data("ui-selectmenu-item");
                    null != b.focusIndex && d.index !== b.focusIndex && (b._trigger("focus", a, {
                        item: d
                    }), b.isOpen || b._select(d, a)), b.focusIndex = d.index, b.button.attr("aria-activedescendant", b.menuItems.eq(d.index).attr("id"));
                }
            }).menu("instance"), this.menuInstance._off(this.menu, "mouseleave"), this.menuInstance._closeOnDocumentClick = function() {
                return !1;
            }, this.menuInstance._isDivider = function() {
                return !1;
            };
        },
        refresh: function() {
            this._refreshMenu(), this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(this._getSelectedItem().data("ui-selectmenu-item") || {})), 
            null === this.options.width && this._resizeButton();
        },
        _refreshMenu: function() {
            var a, b = this.element.find("option");
            this.menu.empty(), this._parseOptions(b), this._renderMenu(this.menu, this.items), 
            this.menuInstance.refresh(), this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup").find(".ui-menu-item-wrapper"), 
            this._rendered = !0, b.length && (a = this._getSelectedItem(), this.menuInstance.focus(null, a), 
            this._setAria(a.data("ui-selectmenu-item")), this._setOption("disabled", this.element.prop("disabled")));
        },
        open: function(a) {
            this.options.disabled || (this._rendered ? (this._removeClass(this.menu.find(".ui-state-active"), null, "ui-state-active"), 
            this.menuInstance.focus(null, this._getSelectedItem())) : this._refreshMenu(), this.menuItems.length && (this.isOpen = !0, 
            this._toggleAttr(), this._resizeMenu(), this._position(), this._on(this.document, this._documentClick), 
            this._trigger("open", a)));
        },
        _position: function() {
            this.menuWrap.position(a.extend({
                of: this.button
            }, this.options.position));
        },
        close: function(a) {
            this.isOpen && (this.isOpen = !1, this._toggleAttr(), this.range = null, this._off(this.document), 
            this._trigger("close", a));
        },
        widget: function() {
            return this.button;
        },
        menuWidget: function() {
            return this.menu;
        },
        _renderButtonItem: function(b) {
            var c = a("<span>");
            return this._setText(c, b.label), this._addClass(c, "ui-selectmenu-text"), c;
        },
        _renderMenu: function(b, c) {
            var d = this, e = "";
            a.each(c, function(c, f) {
                var g;
                f.optgroup !== e && (g = a("<li>", {
                    text: f.optgroup
                }), d._addClass(g, "ui-selectmenu-optgroup", "ui-menu-divider" + (f.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : "")), 
                g.appendTo(b), e = f.optgroup), d._renderItemData(b, f);
            });
        },
        _renderItemData: function(a, b) {
            return this._renderItem(a, b).data("ui-selectmenu-item", b);
        },
        _renderItem: function(b, c) {
            var d = a("<li>"), e = a("<div>", {
                title: c.element.attr("title")
            });
            return c.disabled && this._addClass(d, null, "ui-state-disabled"), this._setText(e, c.label), 
            d.append(e).appendTo(b);
        },
        _setText: function(a, b) {
            b ? a.text(b) : a.html("&#160;");
        },
        _move: function(a, b) {
            var c, d, e = ".ui-menu-item";
            this.isOpen ? c = this.menuItems.eq(this.focusIndex).parent("li") : (c = this.menuItems.eq(this.element[0].selectedIndex).parent("li"), 
            e += ":not(.ui-state-disabled)"), (d = "first" === a || "last" === a ? c["first" === a ? "prevAll" : "nextAll"](e).eq(-1) : c[a + "All"](e).eq(0)).length && this.menuInstance.focus(b, d);
        },
        _getSelectedItem: function() {
            return this.menuItems.eq(this.element[0].selectedIndex).parent("li");
        },
        _toggle: function(a) {
            this[this.isOpen ? "close" : "open"](a);
        },
        _setSelection: function() {
            var a;
            this.range && (window.getSelection ? ((a = window.getSelection()).removeAllRanges(), 
            a.addRange(this.range)) : this.range.select(), this.button.focus());
        },
        _documentClick: {
            mousedown: function(b) {
                this.isOpen && (a(b.target).closest(".ui-selectmenu-menu, #" + a.ui.escapeSelector(this.ids.button)).length || this.close(b));
            }
        },
        _buttonEvents: {
            mousedown: function() {
                var a;
                window.getSelection ? (a = window.getSelection()).rangeCount && (this.range = a.getRangeAt(0)) : this.range = document.selection.createRange();
            },
            click: function(a) {
                this._setSelection(), this._toggle(a);
            },
            keydown: function(b) {
                var c = !0;
                switch (b.keyCode) {
                  case a.ui.keyCode.TAB:
                  case a.ui.keyCode.ESCAPE:
                    this.close(b), c = !1;
                    break;

                  case a.ui.keyCode.ENTER:
                    this.isOpen && this._selectFocusedItem(b);
                    break;

                  case a.ui.keyCode.UP:
                    b.altKey ? this._toggle(b) : this._move("prev", b);
                    break;

                  case a.ui.keyCode.DOWN:
                    b.altKey ? this._toggle(b) : this._move("next", b);
                    break;

                  case a.ui.keyCode.SPACE:
                    this.isOpen ? this._selectFocusedItem(b) : this._toggle(b);
                    break;

                  case a.ui.keyCode.LEFT:
                    this._move("prev", b);
                    break;

                  case a.ui.keyCode.RIGHT:
                    this._move("next", b);
                    break;

                  case a.ui.keyCode.HOME:
                  case a.ui.keyCode.PAGE_UP:
                    this._move("first", b);
                    break;

                  case a.ui.keyCode.END:
                  case a.ui.keyCode.PAGE_DOWN:
                    this._move("last", b);
                    break;

                  default:
                    this.menu.trigger(b), c = !1;
                }
                c && b.preventDefault();
            }
        },
        _selectFocusedItem: function(a) {
            var b = this.menuItems.eq(this.focusIndex).parent("li");
            b.hasClass("ui-state-disabled") || this._select(b.data("ui-selectmenu-item"), a);
        },
        _select: function(a, b) {
            var c = this.element[0].selectedIndex;
            this.element[0].selectedIndex = a.index, this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(a)), 
            this._setAria(a), this._trigger("select", b, {
                item: a
            }), a.index !== c && this._trigger("change", b, {
                item: a
            }), this.close(b);
        },
        _setAria: function(a) {
            var b = this.menuItems.eq(a.index).attr("id");
            this.button.attr({
                "aria-labelledby": b,
                "aria-activedescendant": b
            }), this.menu.attr("aria-activedescendant", b);
        },
        _setOption: function(a, b) {
            if ("icons" === a) {
                var c = this.button.find("span.ui-icon");
                this._removeClass(c, null, this.options.icons.button)._addClass(c, null, b.button);
            }
            this._super(a, b), "appendTo" === a && this.menuWrap.appendTo(this._appendTo()), 
            "width" === a && this._resizeButton();
        },
        _setOptionDisabled: function(a) {
            this._super(a), this.menuInstance.option("disabled", a), this.button.attr("aria-disabled", a), 
            this._toggleClass(this.button, null, "ui-state-disabled", a), this.element.prop("disabled", a), 
            a ? (this.button.attr("tabindex", -1), this.close()) : this.button.attr("tabindex", 0);
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            return (b = b && (b.jquery || b.nodeType ? a(b) : this.document.find(b).eq(0))) && b[0] || (b = this.element.closest(".ui-front, dialog")), 
            b.length || (b = this.document[0].body), b;
        },
        _toggleAttr: function() {
            this.button.attr("aria-expanded", this.isOpen), this._removeClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "closed" : "open"))._addClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "open" : "closed"))._toggleClass(this.menuWrap, "ui-selectmenu-open", null, this.isOpen), 
            this.menu.attr("aria-hidden", !this.isOpen);
        },
        _resizeButton: function() {
            var a = this.options.width;
            return !1 === a ? void this.button.css("width", "") : (null === a && (a = this.element.show().outerWidth(), 
            this.element.hide()), void this.button.outerWidth(a));
        },
        _resizeMenu: function() {
            this.menu.outerWidth(Math.max(this.button.outerWidth(), this.menu.width("").outerWidth() + 1));
        },
        _getCreateOptions: function() {
            var a = this._super();
            return a.disabled = this.element.prop("disabled"), a;
        },
        _parseOptions: function(b) {
            var c = this, d = [];
            b.each(function(b, e) {
                d.push(c._parseOption(a(e), b));
            }), this.items = d;
        },
        _parseOption: function(a, b) {
            var c = a.parent("optgroup");
            return {
                element: a,
                index: b,
                value: a.val(),
                label: a.text(),
                optgroup: c.attr("label") || "",
                disabled: c.prop("disabled") || a.prop("disabled")
            };
        },
        _destroy: function() {
            this._unbindFormResetHandler(), this.menuWrap.remove(), this.button.remove(), this.element.show(), 
            this.element.removeUniqueId(), this.labels.attr("for", this.ids.element);
        }
    } ]), a.widget("ui.slider", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "slide",
        options: {
            animate: !1,
            classes: {
                "ui-slider": "ui-corner-all",
                "ui-slider-handle": "ui-corner-all",
                "ui-slider-range": "ui-corner-all ui-widget-header"
            },
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: !1,
            step: 1,
            value: 0,
            values: null,
            change: null,
            slide: null,
            start: null,
            stop: null
        },
        numPages: 5,
        _create: function() {
            this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, 
            this._detectOrientation(), this._mouseInit(), this._calculateNewMax(), this._addClass("ui-slider ui-slider-" + this.orientation, "ui-widget ui-widget-content"), 
            this._refresh(), this._animateOff = !1;
        },
        _refresh: function() {
            this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue();
        },
        _createHandles: function() {
            var b, c, d = this.options, e = this.element.find(".ui-slider-handle"), f = [];
            for (c = d.values && d.values.length || 1, e.length > c && (e.slice(c).remove(), 
            e = e.slice(0, c)), b = e.length; b < c; b++) f.push("<span tabindex='0'></span>");
            this.handles = e.add(a(f.join("")).appendTo(this.element)), this._addClass(this.handles, "ui-slider-handle", "ui-state-default"), 
            this.handle = this.handles.eq(0), this.handles.each(function(b) {
                a(this).data("ui-slider-handle-index", b).attr("tabIndex", 0);
            });
        },
        _createRange: function() {
            var b = this.options;
            b.range ? (!0 === b.range && (b.values ? b.values.length && 2 !== b.values.length ? b.values = [ b.values[0], b.values[0] ] : a.isArray(b.values) && (b.values = b.values.slice(0)) : b.values = [ this._valueMin(), this._valueMin() ]), 
            this.range && this.range.length ? (this._removeClass(this.range, "ui-slider-range-min ui-slider-range-max"), 
            this.range.css({
                left: "",
                bottom: ""
            })) : (this.range = a("<div>").appendTo(this.element), this._addClass(this.range, "ui-slider-range")), 
            "min" !== b.range && "max" !== b.range || this._addClass(this.range, "ui-slider-range-" + b.range)) : (this.range && this.range.remove(), 
            this.range = null);
        },
        _setupEvents: function() {
            this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), 
            this._focusable(this.handles);
        },
        _destroy: function() {
            this.handles.remove(), this.range && this.range.remove(), this._mouseDestroy();
        },
        _mouseCapture: function(b) {
            var c, d, e, f, g, h, i, j = this, k = this.options;
            return !k.disabled && (this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            }, this.elementOffset = this.element.offset(), c = {
                x: b.pageX,
                y: b.pageY
            }, d = this._normValueFromMouse(c), e = this._valueMax() - this._valueMin() + 1, 
            this.handles.each(function(b) {
                var c = Math.abs(d - j.values(b));
                (c < e || e === c && (b === j._lastChangedValue || j.values(b) === k.min)) && (e = c, 
                f = a(this), g = b);
            }), !1 !== this._start(b, g) && (this._mouseSliding = !0, this._handleIndex = g, 
            this._addClass(f, null, "ui-state-active"), f.trigger("focus"), h = f.offset(), 
            i = !a(b.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = i ? {
                left: 0,
                top: 0
            } : {
                left: b.pageX - h.left - f.width() / 2,
                top: b.pageY - h.top - f.height() / 2 - (parseInt(f.css("borderTopWidth"), 10) || 0) - (parseInt(f.css("borderBottomWidth"), 10) || 0) + (parseInt(f.css("marginTop"), 10) || 0)
            }, this.handles.hasClass("ui-state-hover") || this._slide(b, g, d), this._animateOff = !0));
        },
        _mouseStart: function() {
            return !0;
        },
        _mouseDrag: function(a) {
            var b = {
                x: a.pageX,
                y: a.pageY
            }, c = this._normValueFromMouse(b);
            return this._slide(a, this._handleIndex, c), !1;
        },
        _mouseStop: function(a) {
            return this._removeClass(this.handles, null, "ui-state-active"), this._mouseSliding = !1, 
            this._stop(a, this._handleIndex), this._change(a, this._handleIndex), this._handleIndex = null, 
            this._clickOffset = null, this._animateOff = !1;
        },
        _detectOrientation: function() {
            this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal";
        },
        _normValueFromMouse: function(a) {
            var b, c, d, e;
            return 1 < (c = ("horizontal" === this.orientation ? (b = this.elementSize.width, 
            a.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (b = this.elementSize.height, 
            a.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0))) / b) && (c = 1), 
            c < 0 && (c = 0), "vertical" === this.orientation && (c = 1 - c), d = this._valueMax() - this._valueMin(), 
            e = this._valueMin() + c * d, this._trimAlignValue(e);
        },
        _uiHash: function(a, b, c) {
            var d = {
                handle: this.handles[a],
                handleIndex: a,
                value: void 0 !== b ? b : this.value()
            };
            return this._hasMultipleValues() && (d.value = void 0 !== b ? b : this.values(a), 
            d.values = c || this.values()), d;
        },
        _hasMultipleValues: function() {
            return this.options.values && this.options.values.length;
        },
        _start: function(a, b) {
            return this._trigger("start", a, this._uiHash(b));
        },
        _slide: function(a, b, c) {
            var d, e = this.value(), f = this.values();
            this._hasMultipleValues() && (d = this.values(b ? 0 : 1), e = this.values(b), 2 === this.options.values.length && !0 === this.options.range && (c = 0 === b ? Math.min(d, c) : Math.max(d, c)), 
            f[b] = c), c === e || !1 !== this._trigger("slide", a, this._uiHash(b, c, f)) && (this._hasMultipleValues() ? this.values(b, c) : this.value(c));
        },
        _stop: function(a, b) {
            this._trigger("stop", a, this._uiHash(b));
        },
        _change: function(a, b) {
            this._keySliding || this._mouseSliding || (this._lastChangedValue = b, this._trigger("change", a, this._uiHash(b)));
        },
        value: function(a) {
            return arguments.length ? (this.options.value = this._trimAlignValue(a), this._refreshValue(), 
            void this._change(null, 0)) : this._value();
        },
        values: function(b, c) {
            var d, e, f;
            if (1 < arguments.length) return this.options.values[b] = this._trimAlignValue(c), 
            this._refreshValue(), void this._change(null, b);
            if (!arguments.length) return this._values();
            if (!a.isArray(b)) return this._hasMultipleValues() ? this._values(b) : this.value();
            for (d = this.options.values, e = b, f = 0; d.length > f; f += 1) d[f] = this._trimAlignValue(e[f]), 
            this._change(null, f);
            this._refreshValue();
        },
        _setOption: function(b, c) {
            var d, e = 0;
            switch ("range" === b && !0 === this.options.range && ("min" === c ? (this.options.value = this._values(0), 
            this.options.values = null) : "max" === c && (this.options.value = this._values(this.options.values.length - 1), 
            this.options.values = null)), a.isArray(this.options.values) && (e = this.options.values.length), 
            this._super(b, c), b) {
              case "orientation":
                this._detectOrientation(), this._removeClass("ui-slider-horizontal ui-slider-vertical")._addClass("ui-slider-" + this.orientation), 
                this._refreshValue(), this.options.range && this._refreshRange(c), this.handles.css("horizontal" === c ? "bottom" : "left", "");
                break;

              case "value":
                this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                break;

              case "values":
                for (this._animateOff = !0, this._refreshValue(), d = e - 1; 0 <= d; d--) this._change(null, d);
                this._animateOff = !1;
                break;

              case "step":
              case "min":
              case "max":
                this._animateOff = !0, this._calculateNewMax(), this._refreshValue(), this._animateOff = !1;
                break;

              case "range":
                this._animateOff = !0, this._refresh(), this._animateOff = !1;
            }
        },
        _setOptionDisabled: function(a) {
            this._super(a), this._toggleClass(null, "ui-state-disabled", !!a);
        },
        _value: function() {
            var a = this.options.value;
            return this._trimAlignValue(a);
        },
        _values: function(a) {
            var b, c, d;
            if (arguments.length) return b = this.options.values[a], this._trimAlignValue(b);
            if (this._hasMultipleValues()) {
                for (c = this.options.values.slice(), d = 0; c.length > d; d += 1) c[d] = this._trimAlignValue(c[d]);
                return c;
            }
            return [];
        },
        _trimAlignValue: function(a) {
            if (this._valueMin() >= a) return this._valueMin();
            if (a >= this._valueMax()) return this._valueMax();
            var b = 0 < this.options.step ? this.options.step : 1, c = (a - this._valueMin()) % b, d = a - c;
            return 2 * Math.abs(c) >= b && (d += 0 < c ? b : -b), parseFloat(d.toFixed(5));
        },
        _calculateNewMax: function() {
            var a = this.options.max, b = this._valueMin(), c = this.options.step;
            (a = Math.round((a - b) / c) * c + b) > this.options.max && (a -= c), this.max = parseFloat(a.toFixed(this._precision()));
        },
        _precision: function() {
            var a = this._precisionOf(this.options.step);
            return null !== this.options.min && (a = Math.max(a, this._precisionOf(this.options.min))), 
            a;
        },
        _precisionOf: function(a) {
            var b = "" + a, c = b.indexOf(".");
            return -1 === c ? 0 : b.length - c - 1;
        },
        _valueMin: function() {
            return this.options.min;
        },
        _valueMax: function() {
            return this.max;
        },
        _refreshRange: function(a) {
            "vertical" === a && this.range.css({
                width: "",
                left: ""
            }), "horizontal" === a && this.range.css({
                height: "",
                bottom: ""
            });
        },
        _refreshValue: function() {
            var b, c, d, e, f, g = this.options.range, h = this.options, i = this, j = !this._animateOff && h.animate, k = {};
            this._hasMultipleValues() ? this.handles.each(function(d) {
                c = (i.values(d) - i._valueMin()) / (i._valueMax() - i._valueMin()) * 100, k["horizontal" === i.orientation ? "left" : "bottom"] = c + "%", 
                a(this).stop(1, 1)[j ? "animate" : "css"](k, h.animate), !0 === i.options.range && ("horizontal" === i.orientation ? (0 === d && i.range.stop(1, 1)[j ? "animate" : "css"]({
                    left: c + "%"
                }, h.animate), 1 === d && i.range[j ? "animate" : "css"]({
                    width: c - b + "%"
                }, {
                    queue: !1,
                    duration: h.animate
                })) : (0 === d && i.range.stop(1, 1)[j ? "animate" : "css"]({
                    bottom: c + "%"
                }, h.animate), 1 === d && i.range[j ? "animate" : "css"]({
                    height: c - b + "%"
                }, {
                    queue: !1,
                    duration: h.animate
                }))), b = c;
            }) : (d = this.value(), e = this._valueMin(), f = this._valueMax(), c = f !== e ? (d - e) / (f - e) * 100 : 0, 
            k["horizontal" === this.orientation ? "left" : "bottom"] = c + "%", this.handle.stop(1, 1)[j ? "animate" : "css"](k, h.animate), 
            "min" === g && "horizontal" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({
                width: c + "%"
            }, h.animate), "max" === g && "horizontal" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({
                width: 100 - c + "%"
            }, h.animate), "min" === g && "vertical" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({
                height: c + "%"
            }, h.animate), "max" === g && "vertical" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({
                height: 100 - c + "%"
            }, h.animate));
        },
        _handleEvents: {
            keydown: function(b) {
                var c, d, e, f = a(b.target).data("ui-slider-handle-index");
                switch (b.keyCode) {
                  case a.ui.keyCode.HOME:
                  case a.ui.keyCode.END:
                  case a.ui.keyCode.PAGE_UP:
                  case a.ui.keyCode.PAGE_DOWN:
                  case a.ui.keyCode.UP:
                  case a.ui.keyCode.RIGHT:
                  case a.ui.keyCode.DOWN:
                  case a.ui.keyCode.LEFT:
                    if (b.preventDefault(), !this._keySliding && (this._keySliding = !0, this._addClass(a(b.target), null, "ui-state-active"), 
                    !1 === this._start(b, f))) return;
                }
                switch (e = this.options.step, c = d = this._hasMultipleValues() ? this.values(f) : this.value(), 
                b.keyCode) {
                  case a.ui.keyCode.HOME:
                    d = this._valueMin();
                    break;

                  case a.ui.keyCode.END:
                    d = this._valueMax();
                    break;

                  case a.ui.keyCode.PAGE_UP:
                    d = this._trimAlignValue(c + (this._valueMax() - this._valueMin()) / this.numPages);
                    break;

                  case a.ui.keyCode.PAGE_DOWN:
                    d = this._trimAlignValue(c - (this._valueMax() - this._valueMin()) / this.numPages);
                    break;

                  case a.ui.keyCode.UP:
                  case a.ui.keyCode.RIGHT:
                    if (c === this._valueMax()) return;
                    d = this._trimAlignValue(c + e);
                    break;

                  case a.ui.keyCode.DOWN:
                  case a.ui.keyCode.LEFT:
                    if (c === this._valueMin()) return;
                    d = this._trimAlignValue(c - e);
                }
                this._slide(b, f, d);
            },
            keyup: function(b) {
                var c = a(b.target).data("ui-slider-handle-index");
                this._keySliding && (this._keySliding = !1, this._stop(b, c), this._change(b, c), 
                this._removeClass(a(b.target), null, "ui-state-active"));
            }
        }
    }), a.widget("ui.sortable", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "sort",
        ready: !1,
        options: {
            appendTo: "parent",
            axis: !1,
            connectWith: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            dropOnEmpty: !0,
            forcePlaceholderSize: !1,
            forceHelperSize: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            items: "> *",
            opacity: !1,
            placeholder: !1,
            revert: !1,
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3,
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null
        },
        _isOverAxis: function(a, b, c) {
            return b <= a && a < b + c;
        },
        _isFloating: function(a) {
            return /left|right/.test(a.css("float")) || /inline|table-cell/.test(a.css("display"));
        },
        _create: function() {
            this.containerCache = {}, this._addClass("ui-sortable"), this.refresh(), this.offset = this.element.offset(), 
            this._mouseInit(), this._setHandleClassName(), this.ready = !0;
        },
        _setOption: function(a, b) {
            this._super(a, b), "handle" === a && this._setHandleClassName();
        },
        _setHandleClassName: function() {
            var b = this;
            this._removeClass(this.element.find(".ui-sortable-handle"), "ui-sortable-handle"), 
            a.each(this.items, function() {
                b._addClass(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item, "ui-sortable-handle");
            });
        },
        _destroy: function() {
            this._mouseDestroy();
            for (var a = this.items.length - 1; 0 <= a; a--) this.items[a].item.removeData(this.widgetName + "-item");
            return this;
        },
        _mouseCapture: function(b, c) {
            var d = null, e = !1, f = this;
            return !this.reverting && !this.options.disabled && "static" !== this.options.type && (this._refreshItems(b), 
            a(b.target).parents().each(function() {
                return a.data(this, f.widgetName + "-item") === f ? (d = a(this), !1) : void 0;
            }), a.data(b.target, f.widgetName + "-item") === f && (d = a(b.target)), !!d && !(this.options.handle && !c && (a(this.options.handle, d).find("*").addBack().each(function() {
                this === b.target && (e = !0);
            }), !e)) && (this.currentItem = d, this._removeCurrentsFromItems(), !0));
        },
        _mouseStart: function(b, c, d) {
            var e, f, g = this.options;
            if ((this.currentContainer = this).refreshPositions(), this.helper = this._createHelper(b), 
            this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), 
            this.offset = this.currentItem.offset(), this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            }, a.extend(this.offset, {
                click: {
                    left: b.pageX - this.offset.left,
                    top: b.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), 
            this.originalPosition = this._generatePosition(b), this.originalPageX = b.pageX, 
            this.originalPageY = b.pageY, g.cursorAt && this._adjustOffsetFromHelper(g.cursorAt), 
            this.domPosition = {
                prev: this.currentItem.prev()[0],
                parent: this.currentItem.parent()[0]
            }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), 
            g.containment && this._setContainment(), g.cursor && "auto" !== g.cursor && (f = this.document.find("body"), 
            this.storedCursor = f.css("cursor"), f.css("cursor", g.cursor), this.storedStylesheet = a("<style>*{ cursor: " + g.cursor + " !important; }</style>").appendTo(f)), 
            g.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), 
            this.helper.css("opacity", g.opacity)), g.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), 
            this.helper.css("zIndex", g.zIndex)), this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), 
            this._trigger("start", b, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), 
            !d) for (e = this.containers.length - 1; 0 <= e; e--) this.containers[e]._trigger("activate", b, this._uiHash(this));
            return a.ui.ddmanager && (a.ui.ddmanager.current = this), a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), 
            this.dragging = !0, this._addClass(this.helper, "ui-sortable-helper"), this._mouseDrag(b), 
            !0;
        },
        _mouseDrag: function(b) {
            var c, d, e, f, g = this.options, h = !1;
            for (this.position = this._generatePosition(b), this.positionAbs = this._convertPositionTo("absolute"), 
            this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - b.pageY < g.scrollSensitivity ? this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop + g.scrollSpeed : b.pageY - this.overflowOffset.top < g.scrollSensitivity && (this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop - g.scrollSpeed), 
            this.overflowOffset.left + this.scrollParent[0].offsetWidth - b.pageX < g.scrollSensitivity ? this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft + g.scrollSpeed : b.pageX - this.overflowOffset.left < g.scrollSensitivity && (this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft - g.scrollSpeed)) : (b.pageY - this.document.scrollTop() < g.scrollSensitivity ? h = this.document.scrollTop(this.document.scrollTop() - g.scrollSpeed) : this.window.height() - (b.pageY - this.document.scrollTop()) < g.scrollSensitivity && (h = this.document.scrollTop(this.document.scrollTop() + g.scrollSpeed)), 
            b.pageX - this.document.scrollLeft() < g.scrollSensitivity ? h = this.document.scrollLeft(this.document.scrollLeft() - g.scrollSpeed) : this.window.width() - (b.pageX - this.document.scrollLeft()) < g.scrollSensitivity && (h = this.document.scrollLeft(this.document.scrollLeft() + g.scrollSpeed))), 
            !1 !== h && a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b)), 
            this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), 
            this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), 
            c = this.items.length - 1; 0 <= c; c--) if (e = (d = this.items[c]).item[0], (f = this._intersectsWithPointer(d)) && d.instance === this.currentContainer && e !== this.currentItem[0] && this.placeholder[1 === f ? "next" : "prev"]()[0] !== e && !a.contains(this.placeholder[0], e) && ("semi-dynamic" !== this.options.type || !a.contains(this.element[0], e))) {
                if (this.direction = 1 === f ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(d)) break;
                this._rearrange(b, d), this._trigger("change", b, this._uiHash());
                break;
            }
            return this._contactContainers(b), a.ui.ddmanager && a.ui.ddmanager.drag(this, b), 
            this._trigger("sort", b, this._uiHash()), this.lastPositionAbs = this.positionAbs, 
            !1;
        },
        _mouseStop: function(b, c) {
            if (b) {
                if (a.ui.ddmanager && !this.options.dropBehaviour && a.ui.ddmanager.drop(this, b), 
                this.options.revert) {
                    var d = this, e = this.placeholder.offset(), f = this.options.axis, g = {};
                    f && "x" !== f || (g.left = e.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft)), 
                    f && "y" !== f || (g.top = e.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop)), 
                    this.reverting = !0, a(this.helper).animate(g, parseInt(this.options.revert, 10) || 500, function() {
                        d._clear(b);
                    });
                } else this._clear(b, c);
                return !1;
            }
        },
        cancel: function() {
            if (this.dragging) {
                this._mouseUp(new a.Event("mouseup", {
                    target: null
                })), "original" === this.options.helper ? (this.currentItem.css(this._storedCSS), 
                this._removeClass(this.currentItem, "ui-sortable-helper")) : this.currentItem.show();
                for (var b = this.containers.length - 1; 0 <= b; b--) this.containers[b]._trigger("deactivate", null, this._uiHash(this)), 
                this.containers[b].containerCache.over && (this.containers[b]._trigger("out", null, this._uiHash(this)), 
                this.containers[b].containerCache.over = 0);
            }
            return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), 
            "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), 
            a.extend(this, {
                helper: null,
                dragging: !1,
                reverting: !1,
                _noFinalSort: null
            }), this.domPosition.prev ? a(this.domPosition.prev).after(this.currentItem) : a(this.domPosition.parent).prepend(this.currentItem)), 
            this;
        },
        serialize: function(b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            return b = b || {}, a(c).each(function() {
                var c = (a(b.item || this).attr(b.attribute || "id") || "").match(b.expression || /(.+)[\-=_](.+)/);
                c && d.push((b.key || c[1] + "[]") + "=" + (b.key && b.expression ? c[1] : c[2]));
            }), !d.length && b.key && d.push(b.key + "="), d.join("&");
        },
        toArray: function(b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            return b = b || {}, c.each(function() {
                d.push(a(b.item || this).attr(b.attribute || "id") || "");
            }), d;
        },
        _intersectsWith: function(a) {
            var b = this.positionAbs.left, c = b + this.helperProportions.width, d = this.positionAbs.top, e = d + this.helperProportions.height, f = a.left, g = f + a.width, h = a.top, i = h + a.height, j = this.offset.click.top, k = this.offset.click.left, l = "x" === this.options.axis || h < d + j && d + j < i, m = "y" === this.options.axis || f < b + k && b + k < g, n = l && m;
            return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > a[this.floating ? "width" : "height"] ? n : b + this.helperProportions.width / 2 > f && g > c - this.helperProportions.width / 2 && d + this.helperProportions.height / 2 > h && i > e - this.helperProportions.height / 2;
        },
        _intersectsWithPointer: function(a) {
            var b, c, d = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top, a.height), e = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left, a.width);
            return !!(d && e) && (b = this._getDragVerticalDirection(), c = this._getDragHorizontalDirection(), 
            this.floating ? "right" === c || "down" === b ? 2 : 1 : b && ("down" === b ? 2 : 1));
        },
        _intersectsWithSides: function(a) {
            var b = this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top + a.height / 2, a.height), c = this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left + a.width / 2, a.width), d = this._getDragVerticalDirection(), e = this._getDragHorizontalDirection();
            return this.floating && e ? "right" === e && c || "left" === e && !c : d && ("down" === d && b || "up" === d && !b);
        },
        _getDragVerticalDirection: function() {
            var a = this.positionAbs.top - this.lastPositionAbs.top;
            return 0 != a && (0 < a ? "down" : "up");
        },
        _getDragHorizontalDirection: function() {
            var a = this.positionAbs.left - this.lastPositionAbs.left;
            return 0 != a && (0 < a ? "right" : "left");
        },
        refresh: function(a) {
            return this._refreshItems(a), this._setHandleClassName(), this.refreshPositions(), 
            this;
        },
        _connectWith: function() {
            var a = this.options;
            return a.connectWith.constructor === String ? [ a.connectWith ] : a.connectWith;
        },
        _getItemsAsjQuery: function(b) {
            function c() {
                h.push(this);
            }
            var d, e, f, g, h = [], i = [], j = this._connectWith();
            if (j && b) for (d = j.length - 1; 0 <= d; d--) for (e = (f = a(j[d], this.document[0])).length - 1; 0 <= e; e--) (g = a.data(f[e], this.widgetFullName)) && g !== this && !g.options.disabled && i.push([ a.isFunction(g.options.items) ? g.options.items.call(g.element) : a(g.options.items, g.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), g ]);
            for (i.push([ a.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                options: this.options,
                item: this.currentItem
            }) : a(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this ]), 
            d = i.length - 1; 0 <= d; d--) i[d][0].each(c);
            return a(h);
        },
        _removeCurrentsFromItems: function() {
            var b = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = a.grep(this.items, function(a) {
                for (var c = 0; b.length > c; c++) if (b[c] === a.item[0]) return !1;
                return !0;
            });
        },
        _refreshItems: function(b) {
            this.items = [], this.containers = [ this ];
            var c, d, e, f, g, h, i, j, k = this.items, l = [ [ a.isFunction(this.options.items) ? this.options.items.call(this.element[0], b, {
                item: this.currentItem
            }) : a(this.options.items, this.element), this ] ], m = this._connectWith();
            if (m && this.ready) for (c = m.length - 1; 0 <= c; c--) for (d = (e = a(m[c], this.document[0])).length - 1; 0 <= d; d--) (f = a.data(e[d], this.widgetFullName)) && f !== this && !f.options.disabled && (l.push([ a.isFunction(f.options.items) ? f.options.items.call(f.element[0], b, {
                item: this.currentItem
            }) : a(f.options.items, f.element), f ]), this.containers.push(f));
            for (c = l.length - 1; 0 <= c; c--) for (g = l[c][1], d = 0, j = (h = l[c][0]).length; d < j; d++) (i = a(h[d])).data(this.widgetName + "-item", g), 
            k.push({
                item: i,
                instance: g,
                width: 0,
                height: 0,
                left: 0,
                top: 0
            });
        },
        refreshPositions: function(b) {
            var c, d, e, f;
            for (this.floating = !!this.items.length && ("x" === this.options.axis || this._isFloating(this.items[0].item)), 
            this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset()), 
            c = this.items.length - 1; 0 <= c; c--) (d = this.items[c]).instance !== this.currentContainer && this.currentContainer && d.item[0] !== this.currentItem[0] || (e = this.options.toleranceElement ? a(this.options.toleranceElement, d.item) : d.item, 
            b || (d.width = e.outerWidth(), d.height = e.outerHeight()), f = e.offset(), d.left = f.left, 
            d.top = f.top);
            if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this); else for (c = this.containers.length - 1; 0 <= c; c--) f = this.containers[c].element.offset(), 
            this.containers[c].containerCache.left = f.left, this.containers[c].containerCache.top = f.top, 
            this.containers[c].containerCache.width = this.containers[c].element.outerWidth(), 
            this.containers[c].containerCache.height = this.containers[c].element.outerHeight();
            return this;
        },
        _createPlaceholder: function(b) {
            var c, d = (b = b || this).options;
            d.placeholder && d.placeholder.constructor !== String || (c = d.placeholder, d.placeholder = {
                element: function() {
                    var d = b.currentItem[0].nodeName.toLowerCase(), e = a("<" + d + ">", b.document[0]);
                    return b._addClass(e, "ui-sortable-placeholder", c || b.currentItem[0].className)._removeClass(e, "ui-sortable-helper"), 
                    "tbody" === d ? b._createTrPlaceholder(b.currentItem.find("tr").eq(0), a("<tr>", b.document[0]).appendTo(e)) : "tr" === d ? b._createTrPlaceholder(b.currentItem, e) : "img" === d && e.attr("src", b.currentItem.attr("src")), 
                    c || e.css("visibility", "hidden"), e;
                },
                update: function(a, e) {
                    c && !d.forcePlaceholderSize || (e.height() || e.height(b.currentItem.innerHeight() - parseInt(b.currentItem.css("paddingTop") || 0, 10) - parseInt(b.currentItem.css("paddingBottom") || 0, 10)), 
                    e.width() || e.width(b.currentItem.innerWidth() - parseInt(b.currentItem.css("paddingLeft") || 0, 10) - parseInt(b.currentItem.css("paddingRight") || 0, 10)));
                }
            }), b.placeholder = a(d.placeholder.element.call(b.element, b.currentItem)), b.currentItem.after(b.placeholder), 
            d.placeholder.update(b, b.placeholder);
        },
        _createTrPlaceholder: function(b, c) {
            var d = this;
            b.children().each(function() {
                a("<td>&#160;</td>", d.document[0]).attr("colspan", a(this).attr("colspan") || 1).appendTo(c);
            });
        },
        _contactContainers: function(b) {
            var c, d, e, f, g, h, i, j, k, l, m = null, n = null;
            for (c = this.containers.length - 1; 0 <= c; c--) if (!a.contains(this.currentItem[0], this.containers[c].element[0])) if (this._intersectsWith(this.containers[c].containerCache)) {
                if (m && a.contains(this.containers[c].element[0], m.element[0])) continue;
                m = this.containers[c], n = c;
            } else this.containers[c].containerCache.over && (this.containers[c]._trigger("out", b, this._uiHash(this)), 
            this.containers[c].containerCache.over = 0);
            if (m) if (1 === this.containers.length) this.containers[n].containerCache.over || (this.containers[n]._trigger("over", b, this._uiHash(this)), 
            this.containers[n].containerCache.over = 1); else {
                for (e = 1e4, f = null, g = (k = m.floating || this._isFloating(this.currentItem)) ? "left" : "top", 
                h = k ? "width" : "height", l = k ? "pageX" : "pageY", d = this.items.length - 1; 0 <= d; d--) a.contains(this.containers[n].element[0], this.items[d].item[0]) && this.items[d].item[0] !== this.currentItem[0] && (i = this.items[d].item.offset()[g], 
                j = !1, b[l] - i > this.items[d][h] / 2 && (j = !0), e > Math.abs(b[l] - i) && (e = Math.abs(b[l] - i), 
                f = this.items[d], this.direction = j ? "up" : "down"));
                if (!f && !this.options.dropOnEmpty) return;
                if (this.currentContainer === this.containers[n]) return void (this.currentContainer.containerCache.over || (this.containers[n]._trigger("over", b, this._uiHash()), 
                this.currentContainer.containerCache.over = 1));
                f ? this._rearrange(b, f, null, !0) : this._rearrange(b, null, this.containers[n].element, !0), 
                this._trigger("change", b, this._uiHash()), this.containers[n]._trigger("change", b, this._uiHash(this)), 
                this.currentContainer = this.containers[n], this.options.placeholder.update(this.currentContainer, this.placeholder), 
                this.containers[n]._trigger("over", b, this._uiHash(this)), this.containers[n].containerCache.over = 1;
            }
        },
        _createHelper: function(b) {
            var c = this.options, d = a.isFunction(c.helper) ? a(c.helper.apply(this.element[0], [ b, this.currentItem ])) : "clone" === c.helper ? this.currentItem.clone() : this.currentItem;
            return d.parents("body").length || a("parent" !== c.appendTo ? c.appendTo : this.currentItem[0].parentNode)[0].appendChild(d[0]), 
            d[0] === this.currentItem[0] && (this._storedCSS = {
                width: this.currentItem[0].style.width,
                height: this.currentItem[0].style.height,
                position: this.currentItem.css("position"),
                top: this.currentItem.css("top"),
                left: this.currentItem.css("left")
            }), d[0].style.width && !c.forceHelperSize || d.width(this.currentItem.width()), 
            d[0].style.height && !c.forceHelperSize || d.height(this.currentItem.height()), 
            d;
        },
        _adjustOffsetFromHelper: function(b) {
            "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
                left: +b[0],
                top: +b[1] || 0
            }), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), 
            "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top);
        },
        _getParentOffset: function() {
            this.offsetParent = this.helper.offsetParent();
            var b = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), 
            b.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === this.document[0].body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && a.ui.ie) && (b = {
                top: 0,
                left: 0
            }), {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            };
        },
        _getRelativeOffset: function() {
            if ("relative" !== this.cssPosition) return {
                top: 0,
                left: 0
            };
            var a = this.currentItem.position();
            return {
                top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
            };
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            };
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            };
        },
        _setContainment: function() {
            var b, c, d, e = this.options;
            "parent" === e.containment && (e.containment = this.helper[0].parentNode), "document" !== e.containment && "window" !== e.containment || (this.containment = [ 0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, "document" === e.containment ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left, ("document" === e.containment ? this.document.height() || document.body.parentNode.scrollHeight : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]), 
            /^(document|window|parent)$/.test(e.containment) || (b = a(e.containment)[0], c = a(e.containment).offset(), 
            d = "hidden" !== a(b).css("overflow"), this.containment = [ c.left + (parseInt(a(b).css("borderLeftWidth"), 10) || 0) + (parseInt(a(b).css("paddingLeft"), 10) || 0) - this.margins.left, c.top + (parseInt(a(b).css("borderTopWidth"), 10) || 0) + (parseInt(a(b).css("paddingTop"), 10) || 0) - this.margins.top, c.left + (d ? Math.max(b.scrollWidth, b.offsetWidth) : b.offsetWidth) - (parseInt(a(b).css("borderLeftWidth"), 10) || 0) - (parseInt(a(b).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, c.top + (d ? Math.max(b.scrollHeight, b.offsetHeight) : b.offsetHeight) - (parseInt(a(b).css("borderTopWidth"), 10) || 0) - (parseInt(a(b).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top ]);
        },
        _convertPositionTo: function(b, c) {
            c = c || this.position;
            var d = "absolute" === b ? 1 : -1, e = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, f = /(html|body)/i.test(e[0].tagName);
            return {
                top: c.top + this.offset.relative.top * d + this.offset.parent.top * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : f ? 0 : e.scrollTop()) * d,
                left: c.left + this.offset.relative.left * d + this.offset.parent.left * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : f ? 0 : e.scrollLeft()) * d
            };
        },
        _generatePosition: function(b) {
            var c, d, e = this.options, f = b.pageX, g = b.pageY, h = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, i = /(html|body)/i.test(h[0].tagName);
            return "relative" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), 
            this.originalPosition && (this.containment && (b.pageX - this.offset.click.left < this.containment[0] && (f = this.containment[0] + this.offset.click.left), 
            b.pageY - this.offset.click.top < this.containment[1] && (g = this.containment[1] + this.offset.click.top), 
            b.pageX - this.offset.click.left > this.containment[2] && (f = this.containment[2] + this.offset.click.left), 
            b.pageY - this.offset.click.top > this.containment[3] && (g = this.containment[3] + this.offset.click.top)), 
            e.grid && (c = this.originalPageY + Math.round((g - this.originalPageY) / e.grid[1]) * e.grid[1], 
            g = this.containment ? c - this.offset.click.top >= this.containment[1] && c - this.offset.click.top <= this.containment[3] ? c : c - this.offset.click.top >= this.containment[1] ? c - e.grid[1] : c + e.grid[1] : c, 
            d = this.originalPageX + Math.round((f - this.originalPageX) / e.grid[0]) * e.grid[0], 
            f = this.containment ? d - this.offset.click.left >= this.containment[0] && d - this.offset.click.left <= this.containment[2] ? d : d - this.offset.click.left >= this.containment[0] ? d - e.grid[0] : d + e.grid[0] : d)), 
            {
                top: g - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : i ? 0 : h.scrollTop()),
                left: f - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : i ? 0 : h.scrollLeft())
            };
        },
        _rearrange: function(a, b, c, d) {
            c ? c[0].appendChild(this.placeholder[0]) : b.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? b.item[0] : b.item[0].nextSibling), 
            this.counter = this.counter ? ++this.counter : 1;
            var e = this.counter;
            this._delay(function() {
                e === this.counter && this.refreshPositions(!d);
            });
        },
        _clear: function(a, b) {
            function c(a, b, c) {
                return function(d) {
                    c._trigger(a, d, b._uiHash(b));
                };
            }
            this.reverting = !1;
            var d, e = [];
            if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), 
            this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
                for (d in this._storedCSS) "auto" !== this._storedCSS[d] && "static" !== this._storedCSS[d] || (this._storedCSS[d] = "");
                this.currentItem.css(this._storedCSS), this._removeClass(this.currentItem, "ui-sortable-helper");
            } else this.currentItem.show();
            for (this.fromOutside && !b && e.push(function(a) {
                this._trigger("receive", a, this._uiHash(this.fromOutside));
            }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || b || e.push(function(a) {
                this._trigger("update", a, this._uiHash());
            }), this !== this.currentContainer && (b || (e.push(function(a) {
                this._trigger("remove", a, this._uiHash());
            }), e.push(function(a) {
                return function(b) {
                    a._trigger("receive", b, this._uiHash(this));
                };
            }.call(this, this.currentContainer)), e.push(function(a) {
                return function(b) {
                    a._trigger("update", b, this._uiHash(this));
                };
            }.call(this, this.currentContainer)))), d = this.containers.length - 1; 0 <= d; d--) b || e.push(c("deactivate", this, this.containers[d])), 
            this.containers[d].containerCache.over && (e.push(c("out", this, this.containers[d])), 
            this.containers[d].containerCache.over = 0);
            if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), 
            this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), 
            this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), 
            this.dragging = !1, b || this._trigger("beforeStop", a, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), 
            this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(), 
            this.helper = null), !b) {
                for (d = 0; e.length > d; d++) e[d].call(this, a);
                this._trigger("stop", a, this._uiHash());
            }
            return this.fromOutside = !1, !this.cancelHelperRemoval;
        },
        _trigger: function() {
            !1 === a.Widget.prototype._trigger.apply(this, arguments) && this.cancel();
        },
        _uiHash: function(b) {
            var c = b || this;
            return {
                helper: c.helper,
                placeholder: c.placeholder || a([]),
                position: c.position,
                originalPosition: c.originalPosition,
                offset: c.positionAbs,
                item: c.currentItem,
                sender: b ? b.element : null
            };
        }
    }), a.widget("ui.spinner", {
        version: "1.12.1",
        defaultElement: "<input>",
        widgetEventPrefix: "spin",
        options: {
            classes: {
                "ui-spinner": "ui-corner-all",
                "ui-spinner-down": "ui-corner-br",
                "ui-spinner-up": "ui-corner-tr"
            },
            culture: null,
            icons: {
                down: "ui-icon-triangle-1-s",
                up: "ui-icon-triangle-1-n"
            },
            incremental: !0,
            max: null,
            min: null,
            numberFormat: null,
            page: 10,
            step: 1,
            change: null,
            spin: null,
            start: null,
            stop: null
        },
        _create: function() {
            this._setOption("max", this.options.max), this._setOption("min", this.options.min), 
            this._setOption("step", this.options.step), "" !== this.value() && this._value(this.element.val(), !0), 
            this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete");
                }
            });
        },
        _getCreateOptions: function() {
            var b = this._super(), c = this.element;
            return a.each([ "min", "max", "step" ], function(a, d) {
                var e = c.attr(d);
                null != e && e.length && (b[d] = e);
            }), b;
        },
        _events: {
            keydown: function(a) {
                this._start(a) && this._keydown(a) && a.preventDefault();
            },
            keyup: "_stop",
            focus: function() {
                this.previous = this.element.val();
            },
            blur: function(a) {
                return this.cancelBlur ? void delete this.cancelBlur : (this._stop(), this._refresh(), 
                void (this.previous !== this.element.val() && this._trigger("change", a)));
            },
            mousewheel: function(a, b) {
                if (b) {
                    if (!this.spinning && !this._start(a)) return !1;
                    this._spin((0 < b ? 1 : -1) * this.options.step, a), clearTimeout(this.mousewheelTimer), 
                    this.mousewheelTimer = this._delay(function() {
                        this.spinning && this._stop(a);
                    }, 100), a.preventDefault();
                }
            },
            "mousedown .ui-spinner-button": function(b) {
                function c() {
                    this.element[0] === a.ui.safeActiveElement(this.document[0]) || (this.element.trigger("focus"), 
                    this.previous = d, this._delay(function() {
                        this.previous = d;
                    }));
                }
                var d;
                d = this.element[0] === a.ui.safeActiveElement(this.document[0]) ? this.previous : this.element.val(), 
                b.preventDefault(), c.call(this), this.cancelBlur = !0, this._delay(function() {
                    delete this.cancelBlur, c.call(this);
                }), !1 !== this._start(b) && this._repeat(null, a(b.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, b);
            },
            "mouseup .ui-spinner-button": "_stop",
            "mouseenter .ui-spinner-button": function(b) {
                return a(b.currentTarget).hasClass("ui-state-active") ? !1 !== this._start(b) && void this._repeat(null, a(b.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, b) : void 0;
            },
            "mouseleave .ui-spinner-button": "_stop"
        },
        _enhance: function() {
            this.uiSpinner = this.element.attr("autocomplete", "off").wrap("<span>").parent().append("<a></a><a></a>");
        },
        _draw: function() {
            this._enhance(), this._addClass(this.uiSpinner, "ui-spinner", "ui-widget ui-widget-content"), 
            this._addClass("ui-spinner-input"), this.element.attr("role", "spinbutton"), this.buttons = this.uiSpinner.children("a").attr("tabIndex", -1).attr("aria-hidden", !0).button({
                classes: {
                    "ui-button": ""
                }
            }), this._removeClass(this.buttons, "ui-corner-all"), this._addClass(this.buttons.first(), "ui-spinner-button ui-spinner-up"), 
            this._addClass(this.buttons.last(), "ui-spinner-button ui-spinner-down"), this.buttons.first().button({
                icon: this.options.icons.up,
                showLabel: !1
            }), this.buttons.last().button({
                icon: this.options.icons.down,
                showLabel: !1
            }), this.buttons.height() > Math.ceil(.5 * this.uiSpinner.height()) && 0 < this.uiSpinner.height() && this.uiSpinner.height(this.uiSpinner.height());
        },
        _keydown: function(b) {
            var c = this.options, d = a.ui.keyCode;
            switch (b.keyCode) {
              case d.UP:
                return this._repeat(null, 1, b), !0;

              case d.DOWN:
                return this._repeat(null, -1, b), !0;

              case d.PAGE_UP:
                return this._repeat(null, c.page, b), !0;

              case d.PAGE_DOWN:
                return this._repeat(null, -c.page, b), !0;
            }
            return !1;
        },
        _start: function(a) {
            return !(!this.spinning && !1 === this._trigger("start", a)) && (this.counter || (this.counter = 1), 
            this.spinning = !0);
        },
        _repeat: function(a, b, c) {
            a = a || 500, clearTimeout(this.timer), this.timer = this._delay(function() {
                this._repeat(40, b, c);
            }, a), this._spin(b * this.options.step, c);
        },
        _spin: function(a, b) {
            var c = this.value() || 0;
            this.counter || (this.counter = 1), c = this._adjustValue(c + a * this._increment(this.counter)), 
            this.spinning && !1 === this._trigger("spin", b, {
                value: c
            }) || (this._value(c), this.counter++);
        },
        _increment: function(b) {
            var c = this.options.incremental;
            return c ? a.isFunction(c) ? c(b) : Math.floor(b * b * b / 5e4 - b * b / 500 + 17 * b / 200 + 1) : 1;
        },
        _precision: function() {
            var a = this._precisionOf(this.options.step);
            return null !== this.options.min && (a = Math.max(a, this._precisionOf(this.options.min))), 
            a;
        },
        _precisionOf: function(a) {
            var b = "" + a, c = b.indexOf(".");
            return -1 === c ? 0 : b.length - c - 1;
        },
        _adjustValue: function(a) {
            var b, c, d = this.options;
            return c = a - (b = null !== d.min ? d.min : 0), a = b + (c = Math.round(c / d.step) * d.step), 
            a = parseFloat(a.toFixed(this._precision())), null !== d.max && a > d.max ? d.max : null !== d.min && d.min > a ? d.min : a;
        },
        _stop: function(a) {
            this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), 
            this.counter = 0, this.spinning = !1, this._trigger("stop", a));
        },
        _setOption: function(a, b) {
            var c, d, e;
            return "culture" === a || "numberFormat" === a ? (c = this._parse(this.element.val()), 
            this.options[a] = b, void this.element.val(this._format(c))) : ("max" !== a && "min" !== a && "step" !== a || "string" != typeof b || (b = this._parse(b)), 
            "icons" === a && (d = this.buttons.first().find(".ui-icon"), this._removeClass(d, null, this.options.icons.up), 
            this._addClass(d, null, b.up), e = this.buttons.last().find(".ui-icon"), this._removeClass(e, null, this.options.icons.down), 
            this._addClass(e, null, b.down)), void this._super(a, b));
        },
        _setOptionDisabled: function(a) {
            this._super(a), this._toggleClass(this.uiSpinner, null, "ui-state-disabled", !!a), 
            this.element.prop("disabled", !!a), this.buttons.button(a ? "disable" : "enable");
        },
        _setOptions: f(function(a) {
            this._super(a);
        }),
        _parse: function(a) {
            return "string" == typeof a && "" !== a && (a = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(a, 10, this.options.culture) : +a), 
            "" === a || isNaN(a) ? null : a;
        },
        _format: function(a) {
            return "" === a ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(a, this.options.numberFormat, this.options.culture) : a;
        },
        _refresh: function() {
            this.element.attr({
                "aria-valuemin": this.options.min,
                "aria-valuemax": this.options.max,
                "aria-valuenow": this._parse(this.element.val())
            });
        },
        isValid: function() {
            var a = this.value();
            return null !== a && a === this._adjustValue(a);
        },
        _value: function(a, b) {
            var c;
            "" === a || null !== (c = this._parse(a)) && (b || (c = this._adjustValue(c)), a = this._format(c)), 
            this.element.val(a), this._refresh();
        },
        _destroy: function() {
            this.element.prop("disabled", !1).removeAttr("autocomplete role aria-valuemin aria-valuemax aria-valuenow"), 
            this.uiSpinner.replaceWith(this.element);
        },
        stepUp: f(function(a) {
            this._stepUp(a);
        }),
        _stepUp: function(a) {
            this._start() && (this._spin((a || 1) * this.options.step), this._stop());
        },
        stepDown: f(function(a) {
            this._stepDown(a);
        }),
        _stepDown: function(a) {
            this._start() && (this._spin((a || 1) * -this.options.step), this._stop());
        },
        pageUp: f(function(a) {
            this._stepUp((a || 1) * this.options.page);
        }),
        pageDown: f(function(a) {
            this._stepDown((a || 1) * this.options.page);
        }),
        value: function(a) {
            return arguments.length ? void f(this._value).call(this, a) : this._parse(this.element.val());
        },
        widget: function() {
            return this.uiSpinner;
        }
    }), !1 !== a.uiBackCompat && a.widget("ui.spinner", a.ui.spinner, {
        _enhance: function() {
            this.uiSpinner = this.element.attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
        },
        _uiSpinnerHtml: function() {
            return "<span>";
        },
        _buttonHtml: function() {
            return "<a></a><a></a>";
        }
    }), a.ui.spinner, a.widget("ui.tabs", {
        version: "1.12.1",
        delay: 300,
        options: {
            active: null,
            classes: {
                "ui-tabs": "ui-corner-all",
                "ui-tabs-nav": "ui-corner-all",
                "ui-tabs-panel": "ui-corner-bottom",
                "ui-tabs-tab": "ui-corner-top"
            },
            collapsible: !1,
            event: "click",
            heightStyle: "content",
            hide: null,
            show: null,
            activate: null,
            beforeActivate: null,
            beforeLoad: null,
            load: null
        },
        _isLocal: (fb = /#.*$/, function(a) {
            var b, c;
            b = a.href.replace(fb, ""), c = location.href.replace(fb, "");
            try {
                b = decodeURIComponent(b);
            } catch (a) {}
            try {
                c = decodeURIComponent(c);
            } catch (a) {}
            return 1 < a.hash.length && b === c;
        }),
        _create: function() {
            var b = this, c = this.options;
            this.running = !1, this._addClass("ui-tabs", "ui-widget ui-widget-content"), this._toggleClass("ui-tabs-collapsible", null, c.collapsible), 
            this._processTabs(), c.active = this._initialActive(), a.isArray(c.disabled) && (c.disabled = a.unique(c.disabled.concat(a.map(this.tabs.filter(".ui-state-disabled"), function(a) {
                return b.tabs.index(a);
            }))).sort()), this.active = !1 !== this.options.active && this.anchors.length ? this._findActive(c.active) : a(), 
            this._refresh(), this.active.length && this.load(c.active);
        },
        _initialActive: function() {
            var b = this.options.active, c = this.options.collapsible, d = location.hash.substring(1);
            return null === b && (d && this.tabs.each(function(c, e) {
                return a(e).attr("aria-controls") === d ? (b = c, !1) : void 0;
            }), null === b && (b = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), null !== b && -1 !== b || (b = !!this.tabs.length && 0)), 
            !1 !== b && -1 === (b = this.tabs.index(this.tabs.eq(b))) && (b = !c && 0), !c && !1 === b && this.anchors.length && (b = 0), 
            b;
        },
        _getCreateEventData: function() {
            return {
                tab: this.active,
                panel: this.active.length ? this._getPanelForTab(this.active) : a()
            };
        },
        _tabKeydown: function(b) {
            var c = a(a.ui.safeActiveElement(this.document[0])).closest("li"), d = this.tabs.index(c), e = !0;
            if (!this._handlePageNav(b)) {
                switch (b.keyCode) {
                  case a.ui.keyCode.RIGHT:
                  case a.ui.keyCode.DOWN:
                    d++;
                    break;

                  case a.ui.keyCode.UP:
                  case a.ui.keyCode.LEFT:
                    e = !1, d--;
                    break;

                  case a.ui.keyCode.END:
                    d = this.anchors.length - 1;
                    break;

                  case a.ui.keyCode.HOME:
                    d = 0;
                    break;

                  case a.ui.keyCode.SPACE:
                    return b.preventDefault(), clearTimeout(this.activating), void this._activate(d);

                  case a.ui.keyCode.ENTER:
                    return b.preventDefault(), clearTimeout(this.activating), void this._activate(d !== this.options.active && d);

                  default:
                    return;
                }
                b.preventDefault(), clearTimeout(this.activating), d = this._focusNextTab(d, e), 
                b.ctrlKey || b.metaKey || (c.attr("aria-selected", "false"), this.tabs.eq(d).attr("aria-selected", "true"), 
                this.activating = this._delay(function() {
                    this.option("active", d);
                }, this.delay));
            }
        },
        _panelKeydown: function(b) {
            this._handlePageNav(b) || b.ctrlKey && b.keyCode === a.ui.keyCode.UP && (b.preventDefault(), 
            this.active.trigger("focus"));
        },
        _handlePageNav: function(b) {
            return b.altKey && b.keyCode === a.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), 
            !0) : b.altKey && b.keyCode === a.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), 
            !0) : void 0;
        },
        _findNextTab: function(b, c) {
            for (var d = this.tabs.length - 1; -1 !== a.inArray((d < b && (b = 0), b < 0 && (b = d), 
            b), this.options.disabled); ) b = c ? b + 1 : b - 1;
            return b;
        },
        _focusNextTab: function(a, b) {
            return a = this._findNextTab(a, b), this.tabs.eq(a).trigger("focus"), a;
        },
        _setOption: function(a, b) {
            return "active" === a ? void this._activate(b) : (this._super(a, b), "collapsible" === a && (this._toggleClass("ui-tabs-collapsible", null, b), 
            b || !1 !== this.options.active || this._activate(0)), "event" === a && this._setupEvents(b), 
            void ("heightStyle" === a && this._setupHeightStyle(b)));
        },
        _sanitizeSelector: function(a) {
            return a ? a.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : "";
        },
        refresh: function() {
            var b = this.options, c = this.tablist.children(":has(a[href])");
            b.disabled = a.map(c.filter(".ui-state-disabled"), function(a) {
                return c.index(a);
            }), this._processTabs(), !1 !== b.active && this.anchors.length ? this.active.length && !a.contains(this.tablist[0], this.active[0]) ? this.tabs.length === b.disabled.length ? (b.active = !1, 
            this.active = a()) : this._activate(this._findNextTab(Math.max(0, b.active - 1), !1)) : b.active = this.tabs.index(this.active) : (b.active = !1, 
            this.active = a()), this._refresh();
        },
        _refresh: function() {
            this._setOptionDisabled(this.options.disabled), this._setupEvents(this.options.event), 
            this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
                "aria-selected": "false",
                "aria-expanded": "false",
                tabIndex: -1
            }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
                "aria-hidden": "true"
            }), this.active.length ? (this.active.attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            }), this._addClass(this.active, "ui-tabs-active", "ui-state-active"), this._getPanelForTab(this.active).show().attr({
                "aria-hidden": "false"
            })) : this.tabs.eq(0).attr("tabIndex", 0);
        },
        _processTabs: function() {
            var b = this, c = this.tabs, d = this.anchors, e = this.panels;
            this.tablist = this._getList().attr("role", "tablist"), this._addClass(this.tablist, "ui-tabs-nav", "ui-helper-reset ui-helper-clearfix ui-widget-header"), 
            this.tablist.on("mousedown" + this.eventNamespace, "> li", function(b) {
                a(this).is(".ui-state-disabled") && b.preventDefault();
            }).on("focus" + this.eventNamespace, ".ui-tabs-anchor", function() {
                a(this).closest("li").is(".ui-state-disabled") && this.blur();
            }), this.tabs = this.tablist.find("> li:has(a[href])").attr({
                role: "tab",
                tabIndex: -1
            }), this._addClass(this.tabs, "ui-tabs-tab", "ui-state-default"), this.anchors = this.tabs.map(function() {
                return a("a", this)[0];
            }).attr({
                role: "presentation",
                tabIndex: -1
            }), this._addClass(this.anchors, "ui-tabs-anchor"), this.panels = a(), this.anchors.each(function(c, d) {
                var e, f, g, h = a(d).uniqueId().attr("id"), i = a(d).closest("li"), j = i.attr("aria-controls");
                b._isLocal(d) ? (g = (e = d.hash).substring(1), f = b.element.find(b._sanitizeSelector(e))) : (e = "#" + (g = i.attr("aria-controls") || a({}).uniqueId()[0].id), 
                (f = b.element.find(e)).length || (f = b._createPanel(g)).insertAfter(b.panels[c - 1] || b.tablist), 
                f.attr("aria-live", "polite")), f.length && (b.panels = b.panels.add(f)), j && i.data("ui-tabs-aria-controls", j), 
                i.attr({
                    "aria-controls": g,
                    "aria-labelledby": h
                }), f.attr("aria-labelledby", h);
            }), this.panels.attr("role", "tabpanel"), this._addClass(this.panels, "ui-tabs-panel", "ui-widget-content"), 
            c && (this._off(c.not(this.tabs)), this._off(d.not(this.anchors)), this._off(e.not(this.panels)));
        },
        _getList: function() {
            return this.tablist || this.element.find("ol, ul").eq(0);
        },
        _createPanel: function(b) {
            return a("<div>").attr("id", b).data("ui-tabs-destroy", !0);
        },
        _setOptionDisabled: function(b) {
            var c, d, e;
            for (a.isArray(b) && (b.length ? b.length === this.anchors.length && (b = !0) : b = !1), 
            e = 0; d = this.tabs[e]; e++) c = a(d), !0 === b || -1 !== a.inArray(e, b) ? (c.attr("aria-disabled", "true"), 
            this._addClass(c, null, "ui-state-disabled")) : (c.removeAttr("aria-disabled"), 
            this._removeClass(c, null, "ui-state-disabled"));
            this.options.disabled = b, this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !0 === b);
        },
        _setupEvents: function(b) {
            var c = {};
            b && a.each(b.split(" "), function(a, b) {
                c[b] = "_eventHandler";
            }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(!0, this.anchors, {
                click: function(a) {
                    a.preventDefault();
                }
            }), this._on(this.anchors, c), this._on(this.tabs, {
                keydown: "_tabKeydown"
            }), this._on(this.panels, {
                keydown: "_panelKeydown"
            }), this._focusable(this.tabs), this._hoverable(this.tabs);
        },
        _setupHeightStyle: function(b) {
            var c, d = this.element.parent();
            "fill" === b ? (c = d.height(), c -= this.element.outerHeight() - this.element.height(), 
            this.element.siblings(":visible").each(function() {
                var b = a(this), d = b.css("position");
                "absolute" !== d && "fixed" !== d && (c -= b.outerHeight(!0));
            }), this.element.children().not(this.panels).each(function() {
                c -= a(this).outerHeight(!0);
            }), this.panels.each(function() {
                a(this).height(Math.max(0, c - a(this).innerHeight() + a(this).height()));
            }).css("overflow", "auto")) : "auto" === b && (c = 0, this.panels.each(function() {
                c = Math.max(c, a(this).height("").height());
            }).height(c));
        },
        _eventHandler: function(b) {
            var c = this.options, d = this.active, e = a(b.currentTarget).closest("li"), f = e[0] === d[0], g = f && c.collapsible, h = g ? a() : this._getPanelForTab(e), i = d.length ? this._getPanelForTab(d) : a(), j = {
                oldTab: d,
                oldPanel: i,
                newTab: g ? a() : e,
                newPanel: h
            };
            b.preventDefault(), e.hasClass("ui-state-disabled") || e.hasClass("ui-tabs-loading") || this.running || f && !c.collapsible || !1 === this._trigger("beforeActivate", b, j) || (c.active = !g && this.tabs.index(e), 
            this.active = f ? a() : e, this.xhr && this.xhr.abort(), i.length || h.length || a.error("jQuery UI Tabs: Mismatching fragment identifier."), 
            h.length && this.load(this.tabs.index(e), b), this._toggle(b, j));
        },
        _toggle: function(b, c) {
            function d() {
                f.running = !1, f._trigger("activate", b, c);
            }
            function e() {
                f._addClass(c.newTab.closest("li"), "ui-tabs-active", "ui-state-active"), g.length && f.options.show ? f._show(g, f.options.show, d) : (g.show(), 
                d());
            }
            var f = this, g = c.newPanel, h = c.oldPanel;
            this.running = !0, h.length && this.options.hide ? this._hide(h, this.options.hide, function() {
                f._removeClass(c.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"), e();
            }) : (this._removeClass(c.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"), 
            h.hide(), e()), h.attr("aria-hidden", "true"), c.oldTab.attr({
                "aria-selected": "false",
                "aria-expanded": "false"
            }), g.length && h.length ? c.oldTab.attr("tabIndex", -1) : g.length && this.tabs.filter(function() {
                return 0 === a(this).attr("tabIndex");
            }).attr("tabIndex", -1), g.attr("aria-hidden", "false"), c.newTab.attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            });
        },
        _activate: function(b) {
            var c, d = this._findActive(b);
            d[0] !== this.active[0] && (d.length || (d = this.active), c = d.find(".ui-tabs-anchor")[0], 
            this._eventHandler({
                target: c,
                currentTarget: c,
                preventDefault: a.noop
            }));
        },
        _findActive: function(b) {
            return !1 === b ? a() : this.tabs.eq(b);
        },
        _getIndex: function(b) {
            return "string" == typeof b && (b = this.anchors.index(this.anchors.filter("[href$='" + a.ui.escapeSelector(b) + "']"))), 
            b;
        },
        _destroy: function() {
            this.xhr && this.xhr.abort(), this.tablist.removeAttr("role").off(this.eventNamespace), 
            this.anchors.removeAttr("role tabIndex").removeUniqueId(), this.tabs.add(this.panels).each(function() {
                a.data(this, "ui-tabs-destroy") ? a(this).remove() : a(this).removeAttr("role tabIndex aria-live aria-busy aria-selected aria-labelledby aria-hidden aria-expanded");
            }), this.tabs.each(function() {
                var b = a(this), c = b.data("ui-tabs-aria-controls");
                c ? b.attr("aria-controls", c).removeData("ui-tabs-aria-controls") : b.removeAttr("aria-controls");
            }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "");
        },
        enable: function(b) {
            var c = this.options.disabled;
            !1 !== c && (c = void 0 !== b && (b = this._getIndex(b), a.isArray(c) ? a.map(c, function(a) {
                return a !== b ? a : null;
            }) : a.map(this.tabs, function(a, c) {
                return c !== b ? c : null;
            })), this._setOptionDisabled(c));
        },
        disable: function(b) {
            var c = this.options.disabled;
            if (!0 !== c) {
                if (void 0 === b) c = !0; else {
                    if (b = this._getIndex(b), -1 !== a.inArray(b, c)) return;
                    c = a.isArray(c) ? a.merge([ b ], c).sort() : [ b ];
                }
                this._setOptionDisabled(c);
            }
        },
        load: function(b, c) {
            b = this._getIndex(b);
            function d(a, b) {
                "abort" === b && e.panels.stop(!1, !0), e._removeClass(f, "ui-tabs-loading"), h.removeAttr("aria-busy"), 
                a === e.xhr && delete e.xhr;
            }
            var e = this, f = this.tabs.eq(b), g = f.find(".ui-tabs-anchor"), h = this._getPanelForTab(f), i = {
                tab: f,
                panel: h
            };
            this._isLocal(g[0]) || (this.xhr = a.ajax(this._ajaxSettings(g, c, i)), this.xhr && "canceled" !== this.xhr.statusText && (this._addClass(f, "ui-tabs-loading"), 
            h.attr("aria-busy", "true"), this.xhr.done(function(a, b, f) {
                setTimeout(function() {
                    h.html(a), e._trigger("load", c, i), d(f, b);
                }, 1);
            }).fail(function(a, b) {
                setTimeout(function() {
                    d(a, b);
                }, 1);
            })));
        },
        _ajaxSettings: function(b, c, d) {
            var e = this;
            return {
                url: b.attr("href").replace(/#.*$/, ""),
                beforeSend: function(b, f) {
                    return e._trigger("beforeLoad", c, a.extend({
                        jqXHR: b,
                        ajaxSettings: f
                    }, d));
                }
            };
        },
        _getPanelForTab: function(b) {
            var c = a(b).attr("aria-controls");
            return this.element.find(this._sanitizeSelector("#" + c));
        }
    }), !1 !== a.uiBackCompat && a.widget("ui.tabs", a.ui.tabs, {
        _processTabs: function() {
            this._superApply(arguments), this._addClass(this.tabs, "ui-tab");
        }
    }), a.ui.tabs, a.widget("ui.tooltip", {
        version: "1.12.1",
        options: {
            classes: {
                "ui-tooltip": "ui-corner-all ui-widget-shadow"
            },
            content: function() {
                var b = a(this).attr("title") || "";
                return a("<a>").text(b).html();
            },
            hide: !0,
            items: "[title]:not([disabled])",
            position: {
                my: "left top+15",
                at: "left bottom",
                collision: "flipfit flip"
            },
            show: !0,
            track: !1,
            close: null,
            open: null
        },
        _addDescribedBy: function(b, c) {
            var d = (b.attr("aria-describedby") || "").split(/\s+/);
            d.push(c), b.data("ui-tooltip-id", c).attr("aria-describedby", a.trim(d.join(" ")));
        },
        _removeDescribedBy: function(b) {
            var c = b.data("ui-tooltip-id"), d = (b.attr("aria-describedby") || "").split(/\s+/), e = a.inArray(c, d);
            -1 !== e && d.splice(e, 1), b.removeData("ui-tooltip-id"), (d = a.trim(d.join(" "))) ? b.attr("aria-describedby", d) : b.removeAttr("aria-describedby");
        },
        _create: function() {
            this._on({
                mouseover: "open",
                focusin: "open"
            }), this.tooltips = {}, this.parents = {}, this.liveRegion = a("<div>").attr({
                role: "log",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).appendTo(this.document[0].body), this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"), 
            this.disabledTitles = a([]);
        },
        _setOption: function(b, c) {
            var d = this;
            this._super(b, c), "content" === b && a.each(this.tooltips, function(a, b) {
                d._updateContent(b.element);
            });
        },
        _setOptionDisabled: function(a) {
            this[a ? "_disable" : "_enable"]();
        },
        _disable: function() {
            var b = this;
            a.each(this.tooltips, function(c, d) {
                var e = a.Event("blur");
                e.target = e.currentTarget = d.element[0], b.close(e, !0);
            }), this.disabledTitles = this.disabledTitles.add(this.element.find(this.options.items).addBack().filter(function() {
                var b = a(this);
                return b.is("[title]") ? b.data("ui-tooltip-title", b.attr("title")).removeAttr("title") : void 0;
            }));
        },
        _enable: function() {
            this.disabledTitles.each(function() {
                var b = a(this);
                b.data("ui-tooltip-title") && b.attr("title", b.data("ui-tooltip-title"));
            }), this.disabledTitles = a([]);
        },
        open: function(b) {
            var c = this, d = a(b ? b.target : this.element).closest(this.options.items);
            d.length && !d.data("ui-tooltip-id") && (d.attr("title") && d.data("ui-tooltip-title", d.attr("title")), 
            d.data("ui-tooltip-open", !0), b && "mouseover" === b.type && d.parents().each(function() {
                var b, d = a(this);
                d.data("ui-tooltip-open") && ((b = a.Event("blur")).target = b.currentTarget = this, 
                c.close(b, !0)), d.attr("title") && (d.uniqueId(), c.parents[this.id] = {
                    element: this,
                    title: d.attr("title")
                }, d.attr("title", ""));
            }), this._registerCloseHandlers(b, d), this._updateContent(d, b));
        },
        _updateContent: function(a, b) {
            var c, d = this.options.content, e = this, f = b ? b.type : null;
            return "string" == typeof d || d.nodeType || d.jquery ? this._open(b, a, d) : void ((c = d.call(a[0], function(c) {
                e._delay(function() {
                    a.data("ui-tooltip-open") && (b && (b.type = f), this._open(b, a, c));
                });
            })) && this._open(b, a, c));
        },
        _open: function(b, c, d) {
            function e(a) {
                j.of = a, g.is(":hidden") || g.position(j);
            }
            var f, g, h, i, j = a.extend({}, this.options.position);
            if (d) {
                if (f = this._find(c)) return void f.tooltip.find(".ui-tooltip-content").html(d);
                c.is("[title]") && (b && "mouseover" === b.type ? c.attr("title", "") : c.removeAttr("title")), 
                f = this._tooltip(c), g = f.tooltip, this._addDescribedBy(c, g.attr("id")), g.find(".ui-tooltip-content").html(d), 
                this.liveRegion.children().hide(), (i = a("<div>").html(g.find(".ui-tooltip-content").html())).removeAttr("name").find("[name]").removeAttr("name"), 
                i.removeAttr("id").find("[id]").removeAttr("id"), i.appendTo(this.liveRegion), this.options.track && b && /^mouse/.test(b.type) ? (this._on(this.document, {
                    mousemove: e
                }), e(b)) : g.position(a.extend({
                    of: c
                }, this.options.position)), g.hide(), this._show(g, this.options.show), this.options.track && this.options.show && this.options.show.delay && (h = this.delayedShow = setInterval(function() {
                    g.is(":visible") && (e(j.of), clearInterval(h));
                }, a.fx.interval)), this._trigger("open", b, {
                    tooltip: g
                });
            }
        },
        _registerCloseHandlers: function(b, c) {
            var d = {
                keyup: function(b) {
                    if (b.keyCode === a.ui.keyCode.ESCAPE) {
                        var d = a.Event(b);
                        d.currentTarget = c[0], this.close(d, !0);
                    }
                }
            };
            c[0] !== this.element[0] && (d.remove = function() {
                this._removeTooltip(this._find(c).tooltip);
            }), b && "mouseover" !== b.type || (d.mouseleave = "close"), b && "focusin" !== b.type || (d.focusout = "close"), 
            this._on(!0, c, d);
        },
        close: function(b) {
            var c, d = this, e = a(b ? b.currentTarget : this.element), f = this._find(e);
            return f ? (c = f.tooltip, void (f.closing || (clearInterval(this.delayedShow), 
            e.data("ui-tooltip-title") && !e.attr("title") && e.attr("title", e.data("ui-tooltip-title")), 
            this._removeDescribedBy(e), f.hiding = !0, c.stop(!0), this._hide(c, this.options.hide, function() {
                d._removeTooltip(a(this));
            }), e.removeData("ui-tooltip-open"), this._off(e, "mouseleave focusout keyup"), 
            e[0] !== this.element[0] && this._off(e, "remove"), this._off(this.document, "mousemove"), 
            b && "mouseleave" === b.type && a.each(this.parents, function(b, c) {
                a(c.element).attr("title", c.title), delete d.parents[b];
            }), f.closing = !0, this._trigger("close", b, {
                tooltip: c
            }), f.hiding || (f.closing = !1)))) : void e.removeData("ui-tooltip-open");
        },
        _tooltip: function(b) {
            var c = a("<div>").attr("role", "tooltip"), d = a("<div>").appendTo(c), e = c.uniqueId().attr("id");
            return this._addClass(d, "ui-tooltip-content"), this._addClass(c, "ui-tooltip", "ui-widget ui-widget-content"), 
            c.appendTo(this._appendTo(b)), this.tooltips[e] = {
                element: b,
                tooltip: c
            };
        },
        _find: function(a) {
            var b = a.data("ui-tooltip-id");
            return b ? this.tooltips[b] : null;
        },
        _removeTooltip: function(a) {
            a.remove(), delete this.tooltips[a.attr("id")];
        },
        _appendTo: function(a) {
            var b = a.closest(".ui-front, dialog");
            return b.length || (b = this.document[0].body), b;
        },
        _destroy: function() {
            var b = this;
            a.each(this.tooltips, function(c, d) {
                var e = a.Event("blur"), f = d.element;
                e.target = e.currentTarget = f[0], b.close(e, !0), a("#" + c).remove(), f.data("ui-tooltip-title") && (f.attr("title") || f.attr("title", f.data("ui-tooltip-title")), 
                f.removeData("ui-tooltip-title"));
            }), this.liveRegion.remove();
        }
    }), !1 !== a.uiBackCompat && a.widget("ui.tooltip", a.ui.tooltip, {
        options: {
            tooltipClass: null
        },
        _tooltip: function() {
            var a = this._superApply(arguments);
            return this.options.tooltipClass && a.tooltip.addClass(this.options.tooltipClass), 
            a;
        }
    }), a.ui.tooltip;
}), function(a, b) {
    "object" == typeof exports && "undefined" != typeof module ? b(exports, require("jquery")) : "function" == typeof define && define.amd ? define([ "exports", "jquery" ], b) : b((a = a || self).bootstrap = {}, a.jQuery);
}(this, function(a, b) {
    "use strict";
    function c(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    function d(a, b, d) {
        return b && c(a.prototype, b), d && c(a, d), a;
    }
    function e(a, b) {
        var c = Object.keys(a);
        if (Object.getOwnPropertySymbols) {
            var d = Object.getOwnPropertySymbols(a);
            b && (d = d.filter(function(b) {
                return Object.getOwnPropertyDescriptor(a, b).enumerable;
            })), c.push.apply(c, d);
        }
        return c;
    }
    function f(a) {
        for (var b = 1; b < arguments.length; b++) {
            var c = null != arguments[b] ? arguments[b] : {};
            b % 2 ? e(Object(c), !0).forEach(function(b) {
                var d, e, f;
                d = a, f = c[e = b], e in d ? Object.defineProperty(d, e, {
                    value: f,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : d[e] = f;
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(a, Object.getOwnPropertyDescriptors(c)) : e(Object(c)).forEach(function(b) {
                Object.defineProperty(a, b, Object.getOwnPropertyDescriptor(c, b));
            });
        }
        return a;
    }
    b = b && b.hasOwnProperty("default") ? b.default : b;
    var g = "transitionend";
    var h = {
        TRANSITION_END: "bsTransitionEnd",
        getUID: function(a) {
            for (;a += ~~(1e6 * Math.random()), document.getElementById(a); ) ;
            return a;
        },
        getSelectorFromElement: function(a) {
            var b = a.getAttribute("data-target");
            if (!b || "#" === b) {
                var c = a.getAttribute("href");
                b = c && "#" !== c ? c.trim() : "";
            }
            try {
                return document.querySelector(b) ? b : null;
            } catch (a) {
                return null;
            }
        },
        getTransitionDurationFromElement: function(a) {
            if (!a) return 0;
            var c = b(a).css("transition-duration"), d = b(a).css("transition-delay"), e = parseFloat(c), f = parseFloat(d);
            return e || f ? (c = c.split(",")[0], d = d.split(",")[0], 1e3 * (parseFloat(c) + parseFloat(d))) : 0;
        },
        reflow: function(a) {
            return a.offsetHeight;
        },
        triggerTransitionEnd: function(a) {
            b(a).trigger(g);
        },
        supportsTransitionEnd: function() {
            return Boolean(g);
        },
        isElement: function(a) {
            return (a[0] || a).nodeType;
        },
        typeCheckConfig: function(a, b, c) {
            for (var d in c) if (Object.prototype.hasOwnProperty.call(c, d)) {
                var e = c[d], f = b[d], g = f && h.isElement(f) ? "element" : {}.toString.call(f).match(/\s([a-z]+)/i)[1].toLowerCase();
                if (!new RegExp(e).test(g)) throw new Error(a.toUpperCase() + ': Option "' + d + '" provided type "' + g + '" but expected type "' + e + '".');
            }
        },
        findShadowRoot: function(a) {
            if (!document.documentElement.attachShadow) return null;
            if ("function" != typeof a.getRootNode) return a instanceof ShadowRoot ? a : a.parentNode ? h.findShadowRoot(a.parentNode) : null;
            var b = a.getRootNode();
            return b instanceof ShadowRoot ? b : null;
        },
        jQueryDetection: function() {
            if (void 0 === b) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
            var a = b.fn.jquery.split(" ")[0].split(".");
            if (a[0] < 2 && a[1] < 9 || 1 === a[0] && 9 === a[1] && a[2] < 1 || 4 <= a[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
        }
    };
    h.jQueryDetection(), b.fn.emulateTransitionEnd = function(a) {
        var c = this, d = !1;
        return b(this).one(h.TRANSITION_END, function() {
            d = !0;
        }), setTimeout(function() {
            d || h.triggerTransitionEnd(c);
        }, a), this;
    }, b.event.special[h.TRANSITION_END] = {
        bindType: g,
        delegateType: g,
        handle: function(a) {
            if (b(a.target).is(this)) return a.handleObj.handler.apply(this, arguments);
        }
    };
    var i, j = "alert", k = "bs.alert", l = "." + k, m = b.fn[j], n = {
        CLOSE: "close" + l,
        CLOSED: "closed" + l,
        CLICK_DATA_API: "click" + l + ".data-api"
    }, o = ((i = p.prototype).close = function(a) {
        var b = this._element;
        a && (b = this._getRootElement(a)), this._triggerCloseEvent(b).isDefaultPrevented() || this._removeElement(b);
    }, i.dispose = function() {
        b.removeData(this._element, k), this._element = null;
    }, i._getRootElement = function(a) {
        var c = h.getSelectorFromElement(a), d = !1;
        return c && (d = document.querySelector(c)), d || b(a).closest(".alert")[0];
    }, i._triggerCloseEvent = function(a) {
        var c = b.Event(n.CLOSE);
        return b(a).trigger(c), c;
    }, i._removeElement = function(a) {
        var c = this;
        if (b(a).removeClass("show"), b(a).hasClass("fade")) {
            var d = h.getTransitionDurationFromElement(a);
            b(a).one(h.TRANSITION_END, function(b) {
                return c._destroyElement(a, b);
            }).emulateTransitionEnd(d);
        } else this._destroyElement(a);
    }, i._destroyElement = function(a) {
        b(a).detach().trigger(n.CLOSED).remove();
    }, p._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this), d = c.data(k);
            d || (d = new p(this), c.data(k, d)), "close" === a && d[a](this);
        });
    }, p._handleDismiss = function(a) {
        return function(b) {
            b && b.preventDefault(), a.close(this);
        };
    }, d(p, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    } ]), p);
    function p(a) {
        this._element = a;
    }
    b(document).on(n.CLICK_DATA_API, '[data-dismiss="alert"]', o._handleDismiss(new o())), 
    b.fn[j] = o._jQueryInterface, b.fn[j].Constructor = o, b.fn[j].noConflict = function() {
        return b.fn[j] = m, o._jQueryInterface;
    };
    var q, r = "button", s = "bs.button", t = "." + s, u = ".data-api", v = b.fn[r], w = "active", x = '[data-toggle^="button"]', y = 'input:not([type="hidden"])', z = {
        CLICK_DATA_API: "click" + t + u,
        FOCUS_BLUR_DATA_API: "focus" + t + u + " blur" + t + u,
        LOAD_DATA_API: "load" + t + u
    }, A = ((q = B.prototype).toggle = function() {
        var a = !0, c = !0, d = b(this._element).closest('[data-toggle="buttons"]')[0];
        if (d) {
            var e = this._element.querySelector(y);
            if (e) {
                if ("radio" === e.type) if (e.checked && this._element.classList.contains(w)) a = !1; else {
                    var f = d.querySelector(".active");
                    f && b(f).removeClass(w);
                } else "checkbox" === e.type ? "LABEL" === this._element.tagName && e.checked === this._element.classList.contains(w) && (a = !1) : a = !1;
                a && (e.checked = !this._element.classList.contains(w), b(e).trigger("change")), 
                e.focus(), c = !1;
            }
        }
        this._element.hasAttribute("disabled") || this._element.classList.contains("disabled") || (c && this._element.setAttribute("aria-pressed", !this._element.classList.contains(w)), 
        a && b(this._element).toggleClass(w));
    }, q.dispose = function() {
        b.removeData(this._element, s), this._element = null;
    }, B._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this).data(s);
            c || (c = new B(this), b(this).data(s, c)), "toggle" === a && c[a]();
        });
    }, d(B, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    } ]), B);
    function B(a) {
        this._element = a;
    }
    b(document).on(z.CLICK_DATA_API, x, function(a) {
        var c = a.target;
        if (b(c).hasClass("btn") || (c = b(c).closest(".btn")[0]), !c || c.hasAttribute("disabled") || c.classList.contains("disabled")) a.preventDefault(); else {
            var d = c.querySelector(y);
            if (d && (d.hasAttribute("disabled") || d.classList.contains("disabled"))) return void a.preventDefault();
            A._jQueryInterface.call(b(c), "toggle");
        }
    }).on(z.FOCUS_BLUR_DATA_API, x, function(a) {
        var c = b(a.target).closest(".btn")[0];
        b(c).toggleClass("focus", /^focus(in)?$/.test(a.type));
    }), b(window).on(z.LOAD_DATA_API, function() {
        for (var a = [].slice.call(document.querySelectorAll('[data-toggle="buttons"] .btn')), b = 0, c = a.length; b < c; b++) {
            var d = a[b], e = d.querySelector(y);
            e.checked || e.hasAttribute("checked") ? d.classList.add(w) : d.classList.remove(w);
        }
        for (var f = 0, g = (a = [].slice.call(document.querySelectorAll('[data-toggle="button"]'))).length; f < g; f++) {
            var h = a[f];
            "true" === h.getAttribute("aria-pressed") ? h.classList.add(w) : h.classList.remove(w);
        }
    }), b.fn[r] = A._jQueryInterface, b.fn[r].Constructor = A, b.fn[r].noConflict = function() {
        return b.fn[r] = v, A._jQueryInterface;
    };
    var C, D = "carousel", E = "bs.carousel", F = "." + E, G = b.fn[D], H = {
        interval: 5e3,
        keyboard: !0,
        slide: !1,
        pause: "hover",
        wrap: !0,
        touch: !0
    }, I = {
        interval: "(number|boolean)",
        keyboard: "boolean",
        slide: "(boolean|string)",
        pause: "(string|boolean)",
        wrap: "boolean",
        touch: "boolean"
    }, J = "next", K = "prev", L = {
        SLIDE: "slide" + F,
        SLID: "slid" + F,
        KEYDOWN: "keydown" + F,
        MOUSEENTER: "mouseenter" + F,
        MOUSELEAVE: "mouseleave" + F,
        TOUCHSTART: "touchstart" + F,
        TOUCHMOVE: "touchmove" + F,
        TOUCHEND: "touchend" + F,
        POINTERDOWN: "pointerdown" + F,
        POINTERUP: "pointerup" + F,
        DRAG_START: "dragstart" + F,
        LOAD_DATA_API: "load" + F + ".data-api",
        CLICK_DATA_API: "click" + F + ".data-api"
    }, M = "active", N = ".active.carousel-item", O = {
        TOUCH: "touch",
        PEN: "pen"
    }, P = ((C = Q.prototype).next = function() {
        this._isSliding || this._slide(J);
    }, C.nextWhenVisible = function() {
        !document.hidden && b(this._element).is(":visible") && "hidden" !== b(this._element).css("visibility") && this.next();
    }, C.prev = function() {
        this._isSliding || this._slide(K);
    }, C.pause = function(a) {
        a || (this._isPaused = !0), this._element.querySelector(".carousel-item-next, .carousel-item-prev") && (h.triggerTransitionEnd(this._element), 
        this.cycle(!0)), clearInterval(this._interval), this._interval = null;
    }, C.cycle = function(a) {
        a || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), 
        this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
    }, C.to = function(a) {
        var c = this;
        this._activeElement = this._element.querySelector(N);
        var d = this._getItemIndex(this._activeElement);
        if (!(a > this._items.length - 1 || a < 0)) if (this._isSliding) b(this._element).one(L.SLID, function() {
            return c.to(a);
        }); else {
            if (d === a) return this.pause(), void this.cycle();
            var e = d < a ? J : K;
            this._slide(e, this._items[a]);
        }
    }, C.dispose = function() {
        b(this._element).off(F), b.removeData(this._element, E), this._items = null, this._config = null, 
        this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, 
        this._activeElement = null, this._indicatorsElement = null;
    }, C._getConfig = function(a) {
        return a = f({}, H, {}, a), h.typeCheckConfig(D, a, I), a;
    }, C._handleSwipe = function() {
        var a = Math.abs(this.touchDeltaX);
        if (!(a <= 40)) {
            var b = a / this.touchDeltaX;
            (this.touchDeltaX = 0) < b && this.prev(), b < 0 && this.next();
        }
    }, C._addEventListeners = function() {
        var a = this;
        this._config.keyboard && b(this._element).on(L.KEYDOWN, function(b) {
            return a._keydown(b);
        }), "hover" === this._config.pause && b(this._element).on(L.MOUSEENTER, function(b) {
            return a.pause(b);
        }).on(L.MOUSELEAVE, function(b) {
            return a.cycle(b);
        }), this._config.touch && this._addTouchEventListeners();
    }, C._addTouchEventListeners = function() {
        var a = this;
        if (this._touchSupported) {
            var c = function(b) {
                a._pointerEvent && O[b.originalEvent.pointerType.toUpperCase()] ? a.touchStartX = b.originalEvent.clientX : a._pointerEvent || (a.touchStartX = b.originalEvent.touches[0].clientX);
            }, d = function(b) {
                a._pointerEvent && O[b.originalEvent.pointerType.toUpperCase()] && (a.touchDeltaX = b.originalEvent.clientX - a.touchStartX), 
                a._handleSwipe(), "hover" === a._config.pause && (a.pause(), a.touchTimeout && clearTimeout(a.touchTimeout), 
                a.touchTimeout = setTimeout(function(b) {
                    return a.cycle(b);
                }, 500 + a._config.interval));
            };
            b(this._element.querySelectorAll(".carousel-item img")).on(L.DRAG_START, function(a) {
                return a.preventDefault();
            }), this._pointerEvent ? (b(this._element).on(L.POINTERDOWN, function(a) {
                return c(a);
            }), b(this._element).on(L.POINTERUP, function(a) {
                return d(a);
            }), this._element.classList.add("pointer-event")) : (b(this._element).on(L.TOUCHSTART, function(a) {
                return c(a);
            }), b(this._element).on(L.TOUCHMOVE, function(b) {
                var c;
                (c = b).originalEvent.touches && 1 < c.originalEvent.touches.length ? a.touchDeltaX = 0 : a.touchDeltaX = c.originalEvent.touches[0].clientX - a.touchStartX;
            }), b(this._element).on(L.TOUCHEND, function(a) {
                return d(a);
            }));
        }
    }, C._keydown = function(a) {
        if (!/input|textarea/i.test(a.target.tagName)) switch (a.which) {
          case 37:
            a.preventDefault(), this.prev();
            break;

          case 39:
            a.preventDefault(), this.next();
        }
    }, C._getItemIndex = function(a) {
        return this._items = a && a.parentNode ? [].slice.call(a.parentNode.querySelectorAll(".carousel-item")) : [], 
        this._items.indexOf(a);
    }, C._getItemByDirection = function(a, b) {
        var c = a === J, d = a === K, e = this._getItemIndex(b), f = this._items.length - 1;
        if ((d && 0 === e || c && e === f) && !this._config.wrap) return b;
        var g = (e + (a === K ? -1 : 1)) % this._items.length;
        return -1 == g ? this._items[this._items.length - 1] : this._items[g];
    }, C._triggerSlideEvent = function(a, c) {
        var d = this._getItemIndex(a), e = this._getItemIndex(this._element.querySelector(N)), f = b.Event(L.SLIDE, {
            relatedTarget: a,
            direction: c,
            from: e,
            to: d
        });
        return b(this._element).trigger(f), f;
    }, C._setActiveIndicatorElement = function(a) {
        if (this._indicatorsElement) {
            var c = [].slice.call(this._indicatorsElement.querySelectorAll(".active"));
            b(c).removeClass(M);
            var d = this._indicatorsElement.children[this._getItemIndex(a)];
            d && b(d).addClass(M);
        }
    }, C._slide = function(a, c) {
        var d, e, f, g = this, i = this._element.querySelector(N), j = this._getItemIndex(i), k = c || i && this._getItemByDirection(a, i), l = this._getItemIndex(k), m = Boolean(this._interval);
        if (f = a === J ? (d = "carousel-item-left", e = "carousel-item-next", "left") : (d = "carousel-item-right", 
        e = "carousel-item-prev", "right"), k && b(k).hasClass(M)) this._isSliding = !1; else if (!this._triggerSlideEvent(k, f).isDefaultPrevented() && i && k) {
            this._isSliding = !0, m && this.pause(), this._setActiveIndicatorElement(k);
            var n = b.Event(L.SLID, {
                relatedTarget: k,
                direction: f,
                from: j,
                to: l
            });
            if (b(this._element).hasClass("slide")) {
                b(k).addClass(e), h.reflow(k), b(i).addClass(d), b(k).addClass(d);
                var o = parseInt(k.getAttribute("data-interval"), 10);
                o ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, 
                this._config.interval = o) : this._config.interval = this._config.defaultInterval || this._config.interval;
                var p = h.getTransitionDurationFromElement(i);
                b(i).one(h.TRANSITION_END, function() {
                    b(k).removeClass(d + " " + e).addClass(M), b(i).removeClass(M + " " + e + " " + d), 
                    g._isSliding = !1, setTimeout(function() {
                        return b(g._element).trigger(n);
                    }, 0);
                }).emulateTransitionEnd(p);
            } else b(i).removeClass(M), b(k).addClass(M), this._isSliding = !1, b(this._element).trigger(n);
            m && this.cycle();
        }
    }, Q._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this).data(E), d = f({}, H, {}, b(this).data());
            "object" == typeof a && (d = f({}, d, {}, a));
            var e = "string" == typeof a ? a : d.slide;
            if (c || (c = new Q(this, d), b(this).data(E, c)), "number" == typeof a) c.to(a); else if ("string" == typeof e) {
                if (void 0 === c[e]) throw new TypeError('No method named "' + e + '"');
                c[e]();
            } else d.interval && d.ride && (c.pause(), c.cycle());
        });
    }, Q._dataApiClickHandler = function(a) {
        var c = h.getSelectorFromElement(this);
        if (c) {
            var d = b(c)[0];
            if (d && b(d).hasClass("carousel")) {
                var e = f({}, b(d).data(), {}, b(this).data()), g = this.getAttribute("data-slide-to");
                g && (e.interval = !1), Q._jQueryInterface.call(b(d), e), g && b(d).data(E).to(g), 
                a.preventDefault();
            }
        }
    }, d(Q, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return H;
        }
    } ]), Q);
    function Q(a, b) {
        this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, 
        this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, 
        this._config = this._getConfig(b), this._element = a, this._indicatorsElement = this._element.querySelector(".carousel-indicators"), 
        this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, 
        this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners();
    }
    b(document).on(L.CLICK_DATA_API, "[data-slide], [data-slide-to]", P._dataApiClickHandler), 
    b(window).on(L.LOAD_DATA_API, function() {
        for (var a = [].slice.call(document.querySelectorAll('[data-ride="carousel"]')), c = 0, d = a.length; c < d; c++) {
            var e = b(a[c]);
            P._jQueryInterface.call(e, e.data());
        }
    }), b.fn[D] = P._jQueryInterface, b.fn[D].Constructor = P, b.fn[D].noConflict = function() {
        return b.fn[D] = G, P._jQueryInterface;
    };
    var R, S = "collapse", T = "bs.collapse", U = "." + T, V = b.fn[S], W = {
        toggle: !0,
        parent: ""
    }, X = {
        toggle: "boolean",
        parent: "(string|element)"
    }, Y = {
        SHOW: "show" + U,
        SHOWN: "shown" + U,
        HIDE: "hide" + U,
        HIDDEN: "hidden" + U,
        CLICK_DATA_API: "click" + U + ".data-api"
    }, Z = "show", $ = "collapse", _ = "collapsing", ab = "collapsed", bb = '[data-toggle="collapse"]', cb = ((R = db.prototype).toggle = function() {
        b(this._element).hasClass(Z) ? this.hide() : this.show();
    }, R.show = function() {
        var a, c, d = this;
        if (!(this._isTransitioning || b(this._element).hasClass(Z) || (this._parent && 0 === (a = [].slice.call(this._parent.querySelectorAll(".show, .collapsing")).filter(function(a) {
            return "string" == typeof d._config.parent ? a.getAttribute("data-parent") === d._config.parent : a.classList.contains($);
        })).length && (a = null), a && (c = b(a).not(this._selector).data(T)) && c._isTransitioning))) {
            var e = b.Event(Y.SHOW);
            if (b(this._element).trigger(e), !e.isDefaultPrevented()) {
                a && (db._jQueryInterface.call(b(a).not(this._selector), "hide"), c || b(a).data(T, null));
                var f = this._getDimension();
                b(this._element).removeClass($).addClass(_), this._element.style[f] = 0, this._triggerArray.length && b(this._triggerArray).removeClass(ab).attr("aria-expanded", !0), 
                this.setTransitioning(!0);
                var g = "scroll" + (f[0].toUpperCase() + f.slice(1)), i = h.getTransitionDurationFromElement(this._element);
                b(this._element).one(h.TRANSITION_END, function() {
                    b(d._element).removeClass(_).addClass($).addClass(Z), d._element.style[f] = "", 
                    d.setTransitioning(!1), b(d._element).trigger(Y.SHOWN);
                }).emulateTransitionEnd(i), this._element.style[f] = this._element[g] + "px";
            }
        }
    }, R.hide = function() {
        var a = this;
        if (!this._isTransitioning && b(this._element).hasClass(Z)) {
            var c = b.Event(Y.HIDE);
            if (b(this._element).trigger(c), !c.isDefaultPrevented()) {
                var d = this._getDimension();
                this._element.style[d] = this._element.getBoundingClientRect()[d] + "px", h.reflow(this._element), 
                b(this._element).addClass(_).removeClass($).removeClass(Z);
                var e = this._triggerArray.length;
                if (0 < e) for (var f = 0; f < e; f++) {
                    var g = this._triggerArray[f], i = h.getSelectorFromElement(g);
                    null !== i && (b([].slice.call(document.querySelectorAll(i))).hasClass(Z) || b(g).addClass(ab).attr("aria-expanded", !1));
                }
                this.setTransitioning(!0), this._element.style[d] = "";
                var j = h.getTransitionDurationFromElement(this._element);
                b(this._element).one(h.TRANSITION_END, function() {
                    a.setTransitioning(!1), b(a._element).removeClass(_).addClass($).trigger(Y.HIDDEN);
                }).emulateTransitionEnd(j);
            }
        }
    }, R.setTransitioning = function(a) {
        this._isTransitioning = a;
    }, R.dispose = function() {
        b.removeData(this._element, T), this._config = null, this._parent = null, this._element = null, 
        this._triggerArray = null, this._isTransitioning = null;
    }, R._getConfig = function(a) {
        return (a = f({}, W, {}, a)).toggle = Boolean(a.toggle), h.typeCheckConfig(S, a, X), 
        a;
    }, R._getDimension = function() {
        return b(this._element).hasClass("width") ? "width" : "height";
    }, R._getParent = function() {
        var a, c = this;
        h.isElement(this._config.parent) ? (a = this._config.parent, void 0 !== this._config.parent.jquery && (a = this._config.parent[0])) : a = document.querySelector(this._config.parent);
        var d = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]', e = [].slice.call(a.querySelectorAll(d));
        return b(e).each(function(a, b) {
            c._addAriaAndCollapsedClass(db._getTargetFromElement(b), [ b ]);
        }), a;
    }, R._addAriaAndCollapsedClass = function(a, c) {
        var d = b(a).hasClass(Z);
        c.length && b(c).toggleClass(ab, !d).attr("aria-expanded", d);
    }, db._getTargetFromElement = function(a) {
        var b = h.getSelectorFromElement(a);
        return b ? document.querySelector(b) : null;
    }, db._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this), d = c.data(T), e = f({}, W, {}, c.data(), {}, "object" == typeof a && a ? a : {});
            if (!d && e.toggle && /show|hide/.test(a) && (e.toggle = !1), d || (d = new db(this, e), 
            c.data(T, d)), "string" == typeof a) {
                if (void 0 === d[a]) throw new TypeError('No method named "' + a + '"');
                d[a]();
            }
        });
    }, d(db, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return W;
        }
    } ]), db);
    function db(a, b) {
        this._isTransitioning = !1, this._element = a, this._config = this._getConfig(b), 
        this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + a.id + '"],[data-toggle="collapse"][data-target="#' + a.id + '"]'));
        for (var c = [].slice.call(document.querySelectorAll(bb)), d = 0, e = c.length; d < e; d++) {
            var f = c[d], g = h.getSelectorFromElement(f), i = [].slice.call(document.querySelectorAll(g)).filter(function(b) {
                return b === a;
            });
            null !== g && 0 < i.length && (this._selector = g, this._triggerArray.push(f));
        }
        this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), 
        this._config.toggle && this.toggle();
    }
    b(document).on(Y.CLICK_DATA_API, bb, function(a) {
        "A" === a.currentTarget.tagName && a.preventDefault();
        var c = b(this), d = h.getSelectorFromElement(this), e = [].slice.call(document.querySelectorAll(d));
        b(e).each(function() {
            var a = b(this), d = a.data(T) ? "toggle" : c.data();
            cb._jQueryInterface.call(a, d);
        });
    }), b.fn[S] = cb._jQueryInterface, b.fn[S].Constructor = cb, b.fn[S].noConflict = function() {
        return b.fn[S] = V, cb._jQueryInterface;
    };
    var eb = "undefined" != typeof window && "undefined" != typeof document && "undefined" != typeof navigator, fb = function() {
        for (var a = [ "Edge", "Trident", "Firefox" ], b = 0; b < a.length; b += 1) if (eb && 0 <= navigator.userAgent.indexOf(a[b])) return 1;
        return 0;
    }(), gb = eb && window.Promise ? function(a) {
        var b = !1;
        return function() {
            b || (b = !0, window.Promise.resolve().then(function() {
                b = !1, a();
            }));
        };
    } : function(a) {
        var b = !1;
        return function() {
            b || (b = !0, setTimeout(function() {
                b = !1, a();
            }, fb));
        };
    };
    function hb(a) {
        return a && "[object Function]" === {}.toString.call(a);
    }
    function ib(a, b) {
        if (1 !== a.nodeType) return [];
        var c = a.ownerDocument.defaultView.getComputedStyle(a, null);
        return b ? c[b] : c;
    }
    function jb(a) {
        return "HTML" === a.nodeName ? a : a.parentNode || a.host;
    }
    function kb(a) {
        if (!a) return document.body;
        switch (a.nodeName) {
          case "HTML":
          case "BODY":
            return a.ownerDocument.body;

          case "#document":
            return a.body;
        }
        var b = ib(a), c = b.overflow, d = b.overflowX, e = b.overflowY;
        return /(auto|scroll|overlay)/.test(c + e + d) ? a : kb(jb(a));
    }
    function lb(a) {
        return a && a.referenceNode ? a.referenceNode : a;
    }
    var mb = eb && !(!window.MSInputMethodContext || !document.documentMode), nb = eb && /MSIE 10/.test(navigator.userAgent);
    function ob(a) {
        return 11 === a ? mb : 10 === a ? nb : mb || nb;
    }
    function pb(a) {
        if (!a) return document.documentElement;
        for (var b = ob(10) ? document.body : null, c = a.offsetParent || null; c === b && a.nextElementSibling; ) c = (a = a.nextElementSibling).offsetParent;
        var d = c && c.nodeName;
        return d && "BODY" !== d && "HTML" !== d ? -1 !== [ "TH", "TD", "TABLE" ].indexOf(c.nodeName) && "static" === ib(c, "position") ? pb(c) : c : a ? a.ownerDocument.documentElement : document.documentElement;
    }
    function qb(a) {
        return null !== a.parentNode ? qb(a.parentNode) : a;
    }
    function rb(a, b) {
        if (!(a && a.nodeType && b && b.nodeType)) return document.documentElement;
        var c = a.compareDocumentPosition(b) & Node.DOCUMENT_POSITION_FOLLOWING, d = c ? a : b, e = c ? b : a, f = document.createRange();
        f.setStart(d, 0), f.setEnd(e, 0);
        var g, h, i = f.commonAncestorContainer;
        if (a !== i && b !== i || d.contains(e)) return "BODY" === (h = (g = i).nodeName) || "HTML" !== h && pb(g.firstElementChild) !== g ? pb(i) : i;
        var j = qb(a);
        return j.host ? rb(j.host, b) : rb(a, qb(b).host);
    }
    function sb(a, b) {
        var c = "top" === (1 < arguments.length && void 0 !== b ? b : "top") ? "scrollTop" : "scrollLeft", d = a.nodeName;
        if ("BODY" !== d && "HTML" !== d) return a[c];
        var e = a.ownerDocument.documentElement;
        return (a.ownerDocument.scrollingElement || e)[c];
    }
    function tb(a, b) {
        var c = "x" === b ? "Left" : "Top", d = "Left" == c ? "Right" : "Bottom";
        return parseFloat(a["border" + c + "Width"], 10) + parseFloat(a["border" + d + "Width"], 10);
    }
    function ub(a, b, c, d) {
        return Math.max(b["offset" + a], b["scroll" + a], c["client" + a], c["offset" + a], c["scroll" + a], ob(10) ? parseInt(c["offset" + a]) + parseInt(d["margin" + ("Height" === a ? "Top" : "Left")]) + parseInt(d["margin" + ("Height" === a ? "Bottom" : "Right")]) : 0);
    }
    function vb(a) {
        var b = a.body, c = a.documentElement, d = ob(10) && getComputedStyle(c);
        return {
            height: ub("Height", b, c, d),
            width: ub("Width", b, c, d)
        };
    }
    function wb(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    function xb(a, b, c) {
        return b in a ? Object.defineProperty(a, b, {
            value: c,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : a[b] = c, a;
    }
    var yb = Object.assign || function(a) {
        for (var b = 1; b < arguments.length; b++) {
            var c = arguments[b];
            for (var d in c) Object.prototype.hasOwnProperty.call(c, d) && (a[d] = c[d]);
        }
        return a;
    };
    function zb(a) {
        return yb({}, a, {
            right: a.left + a.width,
            bottom: a.top + a.height
        });
    }
    function Ab(a) {
        var b = {};
        try {
            if (ob(10)) {
                b = a.getBoundingClientRect();
                var c = sb(a, "top"), d = sb(a, "left");
                b.top += c, b.left += d, b.bottom += c, b.right += d;
            } else b = a.getBoundingClientRect();
        } catch (a) {}
        var e = {
            left: b.left,
            top: b.top,
            width: b.right - b.left,
            height: b.bottom - b.top
        }, f = "HTML" === a.nodeName ? vb(a.ownerDocument) : {}, g = f.width || a.clientWidth || e.width, h = f.height || a.clientHeight || e.height, i = a.offsetWidth - g, j = a.offsetHeight - h;
        if (i || j) {
            var k = ib(a);
            i -= tb(k, "x"), j -= tb(k, "y"), e.width -= i, e.height -= j;
        }
        return zb(e);
    }
    function Bb(a, b, c) {
        var d = 2 < arguments.length && void 0 !== c && c, e = ob(10), f = "HTML" === b.nodeName, g = Ab(a), h = Ab(b), i = kb(a), j = ib(b), k = parseFloat(j.borderTopWidth, 10), l = parseFloat(j.borderLeftWidth, 10);
        d && f && (h.top = Math.max(h.top, 0), h.left = Math.max(h.left, 0));
        var m = zb({
            top: g.top - h.top - k,
            left: g.left - h.left - l,
            width: g.width,
            height: g.height
        });
        if (m.marginTop = 0, m.marginLeft = 0, !e && f) {
            var n = parseFloat(j.marginTop, 10), o = parseFloat(j.marginLeft, 10);
            m.top -= k - n, m.bottom -= k - n, m.left -= l - o, m.right -= l - o, m.marginTop = n, 
            m.marginLeft = o;
        }
        return (e && !d ? b.contains(i) : b === i && "BODY" !== i.nodeName) && (m = function(a, b, c) {
            var d = 2 < arguments.length && !1, e = sb(b, "top"), f = sb(b, "left"), g = d ? -1 : 1;
            return a.top += e * g, a.bottom += e * g, a.left += f * g, a.right += f * g, a;
        }(m, b)), m;
    }
    function Cb(a) {
        if (!a || !a.parentElement || ob()) return document.documentElement;
        for (var b = a.parentElement; b && "none" === ib(b, "transform"); ) b = b.parentElement;
        return b || document.documentElement;
    }
    function Db(a, b, c, d, e) {
        var f = 4 < arguments.length && void 0 !== e && e, g = {
            top: 0,
            left: 0
        }, h = f ? Cb(a) : rb(a, lb(b));
        if ("viewport" === d) g = function(a, b) {
            var c = 1 < arguments.length && void 0 !== b && b, d = a.ownerDocument.documentElement, e = Bb(a, d), f = Math.max(d.clientWidth, window.innerWidth || 0), g = Math.max(d.clientHeight, window.innerHeight || 0), h = c ? 0 : sb(d), i = c ? 0 : sb(d, "left");
            return zb({
                top: h - e.top + e.marginTop,
                left: i - e.left + e.marginLeft,
                width: f,
                height: g
            });
        }(h, f); else {
            var i = void 0;
            "scrollParent" === d ? "BODY" === (i = kb(jb(b))).nodeName && (i = a.ownerDocument.documentElement) : i = "window" === d ? a.ownerDocument.documentElement : d;
            var j = Bb(i, h, f);
            if ("HTML" !== i.nodeName || function o(a) {
                var b = a.nodeName;
                if ("BODY" === b || "HTML" === b) return !1;
                if ("fixed" === ib(a, "position")) return !0;
                var c = jb(a);
                return !!c && o(c);
            }(h)) g = j; else {
                var k = vb(a.ownerDocument), l = k.height, m = k.width;
                g.top += j.top - j.marginTop, g.bottom = l + j.top, g.left += j.left - j.marginLeft, 
                g.right = m + j.left;
            }
        }
        var n = "number" == typeof (c = c || 0);
        return g.left += n ? c : c.left || 0, g.top += n ? c : c.top || 0, g.right -= n ? c : c.right || 0, 
        g.bottom -= n ? c : c.bottom || 0, g;
    }
    function Eb(a, b, c, d, e, f) {
        var g = 5 < arguments.length && void 0 !== f ? f : 0;
        if (-1 === a.indexOf("auto")) return a;
        var h = Db(c, d, g, e), i = {
            top: {
                width: h.width,
                height: b.top - h.top
            },
            right: {
                width: h.right - b.right,
                height: h.height
            },
            bottom: {
                width: h.width,
                height: h.bottom - b.bottom
            },
            left: {
                width: b.left - h.left,
                height: h.height
            }
        }, j = Object.keys(i).map(function(a) {
            return yb({
                key: a
            }, i[a], {
                area: (b = i[a]).width * b.height
            });
            var b;
        }).sort(function(a, b) {
            return b.area - a.area;
        }), k = j.filter(function(a) {
            var b = a.width, d = a.height;
            return b >= c.clientWidth && d >= c.clientHeight;
        }), l = 0 < k.length ? k[0].key : j[0].key, m = a.split("-")[1];
        return l + (m ? "-" + m : "");
    }
    function Fb(a, b, c, d) {
        var e = 3 < arguments.length && void 0 !== d ? d : null;
        return Bb(c, e ? Cb(b) : rb(b, lb(c)), e);
    }
    function Gb(a) {
        var b = a.ownerDocument.defaultView.getComputedStyle(a), c = parseFloat(b.marginTop || 0) + parseFloat(b.marginBottom || 0), d = parseFloat(b.marginLeft || 0) + parseFloat(b.marginRight || 0);
        return {
            width: a.offsetWidth + d,
            height: a.offsetHeight + c
        };
    }
    function Hb(a) {
        var b = {
            left: "right",
            right: "left",
            bottom: "top",
            top: "bottom"
        };
        return a.replace(/left|right|bottom|top/g, function(a) {
            return b[a];
        });
    }
    function Ib(a, b, c) {
        c = c.split("-")[0];
        var d = Gb(a), e = {
            width: d.width,
            height: d.height
        }, f = -1 !== [ "right", "left" ].indexOf(c), g = f ? "top" : "left", h = f ? "left" : "top", i = f ? "height" : "width", j = f ? "width" : "height";
        return e[g] = b[g] + b[i] / 2 - d[i] / 2, e[h] = c === h ? b[h] - d[j] : b[Hb(h)], 
        e;
    }
    function Jb(a, b) {
        return Array.prototype.find ? a.find(b) : a.filter(b)[0];
    }
    function Kb(a, b, c) {
        return (void 0 === c ? a : a.slice(0, function(a, b) {
            if (Array.prototype.findIndex) return a.findIndex(function(a) {
                return a.name === b;
            });
            var c = Jb(a, function(a) {
                return a.name === b;
            });
            return a.indexOf(c);
        }(a, c))).forEach(function(a) {
            a.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
            var c = a.function || a.fn;
            a.enabled && hb(c) && (b.offsets.popper = zb(b.offsets.popper), b.offsets.reference = zb(b.offsets.reference), 
            b = c(b, a));
        }), b;
    }
    function Lb(a, b) {
        return a.some(function(a) {
            var c = a.name;
            return a.enabled && c === b;
        });
    }
    function Mb(a) {
        for (var b = [ !1, "ms", "Webkit", "Moz", "O" ], c = a.charAt(0).toUpperCase() + a.slice(1), d = 0; d < b.length; d++) {
            var e = b[d], f = e ? "" + e + c : a;
            if (void 0 !== document.body.style[f]) return f;
        }
        return null;
    }
    function Nb(a) {
        var b = a.ownerDocument;
        return b ? b.defaultView : window;
    }
    function Ob() {
        var a, b;
        this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = (a = this.reference, 
        b = this.state, Nb(a).removeEventListener("resize", b.updateBound), b.scrollParents.forEach(function(a) {
            a.removeEventListener("scroll", b.updateBound);
        }), b.updateBound = null, b.scrollParents = [], b.scrollElement = null, b.eventsEnabled = !1, 
        b));
    }
    function Pb(a) {
        return "" !== a && !isNaN(parseFloat(a)) && isFinite(a);
    }
    function Qb(a, b) {
        Object.keys(b).forEach(function(c) {
            var d = "";
            -1 !== [ "width", "height", "top", "right", "bottom", "left" ].indexOf(c) && Pb(b[c]) && (d = "px"), 
            a.style[c] = b[c] + d;
        });
    }
    var Rb = eb && /Firefox/i.test(navigator.userAgent);
    function Sb(a, b, c) {
        var d = Jb(a, function(a) {
            return a.name === b;
        }), e = !!d && a.some(function(a) {
            return a.name === c && a.enabled && a.order < d.order;
        });
        if (!e) {
            var f = "`" + b + "`", g = "`" + c + "`";
            console.warn(g + " modifier is required by " + f + " modifier in order to work, be sure to include it before " + f + "!");
        }
        return e;
    }
    var Tb = [ "auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start" ], Ub = Tb.slice(3);
    function Vb(a, b) {
        var c = 1 < arguments.length && void 0 !== b && b, d = Ub.indexOf(a), e = Ub.slice(d + 1).concat(Ub.slice(0, d));
        return c ? e.reverse() : e;
    }
    var Wb, Xb, Yb, Zb = {
        placement: "bottom",
        positionFixed: !1,
        eventsEnabled: !0,
        removeOnDestroy: !1,
        onCreate: function() {},
        onUpdate: function() {},
        modifiers: {
            shift: {
                order: 100,
                enabled: !0,
                fn: function(a) {
                    var b = a.placement, c = b.split("-")[0], d = b.split("-")[1];
                    if (d) {
                        var e = a.offsets, f = e.reference, g = e.popper, h = -1 !== [ "bottom", "top" ].indexOf(c), i = h ? "left" : "top", j = h ? "width" : "height", k = {
                            start: xb({}, i, f[i]),
                            end: xb({}, i, f[i] + f[j] - g[j])
                        };
                        a.offsets.popper = yb({}, g, k[d]);
                    }
                    return a;
                }
            },
            offset: {
                order: 200,
                enabled: !0,
                fn: function(a, b) {
                    var c, d = b.offset, e = a.placement, f = a.offsets, g = f.popper, h = f.reference, i = e.split("-")[0];
                    return c = Pb(+d) ? [ +d, 0 ] : function(a, b, c, d) {
                        var e = [ 0, 0 ], f = -1 !== [ "right", "left" ].indexOf(d), g = a.split(/(\+|\-)/).map(function(a) {
                            return a.trim();
                        }), h = g.indexOf(Jb(g, function(a) {
                            return -1 !== a.search(/,|\s/);
                        }));
                        g[h] && -1 === g[h].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
                        var i = /\s*,\s*|\s+/, j = -1 !== h ? [ g.slice(0, h).concat([ g[h].split(i)[0] ]), [ g[h].split(i)[1] ].concat(g.slice(h + 1)) ] : [ g ];
                        return (j = j.map(function(a, d) {
                            var e = (1 === d ? !f : f) ? "height" : "width", g = !1;
                            return a.reduce(function(a, b) {
                                return "" === a[a.length - 1] && -1 !== [ "+", "-" ].indexOf(b) ? (a[a.length - 1] = b, 
                                g = !0, a) : g ? (a[a.length - 1] += b, g = !1, a) : a.concat(b);
                            }, []).map(function(a) {
                                return function(a, b, c, d) {
                                    var e = a.match(/((?:\-|\+)?\d*\.?\d*)(.*)/), f = +e[1], g = e[2];
                                    if (!f) return a;
                                    if (0 !== g.indexOf("%")) return "vh" !== g && "vw" !== g ? f : ("vh" === g ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * f;
                                    var h = void 0;
                                    switch (g) {
                                      case "%p":
                                        h = c;
                                        break;

                                      case "%":
                                      case "%r":
                                      default:
                                        h = d;
                                    }
                                    return zb(h)[b] / 100 * f;
                                }(a, e, b, c);
                            });
                        })).forEach(function(a, b) {
                            a.forEach(function(c, d) {
                                Pb(c) && (e[b] += c * ("-" === a[d - 1] ? -1 : 1));
                            });
                        }), e;
                    }(d, g, h, i), "left" === i ? (g.top += c[0], g.left -= c[1]) : "right" === i ? (g.top += c[0], 
                    g.left += c[1]) : "top" === i ? (g.left += c[0], g.top -= c[1]) : "bottom" === i && (g.left += c[0], 
                    g.top += c[1]), a.popper = g, a;
                },
                offset: 0
            },
            preventOverflow: {
                order: 300,
                enabled: !0,
                fn: function(a, b) {
                    var c = b.boundariesElement || pb(a.instance.popper);
                    a.instance.reference === c && (c = pb(c));
                    var d = Mb("transform"), e = a.instance.popper.style, f = e.top, g = e.left, h = e[d];
                    e.top = "", e.left = "", e[d] = "";
                    var i = Db(a.instance.popper, a.instance.reference, b.padding, c, a.positionFixed);
                    e.top = f, e.left = g, e[d] = h, b.boundaries = i;
                    var j = b.priority, k = a.offsets.popper, l = {
                        primary: function(a) {
                            var c = k[a];
                            return k[a] < i[a] && !b.escapeWithReference && (c = Math.max(k[a], i[a])), xb({}, a, c);
                        },
                        secondary: function(a) {
                            var c = "right" === a ? "left" : "top", d = k[c];
                            return k[a] > i[a] && !b.escapeWithReference && (d = Math.min(k[c], i[a] - ("right" === a ? k.width : k.height))), 
                            xb({}, c, d);
                        }
                    };
                    return j.forEach(function(a) {
                        var b = -1 !== [ "left", "top" ].indexOf(a) ? "primary" : "secondary";
                        k = yb({}, k, l[b](a));
                    }), a.offsets.popper = k, a;
                },
                priority: [ "left", "right", "top", "bottom" ],
                padding: 5,
                boundariesElement: "scrollParent"
            },
            keepTogether: {
                order: 400,
                enabled: !0,
                fn: function(a) {
                    var b = a.offsets, c = b.popper, d = b.reference, e = a.placement.split("-")[0], f = Math.floor, g = -1 !== [ "top", "bottom" ].indexOf(e), h = g ? "right" : "bottom", i = g ? "left" : "top", j = g ? "width" : "height";
                    return c[h] < f(d[i]) && (a.offsets.popper[i] = f(d[i]) - c[j]), c[i] > f(d[h]) && (a.offsets.popper[i] = f(d[h])), 
                    a;
                }
            },
            arrow: {
                order: 500,
                enabled: !0,
                fn: function(a, b) {
                    var c;
                    if (!Sb(a.instance.modifiers, "arrow", "keepTogether")) return a;
                    var d = b.element;
                    if ("string" == typeof d) {
                        if (!(d = a.instance.popper.querySelector(d))) return a;
                    } else if (!a.instance.popper.contains(d)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), 
                    a;
                    var e = a.placement.split("-")[0], f = a.offsets, g = f.popper, h = f.reference, i = -1 !== [ "left", "right" ].indexOf(e), j = i ? "height" : "width", k = i ? "Top" : "Left", l = k.toLowerCase(), m = i ? "left" : "top", n = i ? "bottom" : "right", o = Gb(d)[j];
                    h[n] - o < g[l] && (a.offsets.popper[l] -= g[l] - (h[n] - o)), h[l] + o > g[n] && (a.offsets.popper[l] += h[l] + o - g[n]), 
                    a.offsets.popper = zb(a.offsets.popper);
                    var p = h[l] + h[j] / 2 - o / 2, q = ib(a.instance.popper), r = parseFloat(q["margin" + k], 10), s = parseFloat(q["border" + k + "Width"], 10), t = p - a.offsets.popper[l] - r - s;
                    return t = Math.max(Math.min(g[j] - o, t), 0), a.arrowElement = d, a.offsets.arrow = (xb(c = {}, l, Math.round(t)), 
                    xb(c, m, ""), c), a;
                },
                element: "[x-arrow]"
            },
            flip: {
                order: 600,
                enabled: !0,
                fn: function(a, b) {
                    if (Lb(a.instance.modifiers, "inner")) return a;
                    if (a.flipped && a.placement === a.originalPlacement) return a;
                    var c = Db(a.instance.popper, a.instance.reference, b.padding, b.boundariesElement, a.positionFixed), d = a.placement.split("-")[0], e = Hb(d), f = a.placement.split("-")[1] || "", g = [];
                    switch (b.behavior) {
                      case "flip":
                        g = [ d, e ];
                        break;

                      case "clockwise":
                        g = Vb(d);
                        break;

                      case "counterclockwise":
                        g = Vb(d, !0);
                        break;

                      default:
                        g = b.behavior;
                    }
                    return g.forEach(function(h, i) {
                        if (d !== h || g.length === i + 1) return a;
                        d = a.placement.split("-")[0], e = Hb(d);
                        var j = a.offsets.popper, k = a.offsets.reference, l = Math.floor, m = "left" === d && l(j.right) > l(k.left) || "right" === d && l(j.left) < l(k.right) || "top" === d && l(j.bottom) > l(k.top) || "bottom" === d && l(j.top) < l(k.bottom), n = l(j.left) < l(c.left), o = l(j.right) > l(c.right), p = l(j.top) < l(c.top), q = l(j.bottom) > l(c.bottom), r = "left" === d && n || "right" === d && o || "top" === d && p || "bottom" === d && q, s = -1 !== [ "top", "bottom" ].indexOf(d), t = !!b.flipVariations && (s && "start" === f && n || s && "end" === f && o || !s && "start" === f && p || !s && "end" === f && q), u = !!b.flipVariationsByContent && (s && "start" === f && o || s && "end" === f && n || !s && "start" === f && q || !s && "end" === f && p), v = t || u;
                        (m || r || v) && (a.flipped = !0, (m || r) && (d = g[i + 1]), v && (f = "end" === f ? "start" : "start" === f ? "end" : f), 
                        a.placement = d + (f ? "-" + f : ""), a.offsets.popper = yb({}, a.offsets.popper, Ib(a.instance.popper, a.offsets.reference, a.placement)), 
                        a = Kb(a.instance.modifiers, a, "flip"));
                    }), a;
                },
                behavior: "flip",
                padding: 5,
                boundariesElement: "viewport",
                flipVariations: !1,
                flipVariationsByContent: !1
            },
            inner: {
                order: 700,
                enabled: !1,
                fn: function(a) {
                    var b = a.placement, c = b.split("-")[0], d = a.offsets, e = d.popper, f = d.reference, g = -1 !== [ "left", "right" ].indexOf(c), h = -1 === [ "top", "left" ].indexOf(c);
                    return e[g ? "left" : "top"] = f[c] - (h ? e[g ? "width" : "height"] : 0), a.placement = Hb(b), 
                    a.offsets.popper = zb(e), a;
                }
            },
            hide: {
                order: 800,
                enabled: !0,
                fn: function(a) {
                    if (!Sb(a.instance.modifiers, "hide", "preventOverflow")) return a;
                    var b = a.offsets.reference, c = Jb(a.instance.modifiers, function(a) {
                        return "preventOverflow" === a.name;
                    }).boundaries;
                    if (b.bottom < c.top || b.left > c.right || b.top > c.bottom || b.right < c.left) {
                        if (!0 === a.hide) return a;
                        a.hide = !0, a.attributes["x-out-of-boundaries"] = "";
                    } else {
                        if (!1 === a.hide) return a;
                        a.hide = !1, a.attributes["x-out-of-boundaries"] = !1;
                    }
                    return a;
                }
            },
            computeStyle: {
                order: 850,
                enabled: !0,
                fn: function(a, b) {
                    var c = b.x, d = b.y, e = a.offsets.popper, f = Jb(a.instance.modifiers, function(a) {
                        return "applyStyle" === a.name;
                    }).gpuAcceleration;
                    void 0 !== f && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
                    var g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = void 0 !== f ? f : b.gpuAcceleration, w = pb(a.instance.popper), x = Ab(w), y = {
                        position: e.position
                    }, z = (i = a, j = window.devicePixelRatio < 2 || !Rb, k = i.offsets, l = k.popper, 
                    m = k.reference, n = Math.round, o = Math.floor, p = n(m.width), q = n(l.width), 
                    r = -1 !== [ "left", "right" ].indexOf(i.placement), s = -1 !== i.placement.indexOf("-"), 
                    u = j ? n : D, {
                        left: (t = j ? r || s || p % 2 == q % 2 ? n : o : D)(p % 2 == 1 && q % 2 == 1 && !s && j ? l.left - 1 : l.left),
                        top: u(l.top),
                        bottom: u(l.bottom),
                        right: t(l.right)
                    }), A = "bottom" === c ? "top" : "bottom", B = "right" === d ? "left" : "right", C = Mb("transform");
                    function D(a) {
                        return a;
                    }
                    if (h = "bottom" == A ? "HTML" === w.nodeName ? -w.clientHeight + z.bottom : -x.height + z.bottom : z.top, 
                    g = "right" == B ? "HTML" === w.nodeName ? -w.clientWidth + z.right : -x.width + z.right : z.left, 
                    v && C) y[C] = "translate3d(" + g + "px, " + h + "px, 0)", y[A] = 0, y[B] = 0, y.willChange = "transform"; else {
                        var E = "bottom" == A ? -1 : 1, F = "right" == B ? -1 : 1;
                        y[A] = h * E, y[B] = g * F, y.willChange = A + ", " + B;
                    }
                    var G = {
                        "x-placement": a.placement
                    };
                    return a.attributes = yb({}, G, a.attributes), a.styles = yb({}, y, a.styles), a.arrowStyles = yb({}, a.offsets.arrow, a.arrowStyles), 
                    a;
                },
                gpuAcceleration: !0,
                x: "bottom",
                y: "right"
            },
            applyStyle: {
                order: 900,
                enabled: !0,
                fn: function(a) {
                    return Qb(a.instance.popper, a.styles), b = a.instance.popper, c = a.attributes, 
                    Object.keys(c).forEach(function(a) {
                        !1 !== c[a] ? b.setAttribute(a, c[a]) : b.removeAttribute(a);
                    }), a.arrowElement && Object.keys(a.arrowStyles).length && Qb(a.arrowElement, a.arrowStyles), 
                    a;
                    var b, c;
                },
                onLoad: function(a, b, c, d, e) {
                    var f = Fb(e, b, a, c.positionFixed), g = Eb(c.placement, f, b, a, c.modifiers.flip.boundariesElement, c.modifiers.flip.padding);
                    return b.setAttribute("x-placement", g), Qb(b, {
                        position: c.positionFixed ? "fixed" : "absolute"
                    }), c;
                },
                gpuAcceleration: void 0
            }
        }
    }, $b = (Wb = _b, (Xb = [ {
        key: "update",
        value: function() {
            return function() {
                if (!this.state.isDestroyed) {
                    var a = {
                        instance: this,
                        styles: {},
                        arrowStyles: {},
                        attributes: {},
                        flipped: !1,
                        offsets: {}
                    };
                    a.offsets.reference = Fb(this.state, this.popper, this.reference, this.options.positionFixed), 
                    a.placement = Eb(this.options.placement, a.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), 
                    a.originalPlacement = a.placement, a.positionFixed = this.options.positionFixed, 
                    a.offsets.popper = Ib(this.popper, a.offsets.reference, a.placement), a.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", 
                    a = Kb(this.modifiers, a), this.state.isCreated ? this.options.onUpdate(a) : (this.state.isCreated = !0, 
                    this.options.onCreate(a));
                }
            }.call(this);
        }
    }, {
        key: "destroy",
        value: function() {
            return function() {
                return this.state.isDestroyed = !0, Lb(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), 
                this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", 
                this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", 
                this.popper.style[Mb("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), 
                this;
            }.call(this);
        }
    }, {
        key: "enableEventListeners",
        value: function() {
            return function() {
                this.state.eventsEnabled || (this.state = function(a, b, c) {
                    b.updateBound = c, Nb(a).addEventListener("resize", b.updateBound, {
                        passive: !0
                    });
                    var d = kb(a);
                    return function e(a, b, c, d) {
                        var f = "BODY" === a.nodeName, g = f ? a.ownerDocument.defaultView : a;
                        g.addEventListener(b, c, {
                            passive: !0
                        }), f || e(kb(g.parentNode), b, c, d), d.push(g);
                    }(d, "scroll", b.updateBound, b.scrollParents), b.scrollElement = d, b.eventsEnabled = !0, 
                    b;
                }(this.reference, (this.options, this.state), this.scheduleUpdate));
            }.call(this);
        }
    }, {
        key: "disableEventListeners",
        value: function() {
            return Ob.call(this);
        }
    } ]) && wb(Wb.prototype, Xb), Yb && wb(Wb, Yb), _b);
    function _b(a, b) {
        var c = this, d = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
        !function(a) {
            if (!(a instanceof _b)) throw new TypeError("Cannot call a class as a function");
        }(this), this.scheduleUpdate = function() {
            return requestAnimationFrame(c.update);
        }, this.update = gb(this.update.bind(this)), this.options = yb({}, _b.Defaults, d), 
        this.state = {
            isDestroyed: !1,
            isCreated: !1,
            scrollParents: []
        }, this.reference = a && a.jquery ? a[0] : a, this.popper = b && b.jquery ? b[0] : b, 
        this.options.modifiers = {}, Object.keys(yb({}, _b.Defaults.modifiers, d.modifiers)).forEach(function(a) {
            c.options.modifiers[a] = yb({}, _b.Defaults.modifiers[a] || {}, d.modifiers ? d.modifiers[a] : {});
        }), this.modifiers = Object.keys(this.options.modifiers).map(function(a) {
            return yb({
                name: a
            }, c.options.modifiers[a]);
        }).sort(function(a, b) {
            return a.order - b.order;
        }), this.modifiers.forEach(function(a) {
            a.enabled && hb(a.onLoad) && a.onLoad(c.reference, c.popper, c.options, a, c.state);
        }), this.update();
        var e = this.options.eventsEnabled;
        e && this.enableEventListeners(), this.state.eventsEnabled = e;
    }
    $b.Utils = ("undefined" != typeof window ? window : global).PopperUtils, $b.placements = Tb, 
    $b.Defaults = Zb;
    var ac, bc = "dropdown", cc = "bs.dropdown", dc = "." + cc, ec = ".data-api", fc = b.fn[bc], gc = new RegExp("38|40|27"), hc = {
        HIDE: "hide" + dc,
        HIDDEN: "hidden" + dc,
        SHOW: "show" + dc,
        SHOWN: "shown" + dc,
        CLICK: "click" + dc,
        CLICK_DATA_API: "click" + dc + ec,
        KEYDOWN_DATA_API: "keydown" + dc + ec,
        KEYUP_DATA_API: "keyup" + dc + ec
    }, ic = "disabled", jc = "show", kc = "dropdown-menu-right", lc = '[data-toggle="dropdown"]', mc = ".dropdown-menu", nc = {
        offset: 0,
        flip: !0,
        boundary: "scrollParent",
        reference: "toggle",
        display: "dynamic",
        popperConfig: null
    }, oc = {
        offset: "(number|string|function)",
        flip: "boolean",
        boundary: "(string|element)",
        reference: "(string|element)",
        display: "string",
        popperConfig: "(null|object)"
    }, pc = ((ac = qc.prototype).toggle = function() {
        if (!this._element.disabled && !b(this._element).hasClass(ic)) {
            var a = b(this._menu).hasClass(jc);
            qc._clearMenus(), a || this.show(!0);
        }
    }, ac.show = function(a) {
        if (void 0 === a && (a = !1), !(this._element.disabled || b(this._element).hasClass(ic) || b(this._menu).hasClass(jc))) {
            var c = {
                relatedTarget: this._element
            }, d = b.Event(hc.SHOW, c), e = qc._getParentFromElement(this._element);
            if (b(e).trigger(d), !d.isDefaultPrevented()) {
                if (!this._inNavbar && a) {
                    if (void 0 === $b) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
                    var f = this._element;
                    "parent" === this._config.reference ? f = e : h.isElement(this._config.reference) && (f = this._config.reference, 
                    void 0 !== this._config.reference.jquery && (f = this._config.reference[0])), "scrollParent" !== this._config.boundary && b(e).addClass("position-static"), 
                    this._popper = new $b(f, this._menu, this._getPopperConfig());
                }
                "ontouchstart" in document.documentElement && 0 === b(e).closest(".navbar-nav").length && b(document.body).children().on("mouseover", null, b.noop), 
                this._element.focus(), this._element.setAttribute("aria-expanded", !0), b(this._menu).toggleClass(jc), 
                b(e).toggleClass(jc).trigger(b.Event(hc.SHOWN, c));
            }
        }
    }, ac.hide = function() {
        if (!this._element.disabled && !b(this._element).hasClass(ic) && b(this._menu).hasClass(jc)) {
            var a = {
                relatedTarget: this._element
            }, c = b.Event(hc.HIDE, a), d = qc._getParentFromElement(this._element);
            b(d).trigger(c), c.isDefaultPrevented() || (this._popper && this._popper.destroy(), 
            b(this._menu).toggleClass(jc), b(d).toggleClass(jc).trigger(b.Event(hc.HIDDEN, a)));
        }
    }, ac.dispose = function() {
        b.removeData(this._element, cc), b(this._element).off(dc), this._element = null, 
        (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null);
    }, ac.update = function() {
        this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
    }, ac._addEventListeners = function() {
        var a = this;
        b(this._element).on(hc.CLICK, function(b) {
            b.preventDefault(), b.stopPropagation(), a.toggle();
        });
    }, ac._getConfig = function(a) {
        return a = f({}, this.constructor.Default, {}, b(this._element).data(), {}, a), 
        h.typeCheckConfig(bc, a, this.constructor.DefaultType), a;
    }, ac._getMenuElement = function() {
        if (!this._menu) {
            var a = qc._getParentFromElement(this._element);
            a && (this._menu = a.querySelector(mc));
        }
        return this._menu;
    }, ac._getPlacement = function() {
        var a = b(this._element.parentNode), c = "bottom-start";
        return a.hasClass("dropup") ? (c = "top-start", b(this._menu).hasClass(kc) && (c = "top-end")) : a.hasClass("dropright") ? c = "right-start" : a.hasClass("dropleft") ? c = "left-start" : b(this._menu).hasClass(kc) && (c = "bottom-end"), 
        c;
    }, ac._detectNavbar = function() {
        return 0 < b(this._element).closest(".navbar").length;
    }, ac._getOffset = function() {
        var a = this, b = {};
        return "function" == typeof this._config.offset ? b.fn = function(b) {
            return b.offsets = f({}, b.offsets, {}, a._config.offset(b.offsets, a._element) || {}), 
            b;
        } : b.offset = this._config.offset, b;
    }, ac._getPopperConfig = function() {
        var a = {
            placement: this._getPlacement(),
            modifiers: {
                offset: this._getOffset(),
                flip: {
                    enabled: this._config.flip
                },
                preventOverflow: {
                    boundariesElement: this._config.boundary
                }
            }
        };
        return "static" === this._config.display && (a.modifiers.applyStyle = {
            enabled: !1
        }), f({}, a, {}, this._config.popperConfig);
    }, qc._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this).data(cc);
            if (c || (c = new qc(this, "object" == typeof a ? a : null), b(this).data(cc, c)), 
            "string" == typeof a) {
                if (void 0 === c[a]) throw new TypeError('No method named "' + a + '"');
                c[a]();
            }
        });
    }, qc._clearMenus = function(a) {
        if (!a || 3 !== a.which && ("keyup" !== a.type || 9 === a.which)) for (var c = [].slice.call(document.querySelectorAll(lc)), d = 0, e = c.length; d < e; d++) {
            var f = qc._getParentFromElement(c[d]), g = b(c[d]).data(cc), h = {
                relatedTarget: c[d]
            };
            if (a && "click" === a.type && (h.clickEvent = a), g) {
                var i = g._menu;
                if (b(f).hasClass(jc) && !(a && ("click" === a.type && /input|textarea/i.test(a.target.tagName) || "keyup" === a.type && 9 === a.which) && b.contains(f, a.target))) {
                    var j = b.Event(hc.HIDE, h);
                    b(f).trigger(j), j.isDefaultPrevented() || ("ontouchstart" in document.documentElement && b(document.body).children().off("mouseover", null, b.noop), 
                    c[d].setAttribute("aria-expanded", "false"), g._popper && g._popper.destroy(), b(i).removeClass(jc), 
                    b(f).removeClass(jc).trigger(b.Event(hc.HIDDEN, h)));
                }
            }
        }
    }, qc._getParentFromElement = function(a) {
        var b, c = h.getSelectorFromElement(a);
        return c && (b = document.querySelector(c)), b || a.parentNode;
    }, qc._dataApiKeydownHandler = function(a) {
        if ((/input|textarea/i.test(a.target.tagName) ? !(32 === a.which || 27 !== a.which && (40 !== a.which && 38 !== a.which || b(a.target).closest(mc).length)) : gc.test(a.which)) && (a.preventDefault(), 
        a.stopPropagation(), !this.disabled && !b(this).hasClass(ic))) {
            var c = qc._getParentFromElement(this), d = b(c).hasClass(jc);
            if (d || 27 !== a.which) if (d && (!d || 27 !== a.which && 32 !== a.which)) {
                var e = [].slice.call(c.querySelectorAll(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)")).filter(function(a) {
                    return b(a).is(":visible");
                });
                if (0 !== e.length) {
                    var f = e.indexOf(a.target);
                    38 === a.which && 0 < f && f--, 40 === a.which && f < e.length - 1 && f++, f < 0 && (f = 0), 
                    e[f].focus();
                }
            } else {
                if (27 === a.which) {
                    var g = c.querySelector(lc);
                    b(g).trigger("focus");
                }
                b(this).trigger("click");
            }
        }
    }, d(qc, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return nc;
        }
    }, {
        key: "DefaultType",
        get: function() {
            return oc;
        }
    } ]), qc);
    function qc(a, b) {
        this._element = a, this._popper = null, this._config = this._getConfig(b), this._menu = this._getMenuElement(), 
        this._inNavbar = this._detectNavbar(), this._addEventListeners();
    }
    b(document).on(hc.KEYDOWN_DATA_API, lc, pc._dataApiKeydownHandler).on(hc.KEYDOWN_DATA_API, mc, pc._dataApiKeydownHandler).on(hc.CLICK_DATA_API + " " + hc.KEYUP_DATA_API, pc._clearMenus).on(hc.CLICK_DATA_API, lc, function(a) {
        a.preventDefault(), a.stopPropagation(), pc._jQueryInterface.call(b(this), "toggle");
    }).on(hc.CLICK_DATA_API, ".dropdown form", function(a) {
        a.stopPropagation();
    }), b.fn[bc] = pc._jQueryInterface, b.fn[bc].Constructor = pc, b.fn[bc].noConflict = function() {
        return b.fn[bc] = fc, pc._jQueryInterface;
    };
    var rc, sc = "modal", tc = "bs.modal", uc = "." + tc, vc = b.fn[sc], wc = {
        backdrop: !0,
        keyboard: !0,
        focus: !0,
        show: !0
    }, xc = {
        backdrop: "(boolean|string)",
        keyboard: "boolean",
        focus: "boolean",
        show: "boolean"
    }, yc = {
        HIDE: "hide" + uc,
        HIDE_PREVENTED: "hidePrevented" + uc,
        HIDDEN: "hidden" + uc,
        SHOW: "show" + uc,
        SHOWN: "shown" + uc,
        FOCUSIN: "focusin" + uc,
        RESIZE: "resize" + uc,
        CLICK_DISMISS: "click.dismiss" + uc,
        KEYDOWN_DISMISS: "keydown.dismiss" + uc,
        MOUSEUP_DISMISS: "mouseup.dismiss" + uc,
        MOUSEDOWN_DISMISS: "mousedown.dismiss" + uc,
        CLICK_DATA_API: "click" + uc + ".data-api"
    }, zc = "modal-open", Ac = "fade", Bc = "show", Cc = "modal-static", Dc = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top", Ec = ".sticky-top", Fc = ((rc = Gc.prototype).toggle = function(a) {
        return this._isShown ? this.hide() : this.show(a);
    }, rc.show = function(a) {
        var c = this;
        if (!this._isShown && !this._isTransitioning) {
            b(this._element).hasClass(Ac) && (this._isTransitioning = !0);
            var d = b.Event(yc.SHOW, {
                relatedTarget: a
            });
            b(this._element).trigger(d), this._isShown || d.isDefaultPrevented() || (this._isShown = !0, 
            this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), 
            this._setResizeEvent(), b(this._element).on(yc.CLICK_DISMISS, '[data-dismiss="modal"]', function(a) {
                return c.hide(a);
            }), b(this._dialog).on(yc.MOUSEDOWN_DISMISS, function() {
                b(c._element).one(yc.MOUSEUP_DISMISS, function(a) {
                    b(a.target).is(c._element) && (c._ignoreBackdropClick = !0);
                });
            }), this._showBackdrop(function() {
                return c._showElement(a);
            }));
        }
    }, rc.hide = function(a) {
        var c = this;
        if (a && a.preventDefault(), this._isShown && !this._isTransitioning) {
            var d = b.Event(yc.HIDE);
            if (b(this._element).trigger(d), this._isShown && !d.isDefaultPrevented()) {
                this._isShown = !1;
                var e = b(this._element).hasClass(Ac);
                if (e && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), 
                b(document).off(yc.FOCUSIN), b(this._element).removeClass(Bc), b(this._element).off(yc.CLICK_DISMISS), 
                b(this._dialog).off(yc.MOUSEDOWN_DISMISS), e) {
                    var f = h.getTransitionDurationFromElement(this._element);
                    b(this._element).one(h.TRANSITION_END, function(a) {
                        return c._hideModal(a);
                    }).emulateTransitionEnd(f);
                } else this._hideModal();
            }
        }
    }, rc.dispose = function() {
        [ window, this._element, this._dialog ].forEach(function(a) {
            return b(a).off(uc);
        }), b(document).off(yc.FOCUSIN), b.removeData(this._element, tc), this._config = null, 
        this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, 
        this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, 
        this._scrollbarWidth = null;
    }, rc.handleUpdate = function() {
        this._adjustDialog();
    }, rc._getConfig = function(a) {
        return a = f({}, wc, {}, a), h.typeCheckConfig(sc, a, xc), a;
    }, rc._triggerBackdropTransition = function() {
        var a = this;
        if ("static" === this._config.backdrop) {
            var c = b.Event(yc.HIDE_PREVENTED);
            if (b(this._element).trigger(c), c.defaultPrevented) return;
            this._element.classList.add(Cc);
            var d = h.getTransitionDurationFromElement(this._element);
            b(this._element).one(h.TRANSITION_END, function() {
                a._element.classList.remove(Cc);
            }).emulateTransitionEnd(d), this._element.focus();
        } else this.hide();
    }, rc._showElement = function(a) {
        var c = this, d = b(this._element).hasClass(Ac), e = this._dialog ? this._dialog.querySelector(".modal-body") : null;
        function f() {
            c._config.focus && c._element.focus(), c._isTransitioning = !1, b(c._element).trigger(g);
        }
        this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), 
        this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), 
        this._element.setAttribute("aria-modal", !0), b(this._dialog).hasClass("modal-dialog-scrollable") && e ? e.scrollTop = 0 : this._element.scrollTop = 0, 
        d && h.reflow(this._element), b(this._element).addClass(Bc), this._config.focus && this._enforceFocus();
        var g = b.Event(yc.SHOWN, {
            relatedTarget: a
        });
        if (d) {
            var i = h.getTransitionDurationFromElement(this._dialog);
            b(this._dialog).one(h.TRANSITION_END, f).emulateTransitionEnd(i);
        } else f();
    }, rc._enforceFocus = function() {
        var a = this;
        b(document).off(yc.FOCUSIN).on(yc.FOCUSIN, function(c) {
            document !== c.target && a._element !== c.target && 0 === b(a._element).has(c.target).length && a._element.focus();
        });
    }, rc._setEscapeEvent = function() {
        var a = this;
        this._isShown && this._config.keyboard ? b(this._element).on(yc.KEYDOWN_DISMISS, function(b) {
            27 === b.which && a._triggerBackdropTransition();
        }) : this._isShown || b(this._element).off(yc.KEYDOWN_DISMISS);
    }, rc._setResizeEvent = function() {
        var a = this;
        this._isShown ? b(window).on(yc.RESIZE, function(b) {
            return a.handleUpdate(b);
        }) : b(window).off(yc.RESIZE);
    }, rc._hideModal = function() {
        var a = this;
        this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), 
        this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function() {
            b(document.body).removeClass(zc), a._resetAdjustments(), a._resetScrollbar(), b(a._element).trigger(yc.HIDDEN);
        });
    }, rc._removeBackdrop = function() {
        this._backdrop && (b(this._backdrop).remove(), this._backdrop = null);
    }, rc._showBackdrop = function(a) {
        var c = this, d = b(this._element).hasClass(Ac) ? Ac : "";
        if (this._isShown && this._config.backdrop) {
            if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", 
            d && this._backdrop.classList.add(d), b(this._backdrop).appendTo(document.body), 
            b(this._element).on(yc.CLICK_DISMISS, function(a) {
                c._ignoreBackdropClick ? c._ignoreBackdropClick = !1 : a.target === a.currentTarget && c._triggerBackdropTransition();
            }), d && h.reflow(this._backdrop), b(this._backdrop).addClass(Bc), !a) return;
            if (!d) return void a();
            var e = h.getTransitionDurationFromElement(this._backdrop);
            b(this._backdrop).one(h.TRANSITION_END, a).emulateTransitionEnd(e);
        } else if (!this._isShown && this._backdrop) {
            b(this._backdrop).removeClass(Bc);
            var f = function() {
                c._removeBackdrop(), a && a();
            };
            if (b(this._element).hasClass(Ac)) {
                var g = h.getTransitionDurationFromElement(this._backdrop);
                b(this._backdrop).one(h.TRANSITION_END, f).emulateTransitionEnd(g);
            } else f();
        } else a && a();
    }, rc._adjustDialog = function() {
        var a = this._element.scrollHeight > document.documentElement.clientHeight;
        !this._isBodyOverflowing && a && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), 
        this._isBodyOverflowing && !a && (this._element.style.paddingRight = this._scrollbarWidth + "px");
    }, rc._resetAdjustments = function() {
        this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
    }, rc._checkScrollbar = function() {
        var a = document.body.getBoundingClientRect();
        this._isBodyOverflowing = a.left + a.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
    }, rc._setScrollbar = function() {
        var a = this;
        if (this._isBodyOverflowing) {
            var c = [].slice.call(document.querySelectorAll(Dc)), d = [].slice.call(document.querySelectorAll(Ec));
            b(c).each(function(c, d) {
                var e = d.style.paddingRight, f = b(d).css("padding-right");
                b(d).data("padding-right", e).css("padding-right", parseFloat(f) + a._scrollbarWidth + "px");
            }), b(d).each(function(c, d) {
                var e = d.style.marginRight, f = b(d).css("margin-right");
                b(d).data("margin-right", e).css("margin-right", parseFloat(f) - a._scrollbarWidth + "px");
            });
            var e = document.body.style.paddingRight, f = b(document.body).css("padding-right");
            b(document.body).data("padding-right", e).css("padding-right", parseFloat(f) + this._scrollbarWidth + "px");
        }
        b(document.body).addClass(zc);
    }, rc._resetScrollbar = function() {
        var a = [].slice.call(document.querySelectorAll(Dc));
        b(a).each(function(a, c) {
            var d = b(c).data("padding-right");
            b(c).removeData("padding-right"), c.style.paddingRight = d || "";
        });
        var c = [].slice.call(document.querySelectorAll(Ec));
        b(c).each(function(a, c) {
            var d = b(c).data("margin-right");
            void 0 !== d && b(c).css("margin-right", d).removeData("margin-right");
        });
        var d = b(document.body).data("padding-right");
        b(document.body).removeData("padding-right"), document.body.style.paddingRight = d || "";
    }, rc._getScrollbarWidth = function() {
        var a = document.createElement("div");
        a.className = "modal-scrollbar-measure", document.body.appendChild(a);
        var b = a.getBoundingClientRect().width - a.clientWidth;
        return document.body.removeChild(a), b;
    }, Gc._jQueryInterface = function(a, c) {
        return this.each(function() {
            var d = b(this).data(tc), e = f({}, wc, {}, b(this).data(), {}, "object" == typeof a && a ? a : {});
            if (d || (d = new Gc(this, e), b(this).data(tc, d)), "string" == typeof a) {
                if (void 0 === d[a]) throw new TypeError('No method named "' + a + '"');
                d[a](c);
            } else e.show && d.show(c);
        });
    }, d(Gc, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return wc;
        }
    } ]), Gc);
    function Gc(a, b) {
        this._config = this._getConfig(b), this._element = a, this._dialog = a.querySelector(".modal-dialog"), 
        this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, 
        this._isTransitioning = !1, this._scrollbarWidth = 0;
    }
    b(document).on(yc.CLICK_DATA_API, '[data-toggle="modal"]', function(a) {
        var c, d = this, e = h.getSelectorFromElement(this);
        e && (c = document.querySelector(e));
        var g = b(c).data(tc) ? "toggle" : f({}, b(c).data(), {}, b(this).data());
        "A" !== this.tagName && "AREA" !== this.tagName || a.preventDefault();
        var i = b(c).one(yc.SHOW, function(a) {
            a.isDefaultPrevented() || i.one(yc.HIDDEN, function() {
                b(d).is(":visible") && d.focus();
            });
        });
        Fc._jQueryInterface.call(b(c), g, this);
    }), b.fn[sc] = Fc._jQueryInterface, b.fn[sc].Constructor = Fc, b.fn[sc].noConflict = function() {
        return b.fn[sc] = vc, Fc._jQueryInterface;
    };
    var Hc = [ "background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href" ], Ic = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:\/?#]*(?:[\/?#]|$))/gi, Jc = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+\/]+=*$/i;
    function Kc(a, b, c) {
        if (0 === a.length) return a;
        if (c && "function" == typeof c) return c(a);
        for (var d = new window.DOMParser().parseFromString(a, "text/html"), e = Object.keys(b), f = [].slice.call(d.body.querySelectorAll("*")), g = function(a) {
            var c = f[a], d = c.nodeName.toLowerCase();
            if (-1 === e.indexOf(c.nodeName.toLowerCase())) return c.parentNode.removeChild(c), 
            "continue";
            var g = [].slice.call(c.attributes), h = [].concat(b["*"] || [], b[d] || []);
            g.forEach(function(a) {
                !function(a, b) {
                    var c = a.nodeName.toLowerCase();
                    if (-1 !== b.indexOf(c)) return -1 === Hc.indexOf(c) || Boolean(a.nodeValue.match(Ic) || a.nodeValue.match(Jc));
                    for (var d = b.filter(function(a) {
                        return a instanceof RegExp;
                    }), e = 0, f = d.length; e < f; e++) if (c.match(d[e])) return 1;
                }(a, h) && c.removeAttribute(a.nodeName);
            });
        }, h = 0, i = f.length; h < i; h++) g(h);
        return d.body.innerHTML;
    }
    var Lc, Mc = "tooltip", Nc = "bs.tooltip", Oc = "." + Nc, Pc = b.fn[Mc], Qc = "bs-tooltip", Rc = new RegExp("(^|\\s)" + Qc + "\\S+", "g"), Sc = [ "sanitize", "whiteList", "sanitizeFn" ], Tc = {
        animation: "boolean",
        template: "string",
        title: "(string|element|function)",
        trigger: "string",
        delay: "(number|object)",
        html: "boolean",
        selector: "(string|boolean)",
        placement: "(string|function)",
        offset: "(number|string|function)",
        container: "(string|element|boolean)",
        fallbackPlacement: "(string|array)",
        boundary: "(string|element)",
        sanitize: "boolean",
        sanitizeFn: "(null|function)",
        whiteList: "object",
        popperConfig: "(null|object)"
    }, Uc = {
        AUTO: "auto",
        TOP: "top",
        RIGHT: "right",
        BOTTOM: "bottom",
        LEFT: "left"
    }, Vc = {
        animation: !0,
        template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        selector: !1,
        placement: "top",
        offset: 0,
        container: !1,
        fallbackPlacement: "flip",
        boundary: "scrollParent",
        sanitize: !0,
        sanitizeFn: null,
        whiteList: {
            "*": [ "class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i ],
            a: [ "target", "href", "title", "rel" ],
            area: [],
            b: [],
            br: [],
            col: [],
            code: [],
            div: [],
            em: [],
            hr: [],
            h1: [],
            h2: [],
            h3: [],
            h4: [],
            h5: [],
            h6: [],
            i: [],
            img: [ "src", "alt", "title", "width", "height" ],
            li: [],
            ol: [],
            p: [],
            pre: [],
            s: [],
            small: [],
            span: [],
            sub: [],
            sup: [],
            strong: [],
            u: [],
            ul: []
        },
        popperConfig: null
    }, Wc = "show", Xc = {
        HIDE: "hide" + Oc,
        HIDDEN: "hidden" + Oc,
        SHOW: "show" + Oc,
        SHOWN: "shown" + Oc,
        INSERTED: "inserted" + Oc,
        CLICK: "click" + Oc,
        FOCUSIN: "focusin" + Oc,
        FOCUSOUT: "focusout" + Oc,
        MOUSEENTER: "mouseenter" + Oc,
        MOUSELEAVE: "mouseleave" + Oc
    }, Yc = "fade", Zc = "show", $c = "hover", _c = ((Lc = ad.prototype).enable = function() {
        this._isEnabled = !0;
    }, Lc.disable = function() {
        this._isEnabled = !1;
    }, Lc.toggleEnabled = function() {
        this._isEnabled = !this._isEnabled;
    }, Lc.toggle = function(a) {
        if (this._isEnabled) if (a) {
            var c = this.constructor.DATA_KEY, d = b(a.currentTarget).data(c);
            d || (d = new this.constructor(a.currentTarget, this._getDelegateConfig()), b(a.currentTarget).data(c, d)), 
            d._activeTrigger.click = !d._activeTrigger.click, d._isWithActiveTrigger() ? d._enter(null, d) : d._leave(null, d);
        } else {
            if (b(this.getTipElement()).hasClass(Zc)) return void this._leave(null, this);
            this._enter(null, this);
        }
    }, Lc.dispose = function() {
        clearTimeout(this._timeout), b.removeData(this.element, this.constructor.DATA_KEY), 
        b(this.element).off(this.constructor.EVENT_KEY), b(this.element).closest(".modal").off("hide.bs.modal", this._hideModalHandler), 
        this.tip && b(this.tip).remove(), this._isEnabled = null, this._timeout = null, 
        this._hoverState = null, this._activeTrigger = null, this._popper && this._popper.destroy(), 
        this._popper = null, this.element = null, this.config = null, this.tip = null;
    }, Lc.show = function() {
        var a = this;
        if ("none" === b(this.element).css("display")) throw new Error("Please use show on visible elements");
        var c = b.Event(this.constructor.Event.SHOW);
        if (this.isWithContent() && this._isEnabled) {
            b(this.element).trigger(c);
            var d = h.findShadowRoot(this.element), e = b.contains(null !== d ? d : this.element.ownerDocument.documentElement, this.element);
            if (c.isDefaultPrevented() || !e) return;
            var f = this.getTipElement(), g = h.getUID(this.constructor.NAME);
            f.setAttribute("id", g), this.element.setAttribute("aria-describedby", g), this.setContent(), 
            this.config.animation && b(f).addClass(Yc);
            var i = "function" == typeof this.config.placement ? this.config.placement.call(this, f, this.element) : this.config.placement, j = this._getAttachment(i);
            this.addAttachmentClass(j);
            var k = this._getContainer();
            b(f).data(this.constructor.DATA_KEY, this), b.contains(this.element.ownerDocument.documentElement, this.tip) || b(f).appendTo(k), 
            b(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new $b(this.element, f, this._getPopperConfig(j)), 
            b(f).addClass(Zc), "ontouchstart" in document.documentElement && b(document.body).children().on("mouseover", null, b.noop);
            var l = function() {
                a.config.animation && a._fixTransition();
                var c = a._hoverState;
                a._hoverState = null, b(a.element).trigger(a.constructor.Event.SHOWN), "out" === c && a._leave(null, a);
            };
            if (b(this.tip).hasClass(Yc)) {
                var m = h.getTransitionDurationFromElement(this.tip);
                b(this.tip).one(h.TRANSITION_END, l).emulateTransitionEnd(m);
            } else l();
        }
    }, Lc.hide = function(a) {
        function c() {
            d._hoverState !== Wc && e.parentNode && e.parentNode.removeChild(e), d._cleanTipClass(), 
            d.element.removeAttribute("aria-describedby"), b(d.element).trigger(d.constructor.Event.HIDDEN), 
            null !== d._popper && d._popper.destroy(), a && a();
        }
        var d = this, e = this.getTipElement(), f = b.Event(this.constructor.Event.HIDE);
        if (b(this.element).trigger(f), !f.isDefaultPrevented()) {
            if (b(e).removeClass(Zc), "ontouchstart" in document.documentElement && b(document.body).children().off("mouseover", null, b.noop), 
            this._activeTrigger.click = !1, this._activeTrigger.focus = !1, this._activeTrigger[$c] = !1, 
            b(this.tip).hasClass(Yc)) {
                var g = h.getTransitionDurationFromElement(e);
                b(e).one(h.TRANSITION_END, c).emulateTransitionEnd(g);
            } else c();
            this._hoverState = "";
        }
    }, Lc.update = function() {
        null !== this._popper && this._popper.scheduleUpdate();
    }, Lc.isWithContent = function() {
        return Boolean(this.getTitle());
    }, Lc.addAttachmentClass = function(a) {
        b(this.getTipElement()).addClass(Qc + "-" + a);
    }, Lc.getTipElement = function() {
        return this.tip = this.tip || b(this.config.template)[0], this.tip;
    }, Lc.setContent = function() {
        var a = this.getTipElement();
        this.setElementContent(b(a.querySelectorAll(".tooltip-inner")), this.getTitle()), 
        b(a).removeClass("fade show");
    }, Lc.setElementContent = function(a, c) {
        "object" != typeof c || !c.nodeType && !c.jquery ? this.config.html ? (this.config.sanitize && (c = Kc(c, this.config.whiteList, this.config.sanitizeFn)), 
        a.html(c)) : a.text(c) : this.config.html ? b(c).parent().is(a) || a.empty().append(c) : a.text(b(c).text());
    }, Lc.getTitle = function() {
        var a = this.element.getAttribute("data-original-title");
        return a || ("function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title);
    }, Lc._getPopperConfig = function(a) {
        var b = this;
        return f({}, {
            placement: a,
            modifiers: {
                offset: this._getOffset(),
                flip: {
                    behavior: this.config.fallbackPlacement
                },
                arrow: {
                    element: ".arrow"
                },
                preventOverflow: {
                    boundariesElement: this.config.boundary
                }
            },
            onCreate: function(a) {
                a.originalPlacement !== a.placement && b._handlePopperPlacementChange(a);
            },
            onUpdate: function(a) {
                return b._handlePopperPlacementChange(a);
            }
        }, {}, this.config.popperConfig);
    }, Lc._getOffset = function() {
        var a = this, b = {};
        return "function" == typeof this.config.offset ? b.fn = function(b) {
            return b.offsets = f({}, b.offsets, {}, a.config.offset(b.offsets, a.element) || {}), 
            b;
        } : b.offset = this.config.offset, b;
    }, Lc._getContainer = function() {
        return !1 === this.config.container ? document.body : h.isElement(this.config.container) ? b(this.config.container) : b(document).find(this.config.container);
    }, Lc._getAttachment = function(a) {
        return Uc[a.toUpperCase()];
    }, Lc._setListeners = function() {
        var a = this;
        this.config.trigger.split(" ").forEach(function(c) {
            if ("click" === c) b(a.element).on(a.constructor.Event.CLICK, a.config.selector, function(b) {
                return a.toggle(b);
            }); else if ("manual" !== c) {
                var d = c === $c ? a.constructor.Event.MOUSEENTER : a.constructor.Event.FOCUSIN, e = c === $c ? a.constructor.Event.MOUSELEAVE : a.constructor.Event.FOCUSOUT;
                b(a.element).on(d, a.config.selector, function(b) {
                    return a._enter(b);
                }).on(e, a.config.selector, function(b) {
                    return a._leave(b);
                });
            }
        }), this._hideModalHandler = function() {
            a.element && a.hide();
        }, b(this.element).closest(".modal").on("hide.bs.modal", this._hideModalHandler), 
        this.config.selector ? this.config = f({}, this.config, {
            trigger: "manual",
            selector: ""
        }) : this._fixTitle();
    }, Lc._fixTitle = function() {
        var a = typeof this.element.getAttribute("data-original-title");
        !this.element.getAttribute("title") && "string" == a || (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), 
        this.element.setAttribute("title", ""));
    }, Lc._enter = function(a, c) {
        var d = this.constructor.DATA_KEY;
        (c = c || b(a.currentTarget).data(d)) || (c = new this.constructor(a.currentTarget, this._getDelegateConfig()), 
        b(a.currentTarget).data(d, c)), a && (c._activeTrigger["focusin" === a.type ? "focus" : $c] = !0), 
        b(c.getTipElement()).hasClass(Zc) || c._hoverState === Wc ? c._hoverState = Wc : (clearTimeout(c._timeout), 
        c._hoverState = Wc, c.config.delay && c.config.delay.show ? c._timeout = setTimeout(function() {
            c._hoverState === Wc && c.show();
        }, c.config.delay.show) : c.show());
    }, Lc._leave = function(a, c) {
        var d = this.constructor.DATA_KEY;
        (c = c || b(a.currentTarget).data(d)) || (c = new this.constructor(a.currentTarget, this._getDelegateConfig()), 
        b(a.currentTarget).data(d, c)), a && (c._activeTrigger["focusout" === a.type ? "focus" : $c] = !1), 
        c._isWithActiveTrigger() || (clearTimeout(c._timeout), c._hoverState = "out", c.config.delay && c.config.delay.hide ? c._timeout = setTimeout(function() {
            "out" === c._hoverState && c.hide();
        }, c.config.delay.hide) : c.hide());
    }, Lc._isWithActiveTrigger = function() {
        for (var a in this._activeTrigger) if (this._activeTrigger[a]) return !0;
        return !1;
    }, Lc._getConfig = function(a) {
        var c = b(this.element).data();
        return Object.keys(c).forEach(function(a) {
            -1 !== Sc.indexOf(a) && delete c[a];
        }), "number" == typeof (a = f({}, this.constructor.Default, {}, c, {}, "object" == typeof a && a ? a : {})).delay && (a.delay = {
            show: a.delay,
            hide: a.delay
        }), "number" == typeof a.title && (a.title = a.title.toString()), "number" == typeof a.content && (a.content = a.content.toString()), 
        h.typeCheckConfig(Mc, a, this.constructor.DefaultType), a.sanitize && (a.template = Kc(a.template, a.whiteList, a.sanitizeFn)), 
        a;
    }, Lc._getDelegateConfig = function() {
        var a = {};
        if (this.config) for (var b in this.config) this.constructor.Default[b] !== this.config[b] && (a[b] = this.config[b]);
        return a;
    }, Lc._cleanTipClass = function() {
        var a = b(this.getTipElement()), c = a.attr("class").match(Rc);
        null !== c && c.length && a.removeClass(c.join(""));
    }, Lc._handlePopperPlacementChange = function(a) {
        var b = a.instance;
        this.tip = b.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(a.placement));
    }, Lc._fixTransition = function() {
        var a = this.getTipElement(), c = this.config.animation;
        null === a.getAttribute("x-placement") && (b(a).removeClass(Yc), this.config.animation = !1, 
        this.hide(), this.show(), this.config.animation = c);
    }, ad._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this).data(Nc), d = "object" == typeof a && a;
            if ((c || !/dispose|hide/.test(a)) && (c || (c = new ad(this, d), b(this).data(Nc, c)), 
            "string" == typeof a)) {
                if (void 0 === c[a]) throw new TypeError('No method named "' + a + '"');
                c[a]();
            }
        });
    }, d(ad, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return Vc;
        }
    }, {
        key: "NAME",
        get: function() {
            return Mc;
        }
    }, {
        key: "DATA_KEY",
        get: function() {
            return Nc;
        }
    }, {
        key: "Event",
        get: function() {
            return Xc;
        }
    }, {
        key: "EVENT_KEY",
        get: function() {
            return Oc;
        }
    }, {
        key: "DefaultType",
        get: function() {
            return Tc;
        }
    } ]), ad);
    function ad(a, b) {
        if (void 0 === $b) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
        this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, 
        this._popper = null, this.element = a, this.config = this._getConfig(b), this.tip = null, 
        this._setListeners();
    }
    b.fn[Mc] = _c._jQueryInterface, b.fn[Mc].Constructor = _c, b.fn[Mc].noConflict = function() {
        return b.fn[Mc] = Pc, _c._jQueryInterface;
    };
    var bd = "popover", cd = "bs.popover", dd = "." + cd, ed = b.fn[bd], fd = "bs-popover", gd = new RegExp("(^|\\s)" + fd + "\\S+", "g"), hd = f({}, _c.Default, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
    }), id = f({}, _c.DefaultType, {
        content: "(string|element|function)"
    }), jd = {
        HIDE: "hide" + dd,
        HIDDEN: "hidden" + dd,
        SHOW: "show" + dd,
        SHOWN: "shown" + dd,
        INSERTED: "inserted" + dd,
        CLICK: "click" + dd,
        FOCUSIN: "focusin" + dd,
        FOCUSOUT: "focusout" + dd,
        MOUSEENTER: "mouseenter" + dd,
        MOUSELEAVE: "mouseleave" + dd
    }, kd = function(a) {
        function c() {
            return a.apply(this, arguments) || this;
        }
        var e, f;
        f = a, (e = c).prototype = Object.create(f.prototype), (e.prototype.constructor = e).__proto__ = f;
        var g = c.prototype;
        return g.isWithContent = function() {
            return this.getTitle() || this._getContent();
        }, g.addAttachmentClass = function(a) {
            b(this.getTipElement()).addClass(fd + "-" + a);
        }, g.getTipElement = function() {
            return this.tip = this.tip || b(this.config.template)[0], this.tip;
        }, g.setContent = function() {
            var a = b(this.getTipElement());
            this.setElementContent(a.find(".popover-header"), this.getTitle());
            var c = this._getContent();
            "function" == typeof c && (c = c.call(this.element)), this.setElementContent(a.find(".popover-body"), c), 
            a.removeClass("fade show");
        }, g._getContent = function() {
            return this.element.getAttribute("data-content") || this.config.content;
        }, g._cleanTipClass = function() {
            var a = b(this.getTipElement()), c = a.attr("class").match(gd);
            null !== c && 0 < c.length && a.removeClass(c.join(""));
        }, c._jQueryInterface = function(a) {
            return this.each(function() {
                var d = b(this).data(cd), e = "object" == typeof a ? a : null;
                if ((d || !/dispose|hide/.test(a)) && (d || (d = new c(this, e), b(this).data(cd, d)), 
                "string" == typeof a)) {
                    if (void 0 === d[a]) throw new TypeError('No method named "' + a + '"');
                    d[a]();
                }
            });
        }, d(c, null, [ {
            key: "VERSION",
            get: function() {
                return "4.4.1";
            }
        }, {
            key: "Default",
            get: function() {
                return hd;
            }
        }, {
            key: "NAME",
            get: function() {
                return bd;
            }
        }, {
            key: "DATA_KEY",
            get: function() {
                return cd;
            }
        }, {
            key: "Event",
            get: function() {
                return jd;
            }
        }, {
            key: "EVENT_KEY",
            get: function() {
                return dd;
            }
        }, {
            key: "DefaultType",
            get: function() {
                return id;
            }
        } ]), c;
    }(_c);
    b.fn[bd] = kd._jQueryInterface, b.fn[bd].Constructor = kd, b.fn[bd].noConflict = function() {
        return b.fn[bd] = ed, kd._jQueryInterface;
    };
    var ld, md = "scrollspy", nd = "bs.scrollspy", od = "." + nd, pd = b.fn[md], qd = {
        offset: 10,
        method: "auto",
        target: ""
    }, rd = {
        offset: "number",
        method: "string",
        target: "(string|element)"
    }, sd = {
        ACTIVATE: "activate" + od,
        SCROLL: "scroll" + od,
        LOAD_DATA_API: "load" + od + ".data-api"
    }, td = "active", ud = ".nav, .list-group", vd = ".nav-link", wd = ".list-group-item", xd = ((ld = yd.prototype).refresh = function() {
        var a = this, c = this._scrollElement === this._scrollElement.window ? "offset" : "position", d = "auto" === this._config.method ? c : this._config.method, e = "position" === d ? this._getScrollTop() : 0;
        this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), 
        [].slice.call(document.querySelectorAll(this._selector)).map(function(a) {
            var c, f = h.getSelectorFromElement(a);
            if (f && (c = document.querySelector(f)), c) {
                var g = c.getBoundingClientRect();
                if (g.width || g.height) return [ b(c)[d]().top + e, f ];
            }
            return null;
        }).filter(function(a) {
            return a;
        }).sort(function(a, b) {
            return a[0] - b[0];
        }).forEach(function(b) {
            a._offsets.push(b[0]), a._targets.push(b[1]);
        });
    }, ld.dispose = function() {
        b.removeData(this._element, nd), b(this._scrollElement).off(od), this._element = null, 
        this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, 
        this._targets = null, this._activeTarget = null, this._scrollHeight = null;
    }, ld._getConfig = function(a) {
        if ("string" != typeof (a = f({}, qd, {}, "object" == typeof a && a ? a : {})).target) {
            var c = b(a.target).attr("id");
            c || (c = h.getUID(md), b(a.target).attr("id", c)), a.target = "#" + c;
        }
        return h.typeCheckConfig(md, a, rd), a;
    }, ld._getScrollTop = function() {
        return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    }, ld._getScrollHeight = function() {
        return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    }, ld._getOffsetHeight = function() {
        return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    }, ld._process = function() {
        var a = this._getScrollTop() + this._config.offset, b = this._getScrollHeight(), c = this._config.offset + b - this._getOffsetHeight();
        if (this._scrollHeight !== b && this.refresh(), c <= a) {
            var d = this._targets[this._targets.length - 1];
            this._activeTarget !== d && this._activate(d);
        } else {
            if (this._activeTarget && a < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, 
            void this._clear();
            for (var e = this._offsets.length; e--; ) this._activeTarget !== this._targets[e] && a >= this._offsets[e] && (void 0 === this._offsets[e + 1] || a < this._offsets[e + 1]) && this._activate(this._targets[e]);
        }
    }, ld._activate = function(a) {
        this._activeTarget = a, this._clear();
        var c = this._selector.split(",").map(function(b) {
            return b + '[data-target="' + a + '"],' + b + '[href="' + a + '"]';
        }), d = b([].slice.call(document.querySelectorAll(c.join(","))));
        d.hasClass("dropdown-item") ? (d.closest(".dropdown").find(".dropdown-toggle").addClass(td), 
        d.addClass(td)) : (d.addClass(td), d.parents(ud).prev(vd + ", " + wd).addClass(td), 
        d.parents(ud).prev(".nav-item").children(vd).addClass(td)), b(this._scrollElement).trigger(sd.ACTIVATE, {
            relatedTarget: a
        });
    }, ld._clear = function() {
        [].slice.call(document.querySelectorAll(this._selector)).filter(function(a) {
            return a.classList.contains(td);
        }).forEach(function(a) {
            return a.classList.remove(td);
        });
    }, yd._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this).data(nd);
            if (c || (c = new yd(this, "object" == typeof a && a), b(this).data(nd, c)), "string" == typeof a) {
                if (void 0 === c[a]) throw new TypeError('No method named "' + a + '"');
                c[a]();
            }
        });
    }, d(yd, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "Default",
        get: function() {
            return qd;
        }
    } ]), yd);
    function yd(a, c) {
        var d = this;
        this._element = a, this._scrollElement = "BODY" === a.tagName ? window : a, this._config = this._getConfig(c), 
        this._selector = this._config.target + " " + vd + "," + this._config.target + " " + wd + "," + this._config.target + " .dropdown-item", 
        this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, 
        b(this._scrollElement).on(sd.SCROLL, function(a) {
            return d._process(a);
        }), this.refresh(), this._process();
    }
    b(window).on(sd.LOAD_DATA_API, function() {
        for (var a = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')), c = a.length; c--; ) {
            var d = b(a[c]);
            xd._jQueryInterface.call(d, d.data());
        }
    }), b.fn[md] = xd._jQueryInterface, b.fn[md].Constructor = xd, b.fn[md].noConflict = function() {
        return b.fn[md] = pd, xd._jQueryInterface;
    };
    var zd, Ad = "bs.tab", Bd = "." + Ad, Cd = b.fn.tab, Dd = {
        HIDE: "hide" + Bd,
        HIDDEN: "hidden" + Bd,
        SHOW: "show" + Bd,
        SHOWN: "shown" + Bd,
        CLICK_DATA_API: "click.bs.tab.data-api"
    }, Ed = "active", Fd = "> li > .active", Gd = ((zd = Hd.prototype).show = function() {
        var a = this;
        if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && b(this._element).hasClass(Ed) || b(this._element).hasClass("disabled"))) {
            var c, d, e = b(this._element).closest(".nav, .list-group")[0], f = h.getSelectorFromElement(this._element);
            if (e) {
                var g = "UL" === e.nodeName || "OL" === e.nodeName ? Fd : ".active";
                d = (d = b.makeArray(b(e).find(g)))[d.length - 1];
            }
            var i = b.Event(Dd.HIDE, {
                relatedTarget: this._element
            }), j = b.Event(Dd.SHOW, {
                relatedTarget: d
            });
            if (d && b(d).trigger(i), b(this._element).trigger(j), !j.isDefaultPrevented() && !i.isDefaultPrevented()) {
                f && (c = document.querySelector(f)), this._activate(this._element, e);
                var k = function() {
                    var c = b.Event(Dd.HIDDEN, {
                        relatedTarget: a._element
                    }), e = b.Event(Dd.SHOWN, {
                        relatedTarget: d
                    });
                    b(d).trigger(c), b(a._element).trigger(e);
                };
                c ? this._activate(c, c.parentNode, k) : k();
            }
        }
    }, zd.dispose = function() {
        b.removeData(this._element, Ad), this._element = null;
    }, zd._activate = function(a, c, d) {
        function e() {
            return f._transitionComplete(a, g, d);
        }
        var f = this, g = (!c || "UL" !== c.nodeName && "OL" !== c.nodeName ? b(c).children(".active") : b(c).find(Fd))[0], i = d && g && b(g).hasClass("fade");
        if (g && i) {
            var j = h.getTransitionDurationFromElement(g);
            b(g).removeClass("show").one(h.TRANSITION_END, e).emulateTransitionEnd(j);
        } else e();
    }, zd._transitionComplete = function(a, c, d) {
        if (c) {
            b(c).removeClass(Ed);
            var e = b(c.parentNode).find("> .dropdown-menu .active")[0];
            e && b(e).removeClass(Ed), "tab" === c.getAttribute("role") && c.setAttribute("aria-selected", !1);
        }
        if (b(a).addClass(Ed), "tab" === a.getAttribute("role") && a.setAttribute("aria-selected", !0), 
        h.reflow(a), a.classList.contains("fade") && a.classList.add("show"), a.parentNode && b(a.parentNode).hasClass("dropdown-menu")) {
            var f = b(a).closest(".dropdown")[0];
            if (f) {
                var g = [].slice.call(f.querySelectorAll(".dropdown-toggle"));
                b(g).addClass(Ed);
            }
            a.setAttribute("aria-expanded", !0);
        }
        d && d();
    }, Hd._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this), d = c.data(Ad);
            if (d || (d = new Hd(this), c.data(Ad, d)), "string" == typeof a) {
                if (void 0 === d[a]) throw new TypeError('No method named "' + a + '"');
                d[a]();
            }
        });
    }, d(Hd, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    } ]), Hd);
    function Hd(a) {
        this._element = a;
    }
    b(document).on(Dd.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function(a) {
        a.preventDefault(), Gd._jQueryInterface.call(b(this), "show");
    }), b.fn.tab = Gd._jQueryInterface, b.fn.tab.Constructor = Gd, b.fn.tab.noConflict = function() {
        return b.fn.tab = Cd, Gd._jQueryInterface;
    };
    var Id, Jd = "toast", Kd = "bs.toast", Ld = "." + Kd, Md = b.fn[Jd], Nd = {
        CLICK_DISMISS: "click.dismiss" + Ld,
        HIDE: "hide" + Ld,
        HIDDEN: "hidden" + Ld,
        SHOW: "show" + Ld,
        SHOWN: "shown" + Ld
    }, Od = "show", Pd = {
        animation: "boolean",
        autohide: "boolean",
        delay: "number"
    }, Qd = {
        animation: !0,
        autohide: !0,
        delay: 500
    }, Rd = ((Id = Sd.prototype).show = function() {
        var a = this, c = b.Event(Nd.SHOW);
        if (b(this._element).trigger(c), !c.isDefaultPrevented()) {
            this._config.animation && this._element.classList.add("fade");
            var d = function() {
                a._element.classList.remove("showing"), a._element.classList.add(Od), b(a._element).trigger(Nd.SHOWN), 
                a._config.autohide && (a._timeout = setTimeout(function() {
                    a.hide();
                }, a._config.delay));
            };
            if (this._element.classList.remove("hide"), h.reflow(this._element), this._element.classList.add("showing"), 
            this._config.animation) {
                var e = h.getTransitionDurationFromElement(this._element);
                b(this._element).one(h.TRANSITION_END, d).emulateTransitionEnd(e);
            } else d();
        }
    }, Id.hide = function() {
        if (this._element.classList.contains(Od)) {
            var a = b.Event(Nd.HIDE);
            b(this._element).trigger(a), a.isDefaultPrevented() || this._close();
        }
    }, Id.dispose = function() {
        clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(Od) && this._element.classList.remove(Od), 
        b(this._element).off(Nd.CLICK_DISMISS), b.removeData(this._element, Kd), this._element = null, 
        this._config = null;
    }, Id._getConfig = function(a) {
        return a = f({}, Qd, {}, b(this._element).data(), {}, "object" == typeof a && a ? a : {}), 
        h.typeCheckConfig(Jd, a, this.constructor.DefaultType), a;
    }, Id._setListeners = function() {
        var a = this;
        b(this._element).on(Nd.CLICK_DISMISS, '[data-dismiss="toast"]', function() {
            return a.hide();
        });
    }, Id._close = function() {
        function a() {
            c._element.classList.add("hide"), b(c._element).trigger(Nd.HIDDEN);
        }
        var c = this;
        if (this._element.classList.remove(Od), this._config.animation) {
            var d = h.getTransitionDurationFromElement(this._element);
            b(this._element).one(h.TRANSITION_END, a).emulateTransitionEnd(d);
        } else a();
    }, Sd._jQueryInterface = function(a) {
        return this.each(function() {
            var c = b(this), d = c.data(Kd);
            if (d || (d = new Sd(this, "object" == typeof a && a), c.data(Kd, d)), "string" == typeof a) {
                if (void 0 === d[a]) throw new TypeError('No method named "' + a + '"');
                d[a](this);
            }
        });
    }, d(Sd, null, [ {
        key: "VERSION",
        get: function() {
            return "4.4.1";
        }
    }, {
        key: "DefaultType",
        get: function() {
            return Pd;
        }
    }, {
        key: "Default",
        get: function() {
            return Qd;
        }
    } ]), Sd);
    function Sd(a, b) {
        this._element = a, this._config = this._getConfig(b), this._timeout = null, this._setListeners();
    }
    b.fn[Jd] = Rd._jQueryInterface, b.fn[Jd].Constructor = Rd, b.fn[Jd].noConflict = function() {
        return b.fn[Jd] = Md, Rd._jQueryInterface;
    }, a.Alert = o, a.Button = A, a.Carousel = P, a.Collapse = cb, a.Dropdown = pc, 
    a.Modal = Fc, a.Popover = kd, a.Scrollspy = xd, a.Tab = Gd, a.Toast = Rd, a.Tooltip = _c, 
    a.Util = h, Object.defineProperty(a, "__esModule", {
        value: !0
    });
}), function(a) {
    a.fn.extend({
        slimScroll: function(b) {
            var c = a.extend({
                width: "auto",
                height: "250px",
                size: "7px",
                color: "#000",
                position: "right",
                distance: "1px",
                start: "top",
                opacity: .4,
                alwaysVisible: !1,
                disableFadeOut: !1,
                railVisible: !1,
                railColor: "#333",
                railOpacity: .2,
                railDraggable: !0,
                railClass: "slimScrollRail",
                barClass: "slimScrollBar",
                wrapperClass: "slimScrollDiv",
                allowPageScroll: !1,
                wheelStep: 20,
                touchScrollStep: 200,
                borderRadius: "7px",
                railBorderRadius: "7px"
            }, b);
            return this.each(function() {
                function d(b) {
                    if (i) {
                        var d = 0;
                        (b = b || window.event).wheelDelta && (d = -b.wheelDelta / 120), b.detail && (d = b.detail / 3), 
                        a(b.target || b.srcTarget || b.srcElement).closest("." + c.wrapperClass).is(r.parent()) && e(d, !0), 
                        b.preventDefault && !q && b.preventDefault(), q || (b.returnValue = !1);
                    }
                }
                function e(a, b, d) {
                    q = !1;
                    var e = r.outerHeight() - u.outerHeight();
                    b && (b = parseInt(u.css("top")) + a * parseInt(c.wheelStep) / 100 * u.outerHeight(), 
                    b = Math.min(Math.max(b, 0), e), b = 0 < a ? Math.ceil(b) : Math.floor(b), u.css({
                        top: b + "px"
                    })), b = (o = parseInt(u.css("top")) / (r.outerHeight() - u.outerHeight())) * (r[0].scrollHeight - r.outerHeight()), 
                    d && (a = (b = a) / r[0].scrollHeight * r.outerHeight(), a = Math.min(Math.max(a, 0), e), 
                    u.css({
                        top: a + "px"
                    })), r.scrollTop(b), r.trigger("slimscrolling", ~~b), g(), h();
                }
                function f() {
                    n = Math.max(r.outerHeight() / r[0].scrollHeight * r.outerHeight(), 30), u.css({
                        height: n + "px"
                    });
                    var a = n == r.outerHeight() ? "none" : "block";
                    u.css({
                        display: a
                    });
                }
                function g() {
                    f(), clearTimeout(l), o == ~~o ? (q = c.allowPageScroll, p != o && r.trigger("slimscroll", 0 == ~~o ? "top" : "bottom")) : q = !1, 
                    p = o, n >= r.outerHeight() ? q = !0 : (u.stop(!0, !0).fadeIn("fast"), c.railVisible && v.stop(!0, !0).fadeIn("fast"));
                }
                function h() {
                    c.alwaysVisible || (l = setTimeout(function() {
                        c.disableFadeOut && i || j || k || (u.fadeOut("slow"), v.fadeOut("slow"));
                    }, 1e3));
                }
                var i, j, k, l, m, n, o, p, q = !1, r = a(this);
                if (r.parent().hasClass(c.wrapperClass)) {
                    var s = r.scrollTop(), u = r.siblings("." + c.barClass), v = r.siblings("." + c.railClass);
                    if (f(), a.isPlainObject(b)) {
                        if ("height" in b && "auto" == b.height) {
                            r.parent().css("height", "auto"), r.css("height", "auto");
                            var w = r.parent().parent().height();
                            r.parent().css("height", w), r.css("height", w);
                        } else "height" in b && (w = b.height, r.parent().css("height", w), r.css("height", w));
                        if ("scrollTo" in b) s = parseInt(c.scrollTo); else if ("scrollBy" in b) s += parseInt(c.scrollBy); else if ("destroy" in b) return u.remove(), 
                        v.remove(), void r.unwrap();
                        e(s, !1, !0);
                    }
                } else if (!(a.isPlainObject(b) && "destroy" in b)) {
                    c.height = "auto" == c.height ? r.parent().height() : c.height, s = a("<div></div>").addClass(c.wrapperClass).css({
                        position: "relative",
                        overflow: "hidden",
                        width: c.width,
                        height: c.height
                    }), r.css({
                        overflow: "hidden",
                        width: c.width,
                        height: c.height
                    });
                    v = a("<div></div>").addClass(c.railClass).css({
                        width: c.size,
                        height: "100%",
                        position: "absolute",
                        top: 0,
                        display: c.alwaysVisible && c.railVisible ? "block" : "none",
                        "border-radius": c.railBorderRadius,
                        background: c.railColor,
                        opacity: c.railOpacity,
                        zIndex: 90
                    }), u = a("<div></div>").addClass(c.barClass).css({
                        background: c.color,
                        width: c.size,
                        position: "absolute",
                        top: 0,
                        opacity: c.opacity,
                        display: c.alwaysVisible ? "block" : "none",
                        "border-radius": c.borderRadius,
                        BorderRadius: c.borderRadius,
                        MozBorderRadius: c.borderRadius,
                        WebkitBorderRadius: c.borderRadius,
                        zIndex: 99
                    }), w = "right" == c.position ? {
                        right: c.distance
                    } : {
                        left: c.distance
                    };
                    v.css(w), u.css(w), r.wrap(s), r.parent().append(u), r.parent().append(v), c.railDraggable && u.bind("mousedown", function(b) {
                        var c = a(document);
                        return k = !0, t = parseFloat(u.css("top")), pageY = b.pageY, c.bind("mousemove.slimscroll", function(a) {
                            currTop = t + a.pageY - pageY, u.css("top", currTop), e(0, u.position().top, !1);
                        }), c.bind("mouseup.slimscroll", function(a) {
                            k = !1, h(), c.unbind(".slimscroll");
                        }), !1;
                    }).bind("selectstart.slimscroll", function(a) {
                        return a.stopPropagation(), a.preventDefault(), !1;
                    }), v.hover(function() {
                        g();
                    }, function() {
                        h();
                    }), u.hover(function() {
                        j = !0;
                    }, function() {
                        j = !1;
                    }), r.hover(function() {
                        i = !0, g(), h();
                    }, function() {
                        i = !1, h();
                    }), r.bind("touchstart", function(a, b) {
                        a.originalEvent.touches.length && (m = a.originalEvent.touches[0].pageY);
                    }), r.bind("touchmove", function(a) {
                        q || a.originalEvent.preventDefault(), a.originalEvent.touches.length && (e((m - a.originalEvent.touches[0].pageY) / c.touchScrollStep, !0), 
                        m = a.originalEvent.touches[0].pageY);
                    }), f(), "bottom" === c.start ? (u.css({
                        top: r.outerHeight() - u.outerHeight()
                    }), e(0, !0)) : "top" !== c.start && (e(a(c.start).position().top, null, !0), c.alwaysVisible || u.hide()), 
                    window.addEventListener ? (this.addEventListener("DOMMouseScroll", d, !1), this.addEventListener("mousewheel", d, !1)) : document.attachEvent("onmousewheel", d);
                }
            }), this;
        }
    }), a.fn.extend({
        slimscroll: a.fn.slimScroll
    });
}(jQuery), function(a) {
    var b;
    if ("function" == typeof define && define.amd && (define(a), b = !0), "object" == typeof exports && (module.exports = a(), 
    b = !0), !b) {
        var c = window.Cookies, d = window.Cookies = a();
        d.noConflict = function() {
            return window.Cookies = c, d;
        };
    }
}(function() {
    function a() {
        for (var a = 0, b = {}; a < arguments.length; a++) {
            var c = arguments[a];
            for (var d in c) b[d] = c[d];
        }
        return b;
    }
    function b(a) {
        return a.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
    }
    return function c(d) {
        function e() {}
        function f(b, c, f) {
            if ("undefined" != typeof document) {
                "number" == typeof (f = a({
                    path: "/"
                }, e.defaults, f)).expires && (f.expires = new Date(+new Date() + 864e5 * f.expires)), 
                f.expires = f.expires ? f.expires.toUTCString() : "";
                try {
                    var g = JSON.stringify(c);
                    /^[\{\[]/.test(g) && (c = g);
                } catch (b) {}
                c = d.write ? d.write(c, b) : encodeURIComponent(String(c)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), 
                b = encodeURIComponent(String(b)).replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent).replace(/[\(\)]/g, escape);
                var h = "";
                for (var i in f) f[i] && (h += "; " + i, !0 !== f[i] && (h += "=" + f[i].split(";")[0]));
                return document.cookie = b + "=" + c + h;
            }
        }
        function g(a, c) {
            if ("undefined" != typeof document) {
                for (var e = {}, f = document.cookie ? document.cookie.split("; ") : [], g = 0; g < f.length; g++) {
                    var h = f[g].split("="), i = h.slice(1).join("=");
                    c || '"' !== i.charAt(0) || (i = i.slice(1, -1));
                    try {
                        var j = b(h[0]);
                        if (i = (d.read || d)(i, j) || b(i), c) try {
                            i = JSON.parse(i);
                        } catch (a) {}
                        if (e[j] = i, a === j) break;
                    } catch (a) {}
                }
                return a ? e[a] : e;
            }
        }
        return e.set = f, e.get = function(a) {
            return g(a, !1);
        }, e.getJSON = function(a) {
            return g(a, !0);
        }, e.remove = function(b, c) {
            f(b, "", a(c, {
                expires: -1
            }));
        }, e.defaults = {}, e.withConverter = c, e;
    }(function() {});
});

var floatSubMenuTimeout, targetFloatMenu, handleSlimScroll = function() {
    "use strict";
    $.when($("[data-scrollbar=true]").each(function() {
        generateSlimScroll($(this));
    })).done(function() {
        $('[data-scrollbar="true"]').mouseover();
    });
}, generateSlimScroll = function(a) {
    if (!$(a).attr("data-init")) {
        var b = $(a).attr("data-height"), c = {
            height: b = b || $(a).height()
        };
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? ($(a).css("height", b), 
        $(a).css("overflow-x", "scroll")) : $(a).slimScroll(c), $(a).attr("data-init", !0), 
        $(".slimScrollBar").hide();
    }
}, handleSidebarMenu = function() {
    "use strict";
    var a = $(".sidebar").attr("data-disable-slide-animation") ? 0 : 250;
    $(document).on("click", ".sidebar .nav > .has-sub > a", function() {
        var b = $(this).next(".sub-menu"), c = $(".sidebar .nav > li.has-sub > .sub-menu").not(b);
        0 === $(".page-sidebar-minified").length && ($(c).closest("li").addClass("closing"), 
        $(c).slideUp(a, function() {
            $(c).closest("li").addClass("closed").removeClass("expand closing");
        }), $(b).is(":visible") ? $(b).closest("li").addClass("closing").removeClass("expand") : $(b).closest("li").addClass("expanding").removeClass("closed"), 
        $(b).slideToggle(a, function() {
            var a = $(this).closest("li");
            $(b).is(":visible") ? ($(a).addClass("expand"), $(a).removeClass("closed")) : ($(a).addClass("closed"), 
            $(a).removeClass("expand")), $(a).removeClass("expanding closing");
        }));
    }), $(document).on("click", ".sidebar .nav > .has-sub .sub-menu li.has-sub > a", function() {
        if (0 === $(".page-sidebar-minified").length) {
            var b = $(this).next(".sub-menu");
            $(b).is(":visible") ? $(b).closest("li").addClass("closing").removeClass("expand") : $(b).closest("li").addClass("expanding").removeClass("closed"), 
            $(b).slideToggle(a, function() {
                var a = $(this).closest("li");
                $(b).is(":visible") ? ($(a).addClass("expand"), $(a).removeClass("closed")) : ($(a).addClass("closed"), 
                $(a).removeClass("expand")), $(a).removeClass("expanding closing");
            });
        }
    });
}, handleMobileSidebarToggle = function() {
    var a = !1;
    $(document).on("click touchstart", ".sidebar", function(b) {
        0 !== $(b.target).closest(".sidebar").length ? a = !0 : (a = !1, b.stopPropagation());
    }), $(document).on("click touchstart", function(b) {
        0 === $(b.target).closest(".sidebar").length && (a = !1), 0 !== $(b.target).closest("#float-sub-menu").length && (a = !0), 
        b.isPropagationStopped() || !0 === a || ($("#page-container").hasClass("page-sidebar-toggled") && (a = !0, 
        $("#page-container").removeClass("page-sidebar-toggled")), $(window).width() <= 767 && $("#page-container").hasClass("page-right-sidebar-toggled") && (a = !0, 
        $("#page-container").removeClass("page-right-sidebar-toggled")));
    }), $(document).on("click", "[data-click=right-sidebar-toggled]", function(b) {
        b.stopPropagation();
        var c = "#page-container", d = "page-right-sidebar-collapsed";
        d = $(window).width() < 768 ? "page-right-sidebar-toggled" : d, $(c).hasClass(d) ? $(c).removeClass(d) : !0 !== a ? $(c).addClass(d) : a = !1, 
        $(window).width() < 480 && $("#page-container").removeClass("page-sidebar-toggled"), 
        $(window).trigger("resize");
    }), $(document).on("click", "[data-click=sidebar-toggled]", function(b) {
        b.stopPropagation();
        var c = "page-sidebar-toggled", d = "#page-container";
        $(d).hasClass(c) ? $(d).removeClass(c) : !0 !== a ? $(d).addClass(c) : a = !1, $(window).width() < 480 && $("#page-container").removeClass("page-right-sidebar-toggled");
    });
}, handleSidebarMinify = function() {
    $(document).on("click", '[data-click="sidebar-minify"]', function(a) {
        a.preventDefault();
        var b = "page-sidebar-minified", c = "#page-container", d = !1;
        $(c).hasClass(b) ? $(c).removeClass(b) : ($(c).addClass(b), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && ($('#sidebar [data-scrollbar="true"]').css("margin-top", "0"), 
        $('#sidebar [data-scrollbar="true"]').css("overflow-x", "scroll")), d = !0), $(window).trigger("resize"), 
        Cookies && Cookies.set("sidebar-minified", d);
    }), !Cookies || "true" == Cookies.get("sidebar-minified") && $("#page-container").addClass("page-sidebar-minified");
}, handlePageContentView = function() {
    "use strict";
    var a = "", b = "", c = handleCheckBootstrapVersion();
    3 <= c && c < 4 ? (a = "hide", b = "in") : 4 <= c && c < 5 && (a = "d-none", b = "show"), 
    $(window).on("load", function() {
        $.when($("#page-loader").addClass(a)).done(function() {
            $("#page-container").addClass(b);
        });
    });
}, panelActionRunning = !1, handlePanelAction = function() {
    "use strict";
    if (panelActionRunning) return !1;
    panelActionRunning = !0, $(document).on("hover", "[data-click=panel-remove]", function(a) {
        $(this).attr("data-init") || ($(this).tooltip({
            title: "Remove",
            placement: "bottom",
            trigger: "hover",
            container: "body"
        }), $(this).tooltip("show"), $(this).attr("data-init", !0));
    }), $(document).on("click", "[data-click=panel-remove]", function(a) {
        a.preventDefault();
        var b = handleCheckBootstrapVersion();
        4 <= b && b < 5 ? $(this).tooltip("dispose") : $(this).tooltip("destroy"), $(this).closest(".panel").remove();
    }), $(document).on("hover", "[data-click=panel-collapse]", function(a) {
        $(this).attr("data-init") || ($(this).tooltip({
            title: "Collapse / Expand",
            placement: "bottom",
            trigger: "hover",
            container: "body"
        }), $(this).tooltip("show"), $(this).attr("data-init", !0));
    }), $(document).on("click", "[data-click=panel-collapse]", function(a) {
        a.preventDefault(), $(this).closest(".panel").find(".panel-body").slideToggle();
    }), $(document).on("hover", "[data-click=panel-reload]", function(a) {
        $(this).attr("data-init") || ($(this).tooltip({
            title: "Reload",
            placement: "bottom",
            trigger: "hover",
            container: "body"
        }), $(this).tooltip("show"), $(this).attr("data-init", !0));
    }), $(document).on("click", "[data-click=panel-reload]", function(a) {
        a.preventDefault();
        var b = $(this).closest(".panel");
        if (!$(b).hasClass("panel-loading")) {
            var c = $(b).find(".panel-body");
            $(b).addClass("panel-loading"), $(c).prepend('<div class="panel-loader"><span class="spinner-small"></span></div>'), 
            setTimeout(function() {
                $(b).removeClass("panel-loading"), $(b).find(".panel-loader").remove();
            }, 2e3);
        }
    }), $(document).on("hover", "[data-click=panel-expand]", function(a) {
        $(this).attr("data-init") || ($(this).tooltip({
            title: "Expand / Compress",
            placement: "bottom",
            trigger: "hover",
            container: "body"
        }), $(this).tooltip("show"), $(this).attr("data-init", !0));
    }), $(document).on("click", "[data-click=panel-expand]", function(a) {
        a.preventDefault();
        var b = $(this).closest(".panel"), c = $(b).find(".panel-body"), d = 40;
        if (0 !== $(c).length) {
            var e = $(b).offset().top;
            d = $(c).offset().top - e;
        }
        if ($("body").hasClass("panel-expand") && $(b).hasClass("panel-expand")) $("body, .panel").removeClass("panel-expand"), 
        $(".panel").removeAttr("style"), $(c).removeAttr("style"); else if ($("body").addClass("panel-expand"), 
        $(this).closest(".panel").addClass("panel-expand"), 0 !== $(c).length && 40 != d) {
            var f = 40;
            $(b).find(" > *").each(function() {
                var a = $(this).attr("class");
                "panel-heading" != a && "panel-body" != a && (f += $(this).height() + 30);
            }), 40 != f && $(c).css("top", f + "px");
        }
        $(window).trigger("resize");
    });
}, handleDraggablePanel = function() {
    "use strict";
    var a = $('.panel:not([data-sortable="false"])').parent("[class*=col]");
    $(a).sortable({
        handle: ".panel-heading",
        connectWith: ".row > [class*=col]",
        stop: function(a, b) {
            b.item.find(".panel-title").append('<i class="fa fa-refresh fa-spin m-l-5" data-id="title-spinner"></i>'), 
            handleSavePanelPosition(b.item);
        }
    });
}, handelTooltipPopoverActivation = function() {
    "use strict";
    0 !== $('[data-toggle="tooltip"]').length && $("[data-toggle=tooltip]").tooltip(), 
    0 !== $('[data-toggle="popover"]').length && $("[data-toggle=popover]").popover();
}, handleScrollToTopButton = function() {
    "use strict";
    var a = handleCheckBootstrapVersion(), b = "";
    3 <= a && a < 4 ? b = "in" : 4 <= a && a < 5 && (b = "show"), $(document).scroll(function() {
        200 <= $(document).scrollTop() ? $("[data-click=scroll-top]").addClass(b) : $("[data-click=scroll-top]").removeClass(b);
    }), $("[data-click=scroll-top]").click(function(a) {
        a.preventDefault(), $("html, body").animate({
            scrollTop: $("body").offset().top
        }, 500);
    });
}, handleThemePageStructureControl = function() {
    if ($(document).on("click", '.theme-panel [data-click="theme-selector"]', function() {
        var a = $(this).attr("data-theme-file"), b = $(this).attr("data-theme");
        0 === $("#theme-css-link").length ? $("head").append('<link href="' + a + '" rel="stylesheet" id="theme-css-link" />') : $("#theme-css-link").attr("href", a), 
        $('.theme-panel [data-click="theme-selector"]').not(this).closest("li").removeClass("active"), 
        $(this).closest("li").addClass("active"), Cookies && Cookies.set("page-theme", b);
    }), $(document).on("change", '.theme-panel [name="header-inverse"]', function() {
        var a = $(this).is(":checked"), b = a ? "navbar-inverse" : "navbar-default", c = a ? "navbar-default" : "navbar-inverse";
        $("#header").removeClass(c).addClass(b), Cookies && Cookies.set("header-theme", b);
    }), $(document).on("change", '.theme-panel [name="sidebar-grid"]', function() {
        var a = !1;
        $(this).is(":checked") ? ($("#sidebar").addClass("sidebar-grid"), a = !0) : $("#sidebar").removeClass("sidebar-grid"), 
        Cookies && Cookies.set("sidebar-grid", a);
    }), $(document).on("change", '.theme-panel [name="sidebar-gradient"]', function() {
        var a = !1;
        $(this).is(":checked") ? ($("#page-container").addClass("gradient-enabled"), a = !0) : $("#page-container").removeClass("gradient-enabled"), 
        Cookies && Cookies.set("sidebar-gradient", a);
    }), $(document).on("change", '.theme-panel [name="sidebar-fixed"]', function() {
        var a = !1;
        $(this).is(":checked") ? ($('.theme-panel [name="header-fixed"]').is(":checked") || (alert("Default Header with Fixed Sidebar option is not supported. Proceed with Fixed Header with Fixed Sidebar."), 
        $('.theme-panel [name="header-fixed"]').prop("checked", !0), $("#page-container").addClass("page-header-fixed")), 
        $("#page-container").addClass("page-sidebar-fixed"), $("#page-container").hasClass("page-sidebar-minified") || generateSlimScroll($('.sidebar [data-scrollbar="true"]')), 
        a = !0) : ($("#page-container").removeClass("page-sidebar-fixed"), 0 !== $(".sidebar .slimScrollDiv").length && ($(window).width() <= 979 ? $(".sidebar").each(function() {
            if (!$("#page-container").hasClass("page-with-two-sidebar") || !$(this).hasClass("sidebar-right")) {
                $(this).find(".slimScrollBar").remove(), $(this).find(".slimScrollRail").remove(), 
                $(this).find('[data-scrollbar="true"]').removeAttr("style");
                var a = $(this).find('[data-scrollbar="true"]').parent(), b = $(a).html();
                $(a).replaceWith(b);
            }
        }) : 979 < $(window).width() && ($('.sidebar [data-scrollbar="true"]').slimScroll({
            destroy: !0
        }), $('.sidebar [data-scrollbar="true"]').removeAttr("style"), $('.sidebar [data-scrollbar="true"]').removeAttr("data-init"))), 
        0 === $("#page-container .sidebar-bg").length && $("#page-container").append('<div class="sidebar-bg"></div>')), 
        Cookies && Cookies.set("sidebar-fixed", a);
    }), $(document).on("change", '.theme-panel [name="header-fixed"]', function() {
        var a = !1;
        $(this).is(":checked") ? ($("#header").addClass("navbar-fixed-top"), $("#page-container").addClass("page-header-fixed"), 
        a = !0) : ($('.theme-panel [name="sidebar-fixed"]').is(":checked") && (alert("Default Header with Fixed Sidebar option is not supported. Proceed with Default Header with Default Sidebar."), 
        $('.theme-panel [name="sidebar-fixed"]').prop("checked", !1), $('.theme-panel [name="sidebar-fixed"]').trigger("change"), 
        0 === $("#page-container .sidebar-bg").length && $("#page-container").append('<div class="sidebar-bg"></div>')), 
        $("#header").removeClass("navbar-fixed-top"), $("#page-container").removeClass("page-header-fixed")), 
        Cookies && Cookies.set("header-fixed", a);
    }), Cookies) {
        var a = Cookies.get("page-theme"), b = Cookies.get("header-theme"), c = Cookies.get("sidebar-grid"), d = Cookies.get("sidebar-gradient"), e = Cookies.get("sidebar-fixed"), f = Cookies.get("header-fixed");
        a && $('.theme-panel [data-click="theme-selector"][data-theme="' + a + '"]').trigger("click"), 
        b && "navbar-inverse" == b && $('.theme-panel [name="header-inverse"]').prop("checked", !0).trigger("change"), 
        "true" == c && $('.theme-panel [name="sidebar-grid"]').prop("checked", !0).trigger("change"), 
        "true" == d && $('.theme-panel [name="sidebar-gradient"]').prop("checked", !0).trigger("change"), 
        "false" == e && $('.theme-panel [name="sidebar-fixed"]').prop("checked", !1).trigger("change"), 
        "false" == f && $('.theme-panel [name="header-fixed"]').prop("checked", !1).trigger("change");
    }
}, handleThemePanelExpand = function() {
    $(document).on("click", '[data-click="theme-panel-expand"]', function() {
        var a = ".theme-panel", b = "active", c = !1;
        $(a).hasClass(b) ? $(a).removeClass(b) : ($(a).addClass(b), c = !0), Cookies && Cookies.set("theme-panel-expand", c);
    }), !Cookies || "true" == Cookies.get("theme-panel-expand") && $('[data-click="theme-panel-expand"]').trigger("click");
}, handleAfterPageLoadAddClass = function() {
    0 !== $("[data-pageload-addclass]").length && $(window).on("load", function() {
        $("[data-pageload-addclass]").each(function() {
            var a = $(this).attr("data-pageload-addclass");
            $(this).addClass(a);
        });
    });
}, handleSavePanelPosition = function(a) {
    "use strict";
    if (0 !== $(".ui-sortable").length) {
        var b = [];
        $.when($(".ui-sortable").each(function() {
            var a = $(this).find("[data-sortable-id]");
            if (0 !== a.length) {
                var c = [];
                $(a).each(function() {
                    var a = $(this).attr("data-sortable-id");
                    c.push({
                        id: a
                    });
                }), b.push(c);
            } else b.push([]);
        })).done(function() {
            var c = window.location.href;
            c = (c = c.split("?"))[0], localStorage.setItem(c, JSON.stringify(b)), $(a).find('[data-id="title-spinner"]').delay(500).fadeOut(500, function() {
                $(this).remove();
            });
        });
    }
}, handleLocalStorage = function() {
    "use strict";
    try {
        if ("undefined" != typeof Storage && "undefined" != typeof localStorage) {
            var a = window.location.href;
            a = (a = a.split("?"))[0];
            var b = localStorage.getItem(a);
            if (b) {
                b = JSON.parse(b);
                var c = 0;
                $.when($('.panel:not([data-sortable="false"])').parent('[class*="col-"]').each(function() {
                    var a = b[c], d = $(this);
                    a && $.each(a, function(a, b) {
                        var c = $('[data-sortable-id="' + b.id + '"]').not('[data-init="true"]');
                        if (0 !== $(c).length) {
                            var e = $(c).clone();
                            $(c).remove(), $(d).append(e), $('[data-sortable-id="' + b.id + '"]').attr("data-init", "true");
                        }
                    }), c++;
                })).done(function() {
                    window.dispatchEvent(new CustomEvent("localstorage-position-loaded"));
                });
            }
        } else alert("Your browser is not supported with the local storage");
    } catch (a) {
        console.log(a);
    }
}, handleResetLocalStorage = function() {
    "use strict";
    $(document).on("click", "[data-click=reset-local-storage]", function(a) {
        a.preventDefault();
        $("body").append('<div class="modal fade" data-modal-id="reset-local-storage-confirmation">    <div class="modal-dialog">        <div class="modal-content">            <div class="modal-header">                <h4 class="modal-title"><i class="fa fa-redo m-r-5"></i> Reset Local Storage Confirmation</h4>                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>            </div>            <div class="modal-body">                <div class="alert alert-info m-b-0">Would you like to RESET all your saved widgets and clear Local Storage?</div>            </div>            <div class="modal-footer">                <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times"></i> No</a>                <a href="javascript:;" class="btn btn-sm btn-inverse" data-click="confirm-reset-local-storage"><i class="fa fa-check"></i> Yes</a>            </div>        </div>    </div></div>'), 
        $('[data-modal-id="reset-local-storage-confirmation"]').modal("show");
    }), $(document).on("hidden.bs.modal", '[data-modal-id="reset-local-storage-confirmation"]', function(a) {
        $('[data-modal-id="reset-local-storage-confirmation"]').remove();
    }), $(document).on("click", "[data-click=confirm-reset-local-storage]", function(a) {
        a.preventDefault();
        var b = window.location.href;
        b = (b = b.split("?"))[0], localStorage.removeItem(b), location.reload();
    });
}, handleIEFullHeightContent = function() {
    0 < window.navigator.userAgent.indexOf("MSIE ") && $('.vertical-box-row [data-scrollbar="true"][data-height="100%"]').each(function() {
        var a = $(this).closest(".vertical-box-row"), b = $(a).height();
        $(a).find(".vertical-box-cell").height(b);
    });
}, handleUnlimitedTabsRender = function() {
    function a(a, b) {
        var c = $(a).closest(".tab-overflow"), d = "rtl" == $("body").css("direction") ? "margin-right" : "margin-left", e = parseInt($(c).find(".nav.nav-tabs").css(d)), f = $(c).width(), g = 0, h = 0;
        switch ($(c).find("li").each(function() {
            $(this).hasClass("next-button") || $(this).hasClass("prev-button") || (g += $(this).width());
        }), b) {
          case "next":
            (i = g + e - f) <= f ? (h = i - e, setTimeout(function() {
                $(c).removeClass("overflow-right");
            }, 150)) : h = f - e - 80, 0 !== h && ("rtl" != $("body").css("direction") ? $(c).find(".nav.nav-tabs").animate({
                marginLeft: "-" + h + "px"
            }, 150, function() {
                $(c).addClass("overflow-left");
            }) : $(c).find(".nav.nav-tabs").animate({
                marginRight: "-" + h + "px"
            }, 150, function() {
                $(c).addClass("overflow-left");
            }));
            break;

          case "prev":
            var i;
            h = (i = -e) <= f ? ($(c).removeClass("overflow-left"), 0) : i - f + 80, "rtl" != $("body").css("direction") ? $(c).find(".nav.nav-tabs").animate({
                marginLeft: "-" + h + "px"
            }, 150, function() {
                $(c).addClass("overflow-right");
            }) : $(c).find(".nav.nav-tabs").animate({
                marginRight: "-" + h + "px"
            }, 150, function() {
                $(c).addClass("overflow-right");
            });
        }
    }
    function b() {
        $(".tab-overflow").each(function() {
            !function(a, b) {
                var c = "li.active";
                $(a).find("li").first().hasClass("nav-item") && (c = $(a).find(".nav-item .active").closest("li"));
                var d = "rtl" == $("body").css("direction") ? "margin-right" : "margin-left", e = (parseInt($(a).css(d)), 
                $(a).width()), f = $(a).find(c).width(), g = -1 < b ? b : 150, h = 0;
                if ($(a).find(c).prevAll().each(function() {
                    f += $(this).width();
                }), $(a).find("li").each(function() {
                    h += $(this).width();
                }), e <= f) {
                    var i = f - e;
                    h != f && (i += 40), "rtl" == $("body").css("direction") ? $(a).find(".nav.nav-tabs").animate({
                        marginRight: "-" + i + "px"
                    }, g) : $(a).find(".nav.nav-tabs").animate({
                        marginLeft: "-" + i + "px"
                    }, g);
                }
                f != h && e <= h ? $(a).addClass("overflow-right") : $(a).removeClass("overflow-right"), 
                e <= f && e <= h ? $(a).addClass("overflow-left") : $(a).removeClass("overflow-left");
            }(this, 0);
        });
    }
    $('[data-click="next-tab"]').click(function(b) {
        b.preventDefault(), a(this, "next");
    }), $('[data-click="prev-tab"]').click(function(b) {
        b.preventDefault(), a(this, "prev");
    }), $(window).resize(function() {
        $(".tab-overflow .nav.nav-tabs").removeAttr("style"), b();
    }), b();
}, handleUnlimitedTopMenuRender = function() {
    "use strict";
    function a(a, b) {
        var c = $(a).closest(".nav"), d = "rtl" == $("body").css("direction") ? "margin-right" : "margin-left", e = parseInt($(c).css(d)), f = $(".top-menu").width() - 88, g = 0, h = 0;
        switch ($(c).find("li").each(function() {
            $(this).hasClass("menu-control") || (g += $(this).width());
        }), b) {
          case "next":
            (i = g + e - f) <= f ? (h = i - e + 128, setTimeout(function() {
                $(c).find(".menu-control.menu-control-right").removeClass("show");
            }, 150)) : h = f - e - 128, 0 !== h && ("rtl" != $("body").css("direction") ? $(c).animate({
                marginLeft: "-" + h + "px"
            }, 150, function() {
                $(c).find(".menu-control.menu-control-left").addClass("show");
            }) : $(c).animate({
                marginRight: "-" + h + "px"
            }, 150, function() {
                $(c).find(".menu-control.menu-control-left").addClass("show");
            }));
            break;

          case "prev":
            var i;
            h = (i = -e) <= f ? ($(c).find(".menu-control.menu-control-left").removeClass("show"), 
            0) : i - f + 88, "rtl" != $("body").css("direction") ? $(c).animate({
                marginLeft: "-" + h + "px"
            }, 150, function() {
                $(c).find(".menu-control.menu-control-right").addClass("show");
            }) : $(c).animate({
                marginRight: "-" + h + "px"
            }, 150, function() {
                $(c).find(".menu-control.menu-control-right").addClass("show");
            });
        }
    }
    function b() {
        var a = $(".top-menu .nav"), b = $(".top-menu .nav > li"), c = $(".top-menu .nav > li.active"), d = $(".top-menu"), e = "rtl" == $("body").css("direction") ? "margin-right" : "margin-left", f = (parseInt($(a).css(e)), 
        $(d).width() - 128), g = $(".top-menu .nav > li.active").width(), h = 0;
        if ($(c).prevAll().each(function() {
            g += $(this).width();
        }), $(b).each(function() {
            $(this).hasClass("menu-control") || (h += $(this).width());
        }), f <= g) {
            var i = g - f + 128;
            "rtl" != $("body").css("direction") ? $(a).animate({
                marginLeft: "-" + i + "px"
            }, 0) : $(a).animate({
                marginRight: "-" + i + "px"
            }, 0);
        }
        g != h && f <= h ? $(a).find(".menu-control.menu-control-right").addClass("show") : $(a).find(".menu-control.menu-control-right").removeClass("show"), 
        f <= g && f <= h ? $(a).find(".menu-control.menu-control-left").addClass("show") : $(a).find(".menu-control.menu-control-left").removeClass("show");
    }
    $('[data-click="next-menu"]').click(function(b) {
        b.preventDefault(), a(this, "next");
    }), $('[data-click="prev-menu"]').click(function(b) {
        b.preventDefault(), a(this, "prev");
    }), $(window).resize(function() {
        $(".top-menu .nav").removeAttr("style"), b();
    }), b();
}, handleTopMenuSubMenu = function() {
    "use strict";
    $(document).on("click", ".top-menu .sub-menu .has-sub > a", function() {
        var a = $(this).closest("li").find(".sub-menu").first(), b = $(this).closest("ul").find(".sub-menu").not(a);
        $(b).not(a).slideUp(250, function() {
            $(this).closest("li").removeClass("expand");
        }), $(a).slideToggle(250, function() {
            var a = $(this).closest("li");
            $(a).hasClass("expand") ? $(a).removeClass("expand") : $(a).addClass("expand");
        });
    });
}, handleMobileTopMenuSubMenu = function() {
    "use strict";
    $(document).on("click", ".top-menu .nav > li.has-sub > a", function() {
        if ($(window).width() <= 767) {
            var a = $(this).closest("li").find(".sub-menu").first(), b = $(this).closest("ul").find(".sub-menu").not(a);
            $(b).not(a).slideUp(250, function() {
                $(this).closest("li").removeClass("expand");
            }), $(a).slideToggle(250, function() {
                var a = $(this).closest("li");
                $(a).hasClass("expand") ? $(a).removeClass("expand") : $(a).addClass("expand");
            });
        }
    });
}, handleTopMenuMobileToggle = function() {
    "use strict";
    $(document).on("click", '[data-click="top-menu-toggled"]', function() {
        $(".top-menu").slideToggle(250);
    });
}, handleClearSidebarSelection = function() {
    $(".sidebar .nav > li, .sidebar .nav .sub-menu").removeClass("expand").removeAttr("style");
}, handleClearSidebarMobileSelection = function() {
    $("#page-container").removeClass("page-sidebar-toggled");
}, handleCheckBootstrapVersion = function() {
    return parseInt($.fn.tooltip.Constructor.VERSION);
}, handleCheckScrollClass = function() {
    0 < $(window).scrollTop() ? $("#page-container").addClass("has-scroll") : $("#page-container").removeClass("has-scroll");
}, handlePageScrollClass = function() {
    $(window).on("scroll", function() {
        handleCheckScrollClass();
    }), handleCheckScrollClass();
}, handleToggleNavProfile = function() {
    var a = $(".sidebar").attr("data-disable-slide-animation") ? 0 : 250;
    $(document).on("click", '[data-toggle="nav-profile"]', function(b) {
        b.preventDefault();
        var c = $(this).closest("li"), d = $(".sidebar .nav.nav-profile");
        $(d).is(":visible") ? ($(c).removeClass("active"), $(d).removeClass("closing")) : ($(c).addClass("active"), 
        $(d).addClass("expanding")), $(d).slideToggle(a, function() {
            $(d).is(":visible") ? ($(d).addClass("expand"), $(d).removeClass("closed")) : ($(d).addClass("closed"), 
            $(d).removeClass("expand")), $(d).removeClass("expanding closing");
        });
    });
}, handleSidebarScrollMemory = function() {
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) try {
        if ("undefined" != typeof Storage && "undefined" != typeof localStorage) {
            $('.sidebar [data-scrollbar="true"]').slimScroll().bind("slimscrolling", function(a, b) {
                localStorage.setItem("sidebarScrollPosition", b + "px");
            });
            var a = localStorage.getItem("sidebarScrollPosition");
            a && $('.sidebar [data-scrollbar="true"]').slimScroll({
                scrollTo: a
            });
        }
    } catch (a) {
        console.log(a);
    }
}, handleMouseoverFloatSubMenu = function(a) {
    clearTimeout(floatSubMenuTimeout);
}, handleMouseoutFloatSubMenu = function(a) {
    floatSubMenuTimeout = setTimeout(function() {
        $("#float-sub-menu").remove();
    }, 150);
}, handleSidebarMinifyFloatMenu = function() {
    $(document).on("click", "#float-sub-menu li.has-sub > a", function(a) {
        var b = $(this).next(".sub-menu"), c = $(b).closest("li"), d = !1, e = !1;
        $(b).is(":visible") ? ($(c).addClass("closing"), d = !0) : ($(c).addClass("expanding"), 
        e = !0), $(b).slideToggle({
            duration: 250,
            progress: function() {
                var a = $("#float-sub-menu"), b = $(a).height(), c = $(a).offset(), f = $(a).attr("data-offset-top"), g = $(a).attr("data-menu-offset-top"), h = c.top - $(window).scrollTop(), i = $(window).height();
                if (d && f < h && (h = f < h ? f : h, $("#float-sub-menu").css({
                    top: h + "px",
                    bottom: "auto"
                }), $("#float-sub-menu-arrow").css({
                    top: "20px",
                    bottom: "auto"
                }), $("#float-sub-menu-line").css({
                    top: "20px",
                    bottom: "auto"
                })), e && i - h < b) {
                    var j = i - g - 22;
                    $("#float-sub-menu").css({
                        top: "auto",
                        bottom: 0
                    }), $("#float-sub-menu-arrow").css({
                        top: "auto",
                        bottom: j + "px"
                    }), $("#float-sub-menu-line").css({
                        top: "20px",
                        bottom: j + "px"
                    });
                }
            },
            complete: function() {
                $(b).is(":visible") ? ($(c).addClass("expand"), $(c).removeClass("closed")) : ($(c).addClass("closed"), 
                $(c).removeClass("expand")), $(c).removeClass("closing expanding");
            }
        });
    }), $(document).on({
        mouseenter: function() {
            if ($("#page-container").hasClass("page-sidebar-minified")) {
                clearTimeout(floatSubMenuTimeout);
                var a = $(this).closest("li").find(".sub-menu").first();
                if (targetFloatMenu == this && 0 !== $("#float-sub-menu").length) return;
                targetFloatMenu = this;
                var b = $(a).html();
                if (b) {
                    var c = $("#sidebar").offset(), d = parseInt($("#sidebar").width()), e = $("#page-container").hasClass("page-with-right-sidebar") || "rtl" == $("body").css("direction") ? $(window).width() - c.left : c.left + d, f = ($(a).height(), 
                    $(this).offset().top - $(window).scrollTop()), g = $("#page-container").hasClass("page-with-right-sidebar") || "rtl" == $("body").css("direction") ? "auto" : e, h = $("#page-container").hasClass("page-with-right-sidebar") || "rtl" == $("body").css("direction") ? e : "auto", i = $(window).height();
                    if (0 === $("#float-sub-menu").length ? (b = '<div class="float-sub-menu-container" id="float-sub-menu" data-offset-top="' + f + '" data-menu-offset-top="' + f + '" onmouseover="handleMouseoverFloatSubMenu(this)" onmouseout="handleMouseoutFloatSubMenu(this)">	<div class="float-sub-menu-arrow" id="float-sub-menu-arrow"></div>	<div class="float-sub-menu-line" id="float-sub-menu-line"></div>	<ul class="float-sub-menu">' + b + "</ul></div>", 
                    $("#page-container").append(b)) : ($("#float-sub-menu").attr("data-offset-top", f), 
                    $("#float-sub-menu").attr("data-menu-offset-top", f), $(".float-sub-menu").html(b)), 
                    $("#float-sub-menu").height() < i - f) $("#float-sub-menu").css({
                        top: f,
                        left: g,
                        bottom: "auto",
                        right: h
                    }), $("#float-sub-menu-arrow").css({
                        top: "20px",
                        bottom: "auto"
                    }), $("#float-sub-menu-line").css({
                        top: "20px",
                        bottom: "auto"
                    }); else {
                        $("#float-sub-menu").css({
                            bottom: 0,
                            top: "auto",
                            left: g,
                            right: h
                        });
                        var j = i - f - 21;
                        $("#float-sub-menu-arrow").css({
                            top: "auto",
                            bottom: j + "px"
                        }), $("#float-sub-menu-line").css({
                            top: "20px",
                            bottom: j + "px"
                        });
                    }
                } else $("#float-sub-menu").remove(), targetFloatMenu = "";
            }
        },
        mouseleave: function() {
            $("#page-container").hasClass("page-sidebar-minified") && (floatSubMenuTimeout = setTimeout(function() {
                $("#float-sub-menu").remove(), targetFloatMenu = "";
            }, 250));
        }
    }, ".sidebar .nav > li.has-sub > a");
}, CLEAR_OPTION = "", handleAjaxMode = function(a) {
    var b = a.emptyHtml ? a.emptyHtml : '<div class="p-t-40 p-b-40 text-center f-s-20 content"><i class="fa fa-warning fa-lg text-muted m-r-5"></i> <span class="f-w-600 text-inverse">Error 404! Page not found.</span></div>', c = a.ajaxDefaultUrl ? a.ajaxDefaultUrl : "";
    function d(a) {
        a ? ($("#page-content-loader").remove(), $("body").removeClass("page-content-loading")) : 0 === $("#page-content-loader").length && ($("body").addClass("page-content-loading"), 
        $("#content").append('<div id="page-content-loader"><span class="spinner"></span></div>'));
    }
    function e(c, e, f) {
        var g, h, i;
        Pace.restart(), d(!1), $(".jvectormap-label, .jvector-label, .AutoFill_border ,#gritter-notice-wrapper, .ui-autocomplete, .colorpicker, .FixedHeader_Header, .FixedHeader_Cloned .lightboxOverlay, .lightbox, .introjs-hints, .nvtooltip, #float-sub-menu").remove(), 
        $.fn.DataTable && $(".dataTable").DataTable().destroy(), $("#page-container").hasClass("page-sidebar-toggled") && $("#page-container").removeClass("page-sidebar-toggled"), 
        g = '#sidebar [data-toggle="ajax"][href="' + c + '"]', 0 !== $(g).length && ($("#sidebar li").removeClass("active"), 
        $(g).closest("li").addClass("active"), $(g).parents().addClass("active")), CLEAR_OPTION && (App.clearPageOption(CLEAR_OPTION), 
        CLEAR_OPTION = ""), f || (h = c.replace("#", ""), (i = window.navigator.userAgent.indexOf("MSIE ")) && 0 < i && i < 9 ? window.location.href = h : history.pushState("", "", "#" + h));
        var j = c.replace("#", ""), k = a.ajaxType ? a.ajaxType : "GET", l = a.ajaxDataType ? a.ajaxDataType : "html";
        e && (l = $(e).attr("data-type") ? $(e).attr("data-type") : l, targetDataDataType = $(e).attr("data-data-type") ? $(e).attr("data-data-type") : l), 
        $.ajax({
            url: j,
            type: k,
            dataType: l,
            success: function(a) {
                $("#content").html(a);
            },
            error: function(a, c, d) {
                $("#content").html(b);
            }
        }).done(function() {
            d(!0), $("html, body").animate({
                scrollTop: 0
            }, 0), App.initComponent();
        });
    }
    "" === (c = window.location.hash ? window.location.hash : c) ? $("#content").html(b) : e(c, "", !0), 
    $(window).on("hashchange", function() {
        window.location.hash && e(window.location.hash, "", !0);
    }), $(document).on("click", '[data-toggle="ajax"]', function(a) {
        a.preventDefault(), e($(this).attr("href"), this);
    });
}, handleSetPageOption = function(a) {
    a.pageContentFullHeight && $("#page-container").addClass("page-content-full-height"), 
    a.pageSidebarLight && $("#page-container").addClass("page-with-light-sidebar"), 
    a.pageSidebarRight && $("#page-container").addClass("page-with-right-sidebar"), 
    a.pageSidebarWide && $("#page-container").addClass("page-with-wide-sidebar"), a.pageSidebarMinified && $("#page-container").addClass("page-sidebar-minified"), 
    a.pageSidebarTransparent && $("#sidebar").addClass("sidebar-transparent"), a.pageContentFullWidth && $("#content").addClass("content-full-width"), 
    a.pageContentInverseMode && $("#content").addClass("content-inverse-mode"), a.pageBoxedLayout && $("body").addClass("boxed-layout"), 
    a.clearOptionOnLeave && (CLEAR_OPTION = a);
}, handleClearPageOption = function(a) {
    a.pageContentFullHeight && $("#page-container").removeClass("page-content-full-height"), 
    a.pageSidebarLight && $("#page-container").removeClass("page-with-light-sidebar"), 
    a.pageSidebarRight && $("#page-container").removeClass("page-with-right-sidebar"), 
    a.pageSidebarWide && $("#page-container").removeClass("page-with-wide-sidebar"), 
    a.pageSidebarMinified && $("#page-container").removeClass("page-sidebar-minified"), 
    a.pageSidebarTransparent && $("#sidebar").removeClass("sidebar-transparent"), a.pageContentFullWidth && $("#content").removeClass("content-full-width"), 
    a.pageContentInverseMode && $("#content").removeClass("content-inverse-mode"), a.pageBoxedLayout && $("body").removeClass("boxed-layout");
}, handleToggleNavbarSearch = function() {
    $('[data-toggle="navbar-search"]').click(function(a) {
        a.preventDefault(), $(".header").addClass("header-search-toggled");
    }), $('[data-dismiss="navbar-search"]').click(function(a) {
        a.preventDefault(), $(".header").removeClass("header-search-toggled");
    });
}, convertNumberWithCommas = function(a) {
    return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}, checkIsFloat = function(a) {
    return Number(a) === a && a % 1 != 0;
}, checkIsInt = function(a) {
    return Number(a) === a && a % 1 == 0;
}, countDecimals = function(a) {
    var b = a.toString().split(".");
    return b[1] ? b[1].length : 0;
}, handleAnimation = function() {
    $("[data-animation]").each(function() {
        var a = $(this).attr("data-animation"), b = $(this).attr("data-value");
        switch (a) {
          case "width":
            $(this).css("width", b);
            break;

          case "height":
            $(this).css("height", b);
            break;

          case "number":
            for (var c = this, d = countDecimals(b), e = 1, f = d; 0 < f; ) e *= 10, f--;
            $({
                animateNumber: 0
            }).animate({
                animateNumber: b
            }, {
                duration: 1e3,
                easing: "swing",
                step: function() {
                    var a = (Math.ceil(this.animateNumber * e) / e).toFixed(d);
                    a = convertNumberWithCommas(a);
                    $(c).text(a);
                },
                done: function() {
                    $(c).text(convertNumberWithCommas(b));
                }
            });
            break;

          case "class":
            $(this).addClass(b);
        }
    });
}, handleSidebarSearch = function() {
    $(document).on("keyup", '[data-sidebar-search="true"]', function() {
        var a = $(this).val();
        (a = a.toLowerCase()) ? ($(".sidebar:not(.sidebar-right) .nav > li:not(.nav-profile):not(.nav-header):not(.nav-search), .sidebar:not(.sidebar-right) .sub-menu > li").addClass("d-none"), 
        $(".sidebar:not(.sidebar-right) .has-text").removeClass("has-text"), $(".sidebar:not(.sidebar-right) .expand").removeClass("expand"), 
        $(".sidebar:not(.sidebar-right) .nav > li:not(.nav-profile):not(.nav-header):not(.nav-search) > a, .sidebar .sub-menu > li > a").each(function() {
            var b = $(this).text();
            -1 < (b = b.toLowerCase()).search(a) && ($(this).closest("li").removeClass("d-none"), 
            $(this).closest("li").addClass("has-text"), 0 != $(this).closest("li.has-sub").length && $(this).closest("li.has-sub").find(".sub-menu li.d-none").removeClass("d-none"), 
            0 != $(this).closest(".sub-menu").length && ($(this).closest(".sub-menu").css("display", "block"), 
            $(this).closest(".has-sub").removeClass("d-none").addClass("expand"), $(this).closest(".sub-menu").find("li:not(.has-text)").addClass("d-none")));
        })) : ($(".sidebar:not(.sidebar-right) .nav > li:not(.nav-profile):not(.nav-header):not(.nav-search).has-sub .sub-menu").removeAttr("style"), 
        $(".sidebar:not(.sidebar-right) .nav > li:not(.nav-profile):not(.nav-header):not(.nav-search), .sidebar:not(.sidebar-right) .sub-menu > li").removeClass("d-none"), 
        $(".sidebar:not(.sidebar-right) .expand").removeClass("expand"));
    });
}, App = function() {
    "use strict";
    var a;
    return {
        init: function(b) {
            b && (a = b), this.initLocalStorage(), this.initSidebar(), this.initTopMenu(), this.initComponent(), 
            this.initThemePanel(), this.initPageLoad(), $(window).trigger("load"), a && a.ajaxMode && this.initAjax();
        },
        settings: function(b) {
            b && (a = b);
        },
        initSidebar: function() {
            handleSidebarMenu(), handleMobileSidebarToggle(), handleSidebarMinify(), handleSidebarMinifyFloatMenu(), 
            handleToggleNavProfile(), handleToggleNavbarSearch(), handleSidebarSearch(), a && (!a || a.disableSidebarScrollMemory) || handleSidebarScrollMemory();
        },
        initSidebarSelection: function() {
            handleClearSidebarSelection();
        },
        initSidebarMobileSelection: function() {
            handleClearSidebarMobileSelection();
        },
        initTopMenu: function() {
            handleUnlimitedTopMenuRender(), handleTopMenuSubMenu(), handleMobileTopMenuSubMenu(), 
            handleTopMenuMobileToggle();
        },
        initPageLoad: function() {
            handlePageContentView();
        },
        initComponent: function() {
            a && (!a || a.disableDraggablePanel) || handleDraggablePanel(), handleIEFullHeightContent(), 
            handleSlimScroll(), handleUnlimitedTabsRender(), handlePanelAction(), handleScrollToTopButton(), 
            handleAfterPageLoadAddClass(), handlePageScrollClass(), handleAnimation(), 767 < $(window).width() && handelTooltipPopoverActivation();
        },
        initLocalStorage: function() {
            a && (!a || a.disableLocalStorage) || handleLocalStorage();
        },
        initThemePanel: function() {
            handleThemePageStructureControl(), handleThemePanelExpand(), handleResetLocalStorage();
        },
        initAjax: function() {
            handleAjaxMode(a), $.ajaxSetup({
                cache: !0
            });
        },
        setPageTitle: function(a) {
            document.title = a;
        },
        setPageOption: function(a) {
            handleSetPageOption(a);
        },
        clearPageOption: function(a) {
            handleClearPageOption(a);
        },
        restartGlobalFunction: function() {
            this.initLocalStorage(), this.initTopMenu(), this.initComponent();
        },
        scrollTop: function() {
            $("html, body").animate({
                scrollTop: $("body").offset().top
            }, 0);
        }
    };
}();

$(document).ready(function() {
    App.init();
});