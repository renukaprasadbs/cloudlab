"""
    # Author: Bharat Gupta
    # Date - 02nd October, 2019
    # Description: App models file for Users
    # Copyright (c) CloudLab Solution Pvt Ltd. All Rights Reserved.
"""

from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import uuid

class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)

class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)

class UserTypes(models.Model):
    id = models.AutoField(primary_key=True)
    user_type = models.CharField(max_length=200,blank=False,null=True,default="")
    user_type_description = models.CharField(max_length=200,blank=False,null=True,default="")
    date_add = models.DateTimeField(blank=True,null=True)
    date_mod = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        managed = True
        db_table='user_types'

class AuthUser(models.Model):
    uuid_user = models.UUIDField(max_length=100,blank=True,null=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()    # will be true for customers
    username = models.CharField(unique=True, max_length=200)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField(blank=True,null=True)
    date_left = models.DateTimeField(blank=True, null=True)
    countrycode = models.CharField(max_length=15, blank=True, null=True)
    mobile = models.CharField(max_length=30,blank=True,null=True)
    user_type = models.ForeignKey(UserTypes, models.DO_NOTHING,blank=True,null=True)
    mobile_user = models.CharField(max_length=30,blank=True,null=True)
    is_admin = models.CharField(max_length=30,blank=True,null=True)
    
    @staticmethod
    def create_authuser(user_name,user_mail,user_pass,user_type_id,is_customer):
        if not User.objects.filter(username=user_name).exists():
            try:
                User.objects.create_user(user_name, user_mail, user_pass)
                auth_user = AuthUser.objects.get(username=user_name)
                auth_user.is_superuser = is_customer
                auth_user.uuid_user = uuid.uuid1()
                auth_user.user_type = user_type_id
                auth_user.save()
                return auth_user
            except Exception as e:
                print ("Exception in Model AuthUser.create_authuser : " + str(e))
                return None
        else:
            return -1 

    class Meta:
        managed = True
        db_table = 'auth_user'  
        
    """    
    JSONEncoder_olddefault = JSONEncoder.default
    def JSONEncoder_newdefault(self, o):
        if isinstance(o, UUID): return str(o)
        return self.JSONEncoder_olddefault(self, o)
    JSONEncoder.default = JSONEncoder_newdefault  
    
    
    def to_JSON(self):
        import json
        return json.dumps(dict([(attr, getattr(self, attr)) for attr in [f.name for f in self._meta.fields]]))
    """
    
class UserHistory(models.Model):
    uuid_user = models.UUIDField(max_length=100,blank=True,null=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()    # will be true for customers
    username = models.CharField(max_length=200)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField(blank=True,null=True)
    date_left = models.DateTimeField(blank=True, null=True)
    countrycode = models.CharField(max_length=15, blank=True, null=True)
    mobile = models.CharField(max_length=30,blank=True,null=True)
    user_type = models.ForeignKey(UserTypes, models.DO_NOTHING,blank=True,null=True)
    orig_user_id = models.IntegerField(blank=True,null=True)
    mobile_user = models.CharField(max_length=30,blank=True,null=True)
    is_admin = models.CharField(max_length=30,blank=True,null=True)
    
    class Meta:
        managed = True
        db_table='usershistory'

class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)

class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)

class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)

class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'

class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# A single customer represents a single business deal and is a SUPER USER
# of that company. The relationship is that a Customer is an AuthUser
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
class Customers(models.Model):
    authuser_ptr = models.ForeignKey(AuthUser, models.DO_NOTHING)
    uuid_cust = models.UUIDField(blank=True,null=True)
    company_name =  models.CharField(max_length=128)
    company_addr =  models.CharField(max_length=128)
    company_state =  models.CharField(max_length=128)
    company_country =  models.CharField(max_length=128)
    total_credits_purchased  =  models.IntegerField(default=0)
    total_mobile_credits_available = models.IntegerField(default=0)
    total_cloud_credits_available = models.IntegerField(default=0)
    total_mobile_credits_used = models.IntegerField(default=0)
    total_cloud_credits_used = models.IntegerField(default=0)
    total_credits_used = models.IntegerField(default=0)
    date_add = models.DateTimeField()
    date_mod = models.DateTimeField()
    is_demo = models.BooleanField()
    is_active = models.BooleanField()
    date_demo_started = models.DateTimeField(blank=True,null=True)
    date_demo_expired = models.DateTimeField(blank=True,null=True)
    date_paid_started = models.DateTimeField(blank=True,null=True)
    date_paid_expired = models.DateTimeField(blank=True,null=True)
    is_paid_customer = models.BooleanField(blank=True)
    date_demo_expires = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        managed = True
        db_table = "customers"

class CustomerUsers(models.Model):
    id = models.AutoField(primary_key=True)
    customer_users_uuid = uuid.uuid1()
    customer_id = models.ForeignKey(Customers,models.DO_NOTHING)
    auth_user = models.ForeignKey(AuthUser,models.DO_NOTHING)
    date_add = models.DateTimeField()
    date_mod = models.DateTimeField()
    
    class Meta:
        managed = True
        db_table = "customersusers"

class CustomerUsersHistory(models.Model):
    id = models.AutoField(primary_key=True)
    customer_users_uuid = uuid.uuid1()
    customer_id = models.IntegerField()
    auth_user = models.IntegerField()
    date_add = models.DateTimeField()
    date_mod = models.DateTimeField()
    
    class Meta:
        managed = True
        db_table = "customersusershistory"

class Invoices(models.Model):
    uuid = models.UUIDField(blank=True,null=True)
    customer = models.ForeignKey(Customers)
    date_add = models.DateTimeField()
    date_mod = models.DateTimeField()
    total_credits_ordered = models.IntegerField() #no of user licenses
    total_mobile_cr_ordered = models.IntegerField() #no. of mobile user license
    total_cloud_cr_ordered = models.IntegerField() #no. of cloud user license
    total_amount = models.DecimalField(max_digits=100, decimal_places=5, blank=True, null=True)
    invoice_currency = models.CharField(max_length=100)
    invoice_currency_code = models.CharField(max_length=5)
    invoice_country = models.CharField(max_length=128)
    invoice_note = models.TextField() 
    
    class Meta:
        managed = True
        db_table = "invoices"
        
class Payments(models.Model):
    uuid = models.UUIDField(blank=True,null=True)
    date_add = models.DateTimeField()
    date_mod = models.DateTimeField()
    total_payment_made = models.DecimalField(max_digits=100, decimal_places=5, blank=True, null=True)
    PAYMENT_STATUS = (
        ('COMPLETE', 'complete'),
        ('PENDING', 'pending'),
        ('INITIATED', 'initiated'),
    )    
    payment_status = models.CharField(max_length=20, choices=PAYMENT_STATUS)
    payment_type = models.CharField(max_length=100)      
    invoice = models.ForeignKey(Invoices) 
    
    class Meta:
        managed = True
        db_table = 'payments'